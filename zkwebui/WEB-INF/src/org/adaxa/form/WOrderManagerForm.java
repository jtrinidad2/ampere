package org.adaxa.form;

/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adaxa.window.WShipmentViaDialog;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.WZoomAcross;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.ProgressMonitorDialog;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.InfoPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.apps.ProcessCtl;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.I_AD_PInstance_Log;
import org.compiere.model.MCurrency;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MInvoicePaySchedule;
import org.compiere.model.MLocator;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrderPaySchedule;
import org.compiere.model.MOrg;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MPayment;
import org.compiere.model.MPrivateAccess;
import org.compiere.model.MProductionBatch;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.util.ASyncProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Vbox;

/**
 * @author jtrinidad
 * Manage Invoice and Shipment Processing in one window 
 */

public class WOrderManagerForm extends ADForm implements ValueChangeListener, ASyncProcess, WTableModelListener
{
	private static final String ICON_PRODUCTION = "Production";

	/**
	 * 
	 */
	private static final long serialVersionUID = 6804975825156657866L;
	
	/** Logger */
	private static CLogger			log					= CLogger.getCLogger(WOrderManagerForm.class);

	private static final String CONFIG_ACTION_INVOICE = "ORDER_MANAGER_DOCACTION_INVOICE";
	private static final String CONFIG_ACTION_SHIPMENT = "ORDER_MANAGER_DOCACTION_SHIPMENT";

	
	private static final String LBL_TAB_Description = "Description";
	private static final String LBL_TAB_OrderLines = "Order Lines";
	private static final String LBL_TAB_ShipmentLines = "Shipment Lines";
	private static final String LBL_TAB_InvoiceLines = "Invoice Lines";
	private static final String LBL_TAB_Deposits = "Deposit Records";
	private static final String LBL_TAB_SHIPMENTS_TO_CHECK = "Shipments to Check";
	
	public static String STATUS_NO_TRANSACTION = "0";  //neither have shipments nor invoice
	public static String STATUS_MUST_SHIP = "2";  //has invoice that must be ship
	public static String STATUS_MUST_INVOICE = "4"; //has shipment that must be invoice
	public static String STATUS_FULFILLED = "9"; //shipment = ordered = invoiced
	
	private static final String TEXT_STYLE_NORMAL = "text-decoration: none; ";
	private static final String TEXT_STYLE_UNDERLINE = "text-decoration: underline; ";
	
	private static final String ROW_STYLE_SELECTED = "background-color: burlywood;";
	private static final String ROW_STYLE_NO_TRANSACTION = "background-color: #CFDED9";
	private static final String ROW_STYLE_MUST_SHIP = "background-color: #739CBD;";
	private static final String ROW_STYLE_MUST_INVOICE = "background-color: #65CB94;";
	private static final String ROW_STYLE_FULFILLED = "background-color: #FCD55A;";
	
	private static final String STYLE_ALL_ACTIVE = "background-color: #676B73;";
	
	private static final String STYLE_NORMAL_COLOR = "color: #ffffff !important;";
	private static final String STYLE_RED_COLOR = "color: red !important;";

	private Button optAllOpenOrder = new Button();
	private Button optOrderNoTransaction = new Button();
	private Button optOrderRequireShipment = new Button();
	private Button optOrderRequireInvoice = new Button();
	private Button optOrderFulfilled = new Button();
	
	private boolean isAllowBPartnerInfo = MRole.getDefault().isAllow_Info_BPartner();
	
	private static final String SQL_ORDERLINES = "SELECT l.c_orderline_id, l.LINE " +
            ", COALESCE((SELECT concat_ws ('-',p.value,p.name, l.description) FROM m_product p WHERE p.m_product_id = l.m_product_id) ," 
			+ "concat_ws('-', (SELECT name FROM c_charge WHERE c_charge_id = l.c_charge_id), l.description) ) AS name " +
            ", l.qtyordered, l.priceactual, l.linenetamt, l.qtydelivered, l.qtyinvoiced " +
            ", (select sum(s.qtyonhand) from m_storage s inner join m_locator ll on s.m_locator_id = ll.m_locator_id " +
            "      where l.m_warehouse_id = ll.m_warehouse_id and s.m_product_id = l.m_product_id) as qtyonhand" +
            ", COALESCE(ips.ioqtyip, 0) as ioqtyip " +
            ", COALESCE(ipi.invqtyip, 0) as invqtyip " +
            ", CASE WHEN l.qtyentered - l.qtydelivered - coalesce(ips.ioqtyip, 0) > 0 THEN l.qtyentered - l.qtydelivered - coalesce(ips.ioqtyip, 0) ELSE 0 END AS qtytoship " +
            ", CASE WHEN l.qtyentered - l.qtyinvoiced - coalesce(ipi.invqtyip, 0) > 0 THEN l.qtyentered - l.qtyinvoiced - coalesce(ipi.invqtyip, 0) ELSE 0  END AS qtytoinvoice " +
            "FROM c_orderline l " +
            "LEFT JOIN (select il.c_orderline_id, sum (il.qtyinvoiced) as invqtyip from c_invoiceline il " + 
            "	WHERE il.processed = 'N' group by il.c_orderline_id) ipi  on ipi.c_orderline_id = l.c_orderline_id " + 
            "LEFT JOIN (select sl.c_orderline_id, sum (sl.movementqty) as ioqtyip from m_inoutline sl " +
            "	WHERE sl.processed = 'N' group by sl.c_orderline_id) ips  on ips.c_orderline_id = l.c_orderline_id " +
            "WHERE l.C_Order_ID = ? " +
            "ORDER BY l.LINE";

	private static final String SQL_SHIPMENTLINES = "SELECT l.m_inoutline_id, l.LINE " +
            ", COALESCE((SELECT concat_ws ('-',p.value,p.name) FROM m_product p WHERE p.m_product_id = l.m_product_id),(SELECT name FROM c_charge WHERE c_charge_id = l.c_charge_id)) AS name " +
            ", h.documentno" + 
            ", h.docstatus  || CASE WHEN h.shipmentstatus = '9' THEN '|POD' ELSE '' END  " + 
            " || COALESCE('|' || (SELECT NAME FROM M_shipper where m_shipper_id = h.m_shipper_id),''::TEXT) || COALESCE ('|' || TO_CHAR(h.shipdate,'dd/MM/yyyy'), ''::TEXT)  " +
            ", h.movementdate " +
            ", l.movementqty, (select description FROM m_attributesetinstance WHERE m_attributesetinstance_id = l.m_attributesetinstance_id) AS attno " +
            ", (select SUM(qtyinvoiced) FROM c_invoiceline WHERE c_invoiceline_id = l.c_invoiceline_id OR m_inoutline_id = l.m_inoutline_id ) AS qtyinvoiced " +
            "FROM m_inoutline l " +
            "INNER JOIN m_inout h ON l.m_inout_id = h.m_inout_id " +
            "WHERE h.C_Order_ID = ? " +
            "ORDER BY h.documentno, l.LINE";
	
	private static final String SQL_INVOICELINES = "with invoiceid as ( " +
            "    SELECT DISTINCT (c_invoice_id) as c_invoice_id FROM c_invoice " +
            "          INNER JOIN c_order ON c_invoice.c_order_id = c_order.c_order_id " +
            "          WHERE c_order.c_order_id = ? " +
            "          AND   EXISTS (SELECT ilx.*  FROM c_invoiceline ilx " +
            "                                            WHERE ilx.c_invoice_id = c_invoice.c_invoice_id " +
            "                                            AND   ((COALESCE(ilx.m_product_id, 0) > 0) OR (COALESCE(ilx.c_charge_id, 0) <> c_order.deposit_charge_id))) " +
            " " +
            ") " +
            " " +
			"SELECT l.c_invoiceline_id, l.LINE " +
            ", COALESCE((SELECT concat_ws ('-',p.value,p.name) FROM m_product p WHERE p.m_product_id = l.m_product_id),(SELECT name FROM c_charge WHERE c_charge_id = l.c_charge_id)) AS name " +
            ", h.documentno, h.docstatus || ' | ' || CASE WHEN h.ispaid = 'Y' THEN 'Paid' ELSE 'Not Paid' END AS DOCSTATUS , h.dateinvoiced " +
            ", l.qtyinvoiced, l.priceactual, l.linenetamt, (select description FROM m_attributesetinstance WHERE m_attributesetinstance_id = l.m_attributesetinstance_id) AS attno " +
            ", (select SUM(movementqty) FROM m_inoutline WHERE c_invoiceline_id = l.c_invoiceline_id OR m_inoutline_id = l.m_inoutline_id ) AS movementqty " +
            "FROM c_invoiceline l " +
            "INNER JOIN c_invoice h ON l.c_invoice_id = h.c_invoice_id " +
            "WHERE COALESCE(l.c_orderline_id, 0) > 0 AND h.C_Invoice_ID IN  (select c_invoice_id from invoiceid) " +
            "ORDER BY h.documentno, l.LINE";

	private static final String SQL_DEPOSITRECORDS = "with invoiceid as ( " +
		             "    SELECT DISTINCT (c_invoice_id) as c_invoice_id FROM c_invoice " +
		             "          INNER JOIN c_order ON c_invoice.c_order_id = c_order.c_order_id " +
		             "          WHERE c_order.c_order_id = ? " +
		             "          AND   NOT EXISTS (SELECT ilx.*  FROM c_invoiceline ilx " +
		             "                                            WHERE ilx.c_invoice_id = c_invoice.c_invoice_id " +
		             "                                            AND   ((COALESCE(ilx.m_product_id, 0) > 0) OR (COALESCE(ilx.c_charge_id, 0) <> c_order.deposit_charge_id))) " +
		             " " +
		             ") " +
		             " " +
		             "select ss.trxdate, ss.invoiceno, ss.paymentref, ss.depositamount, ss.invoiceamount, ss.payamount  " +
		             "from              " +
		             "( " +
		             "SELECT i.created, i.dateinvoiced as trxdate, i.documentno as invoiceno, null::varchar(100) as paymentref,  (select linenetamt from c_invoiceline where c_invoice_id = i.c_invoice_id and line = 10) as depositamount, i.grandtotal as invoiceamount, 0 as payamount " +
		             "FROM c_invoice i " +
		             "WHERE i.c_invoice_id IN (select c_invoice_id from invoiceid) " +
		             "UNION  all                               " +
		             "select pa.created, COALESCE(p.datetrx, pa.datetrx, trunc(pa.created)) as trxdate, (select documentno from c_invoice where c_invoice_id = pa.c_invoice_id) as invoiceno, " +
		             "  COALESCE(p.documentno, (select  'Alloc#' || documentno from c_allocationhdr where c_allocationhdr_id = pa.c_allocationhdr_id)) as paymentref" +
		             ", null as depositamount, 0 as invoiceamount, pa.amount as payamount " +
		             "from c_allocationline pa   " +
		             "left join c_payment p on p.c_payment_id = pa.c_payment_id " +
		             "where pa.c_invoice_id in (select c_invoice_id from invoiceid) " +
		             ") ss " +
		             " " +
		             "order by ss.invoiceno, ss.created";
	
	private final String SQL_SHIPMENT_TO_CHECK = 
			"SELECT o.documentno, o.comments " +
				", (SELECT SUM(ROUND(ol.qtyentered*l.priceactual*(100::NUMERIC+ r.rate) / 100::NUMERIC,2)) " +
				"   FROM m_inoutline ol " +
				"   INNER JOIN c_orderline l ON l.c_orderline_id = ol.c_orderline_id " +
				"   LEFT JOIN c_tax r ON l.c_tax_id = r.c_tax_id " +
				"   WHERE ol.m_inout_id = o.m_inout_id) AS totalamount " +	
			"FROM m_inout o " +
			"WHERE o.docstatus = 'IN' AND o.C_Order_ID = ? " +
			"ORDER BY o.documentno";

	private static final int IDX_COL_STATUS = 12;
	private static final int IDX_COL_SHIPMENT_IN_PROGRESS = 13;

	private static final int AD_PROCESS_InOutGenerate = 199; // M_InOut_Generate - org.compiere.process.InOutGenerate
	private static final int AD_PROCESS_InvoiceCreateInOut = 142; // C_Invoice_CreateInOut - org.compiere.process.InvoiceCreateInOut
	private static final int AD_PROCESS_InOutCreateInvoice = 54008; // Shipment_CreateInvoice - org.compiere.process.InOutCreateInvoice

	private static final int SO_WINDOW_ID = 143; //Sales Order Window

	private static final String ICON_PAYDEPOSIT = "PayCard";
	private static final String ICON_PAYINVOICE = "PayCredit";
	
	private Label lblDocumentNo;
    private Label lblDescription;
    private Label lblDateOrdered;
    private Label lblOrderRef;
//    private Label lblGrandTotal;
    
    private Textbox txtDocumentNo;
    private Textbox txtDescription;
    private Textbox txtOrderRef;
    
    private Datebox dateFrom;
    private Datebox dateTo;
    
	private Label lblOrg = new Label();
	private Listbox pickOrg = new Listbox();
	private Label lblWarehouse = new Label();
	private Label lblDocType = new Label();
	private Listbox pickWarehouse = new Listbox();
	private Label lblSalesRep= new Label();
	private Listbox pickSalesRep = new Listbox();
	private Listbox pickDocType = new Listbox();

    private WSearchEditor editorBPartner;
    
	private Borderlayout layout;
	private Div southBody;

	protected ConfirmPanel	confirmPanel = new ConfirmPanel(true, true, false, false, false, false, false);
	protected WListbox contentPanel = ListboxFactory.newDataTable();

	Tabbox tabbedPane = new Tabbox();

	WListbox tblOrderLines = ListboxFactory.newDataTable();
	WListbox tblShipmentLines = ListboxFactory.newDataTable();
	WListbox tblInvoiceLines = ListboxFactory.newDataTable();
	WListbox tblDepositRecords = ListboxFactory.newDataTable();
	WListbox tblShipmentstoCheck = ListboxFactory.newDataTable();
	
	private Vector<String> orderLineColumns = new Vector<String>();
	private Vector<String> shipmentLineColumns = new Vector<String>();
	private Vector<String> invoiceLineColumns = new Vector<String>();
	private Vector<String> depositRecordColumns = new Vector<String>();
	private Vector<String> shipmentToCheckColumns = new Vector<String>();
	
	private Textbox fieldDescription = new Textbox();
	
	public Object 			m_AD_Org_ID = null;
	public Object 			m_M_Warehouse_ID = null;
	public Object 			m_SalesRep_ID = null;
	public Object 			m_C_BPartner_ID = null;
	public Object 			m_Order_DocType_ID = null;
	public String			m_DocumentNo = null;
	public String 			m_OrderRef = null;
	public Date 			m_DateOrderedFrom = null;
	public Date 			m_DateOrderedTo = null;
	private int 			docType_ARPayment = 0;
	private int 			docType_DepositPayment = 0;
	private int 			docType_Production = 0;

	private ListModelTable m_modelAll;
	private int m_no_transaction;
	private int m_must_ship;
	private int m_must_invoice;
	private int m_fulfilled;

	private int  m_C_Order_ID = 0;
	
	private ProgressMonitorDialog progressWindow;
	private Desktop m_desktop = null;

	private MPInstance m_PInstance;

	private ProcessInfo m_ProcessInfo; 
	
	//
	private String docActionInvoice = MInvoice.DOCACTION_Prepare;
	private String docActionShipment = MInOut.DOCACTION_Prepare;

	private BigDecimal m_depositRunningBalance;

	private Tab tabDeposits = null;
	private Tab tabShipmentToCheck = null;

	private boolean m_IsCreditOrder = true;
	private boolean isTableOnQuery = false;

	/**
	 *	initialize fields
	 */
	private void initComponents()
	{
		m_desktop = AEnv.getDesktop();
		
        lblDocumentNo = new Label(Util.cleanAmp(Msg.translate(Env.getCtx(), "DocumentNo")));
        lblDescription = new Label(Msg.translate(Env.getCtx(), "Description"));
        lblDateOrdered = new Label(Msg.translate(Env.getCtx(), "DateOrdered"));
        lblOrderRef = new Label(Msg.translate(Env.getCtx(), "POReference"));
        
        lblOrg = new Label(Msg.translate(Env.getCtx(), "AD_Org_ID"));
        lblWarehouse = new Label(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
        lblSalesRep = new Label(Msg.translate(Env.getCtx(), "SalesRep_ID"));
        lblDocType = new Label(Msg.translate(Env.getCtx(), "C_DocType_ID"));
        
        txtDocumentNo = new Textbox();
        txtDescription = new Textbox();
        txtOrderRef = new Textbox();
        
        dateFrom = new Datebox();
        dateTo= new Datebox();
        
		pickOrg= new Listbox();
		pickOrg.setRows(0);
		pickOrg.setMultiple(false);
		pickOrg.setMold("select");
		ZKUpdateUtil.setWidth(pickOrg, "100%");
		
		pickWarehouse = new Listbox();
		pickWarehouse.setRows(0);
		pickWarehouse.setMultiple(false);
		pickWarehouse.setMold("select");
		ZKUpdateUtil.setWidth(pickWarehouse, "100%");
		
		pickSalesRep = new Listbox();
		pickSalesRep.setRows(0);
		pickSalesRep.setMultiple(false);
		pickSalesRep.setMold("select");
		ZKUpdateUtil.setWidth(pickSalesRep, "100%");

		pickDocType = new Listbox();
		pickDocType.setRows(0);
		pickDocType.setMultiple(false);
		pickDocType.setMold("select");
		ZKUpdateUtil.setWidth(pickDocType, "100%");

        MLookup lookupBP = MLookupFactory.get(Env.getCtx(), 0,
                0, 3499, DisplayType.Search);
        editorBPartner = new WSearchEditor(lookupBP, Msg.translate(
                Env.getCtx(), "C_BPartner_ID"), "", true, false, true);
        editorBPartner.addValueChangeListener(this);
        
        pickOrg.addEventListener(Events.ON_SELECT, this);
        //pickWarehouse.addEventListener(Events.ON_SELECT, this);
        //pickSalesRep.addEventListener(Events.ON_SELECT, this);
        fillOrgs();
        m_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
        setPickID(pickOrg, (int) m_AD_Org_ID);
        
        fillWarehouse();
        setPickID(pickWarehouse,Env.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID"));
        
        fillSalesRep();
        setPickID(pickSalesRep,Env.getAD_User_ID(Env.getCtx()));

        fillDocType();

	}	//	initComponents

	private void init()
	{
		ZKUpdateUtil.setWidth(txtDocumentNo, "100%");
		ZKUpdateUtil.setWidth(txtDescription, "100%");
		ZKUpdateUtil.setWidth(txtOrderRef, "100%");
		ZKUpdateUtil.setWidth(dateFrom, "75px");
		ZKUpdateUtil.setWidth(dateTo, "75px");
		
		docType_ARPayment = DB.getSQLValue(null, "SELECT C_DocType_ID FROM C_DocType WHERE docbasetype = 'ARR' AND IsDepositPayment = 'N' AND AD_Client_ID=?"
				, Env.getAD_Client_ID(Env.getCtx()));
		
		docType_DepositPayment = DB.getSQLValue(null, "SELECT C_DocType_ID FROM C_DocType WHERE docbasetype = 'ARR' AND IsDepositPayment = 'Y' AND AD_Client_ID=?"
				, Env.getAD_Client_ID(Env.getCtx()));

		docType_Production = DB.getSQLValue(null, "SELECT C_DocType_ConfirmedOrder_ID FROM MRP_Run WHERE AD_Client_ID=?"
				, Env.getAD_Client_ID(Env.getCtx()));
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -3);
		dateFrom.setValue(cal.getTime());
		
		ZKUpdateUtil.setHeight(confirmPanel, "40px");
//		confirmPanel.setStyle("width: inherit; position: fixed; bottom: 0px; padding: 0px 5px 5px 5px;");
		confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		confirmPanel.addComponentsLeft(confirmPanel.createButton("BPartner"));
		confirmPanel.addComponentsLeft(confirmPanel.createButton(ConfirmPanel.A_ZOOM));
//		confirmPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Process");
		confirmPanel.addComponentsLeft(confirmPanel.createButton("ZoomAcross"));
		confirmPanel.addComponentsLeft(confirmPanel.createButton("Invoice"));
		confirmPanel.addComponentsLeft(confirmPanel.createButton("Ship"));
		confirmPanel.addComponentsLeft(confirmPanel.createButton("Assignment"));
		if (docType_ARPayment > 0) {
			confirmPanel.addComponentsLeft(confirmPanel.createButton(ICON_PAYINVOICE));
			confirmPanel.getButton(ICON_PAYINVOICE).setTooltiptext("Pay AR Receipt Online");
		}
		if (docType_DepositPayment > 0) {
			confirmPanel.addComponentsLeft(confirmPanel.createButton(ICON_PAYDEPOSIT));
			confirmPanel.getButton(ICON_PAYDEPOSIT).setTooltiptext("Pay Deposit Invoice Online");
		}
		if (docType_Production > 0) {
			confirmPanel.addComponentsLeft(confirmPanel.createButton(ICON_PRODUCTION));
			confirmPanel.getButton(ICON_PRODUCTION).setVisible(true);
			confirmPanel.getButton(ICON_PRODUCTION).setTooltiptext("Create Production Batch");
		}
		confirmPanel.addComponentsLeft(confirmPanel.createButton("Close"));
		confirmPanel.getButton("ZoomAcross").setTooltiptext("Zoom Across");
		confirmPanel.getButton("Invoice").setTooltiptext("Generate Invoice");
		confirmPanel.getButton("Ship").setTooltiptext("Generate Shipment");
		confirmPanel.getButton("Assignment").setTooltiptext("Update Shipper");
		confirmPanel.getButton("Close").setTooltiptext("Close Order Permanently");
		confirmPanel.getButton("BPartner").setVisible(isAllowBPartnerInfo);
		
    	Grid grid = GridFactory.newGridLayout();
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		Row row = new Row();
		rows.appendChild(row);
		row.appendCellChild(lblOrg.rightAlign());
		row.appendCellChild(pickOrg);
		row.appendCellChild(lblWarehouse.rightAlign());
		row.appendCellChild(pickWarehouse);
		row.appendCellChild(lblSalesRep.rightAlign());
		row.appendCellChild(pickSalesRep);

		row = new Row();
		rows.appendChild(row);
		row.appendCellChild(editorBPartner.getLabel().rightAlign());
		row.appendCellChild(editorBPartner.getComponent());
		row.appendCellChild(lblDocumentNo.rightAlign());
		row.appendCellChild(txtDocumentNo);
		row.appendCellChild(lblDateOrdered.rightAlign());

		Hlayout hbox = new Hlayout();
		hbox.appendChild(dateFrom);
		hbox.appendChild(new Label("-"));
		hbox.appendChild(dateTo);
		ZKUpdateUtil.setHflex(dateFrom, "1");
		ZKUpdateUtil.setHflex(dateTo, "1");
		ZKUpdateUtil.setWidth(hbox, "100%");
		row.appendCellChild(hbox);

		row = new Row();
		rows.appendChild(row);
		row.appendCellChild(lblOrderRef.rightAlign());
		row.appendCellChild(txtOrderRef);
		row.appendCellChild(lblDocType.rightAlign());
		row.appendCellChild(pickDocType);
        
		layout = new Borderlayout();
		ZKUpdateUtil.setWidth(layout, "100%");
		ZKUpdateUtil.setHeight(layout, "100%");
       	layout.setStyle("position: absolute");
        this.appendChild(layout);

        North north = new North();
        north.setCollapsible(true);
        north.setSplittable(true);
//        ZKUpdateUtil.setVflex(north, "flex");
        north.setTitle("Parameters");
        layout.appendChild(north);

		Vbox vbox = new Vbox();
		vbox.setStyle("background-color: white;");
		ZKUpdateUtil.setWidth(vbox, "100%");
		
		optAllOpenOrder.setStyle(STYLE_ALL_ACTIVE);
		optOrderNoTransaction.setStyle(ROW_STYLE_NO_TRANSACTION);
		optOrderRequireShipment.setStyle(ROW_STYLE_MUST_SHIP);
		optOrderRequireInvoice.setStyle(ROW_STYLE_MUST_INVOICE);
		optOrderFulfilled.setStyle(ROW_STYLE_FULFILLED);
		
		optAllOpenOrder.setSclass("quickButtons");
		optOrderNoTransaction.setSclass("quickButtons");
		optOrderRequireShipment.setSclass("quickButtons");
		optOrderRequireInvoice.setSclass("quickButtons");
		optOrderFulfilled.setSclass("quickButtons");

		optAllOpenOrder.addActionListener(this);
		optOrderNoTransaction.addActionListener(this);
		optOrderRequireShipment.addActionListener(this);
		optOrderRequireInvoice.addActionListener(this);
		optOrderFulfilled.addActionListener(this);

		confirmPanel.addActionListener(this);
		Hbox statusHbox = new Hbox();
		ZKUpdateUtil.setHeight(statusHbox, "40px");
		statusHbox.appendChild(optAllOpenOrder);
		statusHbox.appendChild(optOrderNoTransaction);
		statusHbox.appendChild(optOrderRequireShipment);
		statusHbox.appendChild(optOrderRequireInvoice);
		statusHbox.appendChild(optOrderFulfilled);
		
		vbox.appendChild(statusHbox);
		vbox.appendChild(grid);	
		ZKUpdateUtil.setHeight(grid, "140px");
		north.appendChild(vbox);
		
        Center center = new Center();
		layout.appendChild(center);
		
        contentPanel.setStyle("width: 99%; margin: 0px auto;");
        contentPanel.setOddRowSclass(null);
        contentPanel.setSclass("advance_layout");
		center.appendChild(contentPanel);
		ZKUpdateUtil.setVflex(contentPanel, "1");
		ZKUpdateUtil.setHflex(contentPanel, "1");

//		ZKUpdateUtil.setHeight(tabbedPane, "92%");
        tabbedPane.addEventListener(Events.ON_SELECT, this);
		Tabpanels tabPanels = new Tabpanels();
		tabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		tabbedPane.appendChild(tabs);

		Tab tab = null;
		Tabpanel desktopTabPanel = null;

		tab = new Tab(LBL_TAB_OrderLines);
		tab.setId("Tab2");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(tblOrderLines);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab(LBL_TAB_ShipmentLines);
		tab.setId("Tab3");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(tblShipmentLines);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(LBL_TAB_InvoiceLines);
		tab.setId("Tab4");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(tblInvoiceLines);
		tabPanels.appendChild(desktopTabPanel);


		
		if (docType_DepositPayment > 0) {
			tabDeposits = new Tab(LBL_TAB_Deposits);
			tabDeposits.setId("Tab5");
			tabs.appendChild(tabDeposits);
			desktopTabPanel = new Tabpanel();
			ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
			desktopTabPanel.appendChild(tblDepositRecords);
			tabPanels.appendChild(desktopTabPanel);
		}

		tabShipmentToCheck = new Tab(LBL_TAB_SHIPMENTS_TO_CHECK);
		tabShipmentToCheck.setId("Tab6");
		tabs.appendChild(tabShipmentToCheck);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(tblShipmentstoCheck);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(Msg.translate(Env.getCtx(), "Description"));
		tab.setId("Tab1");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		ZKUpdateUtil.setWidth(fieldDescription, "99%");
		fieldDescription.setMultiline(true);
		ZKUpdateUtil.setHeight(fieldDescription, "95%");
		desktopTabPanel.appendChild(fieldDescription);
		tabPanels.appendChild(desktopTabPanel);

		
		//
		int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 90 / 100;
		int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 80 / 100;
//
//		ZKUpdateUtil.setWidth(layout, "100%");
//		ZKUpdateUtil.setHeight(layout, "100%");
        layout.setStyle("border: collapse; position: absolute");
       	
		South south = new South();
		int detailHeight = (height * 40 / 100);
		ZKUpdateUtil.setHeight(south, detailHeight + "px");
		south.setCollapsible(true);
		south.setSplittable(true);
//		 ZKUpdateUtil.setVflex(south, "flex");
		south.setTitle("Order Details");
		south.setTooltiptext("Order Details");
		layout.appendChild(south);

		southBody = new Div();
//		ZKUpdateUtil.setWidth(southBody, "100%");
		southBody.appendChild(tabbedPane);
		southBody.appendChild(confirmPanel);
		south.appendChild(southBody);

		ZKUpdateUtil.setVflex(tabbedPane, "1");
		ZKUpdateUtil.setHeight(confirmPanel, "35px");
		ZKUpdateUtil.setWidth(confirmPanel, "100%");
		ZKUpdateUtil.setVflex(southBody, "1");
		ZKUpdateUtil.setHflex(southBody, "1");
		
		contentPanel.addActionListener(new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				int row = contentPanel.getSelectedRow();
				if (row >= 0) {
					int C_Order_ID = contentPanel.getRowKey(row);
        			refresh(C_Order_ID);
        			south.setOpen(true);
				}
			}
		});	

		contentPanel.getModel().addTableModelListener(this);
//        Borderlayout mainPanel = new Borderlayout();
//        mainPanel.setWidth("100%");
//        mainPanel.setHeight("100%");
//        North north = new North();
//        mainPanel.appendChild(north);
//        north.appendChild(grid);
//        center = new Center();
//        mainPanel.appendChild(center);
//        center.appendChild(borderlayout);
//        south = new South();
//        mainPanel.appendChild(south);
//        south.appendChild(confirmPanel);
//        if (!isLookup())
//        {
//        	mainPanel.setStyle("position: absolute");
//        }
//
//		this.appendChild(mainPanel);
//		if (isLookup())
//		{
//			this.setWidth(width + "px");
//			this.setHeight(height + "px");
//		}

	}

//	@Override
//	protected void insertPagingComponent() {
//		North north = new North();
//		north.appendChild(paging);
//		borderlayout.appendChild(north);
//	}



    public void onEvent(Event event)
    {
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_REFRESH)) {
				executeQuery();
			} else if (event.getTarget() == confirmPanel.getButton("BPartner")) {
				Integer C_Order_ID = contentPanel.getSelectedRowKey();
				if (C_Order_ID == null)
					return;
				
				MOrder order  = new MOrder(Env.getCtx(), C_Order_ID, null);
				Env.setContext(Env.getCtx(), getWindowNo(), "C_BPartner_ID", order.getC_BPartner_ID());
				InfoPanel.showBPartner(getWindowNo());
			} else if (event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_ZOOM)) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				zoomToOrder();
			} else if (event.getTarget() == confirmPanel.getButton("ZoomAcross")) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				zoom(event.getTarget());
			} else if (event.getTarget() == confirmPanel.getButton("Close")) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				closeSelectedOrder();
			} else if (event.getTarget() == confirmPanel.getButton("Invoice")) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				createInvoice();
			} else if (event.getTarget() == confirmPanel.getButton("Ship")) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				createShipment();
			} else if (event.getTarget() == confirmPanel.getButton(ICON_PAYINVOICE)) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				createPayment(false);
			} else if (event.getTarget() == confirmPanel.getButton(ICON_PAYDEPOSIT)) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				createPayment(true);
			} else if (event.getTarget() == confirmPanel.getButton(ICON_PRODUCTION)) {
				if (contentPanel.getSelectedRowKey() == null) {
					return;
				}
				createProduction();
			} else if (event.getTarget() == confirmPanel.getButton("Assignment")) {
				if (tblShipmentLines.getSelectedIndex() < 0) {
					return;
				}
				m_C_Order_ID = contentPanel.getSelectedRowKey();
	    		int M_InOutLine_ID = (int) tblShipmentLines.getValueAt(tblShipmentLines.getSelectedIndex(), 0);
				MInOutLine sLine = new MInOutLine(Env.getCtx(), M_InOutLine_ID, null);
				MInOut inout = (MInOut) sLine.getM_InOut();
				
				if (inout.getDocStatus().equals(MInOut.DOCSTATUS_Closed) ||
				inout.getDocStatus().equals(MInOut.DOCSTATUS_Reversed)) {
					FDialog.warn(0, "Shipment is already Closed/Reversed. No need to Update", "Warning");
					return;
				}
				if (inout.get_ValueAsString("shipmentstatus").equals("9")) {  //End of Shipment Cycle
					FDialog.warn(0, "Shipment reached it's end of Shipment Cycle. No need to Update", "Warning");
					return;
				}

				WShipmentViaDialog dlg = new WShipmentViaDialog(inout, getWindowNo());
				dlg.init();
				SessionManager.getAppDesktop().showWindow(dlg);
				if (!dlg.isCancelled()) {
					loadShipmentLines(m_C_Order_ID);;  //reload and update table
				}

				//TODO
			} else if (event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_CANCEL)) {
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			}
			else if (event.getTarget() == optAllOpenOrder) {
				contentPanel.setModel(m_modelAll);
				updateStatus(false);
			}
			else if (event.getTarget() == optOrderNoTransaction) {
				filterBy(STATUS_NO_TRANSACTION, optOrderNoTransaction.getStyle());
			}
			else if (event.getTarget() == optOrderRequireShipment) {
				filterBy(STATUS_MUST_SHIP, optOrderRequireShipment.getStyle());
			}
			else if (event.getTarget() == optOrderRequireInvoice) {
				filterBy(STATUS_MUST_INVOICE, optOrderRequireInvoice.getStyle());
			}
			else if (event.getTarget() == optOrderFulfilled) {
				filterBy(STATUS_FULFILLED, optOrderFulfilled.getStyle());
			}
		} else if (Events.ON_SELECT.equals(event.getName()) && event.getTarget() instanceof Tab) {
			
			Tab selected = (Tab) event.getTarget();
			
			confirmPanel.getButton("Invoice").setEnabled(false);
			confirmPanel.getButton("Ship").setEnabled(false);
			confirmPanel.getButton("Assignment").setEnabled(false);
			confirmPanel.getButton("Invoice").setTooltiptext("NA");
			confirmPanel.getButton("Ship").setTooltiptext("NA");
			
			if (selected.getLabel().equals(LBL_TAB_OrderLines)) {
				confirmPanel.getButton("Invoice").setEnabled(true);
				confirmPanel.getButton("Ship").setEnabled(m_IsCreditOrder );
				confirmPanel.getButton("Invoice").setTooltiptext("Generate Invoice");
				confirmPanel.getButton("Ship").setTooltiptext("Generate Shipment");

			} else if (selected.getLabel().equals(LBL_TAB_ShipmentLines)) {
				confirmPanel.getButton("Invoice").setEnabled(true);
				confirmPanel.getButton("Invoice").setTooltiptext("Generate Invoice from Shipment");
				confirmPanel.getButton("Assignment").setEnabled(true);
			} else if (selected.getLabel().equals(LBL_TAB_InvoiceLines)) {
				confirmPanel.getButton("Ship").setEnabled(true);
				confirmPanel.getButton("Ship").setTooltiptext("Generate Shipment from Invoice");
			} else {
			}
			
		} else if (Events.ON_SELECT.equals(event.getName()) && event.getTarget() == pickOrg) {
			ListItem listitem = pickOrg.getSelectedItem();
			if (listitem != null && pickOrg.getSelectedIndex() != 0)
				m_AD_Org_ID = (Integer)listitem.getValue();
			else 
				m_AD_Org_ID  = 0;
			
			fillWarehouse();
			fillSalesRep();
			
		} else {
			//super.onEvent(event);
		}

    }


    private void createShipment()
    {
    	m_C_Order_ID  = contentPanel.getSelectedRowKey();
    	if (m_C_Order_ID == 0)
    		return;
    	
    	if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_OrderLines)) {
    		createShipmentFromOrder();

    	} else if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_InvoiceLines)) {
    		createShipmentFromInvoice();
    	} else {
    		return;
    	}
    	
    }
    
    private String createShipmentFromOrder()
    {
    	String title = null;
    	StringBuffer message = new StringBuffer("Shipment will be created based on Qty Set.\n Do you wish to proceed?");
    	String info= null;
    	
		title = "Generate Shipment";
		
    	try {
			if (Messagebox.showDialog(message.toString(), title, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION) == Messagebox.YES) {
				log.info("Proceed to Generate shipment.");
				message = new StringBuffer();
				String trxName = Trx.createTrxName("OMF");	
				Trx trx = Trx.get(trxName, true);	//trx needs to be committed too

				try {
					MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);

					MInOut shipment =  null;
					int nLines = 0;
					
					for (int i = 0; i < tblOrderLines.getRowCount(); i++) {
			    		int C_OrderLine_ID = (int) tblOrderLines.getValueAt(i, 0);
			    		MOrderLine line = new MOrderLine(Env.getCtx(), C_OrderLine_ID, trxName);
			    		BigDecimal qtyToShip = (BigDecimal) tblOrderLines.getValueAt(i, 11);
			    		//make sure it doesn't exceed the order
			    		if (line.getQtyOrdered().compareTo(qtyToShip.add(line.getQtyDelivered())) < 0) {
			    			qtyToShip = line.getQtyOrdered().subtract(line.getQtyDelivered());
			    		}
			    		if (qtyToShip.signum() > 0 ) { //create shipment line

			    			//create header if none
			    			if  (shipment == null) {
			    				shipment = new MInOut (order, 0, null);
			    				shipment.setM_Warehouse_ID(order.getM_Warehouse_ID());	//	sets Org too
			    				shipment.setDeliveryViaRule(MInOut.DELIVERYVIARULE_Shipper);
			    				int defaultShipperID = MSysConfig.getIntValue("DEFAULT_SHIPPER_ID",0, Env.getAD_Client_ID(Env.getCtx()));
			    				shipment.setM_Shipper_ID(defaultShipperID);
			    				shipment.saveEx();
			    				message.append("Shipment No " + shipment.getDocumentNo() + " created.\n");
			    			}
			    			MInOutLine shipmentLine = new MInOutLine(shipment);
			    			boolean isBOM = line.get_ValueAsBoolean("IsBOM");
			    			shipmentLine.setOrderLine(line, 0, qtyToShip);
			    			if  (!isBOM) {
			    				shipmentLine.setQty(qtyToShip);	
			    			} else {
			    				shipmentLine.setQty(qtyToShip);
			    				shipmentLine.setMovementQty(Env.ZERO);
			    			}
			    			
			    			
			    			shipmentLine.save();
			    			nLines++;
			    		}
					}
					
					if (shipment != null) {
		    			String status = null;
		    			if (docActionShipment.equals(MInOut.DOCACTION_Complete))
		    				status = shipment.completeIt();
		    			else
		    				status = shipment.prepareIt();
		    			shipment.setDocStatus(status);
		    			shipment.saveEx();


					}
				} catch (Exception e) {
					log.severe(e.getMessage());
				} finally {
		    		trx.close();
		    		trx = null;
					
				}

	    		FDialog.info(0, null, message.toString());
				refresh(m_C_Order_ID);

			} else {
				log.info("Process cancelled");
			}
				
				
		} catch (Exception e) {
			log.severe(e.getMessage());
		}

    	return null;
    }

    

	private void createShipmentFromInvoice()
    {
    	String title = null;
		title = "Generate Shipment from Invoice";
    	StringBuffer message = new StringBuffer();

		ArrayList<Integer> invoice_ID = new ArrayList<>();
		ArrayList<String> invoiceNo = new ArrayList<>();
		MInOut inout = null;

		boolean hasQtyToShip = false;
		
    	for (int i = 0; i < tblInvoiceLines.getRowCount(); i++) {
    		BigDecimal allowedQty = (BigDecimal) tblInvoiceLines.getValueAt(i, 11);
    		BigDecimal actualQty = (BigDecimal) tblInvoiceLines.getValueAt(i, 12);
    		if (allowedQty.signum() != 0 && actualQty.signum() != 0) {
    			hasQtyToShip = true;
    			break;
    		}
    			
    	}
    	if (hasQtyToShip) {
    		message.append("Shipment will be created from invoice. Do you wish to proceed?");
    		
    	} else {
    		FDialog.info(0, null, "No shipment to create.");
    		return;
    	}

    	Trx trx  = null;

    	try {
			if (Messagebox.showDialog(message.toString(), title, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION) == Messagebox.YES) {
				String trxName = Trx.createTrxName("OMF");	
				trx = Trx.get(trxName, true);	//trx needs to be committed too

				try {
					message = new StringBuffer();
					MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);
					for (int i = 0; i < tblInvoiceLines.getRowCount(); i++) {
			    		int C_InvoiceLine_ID = (int) tblInvoiceLines.getValueAt(i, 0);
			    		BigDecimal allowedQty = (BigDecimal) tblInvoiceLines.getValueAt(i, 11);
			    		BigDecimal actualQty = (BigDecimal) tblInvoiceLines.getValueAt(i, 12);
			    		if (allowedQty.signum() == 0 || actualQty.signum() == 0) 
			    			continue;
			    		
			    		if (actualQty.compareTo(allowedQty) > 0) {
			    			actualQty = allowedQty;
			    		}
			    			
			    		MInvoiceLine invoiceLine = new MInvoiceLine(Env.getCtx(), C_InvoiceLine_ID, trxName);
			    		BigDecimal qtyMatched = invoiceLine.getMatchedQty();
			    		BigDecimal qtyInvoiced = invoiceLine.getQtyInvoiced();
			    		BigDecimal qtyNotMatched = qtyInvoiced.subtract(qtyMatched);
			    		// If is fully matched don't create anything
			    		if (qtyNotMatched.signum() == 0)
			    		{
			    			continue;
			    		}
			    		if (invoiceLine.getM_InOutLine_ID() != 0 && !invoiceLine.getM_InOutLine().isProcessed()) {
			    			MInOut existingIO = (MInOut) invoiceLine.getM_InOutLine().getM_InOut();
			    			String s =  invoiceLine.getC_Invoice().getDocumentNo() + "|" + invoiceLine.getLine() 
			    				+  "\nThis Invoice appears to have Shipment in Progress  with a DocumentNo=" 
			    				+ existingIO.getDocumentNo() + ".\n";
			    			message.append(s);

			    			
			    			continue;

			    		}
			    		//ignore applied deposit
			    		if (invoiceLine.getC_Charge_ID() == order.get_ValueAsInt("DEPOSIT_CHARGE_ID")) {
			    			continue;
			    		}
			    		
			    		if (inout == null) {
			    			MInvoice invoice = (MInvoice) invoiceLine.getC_Invoice();
			    			inout = new MInOut (invoice, 0, null, order.getM_Warehouse_ID());
			    			inout.save();
			    		}
			    		MInOutLine sLine = new MInOutLine(inout);
			    		sLine.setInvoiceLine(invoiceLine, 0, actualQty);
			    		
			    		sLine.setQtyEntered(actualQty);
		    			sLine.setMovementQty(actualQty);

		    			sLine.setC_InvoiceLine_ID(invoiceLine.getC_InvoiceLine_ID());
		    			sLine.saveEx();
			    			//
		    			if (invoiceLine.getM_InOutLine_ID() != 0) {
		    				invoiceLine.setM_InOutLine_ID(sLine.getM_InOutLine_ID());
		    				invoiceLine.saveEx();
		    			}
			    	
					}
					
					String status = null;
					if (inout != null) {
						if (docActionShipment != null && docActionShipment.equals(MInOut.DOCACTION_Prepare)) {
							status = inout.prepareIt();
							inout.setDocStatus(status);
							inout.save();
						}
						else if (docActionShipment != null && docActionShipment.equals(MInOut.DOCACTION_Complete)) {
							status = inout.completeIt();
							inout.setDocStatus(status);
							inout.save();
						}
						message.append("New shipment created. DocumentNo=" + inout.getDocumentNo());
					}

				} catch (Exception e) {
					log.severe(e.getMessage());
				} finally {
		    		trx.close();
		    		trx = null;
				}
				
	    		if (message.length() > 0)
	    			FDialog.info(0, null, message.toString());
	    		
	    		refresh(m_C_Order_ID);
			}
		} catch (Exception e) {
			FDialog.info(0, null, e.getMessage());
			if (trx !=null) {
				trx.rollback();
				trx.close();
			}
			log.severe(e.getMessage());		}
    	

    }
    
	private void createProduction()
	{
    	m_C_Order_ID  = contentPanel.getSelectedRowKey();
    	if (m_C_Order_ID == 0)
    		return;
    	StringBuffer msg = new StringBuffer();
    	HashMap<Integer, BigDecimal> pOrder = new HashMap<Integer, BigDecimal>();
    	
    	for (int i = 0; i < tblOrderLines.getRowCount(); i++) {
    		int C_OrderLine_ID = (int) tblOrderLines.getValueAt(i, 0);
    		BigDecimal qtyToProduce = (BigDecimal) tblOrderLines.getValueAt(i, 13);
    		MOrderLine line = new MOrderLine(Env.getCtx(), C_OrderLine_ID, null);
    		
    		if (line.getM_Product_ID() == 0 || !line.getM_Product().isBOM()) {
    			continue;
    		}
    		if (pOrder.containsKey(line.getM_Product_ID())) {
    			BigDecimal qty = pOrder.get(line.getM_Product_ID());
    			qtyToProduce = qtyToProduce.add(qty);
    		} else {
    			
    		}
    		pOrder.put(line.getM_Product_ID(), qtyToProduce);
		}
    	
    	String trxName = Trx.createTrxName("OMF");
    	Trx trx  =  Trx.get(trxName, true);;
    	MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);
    	int M_Locator_ID =  MLocator.getDefault((MWarehouse) order.getM_Warehouse()).getM_Locator_ID();
    	MProductionBatch pBatch = null;
    	int created = 0;
    	try {
        	for (Integer productID : pOrder.keySet()) {
        		BigDecimal totalQty = pOrder.get(productID);
        		pBatch = new MProductionBatch(Env.getCtx(), 0, trxName);
        		pBatch.setC_DocType_ID(docType_Production);
        		pBatch.setQtyOrdered(totalQty);
        		pBatch.setM_Product_ID(productID);
        		pBatch.setDescription("Production batch from Order fulfillment manager.");
        		pBatch.setMovementDate(new Timestamp(System.currentTimeMillis()));
        		pBatch.setTargetQty(totalQty);
        		pBatch.setM_Locator_ID(M_Locator_ID);
        		pBatch.setDocAction(MProductionBatch.DOCACTION_Complete);
        		pBatch.setDocStatus(MProductionBatch.DOCSTATUS_Drafted);
        		pBatch.setCountOrder(0);
        		pBatch.setQtyCompleted(Env.ZERO);
        		pBatch.saveEx();
        		pBatch.completeIt();
        		pBatch.saveEx();
        		trx.commit();
        		created++;
        		AEnv.zoom(MProductionBatch.Table_ID, pBatch.getM_Production_Batch_ID());
        	}
    		
    	} catch (Exception e) {
    			msg.append(e.getMessage() + "\n");
    		trx.rollback();
    		
    	} finally {
    		trx.close();
    		trx = null;

    	}
    	
    	if (created == 0) 
    		msg.append("No production batch created.");
    	
    	if (msg.length() > 0) {
    		FDialog.warn(0, msg.toString());
    	}
	}
	
    private void createInvoice()
    {
    	m_C_Order_ID  = contentPanel.getSelectedRowKey();
    	if (m_C_Order_ID == 0)
    		return;
    	
    	if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_OrderLines)) {
    		createInvoiceFromOrder();

    	} else if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_ShipmentLines)) {
    		createInvoiceFromShipment();
    	} else {
    		return;
    	}
    }

    
    private void createInvoiceFromOrder()
    {
    	String title = null;
		title = "Generate Invoice from Order";
    	StringBuffer message = new StringBuffer();

    	if  (m_depositRunningBalance.signum() > 0) {
    		
    		message.append("Please check. Insufficient Deposit Invoice Paid.  Running Balance = " + DisplayType.getNumberFormat(DisplayType.Account).format(m_depositRunningBalance) );
			FDialog.warn(0, message.toString());
			return;
		}
		

    	message.append("Invoice will be created from qty you set. Please note you can't invoice more than the Order Qty");
    	message.append(" .\n Do you wish to proceed?");
    		
    	Trx trx  = null;
    	try {
			if (Messagebox.showDialog(message.toString(), title, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION) == Messagebox.YES) {
				String trxName = Trx.createTrxName("OMF");	
				message = new StringBuffer();
				trx = Trx.get(trxName, true);	//trx needs to be committed too
				
				try {
					MInvoice invoice = null;
					MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);
					int nLines = 0;
					
			    	for (int i = 0; i < tblOrderLines.getRowCount(); i++) {
			    		int C_OrderLine_ID = (int) tblOrderLines.getValueAt(i, 0);
			    		MOrderLine line = new MOrderLine(Env.getCtx(), C_OrderLine_ID, null);
			    		BigDecimal qtyToInvoice = (BigDecimal) tblOrderLines.getValueAt(i, 12);
			    		//make sure it doesn't exceed the order
			    		if (line.getQtyOrdered().compareTo(qtyToInvoice.add(line.getQtyInvoiced())) < 0) {
			    			qtyToInvoice = line.getQtyOrdered().subtract(line.getQtyInvoiced());
			    		}
			    		if (qtyToInvoice.signum() > 0 ) { //create invoice line

			    			//create header if none
			    			if  (invoice == null) {
			    				invoice = new MInvoice(order, 0, null);
			    				invoice.saveEx();
			    				message.append("Invoice No " + invoice.getDocumentNo() + " created.\n");
			    			}
			    			MInvoiceLine invoiceLine = new MInvoiceLine(invoice);
			    			invoiceLine.setOrderLine(line);
			    			invoiceLine.setQty(qtyToInvoice);
			    			invoiceLine.setLineNetAmt();
			    			
			    			invoiceLine.save();
			    			nLines++;
			    		}
		    		}
			    		
			    	message.append("Total number of lines=" + nLines);
		    		if (invoice != null) {
		    			invoice.setPaymentRule(order.getPaymentRule());
		    			invoice.setC_PaymentTerm_ID(order.getC_PaymentTerm_ID());
		    			invoice.saveEx();
		    			invoice.load(invoice.get_TrxName()); // refresh from DB
		    			// copy payment schedule from order if invoice doesn't have a current payment schedule
		    			MOrderPaySchedule[] opss = MOrderPaySchedule.getOrderPaySchedule(Env.getCtx(), order.getC_Order_ID(), 0, trx.getTrxName());
		    			MInvoicePaySchedule[] ipss = MInvoicePaySchedule.getInvoicePaySchedule(Env.getCtx(), invoice.getC_Invoice_ID(), 0, trx.getTrxName());
		    			if (ipss.length == 0 && opss.length > 0) {
		    				BigDecimal ogt = order.getGrandTotal();
		    				BigDecimal igt = invoice.getGrandTotal();
		    				BigDecimal percent = Env.ONE;
		    				if (ogt.compareTo(igt) != 0)
		    					percent = igt.divide(ogt, 10, RoundingMode.HALF_UP);
		    				MCurrency cur = MCurrency.get(order.getCtx(), order.getC_Currency_ID());
		    				int scale = cur.getStdPrecision();
		    			
		    				for (MOrderPaySchedule ops : opss) {
		    					MInvoicePaySchedule ips = new MInvoicePaySchedule(Env.getCtx(), 0, trx.getTrxName());
		    					PO.copyValues(ops, ips);
		    					if (percent != Env.ONE) {
		    						BigDecimal propDueAmt = ops.getDueAmt().multiply(percent);
		    						if (propDueAmt.scale() > scale)
		    							propDueAmt = propDueAmt.setScale(scale, RoundingMode.HALF_UP);
		    						ips.setDueAmt(propDueAmt);
		    					}
		    					ips.setC_Invoice_ID(invoice.getC_Invoice_ID());
		    					ips.setAD_Org_ID(ops.getAD_Org_ID());
		    					ips.setProcessing(ops.isProcessing());
		    					ips.setIsActive(ops.isActive());
		    					ips.saveEx();
		    				}
		    			}
		    			invoice.validatePaySchedule();
		    			String status = null;
		    			if (docActionInvoice.equals(MInvoice.DOCACTION_Complete))
		    				status = invoice.completeIt();
		    			else
		    				status = invoice.prepareIt();
		    			invoice.setDocStatus(status);
		    			invoice.saveEx();
		    		}
				} catch (Exception e) {
					log.severe(e.getMessage());
				} finally {
		    		trx.close();
		    		trx = null;
					
				}

		    		FDialog.info(0, null, message.toString());

				refresh(m_C_Order_ID);

			} else {
			log.info("Process cancelled");
			}
			
		} catch (Exception e) {
			FDialog.info(0, null, e.getMessage());
			if (trx !=null) {
				trx.rollback();
				trx.close();
			}
			log.severe(e.getMessage());
		}

    }
	private void createInvoiceFromShipment()
    {
    	String title = null;
		title = "Generate Invoice from Shipment";
    	StringBuffer message = new StringBuffer();

		ArrayList<Integer> shipment_ID = new ArrayList<>();
		ArrayList<String> shipmentNo = new ArrayList<>();
		
    	for (int i = 0; i < tblShipmentLines.getRowCount(); i++) {
    		int M_InOutLine_ID = (int) tblShipmentLines.getValueAt(i, 0);
    		MInOutLine line = new MInOutLine(Env.getCtx(), M_InOutLine_ID, null);
    		MInOut inout = (MInOut) line.getM_InOut();
    		if (inout.getDocStatus().equals(MInOut.DOCSTATUS_Completed) && line.get_ValueAsInt("C_InvoiceLine_ID") == 0 && line.getC_OrderLine_ID() != 0) {
    			if (!shipment_ID.contains(inout.getM_InOut_ID())) {
    				shipment_ID.add(inout.getM_InOut_ID());
    				shipmentNo.add(inout.getDocumentNo());
    			}
    		}
    	}
    	if (shipment_ID.size() > 0) {
    		message.append("Invoice will be created from the following shipment - " + shipmentNo.toString());
    		message.append(" .\n Do you wish to proceed?");
    		
    	} else {
    		FDialog.info(0, null, "No further invoice to create.");

    		return;
    	}
    	try {
			if (Messagebox.showDialog(message.toString(), title, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION) == Messagebox.YES) {
				String trxName = Trx.createTrxName("OMF");	
				Trx trx = Trx.get(trxName, true);	//trx needs to be committed too
				
				try {
					MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);
					
					for (int id : shipment_ID) {
						initProcess(AD_PROCESS_InOutCreateInvoice, id);
						processAddPara(10, "DocAction", docActionInvoice);
						processExecute(trx);
					}
					
				} catch (Exception e) {
					log.severe(e.getMessage());
				} finally {
					trx.close();
					trx = null;
				}
				
				refresh(m_C_Order_ID);

			} else {
			log.info("Process cancelled");
			}
			
		} catch (Exception e) {
			log.severe(e.getMessage());
		}

    }

    private void initProcess(int AD_Process_ID, int Record_ID)
    {
    	m_PInstance = new MPInstance(Env.getCtx(), AD_Process_ID, Record_ID);
    	m_PInstance.save();
    	m_ProcessInfo = new ProcessInfo ("WOrderManagerForm", AD_Process_ID);
    	m_ProcessInfo.setRecord_ID(Record_ID);
		m_ProcessInfo.setAD_PInstance_ID (m_PInstance.getAD_PInstance_ID());
    }
    
    private void processAddPara(int seqNo, String param, Object paramValue)
    {
    	MPInstancePara ip = new MPInstancePara(m_PInstance, seqNo);
    	if (paramValue instanceof String) 
    		ip.setParameter(param, (String)paramValue);
    	else if (paramValue instanceof Integer)
    		ip.setParameter(param, (Integer)paramValue);
    	else if (paramValue instanceof BigDecimal)
    		ip.setParameter(param, (BigDecimal)paramValue);
    	else {
    		log.severe("Unexpected paramater type.");
    	}
    		
		ip.save();
    }
    
    private void processExecute(Trx trx)
    {
		ProcessCtl worker = new ProcessCtl(this, m_ProcessInfo.getWindowNo(), m_ProcessInfo , trx);
		worker.run();
		
		StringBuffer msg = new StringBuffer(m_ProcessInfo.getSummary());
		
		List<PO> logs = new Query(Env.getCtx(), I_AD_PInstance_Log.Table_Name, "ad_pinstance_id=?", trx.getTrxName())
				.setParameters(m_PInstance.getAD_PInstance_ID())
				.setOrderBy("log_id")
				.list();
		
		for (PO log : logs) {
			msg.append(log.get_ValueAsString("p_msg") + "\n");
		}
		if (msg.length() == 0 && !m_ProcessInfo.isError())
			msg.append("Process Successful.");
		
		FDialog.info(0, null, msg.toString());
		
    }
    
	private void showBusyDialog() 
	{
		progressWindow = new ProgressMonitorDialog(null, m_desktop);
		progressWindow.setPage(getPage());
		progressWindow.doHighlighted();
//		progressWindow.setVisible(true);
		
	}
	
	private void hideBusyDialog() {
		if (progressWindow != null) {
//			progressWindow.setVisible(false);
			progressWindow.dispose();
			progressWindow = null;
		}
	}
    
	/**
	 *	Zoom to Order
	 */
	private void zoomToOrder()
	{
		log.info("");
		Integer C_Order_ID = contentPanel.getSelectedRowKey();
		if (C_Order_ID == null)
			return;

		AEnv.zoom(MOrder.Table_ID, C_Order_ID);
	}	//	zoom to order

	/**
	 * Zoom across
	 * @param comp
	 */
	private void zoom(Component comp) {
		Integer C_Order_ID = contentPanel.getSelectedRowKey();
		if (C_Order_ID == null)
			return;

		//	Query
		MQuery query = new MQuery();
		//	Current row
		String link = "C_Order_ID";
		query.addRestriction(link, MQuery.EQUAL, C_Order_ID);
		
		new WZoomAcross(comp, MOrder.Table_Name, SO_WINDOW_ID, query);
	}
	
	private void closeSelectedOrder()
	{
		ArrayList<Integer> ids = contentPanel.getSelectedKeys();
		ArrayList<String> orderNo = new ArrayList<>();
		try {
			if (Messagebox.showDialog("There are " + ids.size() + " orders that you intend to close.  Are you sure?"
					, "Please confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION) == Messagebox.YES) {
				
				for (int C_Order_ID : ids) {
					MOrder order = new MOrder(Env.getCtx(), C_Order_ID, null);
					if (order.closeIt()) {
						order.setDocStatus(MOrder.DOCSTATUS_Closed);
						order.save();
						orderNo.add(order.getDocumentNo());
					}
				}
				String msg = "Orders closed: " + orderNo.toString();
				FDialog.info(0, null, msg);
				executeQuery();
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}
	
	private void loadOrderLines(int C_Order_ID)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		Vector<Object> line = null;
		
		int i;
		try {
			pstmt = DB.prepareStatement(SQL_ORDERLINES, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
				
				line = new Vector<Object>(13);
				i=1;
				line.add(rs.getInt(i++));  // C_OrderLine_ID
				line.add(rs.getInt(i++));   // Line
				line.add(rs.getString(i++));    // Product/Charge
				line.add(rs.getBigDecimal(i++));    // QtyOrdered
				line.add(rs.getBigDecimal(i++));    // PriceActual
				line.add(rs.getBigDecimal(i++));    // LineNetAmount
				line.add(rs.getBigDecimal(i++));    // QtyDelivered
				line.add(rs.getBigDecimal(i++));    // QtyInvoiced
				line.add(rs.getBigDecimal(i++));    // QtyOnHand
				line.add(rs.getBigDecimal(i++));    // Ship IP
				line.add(rs.getBigDecimal(i++));    // Inv IP
				line.add(rs.getBigDecimal(i++));    // Qty to Ship - user entry
				line.add(rs.getBigDecimal(i++));    // Qty to Invoice - user entry
				line.add(0);    // Qty to Mfg
				data.add(line);
				
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, SQL_ORDERLINES, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		tblOrderLines.setData(model, orderLineColumns);
		
		i = 0;
		tblOrderLines.setColumnClass(i++, IDColumn.class, true);  // C_OrderLine_ID
		tblOrderLines.setColumnClass(i++, Integer.class, true);   // Line
		tblOrderLines.setColumnClass(i++, String.class, true);    // Product/Charge
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // QtyOrdered
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);     // PriceActual
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // LineNetAmount
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // QtyDelivered
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // QtyInvoiced
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // QtyOnHand
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // Ship IP
		tblOrderLines.setColumnClass(i++, BigDecimal.class, true);    // Inv IP
		tblOrderLines.setColumnClass(i++, BigDecimal.class, !m_IsCreditOrder);    // Qty to Ship
		tblOrderLines.setColumnClass(i++, BigDecimal.class, false);    // Qty to Invoice - user entry
		tblOrderLines.setColumnClass(i++, BigDecimal.class, false);    // Qty to Mfg
		
		
		WListItemRenderer renderer = (WListItemRenderer) tblOrderLines.getItemRenderer();
		renderer.getHeaders().get(0).setVisible(false);  //ID
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(2), "250px"); //Name

		renderer.getHeaders().get(i-1).setVisible(docType_Production > 0);  //Qty to Mfg

		tblOrderLines.autoSize();
		
		
	}

	private void refresh(int C_Order_ID)
	{
		MOrder order = new MOrder(Env.getCtx(), C_Order_ID, null);
		m_IsCreditOrder = false; //!order.getC_DocType().isHasDeposit();
		
		fieldDescription.setText(order.getDescription());
		loadOrderLines(C_Order_ID);
		loadShipmentLines(C_Order_ID);
		loadInvoiceLines(C_Order_ID);
		loadDepositRecords(C_Order_ID);
		loadShipmentToCheck(C_Order_ID);
		
		confirmPanel.getButton("Assignment").setEnabled(false);
		if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_OrderLines)) {
			confirmPanel.getButton("Ship").setEnabled(m_IsCreditOrder );
		} else if (tabbedPane.getSelectedTab().getLabel().equals(LBL_TAB_ShipmentLines)) {
			confirmPanel.getButton("Assignment").setEnabled(true);
		}
	}
	
	private void loadShipmentLines(int C_Order_ID)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		Vector<Object> line = null;
		
		int i;
		try {
			pstmt = DB.prepareStatement(SQL_SHIPMENTLINES, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
				line = new Vector<Object>(9);
				i = 1;
				line.add(rs.getInt(i++));  // M_InOutLine_ID
				line.add(rs.getInt(i++));   // Line
				line.add(rs.getString(i++));    // Product/Charge
				line.add(rs.getString(i++));    // DocumentNo
				line.add(rs.getString(i++));    // DocStatus
				line.add(rs.getTimestamp(i++));    // MovementDate
				line.add(rs.getBigDecimal(i++));    // MovementQty
				line.add(rs.getString(i++));    // AttNo
				line.add(rs.getBigDecimal(i++));    // QtyInvoiced
				data.add(line);
			
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, SQL_SHIPMENTLINES, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		tblShipmentLines.setData(model, shipmentLineColumns);
		
		i = 0;
		tblShipmentLines.setColumnClass(i++, IDColumn.class, true); // M_InOutLine_ID
		tblShipmentLines.setColumnClass(i++, Integer.class, true);  // Line
		tblShipmentLines.setColumnClass(i++, String.class, true);    // Product/Charge
		tblShipmentLines.setColumnClass(i++, String.class, true);    // DocumentNo
		tblShipmentLines.setColumnClass(i++, String.class, true);    // DocStatus
		tblShipmentLines.setColumnClass(i++, Timestamp.class, true);    // MovementDate
		tblShipmentLines.setColumnClass(i++, BigDecimal.class, true);    // MovementQty
		tblShipmentLines.setColumnClass(i++, String.class, true);    // AttNo
		tblShipmentLines.setColumnClass(i++, BigDecimal.class, true);    // QtyInvoiced
		
		tblShipmentLines.setKeyColumnIndex(0);

		WListItemRenderer renderer = (WListItemRenderer) tblShipmentLines.getItemRenderer();
		renderer.getHeaders().get(0).setVisible(false);  //ID
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(2), "250px");  //Name
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(4), "150px");  //ShipmentNo
		tblShipmentLines.autoSize();
		
		
	}

	private void loadInvoiceLines(int C_Order_ID)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		Vector<Object> line = null;
		
		int i;
		try {
			pstmt = DB.prepareStatement(SQL_INVOICELINES, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
				line = new Vector<Object>(11);
				i = 1;
				line.add(rs.getInt(i++));  // C_InvoiceLine_ID
				line.add(rs.getInt(i++));   // Line
				line.add(rs.getString(i++));    // Product/Charge
				line.add(rs.getString(i++));    // DocumentNo
				String docStatus = rs.getString(i++); 
				line.add(docStatus);    // DocStatus
				line.add(rs.getTimestamp(i++));    // DateInvoiced
				BigDecimal qtyInvoiced = rs.getBigDecimal(i++); 
				line.add(qtyInvoiced);    // QtyInvoiced
				line.add(rs.getBigDecimal(i++));    // PriceActual
				line.add(rs.getBigDecimal(i++));    // TotalAmount
				line.add(rs.getString(i++));    // AttNo
				BigDecimal qtyDelivered = rs.getBigDecimal(i++); 
				line.add(qtyDelivered);    // MovementQty
				BigDecimal qtyToShip = Env.ZERO;
				if (docStatus.equals("CO | Paid")) {
					if (qtyDelivered == null)
						qtyToShip = qtyInvoiced;
					else 
						qtyToShip = qtyInvoiced.subtract(qtyDelivered);
				}
				line.add(qtyToShip);  // Allowed Qty To Ship 
				line.add(qtyToShip);  // Actual Qty To Ship - user input 
				data.add(line);

			}

		} catch (Exception e) {
			log.log(Level.SEVERE, SQL_INVOICELINES, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		tblInvoiceLines.setData(model, invoiceLineColumns);
		
		i = 0;
		tblInvoiceLines.setColumnClass(i++, IDColumn.class, true); // C_InvoiceLine_ID
		tblInvoiceLines.setColumnClass(i++, Integer.class, true);  // Line
		tblInvoiceLines.setColumnClass(i++, String.class, true);    // Product/Charge
		tblInvoiceLines.setColumnClass(i++, String.class, true);    // DocumentNo
		tblInvoiceLines.setColumnClass(i++, String.class, true);    // DocStatus
		tblInvoiceLines.setColumnClass(i++, Timestamp.class, true);    // DateInvoiced
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, true);    // QtyInvoiced
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, true);    // Price Actual
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, true);    // Total Amount
		tblInvoiceLines.setColumnClass(i++, String.class, true);    // AttNo
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, true);    // MovementQty				
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, true);    // QtyToShip				
		tblInvoiceLines.setColumnClass(i++, BigDecimal.class, false);    // QtyToShip Actual				

		tblInvoiceLines.setKeyColumnIndex(0);
		WListItemRenderer renderer = (WListItemRenderer) tblInvoiceLines.getItemRenderer();
		renderer.getHeaders().get(0).setVisible(false);  //ID
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(2), "250px"); // Name

		tblInvoiceLines.autoSize();
		
		MOrder order = new MOrder(Env.getCtx(), C_Order_ID, null);
		if (order.getC_BPartner().getTotalOpenBalance().signum() > 0) {
			confirmPanel.getButton(ICON_PAYINVOICE).setEnabled(true);
		} else {
			confirmPanel.getButton(ICON_PAYINVOICE).setEnabled(false);
		}
		
	}
	
	private void loadDepositRecords(int C_Order_ID)
	{
		if (docType_DepositPayment <= 0)
			return;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		Vector<Object> line = null;
		
		int i;
		try {
			pstmt = DB.prepareStatement(SQL_DEPOSITRECORDS, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
			m_depositRunningBalance = Env.ZERO;
			while (rs.next()) {
				line = new Vector<Object>(11);
				i = 1;
				line.add(rs.getTimestamp(i++));    // Trx Date
				line.add(rs.getString(i++));    // Invoice No
				line.add(rs.getString(i++));    // Payment Ref
				line.add(rs.getBigDecimal(i++));    // Deposit Amount
				BigDecimal invAmount = rs.getBigDecimal(i++);
				line.add(invAmount);    // Invoice Amount
				BigDecimal payAmount = rs.getBigDecimal(i++);
				line.add(payAmount);    // Payment Amount
				m_depositRunningBalance = m_depositRunningBalance.add(invAmount).subtract(payAmount);
				line.add(m_depositRunningBalance);    // Running Balance
				
				data.add(line);

			}

		} catch (Exception e) {
			log.log(Level.SEVERE, SQL_DEPOSITRECORDS, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		tblDepositRecords.setData(model, depositRecordColumns);
		
		i = 0;

		tblDepositRecords.setColumnClass(i++, Timestamp.class, true);    // Trx Date
		tblDepositRecords.setColumnClass(i++, String.class, true);    // Invoice No
		tblDepositRecords.setColumnClass(i++, String.class, true);    // Payment Ref
		tblDepositRecords.setColumnClass(i++, BigDecimal.class, true);    // Deposit Amount
		tblDepositRecords.setColumnClass(i++, BigDecimal.class, true);    // Invoice Amount
		tblDepositRecords.setColumnClass(i++, BigDecimal.class, true);    // Payment Amount
		tblDepositRecords.setColumnClass(i++, BigDecimal.class, true);    // Running Balance

//		WListItemRenderer renderer = (WListItemRenderer) tblDepositRecords.getItemRenderer();
//		renderer.getHeaders().get(0).setVisible(false);  //ID
//		renderer.getHeaders().get(2).setWidth("250px");  //Name

		tblDepositRecords.autoSize();
		
		if (tabDeposits != null) {
			if (m_depositRunningBalance.signum() > 0) {
				tabDeposits.setLabel(LBL_TAB_Deposits + " Balance=" + DisplayType.getNumberFormat(DisplayType.Account).format(m_depositRunningBalance)  );
				confirmPanel.getButton(ICON_PAYDEPOSIT).setEnabled(true);
			} else {
				tabDeposits.setLabel(LBL_TAB_Deposits);
				confirmPanel.getButton(ICON_PAYDEPOSIT).setEnabled(false);

			}
		}
	
	}

	private void loadShipmentToCheck(int C_Order_ID)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		
		Vector<Object> line = null;
		int nLines = 0;
		try {
			pstmt = DB.prepareStatement(SQL_SHIPMENT_TO_CHECK, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				line = new Vector<Object>(2);
				line.add(rs.getString(1));  // DocumentNo
				line.add(rs.getString(2));  // Messages
				line.add(rs.getBigDecimal(3));  // Amount
				data.add(line);
				nLines++;
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, SQL_SHIPMENT_TO_CHECK, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		tblShipmentstoCheck.setData(model, shipmentToCheckColumns);
		
		tblShipmentstoCheck.setColumnClass(0, String.class, true);  //DocumentNo
		tblShipmentstoCheck.setColumnClass(1, String.class, true);  //Messages
		tblShipmentstoCheck.setColumnClass(1, BigDecimal.class, true);  //Amount
		
		
		WListItemRenderer renderer = (WListItemRenderer) tblShipmentstoCheck.getItemRenderer();
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(0), "50px");  //line
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(2), "75px");  //Amount
		tblShipmentstoCheck.autoSize();
		
		if (tabShipmentToCheck != null) {
			if (nLines  > 0) {
				tabShipmentToCheck.setLabel(LBL_TAB_SHIPMENTS_TO_CHECK + "***");
			} else {
				tabShipmentToCheck.setLabel(LBL_TAB_SHIPMENTS_TO_CHECK);
			}
		}
	

	}

	@Override
	public void valueChange(ValueChangeEvent evt) {

	}

	public WOrderManagerForm()
	{
		super();
	}
	
	@Override
	protected void initForm() {
		initZk();
		dynInit();
		
	}
	
	protected void initZk()
	{
		initComponents();
		init();
		initDetailColumns();
	}
	
	protected void dynInit()
	{
		initContentTable();
		updateStatus(true);
		initConfig();
	}
	
	private void initConfig() {
		String action = MSysConfig.getValue(CONFIG_ACTION_SHIPMENT, Env.getAD_Client_ID(Env.getCtx()));
		MSysConfig config = null;
		if (action == null) {
			//not set - set default value for client
			config = new MSysConfig(Env.getCtx(), 0, null);
			config.setAD_Org_ID(0);
			config.setName(CONFIG_ACTION_SHIPMENT);
			config.setValue(docActionShipment);
			config.setDescription("Default value created from OrderManagerForm. Please update");
			config.setConfigurationLevel(MSysConfig.CONFIGURATIONLEVEL_Client);
			config.save();
		} else {
			docActionShipment = action;
		}

		action = MSysConfig.getValue(CONFIG_ACTION_INVOICE, Env.getAD_Client_ID(Env.getCtx()));
		if (action == null) {
			//not set - set default value for client
			config = new MSysConfig(Env.getCtx(), 0, null);
			config.setAD_Org_ID(0);
			config.setName(CONFIG_ACTION_INVOICE);
			config.setValue(docActionInvoice);
			config.setDescription("Default value created from OrderManagerForm. Please review and update.");
			config.setConfigurationLevel(MSysConfig.CONFIGURATIONLEVEL_Client);
			config.save();
		} else {
			docActionInvoice = action;
		}

	}

	private void initDetailColumns()
	{
		orderLineColumns.add("ID");  // C_OrderLine_ID
		orderLineColumns.add("Line");   // Line
		orderLineColumns.add("Name");  // Product/Charge
		orderLineColumns.add("Qty Ordered");  // QtyOrdered
		orderLineColumns.add("Price Actual");   // PriceActual
		orderLineColumns.add("Total Amount");  // LineNetAmount
		orderLineColumns.add("Qty Delivered");  // QtyDelivered
		orderLineColumns.add("Qty Invoiced");  // QtyInvoiced
		orderLineColumns.add("Qty On Hand");  // QtyOnHand
		orderLineColumns.add("Ship IP");  // Ship In Progress
		orderLineColumns.add("Inv IP");  // Invoice in Progress
		orderLineColumns.add("Qty to Ship");  // Qty to Ship - user entry
		orderLineColumns.add("Qty to Invoice");  // Qty to Invoice - user entry
		orderLineColumns.add("Qty to Mfg");  // Qty to Mfg - user entry
		
		shipmentLineColumns.add("ID");  // M_InOutLine_ID
		shipmentLineColumns.add("Line");   // Line
		shipmentLineColumns.add("Name");  // Product/Charge
		shipmentLineColumns.add("Shipment No");  // DocumentNo
		shipmentLineColumns.add("Status");  // DocStatus
		shipmentLineColumns.add("Movement Date");  // MovementDate
		shipmentLineColumns.add("Movement Qty");  // MovementQty
		shipmentLineColumns.add("Att No");   // AttNo
		shipmentLineColumns.add("Qty Invoiced");  // QtyInvoiced
		
		invoiceLineColumns.add("ID");  // C_InvoiceLine_ID
		invoiceLineColumns.add("Line");   // Line
		invoiceLineColumns.add("Name");  // Product/Charge
		invoiceLineColumns.add("Invoice No");  // DocumentNo
		invoiceLineColumns.add("DocStatus");  // DocStatus
		invoiceLineColumns.add("Invoice Date");  // DateInvoiced
		invoiceLineColumns.add("Invoice Qty");  // QtyInvoiced
		invoiceLineColumns.add("Price Actual");  // QtyInvoiced
		invoiceLineColumns.add("Total Amount");  // QtyInvoiced
		invoiceLineColumns.add("Att No");   // AttNo
		invoiceLineColumns.add("Qty Delivered");  // MovementQty
		invoiceLineColumns.add("Allowed to Ship"); 
		invoiceLineColumns.add("Qty To Ship");  

		depositRecordColumns.add("Trx Date");
		depositRecordColumns.add("Invoice No");
		depositRecordColumns.add("Payment Ref");
		depositRecordColumns.add("Deposit Amount");
		depositRecordColumns.add("Invoice Amount");
		depositRecordColumns.add("Payment");
		depositRecordColumns.add("Running Balance");
		
		shipmentToCheckColumns.add("DocumentNo");
		shipmentToCheckColumns.add("Message / Comments");
		shipmentToCheckColumns.add("Amount");
	}
	
	public void executeQuery()
	{
		log.info("");
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		
		setParameters();
		String sql = getOrderSQL();
		isTableOnQuery = true;

		log.fine(sql);
		//  reset table
		int row = 0;
		//contentPanel.setRowCount(row);
		//  Execute
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, AD_Client_ID);
			
			ResultSet rs = pstmt.executeQuery();
			
			if (m_modelAll != null) {
				contentPanel.setModel(m_modelAll);
			}
			contentPanel.clearTable();
			contentPanel.setRowCount(row);
			
			//
			while (rs.next())
			{
				//  extend table
				contentPanel.setRowCount(row+1);
				//  set values
				contentPanel.setValueAt(new IDColumn(rs.getInt(1)), row, 0);   //  C_Order_ID
				contentPanel.setValueAt(rs.getString(2), row, 1);              //  Org
				contentPanel.setValueAt(rs.getString(3), row, 2);              //  Warehouse
				contentPanel.setValueAt(rs.getString(4), row, 3);              //  Sales Rep
				contentPanel.setValueAt(rs.getString(5), row, 4);              //  Doc No
				contentPanel.setValueAt(rs.getString(6), row, 5);              //  PO Reference
				contentPanel.setValueAt(rs.getString(7), row, 6);              //  BPartner
				contentPanel.setValueAt(rs.getTimestamp(8), row, 7);           //  DateOrdered
				contentPanel.setValueAt(rs.getTimestamp(9), row, 8);              //  Date Promised
				contentPanel.setValueAt(rs.getBigDecimal(10), row, 9);          //  TotalLines
				contentPanel.setValueAt(rs.getBigDecimal(11), row, 10);          //  UnshippedTotal
				contentPanel.setValueAt(rs.getBigDecimal(12), row, 11);          //  UninvoiceTotal
				contentPanel.setValueAt(rs.getString(13), row, 12);          //  Col Status
				contentPanel.setValueAt(rs.getString(14), row, 13);          //  Ship In Progress

				//  prepare next
				row++;
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		//
	//	contentPanel.autoSize(); jo
		updateStatus(true);
		contentPanel.repaint();
		this.invalidate();
		
		m_modelAll = contentPanel.getModel();
		
		if (contentPanel.getSelectedRowKey() != null) {
			m_C_Order_ID = contentPanel.getSelectedRowKey();
		} else {
			m_C_Order_ID = 0;
		}
		refresh(m_C_Order_ID);
		isTableOnQuery = false;
		
	}   //  executeQuery

	private void setParameters()
	{
		m_AD_Org_ID = null;
		m_M_Warehouse_ID = null;
		m_SalesRep_ID = null;
		m_C_BPartner_ID = null;
		m_DocumentNo = null;
		m_OrderRef = null;
		m_DateOrderedFrom = null;
		m_DateOrderedTo = null;
		
		ListItem listitem = pickOrg.getSelectedItem();
		if (listitem != null && pickOrg.getSelectedIndex() != 0)
			m_AD_Org_ID = (Integer)listitem.getValue();
		
		listitem = pickWarehouse.getSelectedItem();
		if (listitem != null && pickWarehouse.getSelectedIndex() != 0)
			m_M_Warehouse_ID = (Integer)listitem.getValue();

		listitem = pickSalesRep.getSelectedItem();
		if (listitem != null && pickSalesRep.getSelectedIndex() != 0)
			m_SalesRep_ID = (Integer)listitem.getValue();

		listitem = pickDocType.getSelectedItem();
		if (listitem != null && pickDocType.getSelectedIndex() != 0)
			m_Order_DocType_ID = (Integer)listitem.getValue();

		m_C_BPartner_ID =  editorBPartner.getValue();
		
		m_DocumentNo = txtDocumentNo.getValue();
		m_OrderRef = txtOrderRef.getValue();
		m_DateOrderedFrom = dateFrom.getValue();
		m_DateOrderedTo = dateTo.getValue();
		
	}
	
	private boolean setPickID(Listbox picker, int ID)
	{
		for (int i = 0; i < picker.getItemCount(); i++)
		{
			 Integer key = (Integer) picker.getItemAtIndex(i).getValue();
			if (key == ID)
			{
				picker.setSelectedIndex(i);
				return true;
			}
		}
		return false;
	}
	
	private void filterBy(String status, String style) {
		
		ListModelTable current = new ListModelTable();
		for (int i=0; i < m_modelAll.size(); i++)
		{
			if (status.equals((m_modelAll.getValueAt(i, IDX_COL_STATUS)))) {
				current.add(m_modelAll.get(i));
			}
		}
		
		contentPanel.setModel(current);
		updateStatus(false);
	}
	
	private void updateStatus(boolean updateCount)
	{
		m_no_transaction = 0;
		m_must_ship = 0;
		m_must_invoice = 0;
		m_fulfilled = 0;
			
		
		for (int i=0; i < contentPanel.getRowCount(); i++) {
			ListItem item = (ListItem) contentPanel.getItems().get(i);
			assignVisual(item, i);
		}
		
		if (updateCount) {
			int total = m_no_transaction + m_must_ship + m_must_invoice + m_fulfilled;
			
			optAllOpenOrder.setLabel("All Open Orders = " + total);
			optOrderNoTransaction.setLabel("Orders with No Transactions = " + m_no_transaction);
			optOrderRequireShipment.setLabel("Orders require shipment = " + m_must_ship);
			optOrderRequireInvoice.setLabel("Orders require invoice = " + m_must_invoice);
			optOrderFulfilled.setLabel("Orders Good to Close = " + m_fulfilled);
		}
	}
	
	private void assignVisual(ListItem item, int index)
	{
		String status = (String) contentPanel.getValueAt(index, IDX_COL_STATUS);
		String textStyle = getTextStyle(index);

		if (STATUS_NO_TRANSACTION.equals(status)) {
			item.setStyle(textStyle + ROW_STYLE_NO_TRANSACTION);
			m_no_transaction++;
		} else if (STATUS_MUST_SHIP.equals(status)) {
			item.setStyle(textStyle + ROW_STYLE_MUST_SHIP);
			m_must_ship++;
		} else if (STATUS_MUST_INVOICE.equals(status)) {
			item.setStyle(textStyle + ROW_STYLE_MUST_INVOICE);
			m_must_invoice++;
		} else if (STATUS_FULFILLED.equals(status)) {
			item.setStyle(textStyle + ROW_STYLE_FULFILLED);
			m_fulfilled++;
		} else {
			item.setStyle(textStyle);
		}
		
	}
	
	private String getTextStyle(int i) 
	{
		String textStyle = null;
		String hascomment = (String) contentPanel.getValueAt(i, IDX_COL_SHIPMENT_IN_PROGRESS);
		if ( "Y".equals(hascomment)) {
			textStyle  = TEXT_STYLE_UNDERLINE;
		}
		else {
			textStyle = TEXT_STYLE_NORMAL;
		}
		return textStyle;
	}

	
	private void initContentTable()
	{
		ColumnInfo[] layout = new ColumnInfo[] { new ColumnInfo(" ", ".", IDColumn.class, false, false, ""), // 0
				new ColumnInfo(Msg.translate(Env.getCtx(), "AD_Org_ID"), ".", String.class), // 1
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Warehouse_ID"), ".", String.class), // 2
				new ColumnInfo(Msg.translate(Env.getCtx(), "SalesRep_ID"), ".", String.class), // 3
				new ColumnInfo(Msg.translate(Env.getCtx(), "DocumentNo"), ".", String.class), // 4
				new ColumnInfo(Msg.translate(Env.getCtx(), "POReference"), ".", String.class, true), // 5
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"), ".", String.class, true), // 6
				new ColumnInfo("Date Ordered", ".", Timestamp.class, true), // 7
				new ColumnInfo("Date Promised", ".", Timestamp.class, true), // 8
				new ColumnInfo("Total Lines", ".", BigDecimal.class), // 9
				new ColumnInfo("Unshipped Total", ".", BigDecimal.class), // 10
				new ColumnInfo("Uninvoiced Total", ".", BigDecimal.class), // 11
				new ColumnInfo("ColStatus", ".", String.class), //12
				new ColumnInfo("ShipInProgress", ".", String.class), //13
		};

		contentPanel.prepareTable(layout, "", "", true, "");
		
//		contentPanel.setKeyColumnIndex(0);
		//
		contentPanel.autoSize();
		
		executeQuery();
		
		WListItemRenderer renderer = (WListItemRenderer) contentPanel.getItemRenderer();
		renderer.getHeaders().get(IDX_COL_STATUS).setVisible(false);  // Col Status
		renderer.getHeaders().get(IDX_COL_SHIPMENT_IN_PROGRESS).setVisible(false);  // Ship in Progress

	}


	private void fillOrgs()
	{
		try {
			//	Org
			String sql = MRole.getDefault().addAccessSQL (
				"SELECT AD_Org_ID, Value || ' - ' || Name AS ValueName "
				+ "FROM AD_Org "
				+ "WHERE IsActive='Y'",
					"AD_Org", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Value";
			pickOrg.appendItem("", 0);
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickOrg.appendItem(rs.getString("ValueName"), rs.getInt("AD_Org_ID"));
			}
			rs.close();
			pstmt.close();
			
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
	}
	
	private void fillWarehouse()
	{
		try {
			//	Warehouse
			String sql = MRole.getDefault().addAccessSQL (
				"SELECT M_Warehouse_ID, Value || ' - ' || Name AS ValueName "
				+ "FROM M_Warehouse "
				+ "WHERE IsActive='Y' AND ? IN (AD_Org_ID, 0) ",
					"M_Warehouse", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Value";
			pickWarehouse.getItems().clear();
			pickWarehouse.appendItem("", 0);
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (int) m_AD_Org_ID);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickWarehouse.appendItem(rs.getString("ValueName"), rs.getInt("M_Warehouse_ID"));
			}
			rs.close();
			pstmt.close();

			
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
	}

	private void fillSalesRep()
	{
		try {
			//	SalesRep
			String sql = MRole.getDefault().addAccessSQL (
				"SELECT AD_User_ID, Name AS ValueName "
				+ "FROM AD_User "
				+ "WHERE IsActive='Y' AND ? IN (AD_Org_ID, 0) AND EXISTS (SELECT * FROM C_BPartner bp WHERE AD_User.C_BPartner_ID=bp.C_BPartner_ID AND bp.IsSalesRep='Y') ",
					"AD_User", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Name";
			pickSalesRep.getItems().clear();
			pickSalesRep.appendItem("", 0);
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (int) m_AD_Org_ID);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickSalesRep.appendItem(rs.getString("ValueName"), rs.getInt("AD_User_ID"));
			}
			rs.close();
			pstmt.close();
			
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
	}

	private void fillDocType()
	{
		try {
			//	DocTypes
			String sql = MRole.getDefault().addAccessSQL (
				"SELECT C_DocType_ID, Name AS ValueName "
				+ "FROM C_DocType "
				+ "WHERE IsActive='Y' AND docsubtypeso  = 'SO' ",
					"C_DocType", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Name";
			pickDocType.appendItem("", 0);
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickDocType.appendItem(rs.getString("ValueName"), rs.getInt("C_DocType_ID"));
			}
			rs.close();
			pstmt.close();
			
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
	}

	private void createPayment(boolean isDepositPayment)
	{
		m_C_Order_ID  = contentPanel.getSelectedRowKey();
		int C_DocType_ID = isDepositPayment ? docType_DepositPayment : docType_ARPayment;
		String trxName = Trx.createTrxName("OMF");	
		Trx trx = Trx.get(trxName, true);	//trx needs to be committed too

		MOrder order = new MOrder(Env.getCtx(), m_C_Order_ID, trxName);
		String where = "C_Payment.Processed = 'N' AND C_Payment.AD_Org_ID=? AND C_Payment.C_DocType_ID=? AND C_Payment.C_BPartner_ID=?" ;
		
		
		int C_BankAccount_ID = DB.getSQLValue(null, "SELECT c_bankaccount_id FROM c_paymentprocessor WHERE ad_org_id = ?", order.getAD_Org_ID());
		if (C_BankAccount_ID <= 0) {
			FDialog.error(0, "No Payment Processor set for Org=" + MOrg.get(Env.getCtx(), order.getAD_Org_ID()).getName());
			return;
		}
		int paymentID = 0;
		try {
			MPayment payment = new Query(Env.getCtx(), MPayment.Table_Name, where , trxName)
					.setParameters(order.getAD_Org_ID(), C_DocType_ID, order.getC_BPartner_ID())
					.firstOnly();
			
			if (payment == null) {
				payment = new MPayment(Env.getCtx(), 0, trxName);
				payment.setAD_Org_ID(order.getAD_Org_ID());
				payment.setC_BankAccount_ID(C_BankAccount_ID);
				payment.setC_DocType_ID(C_DocType_ID);
				payment.setC_BPartner_ID(order.getC_BPartner_ID());
				payment.setC_Currency_ID(order.getC_Currency_ID());
				payment.setTrxType(MPayment.TRXTYPE_Sales);
				payment.setTenderType(MPayment.TENDERTYPE_CreditCard);
				payment.setIsOnline(true);
				payment.save();
				
			}
					
			paymentID =  payment.getC_Payment_ID();
		} catch (Exception e) {
			
		} finally {
			trx.close();
			trx = null;
		}


		//FDialog.info(0, null, message.toString());
		
		//refresh(m_C_Order_ID);
		if (paymentID > 0) {
			AEnv.zoom(MPayment.Table_ID, paymentID);
		}

	}
	
	/**
	 * Get SQL for Orders that needs to be fulfilled
	 * @return sql
	 */
	private String getOrderSQL()
	{
	//  Create SQL
        StringBuffer sql = new StringBuffer(
        		"select ss.c_order_id, ss.orgname, ss.warehouse, ss.salesrepname, ss.documentno, ss.poreference, ss.bpname, ss.dateordered, ss.datepromised, ss.totallines, ss.unshippedtotal, ss.uninvoicedtotal " +
        	    ", case  " +
        	    "    when  (ss.unshippedtotal = ss.totallines) and (ss.uninvoicedtotal = ss.totallines) then '0' " +
        	    "    when (ss.uninvoicedtotal < ss.unshippedtotal) then '2' " +
        	    "    when (ss.unshippedtotal < ss.uninvoicedtotal) then '4' " +
        	    "    when  (ss.unshippedtotal = 0) and (ss.uninvoicedtotal = 0)  then '9' " +
        	    "   end as colstatus " +
        	    ", CASE WHEN EXISTS (SELECT * FROM m_inout io WHERE io.c_order_id = ss.c_order_id AND io.processed = 'N') " + 
        	    " OR EXISTS (SELECT * FROM c_invoice i  WHERE i.c_order_id = ss.c_order_id AND   i.processed = 'N') " +        	    
        	    " THEN 'Y' ELSE 'N' END AS shipinprogress " +
        	    "from (  " +
        	    "select o.c_order_id, org.name as orgname, w.name as warehouse " +
        	    ",  (select name from ad_user where ad_user_id = o.salesrep_id) as salesrepname " +
		         ", o.documentno || '-' || dt.name as documentno, o.poreference, bp.name as bpname, o.dateordered, o.datepromised, o.totallines " +
		         ", (select round(sum((qtyordered - qtydelivered) * priceactual),2) from c_orderline  where c_order_id = o.c_order_id) as unshippedtotal " +
		         ", (select round(sum((qtyordered - qtyinvoiced) * priceactual),2) from c_orderline where c_order_id = o.c_order_id) as uninvoicedtotal " +
		         ", o.ad_client_id, o.salesrep_id, o.m_warehouse_id, o.ad_org_id, o.c_bpartner_id, o.c_doctype_id " +
		         "from c_order o " +
		         "inner join ad_org org on o.ad_org_id = org.ad_org_id " +
		         "inner join m_warehouse w on o.m_warehouse_id = w.m_warehouse_id " +
		         "inner join c_bpartner bp on o.c_bpartner_id = bp.c_bpartner_id " +
		         "inner join c_doctype dt on o.c_doctype_id = dt.c_doctype_id " +
		         "where o.issotrx = 'Y' and o.docstatus = 'CO' " +
		         " and dt.docsubtypeso IN ('SO', 'WI', 'WP', 'WR') " +
		         ") ss " +
		         "where ss.ad_client_id =? "
		        );

        if (m_AD_Org_ID != null)
            sql.append(" AND ss.AD_Org_ID=").append(m_AD_Org_ID);
        if (m_M_Warehouse_ID != null)
            sql.append(" AND ss.M_Warehouse_ID=").append(m_M_Warehouse_ID);
        if (m_SalesRep_ID != null)
            sql.append(" AND ss.SalesRep_ID=").append(m_SalesRep_ID);
        if (m_Order_DocType_ID != null)
            sql.append(" AND ss.C_DocType_ID=").append(m_Order_DocType_ID);
        if (m_C_BPartner_ID != null)
            sql.append(" AND ss.C_BPartner_ID=").append(m_C_BPartner_ID);
        if (m_DocumentNo != null && m_DocumentNo.length() > 0)
            sql.append(" AND ss.DocumentNo ILIKE '").append("%" + m_DocumentNo + "%'");
        if (m_OrderRef != null && m_OrderRef.length() > 0)
            sql.append(" AND ss.POReference ILIKE '").append("%" + m_OrderRef + "%'");
        if (m_DateOrderedFrom != null) 
        	sql.append(" AND TRUNC(ss.dateordered) >= ").append("'" + m_DateOrderedFrom + "'");
        if (m_DateOrderedTo != null) 
        	sql.append(" AND TRUNC(ss.dateordered) <= ").append("'" + m_DateOrderedTo + "'");
        
        
//        if (m_Good_To_Ship != 0)
//            sql.append(" AND ic.isGoodToShip='").append(m_Good_To_Ship == 1 ? "Y" : "N").append("'");
//
//        if (m_DatePromised != null) {
//        	sql.append(" AND ic.datepromised <= ? ");
//        }
//        
        
        // bug - [ 1713317 ] Generate Shipments (manual) show locked records
        /* begin - Exclude locked records; @Trifon */
        int AD_User_ID = Env.getContextAsInt(Env.getCtx(), "#AD_User_ID");
        String lockedIDs = MPrivateAccess.getLockedRecordWhere(MOrder.Table_ID, AD_User_ID);
        if (lockedIDs != null)
        {
            if (sql.length() > 0)
                sql.append(" AND ");
            sql.append("ss.C_Order_ID").append(lockedIDs);
        }
        /* eng - Exclude locked records; @Trifon */
          
        //
        sql.append(" ORDER BY ss.orgname, ss.warehouse, ss.documentno");
//        sql.append(" LIMIT 100");
        return sql.toString();
	}

	@Override
	public void lockUI(ProcessInfo pi) {
		showBusyDialog();

		
	}

	@Override
	public void unlockUI(ProcessInfo pi) {
		hideBusyDialog();
		
	}

	@Override
	public boolean isUILocked() {
		return false;
	}

	@Override
	public void executeASync(ProcessInfo pi) {
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		if (!isTableOnQuery ) 
		{

			assignVisual((ListItem) contentPanel.getItems().get(event.getIndex0()), event.getIndex0());
			
			//display comment only when 1 item is selected
			if (contentPanel.getSelectedCount() == 1) {
			
				int idx = contentPanel.getSelectedIndex();
				ListItem item = (ListItem) contentPanel.getItems().get(idx);
				item.setStyle(getTextStyle(idx) + ROW_STYLE_SELECTED);
				
			}
			else {

				
			}
			
		}
		
	}

    
}	//	InfoProduct
