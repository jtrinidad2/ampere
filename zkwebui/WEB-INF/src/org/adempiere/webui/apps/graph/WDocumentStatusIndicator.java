/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin * Copyright (C) 2008 Idalica Corporation *
 * This program is free software; you can redistribute it and/or modify it *
 * under the terms version 2 of the GNU General Public License as published * by
 * the Free Software Foundation. This program is distributed in the hope * that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. * See the
 * GNU General Public License for more details. * You should have received a
 * copy of the GNU General Public License along * with this program; if not,
 * write to the Free Software Foundation, Inc., * 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA. *
 *****************************************************************************/
package org.adempiere.webui.apps.graph;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.model.IDocumentStatus;
import org.adempiere.model.MDocumentStatus;
import org.adempiere.util.FontUtil;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.util.ZKUtils;
import org.adempiere.webui.window.FindWindow;
import org.compiere.model.GridField;
import org.compiere.model.MQuery;
import org.compiere.model.MWindow;
import org.compiere.print.MPrintFont;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;

/**
 * Performance Indicator
 *
 * @author hengsin
 */
public class WDocumentStatusIndicator extends Panel implements EventListener<Event>
{
	/**
	 *
	 */
	private static final long		serialVersionUID	= 3580494126343850939L;

	private static final CLogger	logger				= CLogger.getCLogger(WDocumentStatusIndicator.class);

	private static final String		SQL_GET_TABLE		= "SELECT t.Name, tb.TableName, t.AD_Tab_ID FROM AD_Tab t "
			+ " INNER JOIN AD_Table tb ON (t.AD_Table_ID = tb.AD_Table_ID)"
			+ " WHERE t.AD_Table_ID = ? AND t.AD_Window_ID = ?";

	/**
	 * Constructor
	 * 
	 * @param goal goal model
	 */
	public WDocumentStatusIndicator(IDocumentStatus documentStatus)
	{
		super();

		m_documentStatus = documentStatus;

		init();
		this.setStyle("border:1px solid #B1CBD5;");

		statusLabel.setText("Pending");
	} // PerformanceIndicator

	private IDocumentStatus	m_documentStatus	= null;
	private int				statusCount;
	private Label			statusLabel;

	/**
	 * Get Document Status
	 * 
	 * @return Document Status
	 */
	public IDocumentStatus getDocumentStatus()
	{
		return m_documentStatus;
	} // getGoal

	/**
	 * Init Graph Display Kinamo (pelgrim)
	 */
	private void init()
	{
		int logo_ID = m_documentStatus.getLogo_ID();
		String logoFile = null;

		if (logo_ID > 0)
		{
			logoFile = "" + m_documentStatus.getU_PA_DocumentStatus_ID() + "_" + logo_ID + ".png";
			ZKUtils.createImageFromLogoID(logoFile, logo_ID);
		}

		Image img = new Image(logoFile);
		img.setStyle("padding: 2px; max-width: 40px; max-height: 30px;");
		
		statusLabel = new Label();
		statusLabel.setSclass("dp-doc-status-label");
		/*
		AD_PrintColor_ID = m_documentStatus.getNumber_PrintColor_ID();
		printColor = MPrintColor.get(Env.getCtx(), AD_PrintColor_ID);
		color = ZkCssHelper.createHexColorString(printColor.getColor());
		AD_PrintFont_ID = m_documentStatus.getNumber_PrintFont_ID();
		fontStyle = getStyle(AD_PrintFont_ID);
		statusLabel.setStyle("color:#" + color + ";" + fontStyle);
		*/
		ZKUpdateUtil.setWidth(statusLabel, "60px");

		Label nameLabel = new Label();
		nameLabel.setText(m_documentStatus.getName());
		nameLabel.setSclass("dp-doc-status-name");
		/*
		int AD_PrintColor_ID = m_documentStatus.getName_PrintColor_ID();
		MPrintColor printColor = MPrintColor.get(Env.getCtx(), AD_PrintColor_ID);
		String color = ZkCssHelper.createHexColorString(printColor.getColor());
		int AD_PrintFont_ID = m_documentStatus.getName_PrintFont_ID();
		String fontStyle = getStyle(AD_PrintFont_ID);
		nameLabel.setStyle("color:#" + color + ";" + fontStyle);
		*/
		

		Hlayout box = new Hlayout();
		box.setSclass("dp-doc-status");
		box.appendChild(img);
		box.appendChild(statusLabel);
		box.appendChild(nameLabel);
		box.setTooltiptext(m_documentStatus.getName());
		appendChild(box);

		this.addEventListener(Events.ON_CLICK, this);
	}

	private String getStyle(int AD_PrintFont_ID)
	{
		String family = "";
		boolean bold = false;
		boolean italic = false;
		int pointSize = 10;

		if (AD_PrintFont_ID > 0)
		{
			MPrintFont printFont = MPrintFont.get(AD_PrintFont_ID);
			family = FontUtil.getFontFamily(printFont.getFont().getFamily());
			bold = printFont.getFont().isBold();
			italic = printFont.getFont().isItalic();
			pointSize = printFont.getFont().getSize();
		}

		String fontStyle = "font-family:'" + family + "';font-weight:" + (bold ? "bold" : "normal") + ";font-style:"
				+ (italic ? "italic" : "normal") + ";font-size:" + pointSize + "pt";

		return fontStyle;
	}

	public void onEvent(Event event) throws Exception
	{
		int AD_Window_ID = m_documentStatus.getAD_Window_ID();
		int AD_Form_ID = m_documentStatus.getAD_Form_ID();

		if (AD_Window_ID > 0)
		{
			if (Util.isEmpty(m_documentStatus.getWhereClause(), true) && m_documentStatus.isDisplaySearchDialog())
			{
				openFindWindow(m_documentStatus.getAD_Table_ID(), AD_Window_ID);
			}
			else
			{
				MQuery query = new MQuery(m_documentStatus.getAD_Table_ID());
				query.addRestriction(MDocumentStatus.getWhereClause(m_documentStatus));
				AEnv.zoom(AD_Window_ID, query);
			}
		}
		else if (AD_Form_ID > 0)
		{
			ADForm form = ADForm.openForm(AD_Form_ID);

			form.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
			SessionManager.getAppDesktop().showWindow(form);
		}

	}

	public void refresh()
	{
		statusCount = MDocumentStatus.evaluate(m_documentStatus);
	}

	public void updateUI()
	{
		statusLabel.setText(Integer.toString(statusCount));
	}

	private void openFindWindow(int ad_Table_ID, int ad_Window_ID)
	{
		int tabID = 0;
		String tableName = null;
		String tabName = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(SQL_GET_TABLE, null);
			pstmt.setInt(1, ad_Table_ID);
			pstmt.setInt(2, ad_Window_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				tabName = rs.getString(1);
				tableName = rs.getString(2);
				tabID = rs.getInt(3);
			}
		}
		catch (SQLException e)
		{
			logger.log(Level.SEVERE, "Could find Window", e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		int windowNo = SessionManager.getAppDesktop().registerWindow(this);
		GridField[] fields = GridField.createFields(Env.getCtx(), windowNo, 0, tabID);
		FindWindow find = new FindWindow(ad_Window_ID, tabName, ad_Table_ID, tableName,
				MDocumentStatus.getWhereClause(m_documentStatus), fields, 1, tabID,
				new MWindow(Env.getCtx(), ad_Window_ID, null).getWindowType().equals(MWindow.WINDOWTYPE_Transaction));

		if (find.getTitle() != null && find.getTitle().length() > 0)
		{
			// Title is not set when the number of rows is below the minRecords
			// parameter (10)
			if (!find.isCancel())
			{
				AEnv.zoom(ad_Window_ID, find.getQuery());
			}
			find = null;
			fields = null;
		}
	} // openFindWindow
}
