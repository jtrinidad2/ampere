/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2008 Low Heng Sin. All Rights Reserved.                      *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.window;

import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.ToolBarButton;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.Adempiere;
import org.compiere.model.MUser;
import org.compiere.util.CLogErrorBuffer;
import org.compiere.util.CLogMgt;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Pre;
import org.zkoss.zhtml.Text;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SizeEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Vlayout;

/**
 *
 * @author Low Heng Sin
 *
 */
public class AboutWindow extends Window implements EventListener<Event> {

	/**
	 *
	 */
	private static final long serialVersionUID = -257313771447940626L;
	private Tabbox tabbox;
	private Tabpanels tabPanels;
	private Button btnDownload;
	private Button btnErrorEmail;
	private Button btnRefreshLog;
	private Combobox logLevel;
	private Textbox logBox;
	private Button btnClearLog;

	/**	Logger			*/
    private static final CLogger log = CLogger.getCLogger(AboutWindow.class);
    
	public AboutWindow() {
		super();
		init();
	}

	private void init() {
		ZKUpdateUtil.setWidth(this, "900px");
		ZKUpdateUtil.setHeight(this, "600px");
		this.setPosition("center");
		this.setTitle(Adempiere.NAME);
		this.setClosable(true);
		this.setSizable(true);

		this.addEventListener(Events.ON_SIZE, this);

		Vlayout layout = new Vlayout();
		ZKUpdateUtil.setWidth(layout, "100%");
		ZKUpdateUtil.setHeight(layout, "100%");
		layout.setParent(this);

		
		tabbox = new Tabbox();
		ZKUpdateUtil.setVflex(tabbox, "1");
		Tabs tabs = new Tabs();
		tabs.setParent(tabbox);
		tabPanels = new Tabpanels();
		tabPanels.setParent(tabbox);

		//about
		Tab tab = new Tab();
		tab.setLabel(Msg.getMsg(Env.getCtx(), "About"));
		tab.setParent(tabs);
		Tabpanel tabPanel = createAbout();
		tabPanel.setParent(tabPanels);

		//Credit
		tab = new Tab();
		tab.setLabel(Msg.getMsg(Env.getCtx(), "Credits"));
		tab.setParent(tabs);
		tabPanel = createCredit();
		tabPanel.setParent(tabPanels);

		//Info
		tab = new Tab();
		tab.setLabel(Msg.getMsg(Env.getCtx(), "Info"));
		tab.setParent(tabs);
		tabPanel = createInfo();
		tabPanel.setParent(tabPanels);

		//Trace
		tab = new Tab();
		tab.setLabel(Msg.getMsg(Env.getCtx(),"TraceInfo"));
		tab.setParent(tabs);
		tabPanel = createTrace();
		tabPanel.setParent(tabPanels);

		Div south = new Div();
		Button btnOk = new Button();
		btnOk.setIconSclass("z-icon-Ok");
		btnOk.addEventListener(Events.ON_CLICK, this);
		btnOk.setStyle("float:right;");
		south.appendChild(btnOk);
		south.setHeight("40px");
		
		layout.appendChild(tabbox);
		layout.appendChild(south);
	}

	private Tabpanel createTrace() {
		Tabpanel tabPanel = new Tabpanel();
		Vlayout vbox = new Vlayout();
		vbox.setParent(tabPanel);
		ZKUpdateUtil.setWidth(vbox, "100%");
		ZKUpdateUtil.setHeight(vbox, "100%");

		Hlayout hbox = new Hlayout();
		
		Level[] levels = CLogMgt.LEVELS;
			
		logLevel = new Combobox();
        logLevel.setTooltiptext(Msg.getMsg(Env.getCtx(),"TraceLevel"));
        

		Label label= new Label(Msg.getMsg(Env.getCtx(),"TraceLevel", true));
		label.setParent(hbox);

		MUser user = MUser.get(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()));
		logLevel.setReadonly(!user.isAdministrator());
		int currentLevel = -1;
        for (int i = 0; i < levels.length; i++)
        {
			logLevel.appendItem(levels[i].getName());
			if (CLogMgt.getLevel().getName().equals(levels[i].getName()))
				currentLevel = i;
        }
        if ( currentLevel >= 0 )
        	logLevel.setSelectedIndex(currentLevel);

		logLevel.addEventListener(Events.ON_SELECT, this);
		hbox.appendChild(logLevel);

		btnClearLog = new Button(Msg.getMsg(Env.getCtx(), "Reset"));
		btnClearLog.addEventListener(Events.ON_CLICK, this);
		hbox.appendChild(btnClearLog);
		btnRefreshLog = new Button(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Refresh")));
		btnRefreshLog.addEventListener(Events.ON_CLICK, this);
		hbox.appendChild(btnRefreshLog);
		btnDownload = new Button(Msg.getMsg(Env.getCtx(), "SaveFile"));
		btnDownload.addEventListener(Events.ON_CLICK, this);
		hbox.appendChild(btnDownload);
		btnErrorEmail = new Button(Msg.getMsg(Env.getCtx(), "SendEMail"));
		btnErrorEmail.addEventListener(Events.ON_CLICK, this);
		hbox.appendChild(btnErrorEmail);
		vbox.appendChild(hbox);
		
		logBox = new Textbox();
		logBox.setMultiline(true);
		ZKUpdateUtil.setWidth(logBox, "100%");
		ZKUpdateUtil.setVflex(logBox, "1");
		logBox.setValue(CLogErrorBuffer.get(true).getLogInfo(Env.getCtx()));
		vbox.appendChild(logBox);

		return tabPanel;
	}
	
	private Tabpanel createInfo() {
		Tabpanel tabPanel = new Tabpanel();
		Div div = new Div();
		div.setParent(tabPanel);
		ZKUpdateUtil.setHeight(div, "100%");
		div.setStyle("overflow: auto;");
		Pre pre = new Pre();
		pre.setParent(div);
		Text text = new Text(CLogMgt.getInfo(null).toString());
		text.setParent(pre);

		return tabPanel;
	}

	private Tabpanel createCredit() {
		Tabpanel tabPanel = new Tabpanel();
		Vlayout vbox = new Vlayout();
		vbox.setParent(tabPanel);
		ZKUpdateUtil.setWidth(vbox, "100%");
		ToolBarButton link = new ToolBarButton();

		Separator separator = new Separator();

		Div div = new Div();
		div.setParent(vbox);
		ZKUpdateUtil.setWidth(div, "100%");
		Label caption = new Label("Companies");
		caption.setStyle("font-weight: bold;");
		div.appendChild(caption);
		separator = new Separator();
		separator.setBar(true);
		separator.setParent(div);
		Vlayout content = new Vlayout();
		ZKUpdateUtil.setWidth(content, "100%");
		content.setParent(div);
		link = new ToolBarButton();
		link.setLabel("Adaxa");
		link.setHref("http://www.adaxa.com");
		link.setTarget("_blank");
		link.setParent(content);
		link = new ToolBarButton();
		link.setLabel("Posterita");
		link.setHref("http://www.posterita.org");
		link.setTarget("_blank");
		link.setParent(content);
		link = new ToolBarButton();
		link.setLabel("Sysnova");
		link.setHref("http://www.sysnova.com/");
		link.setTarget("_blank");
		link.setParent(content);
		link = new ToolBarButton();
		link.setLabel("Idalica");
		link.setHref("http://www.idalica.com/");
		link.setTarget("_blank");
		link.setParent(content);
		link = new ToolBarButton();
		link.setLabel("SmartJSP");
		link.setHref("http://www.smartjsp.com");
		link.setTarget("_blank");
		link.setParent(content);

		separator = new Separator();
		separator.setParent(vbox);

		div = new Div();
		div.setParent(vbox);
		ZKUpdateUtil.setWidth(div, "100%");
		caption = new Label("Contributors");
		caption.setStyle("font-weight: bold;");
		div.appendChild(caption);
		separator = new Separator();
		separator.setBar(true);
		separator.setParent(div);
		content = new Vlayout();
		ZKUpdateUtil.setWidth(content, "100%");
		content.setParent(div);
		link = new ToolBarButton();
		link.setLabel("Ashley G Ramdass");
		link.setHref("http://www.adempiere.com/wiki/index.php/User:Agramdass");
		link.setTarget("_blank");
		link.setParent(content);

		link = new ToolBarButton();
		link.setLabel("Low Heng Sin");
		link.setHref("http://www.adempiere.com/wiki/index.php/User:Hengsin");
		link.setTarget("_blank");
		link.setParent(content);

		link = new ToolBarButton();
		link.setLabel("Carlos Ruiz");
		link.setHref("http://www.adempiere.com/wiki/index.php/User:CarlosRuiz");
		link.setTarget("_blank");
		link.setParent(content);

		link = new ToolBarButton();
		link.setLabel("Teo Sarca");
		link.setHref("http://www.adempiere.com/wiki/index.php/User:Teo_sarca");
		link.setTarget("_blank");
		link.setParent(content);

		link = new ToolBarButton();
		link.setLabel("Trifon Trifonov");
		link.setHref("http://www.adempiere.com/wiki/index.php/User:Trifonnt");
		link.setTarget("_blank");
		link.setParent(content);
		

		return tabPanel;
	}

	private Tabpanel createAbout() {
		Tabpanel tabPanel = new Tabpanel();

		Vlayout vbox = new Vlayout();
		ZKUpdateUtil.setWidth(vbox, "100%");
		ZKUpdateUtil.setHeight(vbox, "100%");
		vbox.setParent(tabPanel);

		Image image = new Image(ThemeUtils.resolveImageURL(ThemeUtils.HEADER_LOGO_IMAGE));
		image.setParent(vbox);
		image.setSclass("about-logo");

		Text text = new Text(Adempiere.getSubtitle());
		text.setParent(vbox);
		Separator separator = new Separator();
		separator.setParent(vbox);

		text = new Text(Adempiere.getVersion());
		text.setParent(vbox);

		separator = new Separator();
		separator.setParent(vbox);
		ToolBarButton link = new ToolBarButton();
		link.setLabel("Ampere Project Site");
		link.setHref("https://gitlab.com/adaxa/ampere");
		link.setTarget("_blank");
		link.setParent(vbox);

		return tabPanel;
	}

	public void onEvent(Event event) throws Exception {
		
		if ( event.getName().equals(Events.ON_SELECT) )
		{
			if (event.getTarget() == logLevel)
    		{
    			int index = logLevel.getSelectedIndex();
    			if(index >= 0) 
    				setLogLevel(logLevel.getValue());
    		}
		}
		if (event.getTarget() == btnDownload)
			downloadLog();
		else if (event.getTarget() == btnClearLog)
			clearLog();
		else if (event.getTarget() == btnRefreshLog)
			refreshLog();
		else if (event.getTarget() == btnErrorEmail)
			cmd_errorEMail();
		else if (event instanceof SizeEvent)
			doResize((SizeEvent)event);
		else if (Events.ON_CLICK.equals(event.getName()))
			this.detach();
	}

	
	
	private void clearLog() {
		CLogErrorBuffer.get(true).resetBuffer();
		refreshLog();
		
	}

	private void refreshLog() {
		logBox.setValue(CLogErrorBuffer.get(true).getLogInfo(Env.getCtx()));
	}

	private void setLogLevel(String level) {
		CLogMgt.setLevel(level);
		log.log(Level.parse(level), "Set log level: " + level);
		refreshLog();
	}

	private void doResize(SizeEvent event) {
	}

	private void downloadLog() {
		String log = CLogErrorBuffer.get(true).getLogInfo(Env.getCtx());
		AMedia media = new AMedia("trace.log", null, "text/plain", log.getBytes());
		Filedownload.save(media);
		refreshLog();
	}

	/**
	 * 	EMail Errors
	 */
	private void cmd_errorEMail()
	{
		new WEMailDialog(this,
			"EMail Trace",
			MUser.get(Env.getCtx()),
			"",			//	to
			"Adempiere Trace Info",
			CLogErrorBuffer.get(true).getLogInfo(Env.getCtx()),
			null);

		refreshLog();

	}	//	cmd_errorEMail
}
