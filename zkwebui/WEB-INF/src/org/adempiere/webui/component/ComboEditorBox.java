/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.component;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.adempiere.webui.AdempiereIdGenerator;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;

/**
 * @author Low Heng Sin
 */
public class ComboEditorBox extends Div {
	/**
	 *
	 */
	private static final long serialVersionUID = -3152111756471436612L;
	protected PropertyChangeSupport m_propertyChangeListeners = new PropertyChangeSupport(
			this);
	protected Combobox txt;
	protected Button btn;

	public ComboEditorBox() {
		initComponents();
	}

	/**
	 * @param text
	 */
	public ComboEditorBox(String text) {
		initComponents();
		setText(text);
	}

	/**
	 * @param img
	 */
	public void setButtonImage(String img) {
		if (!img.startsWith("z-icon-"))
			img = "z-icon-" + img;
		btn.setIconSclass(img);
	}

	private void initComponents() {
		ZKUpdateUtil.setWidth(this, "100%");

		Hlayout tr = new Hlayout();
		appendChild(tr);

		txt = new Combobox();
		txt.setButtonVisible(false);
		txt.setAutodrop(true);
		txt.setInstant(false);
		txt.setSubmitByEnter(true);
		ZKUpdateUtil.setHflex(txt, "1");
		LayoutUtils.addSclass("comboeditor-input", txt);
		tr.appendChild(txt);

		btn = new Button();
		btn.setTabindex(100000);
		LayoutUtils.addSclass("comboeditor-button", btn);
		tr.appendChild(btn);
		
		LayoutUtils.addSclass("comboeditor", this);
	}

	/**
	 * @return textbox component
	 */
	public Combobox getTextbox() {
		return txt;
	}

	/**
	 * @param value
	 */
	public void setText(String value) {
		txt.setText(value);
	}

	/**
	 * @return text
	 */
	public String getText() {
		return txt.getText();
	}

	/**
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) {
		txt.setReadonly(!enabled);
		btn.setEnabled(enabled);
		btn.setVisible(enabled);
	}

	/**
	 * @return boolean
	 */
	public boolean isEnabled() {
		return !txt.isReadonly();
	}

	/**
	 * @param evtnm
	 * @param listener
	 */
	public boolean addEventListener(String evtnm, EventListener<?> listener) {
		if (Events.ON_CLICK.equals(evtnm)) {
			return btn.addEventListener(evtnm, listener);
		} else {
			return txt.addEventListener(evtnm, listener);
		}
	}

	/**
	 * @param l
	 */
	public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
		m_propertyChangeListeners.addPropertyChangeListener(l);
	}

	/**
	 * @param tooltiptext
	 */
	public void setToolTipText(String tooltiptext) {
		txt.setTooltiptext(tooltiptext);
	}
	
	/**
	 * @return Button
	 */
	public Button getButton() {
		return btn;
	}
	
	@Override
	public boolean setVisible(boolean visible)
	{
		getTextbox().setStyle(visible ? "display: inline;" : "display: none;");
		return super.setVisible(visible);
	}
	
	public void appendItem(String name, Object value) {
		ComboItem item = new ComboItem(name, value);
		String id = AdempiereIdGenerator.escapeId(name);
		if (getFellowIfAny(id) == null)
			item.setId(id);
		txt.getItems().add(item);
	}
}
