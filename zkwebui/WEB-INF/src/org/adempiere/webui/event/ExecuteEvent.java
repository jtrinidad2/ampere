package org.adempiere.webui.event;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

/**
 * 
 * @author jobriant
 *
 */
public class ExecuteEvent extends Event {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7867053177517684231L;
	
	public static final String ON_EXECUTE = "onExecute";

	public ExecuteEvent(Component target, Object data) {
		super(ON_EXECUTE, target, data);
	}

}
