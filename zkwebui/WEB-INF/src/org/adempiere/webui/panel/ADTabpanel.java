/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.CWindowToolbar;
import org.adempiere.webui.component.EditorBox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridPanel;
import org.adempiere.webui.component.Group;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.SimpleTreeModel;
import org.adempiere.webui.component.ToolBarButton;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.IZoomableEditor;
import org.adempiere.webui.editor.WButtonEditor;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WEditorPopupMenu;
import org.adempiere.webui.editor.WebEditorFactory;
import org.adempiere.webui.event.ContextMenuListener;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.GridTabDataBinder;
import org.adempiere.webui.util.TreeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.WAlertDialog;
import org.compiere.model.DataStatusEvent;
import org.compiere.model.DataStatusListener;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridWindow;
import org.compiere.model.Lookup;
import org.compiere.model.MLookup;
import org.compiere.model.MMemo;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTree;
import org.compiere.model.MTreeNode;
import org.compiere.model.MUIAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Evaluatee;
import org.compiere.util.Util;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Button;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Center;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Div;
import org.zkoss.zul.Panelchildren;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Space;
import org.zkoss.zul.Span;
import org.zkoss.zul.TreeModel;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

/**
 *
 * This class is based on org.compiere.grid.GridController written by Jorg Janke.
 * Changes have been brought for UI compatibility.
 *
 * @author Jorg Janke
 *
 * @author <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @date Feb 25, 2007
 * @version $Revision: 0.10 $
 *
 * @author Low Heng Sin
 */
public class ADTabpanel extends Div implements Evaluatee, EventListener<Event>,
DataStatusListener, IADTabpanel
{
	public static final String ON_DYNAMIC_DISPLAY_EVENT = "onDynamicDisplay";
	
	/**
	 * generated serial version ID
	 */
	private static final long serialVersionUID = 6945934489328360251L;

	private static final CLogger logger;

    static
    {
        logger = CLogger.getCLogger(ADTabpanel.class);
    }

    private GridTab           gridTab;

    @SuppressWarnings("unused")
	private GridWindow        gridWindow;

    private AbstractADWindowPanel      windowPanel;

    private int               windowNo;

    private Div              grid;
    private Div gridContainer = new Div();
    private int numCols = 6;
    
    private Map<Integer, Div> fieldGroupHeaderMap = new HashMap<Integer, Div>();
    private Map<Integer, List<Div>> fieldGroupMemberMap = new HashMap<Integer, List<Div>>();

    private ArrayList<WEditor> editors = new ArrayList<WEditor>();

    private ArrayList<Component> editorComps = new ArrayList<Component>();
    
    private ArrayList<WButtonEditor> toolbarButtonEditors = new ArrayList<WButtonEditor>();

    private ArrayList<ToolbarProcessButton> toolbarProcessButtons = new ArrayList<ToolbarProcessButton>();

    private boolean			  uiCreated = false;

    private GridPanel		  listPanel;

    private Map<String, List<Row>> fieldGroupContents = new HashMap<String, List<Row>>();

    private ArrayList<Row> rowList;
	
	List<Group> allCollapsibleGroups = new ArrayList<Group>();

	private Component formComponent = null;

	private ADTreePanel treePanel = null;

	private GridTabDataBinder dataBinder;

	private Map<Integer, Div> includedTab = new HashMap<Integer, Div>();
	private Map<Integer, Div> includedTabFooter = new HashMap<Integer, Div>();

	private List<EmbeddedPanel> includedPanel = new ArrayList<EmbeddedPanel>();
	
	private Map<Integer, List<Div>> gridRows = new HashMap<Integer, List<Div>>();

	private boolean active = false;
	
	private int tabNo;
	
	public ADTabpanel()
	{
        init();
    }

    private void init()
    {
        initComponents();
    }

    private void initComponents()
    {
    	LayoutUtils.addSclass("grid-layout", this);
        grid = new Div();
        //have problem moving the following out as css class
        ZKUpdateUtil.setWidth(grid, "100%");
        ZKUpdateUtil.setHeight(grid, "100%");
        grid.setStyle("margin:0; padding:0; position: absolute");

        listPanel = new GridPanel();
        listPanel.getListbox().addEventListener(Events.ON_DOUBLE_CLICK, this);
    }

    /**
     *
     * @param winPanel
     * @param windowNo
     * @param gridTab
     * @param gridWindow
     */
    public void init(AbstractADWindowPanel winPanel, int windowNo, GridTab gridTab,
            GridWindow gridWindow)
    {
        this.windowNo = windowNo;
        this.gridWindow = gridWindow;
        this.gridTab = gridTab;
        this.windowPanel = winPanel;
        gridTab.addDataStatusListener(this);
        this.dataBinder = new GridTabDataBinder(gridTab);

        this.getChildren().clear();

        int AD_Tree_ID = 0;
		if (gridTab.isTreeTab())
		{
			String keyColumnName = gridTab.getKeyColumnName();
			if (gridTab.getKeyColumnName().equals("C_ElementValue_ID")
					&& gridTab.getParentTab() != null
					&& !gridTab.getParentTab().get_ValueAsString("ElementType").equals("A")
					&& gridTab.getParentTab().getValue("AD_Tree_ID") != null)				
			{
				AD_Tree_ID= Integer.valueOf(gridTab.getParentTab().get_ValueAsString("AD_Tree_ID"));
			}
			else
				AD_Tree_ID = MTree.getDefaultAD_Tree_ID (
						Env.getAD_Client_ID(Env.getCtx()), keyColumnName);
		}
		if (gridTab.isTreeTab() && AD_Tree_ID != 0)
		{
			Borderlayout layout = new Borderlayout();
			layout.setParent(this);
			layout.setStyle("width: 100%; height: 100%; position: absolute;");

			treePanel = new ADTreePanel(windowNo, gridTab.getTabNo());
			West west = new West();
			west.appendChild(treePanel);
			ZKUpdateUtil.setWidth(west, "300px");
			west.setCollapsible(true);
			west.setSplittable(true);
			west.setAutoscroll(true);
			layout.appendChild(west);

			Center center = new Center();
			center.setStyle("height:100%;");
			ZKUpdateUtil.setVflex(center, "flex");
			center.appendChild(grid);
			layout.appendChild(center);

			formComponent = layout;
			treePanel.getTree().addEventListener(Events.ON_SELECT, this);
		}
		else
		{
			this.appendChild(grid);
			formComponent = grid;
		}
		this.appendChild(listPanel);
        listPanel.setVisible(false);
        listPanel.setWindowNo(windowNo);
        listPanel.setADWindowPanel(winPanel);

    }

	/**
     * Create UI components if not already created
     */
    public void createUI()
    {
    	if (uiCreated)
    	{
    		refreshTree();
    		return;
    	}

    	uiCreated = true;
    	
    	numCols = gridTab.getColumns();
    	if (numCols <= 0) {
    		numCols = 4;
    	}

    	// set size in percentage per column leaving a MARGIN on right
    	double equalWidth = 95.00d / numCols;
    	DecimalFormat decimalFormat = new DecimalFormat("0.00");
    	decimalFormat.setRoundingMode(RoundingMode.DOWN);
    	String columnWidth = decimalFormat.format(equalWidth);
    	
    	gridContainer.setSclass("grid-container");
    	gridContainer.setStyle("display: grid; grid-template-columns: repeat(" + numCols + ", " + columnWidth + "%);");
    	grid.appendChild(gridContainer);

        GridField fields[] = gridTab.getFields();
        Div gridItem = new Div();
        int actualxpos = 0;

        String currentFieldGroup = null;
        List<Div> gridRow = new ArrayList<Div>();
        List<Div> gridGroup = new ArrayList<Div>();
        int rowIndex = 1;
        int groupIndex = 0;
        for (int i = 0; i < fields.length; i++)
        {
            GridField field = fields[i];
            if (!field.isDisplayed())
        		continue;

        	//included tab
        	if (field.getIncluded_Tab_ID() > 0)
        	{
        		// fill the rest of previous row
        		if (numCols - actualxpos > 0) {
        			gridItem = createGridItem(createSpacer(), null, (numCols - actualxpos));
    				gridContainer.appendChild(gridItem);
    			}

        		Separator separator = new Separator();
				separator.setBar(true);
				gridItem = createGridItem(separator, null, numCols+1);
				gridContainer.appendChild(gridItem);

        		Div groupRow = new Div();
        		gridItem = createGridItem(groupRow, null, numCols+1);
        		gridContainer.appendChild(gridItem);
        		includedTab.put(field.getIncluded_Tab_ID(), groupRow);

        		Div groupFootRow = new Div();
        		gridItem = createGridItem(groupFootRow, null, numCols+1);
        		gridContainer.appendChild(gridItem);
        		includedTabFooter.put(field.getIncluded_Tab_ID(), groupFootRow);

        		for (EmbeddedPanel ep : includedPanel) {
        			if (ep.adTabId == field.getIncluded_Tab_ID()) {
        				ep.group = includedTab.get(ep.adTabId);
        				createEmbeddedPanelUI(ep);
        				break;
        			}
        		}

        		actualxpos = 0;
        		continue;
        	}

        	if (field.isToolbarButton()) {
        		WButtonEditor editor = (WButtonEditor) WebEditorFactory.getEditor(gridTab, field, false);

        		if (editor != null) {
        			if (windowPanel != null)
    					editor.addActionListener(windowPanel);
        			editor.setGridTab(this.getGridTab());
        			editor.setADTabpanel(this);
        			field.addPropertyChangeListener(editor);
        			editors.add(editor);
        			toolbarButtonEditors.add(editor);

        			continue;
        		}
        	}
        	
        	if (field.getXPosition() <= actualxpos) {
				// Fill right part of the row with spacers until number of columns
				if (numCols - actualxpos + 1 > 0) {
					gridItem = createGridItem(createSpacer(), (actualxpos+1), (numCols - actualxpos + 1));
					gridContainer.appendChild(gridItem);
					gridRow.add(gridItem);
					gridGroup.add(gridItem);
				}
				actualxpos = 0;
				gridRows.put(rowIndex++, gridRow);
				gridRow = new ArrayList<Div>();
            }
            	
        	// field group
        	String fieldGroup = field.getFieldGroup();
        	if (!Util.isEmpty(fieldGroup) && !fieldGroup.equals(currentFieldGroup)) // group changed
        	{
        		if (!gridGroup.isEmpty()) {
        			fieldGroupMemberMap.put(groupIndex, gridGroup);
        		}
        		gridGroup = new ArrayList<Div>();
        		groupIndex++;
    			currentFieldGroup = fieldGroup;

				rowList = new ArrayList<Row>();
				fieldGroupContents.put(fieldGroup, rowList);

				A groupLabel = new A(fieldGroup);
				if (field.getIsCollapsedByDefault()) {
					groupLabel.setAttribute("collapsed", true);
					groupLabel.setSclass("field-group-label-collapsed");
					groupLabel.setIconSclass("z-group-icon-close");
				} else {
					groupLabel.setAttribute("collapsed", false);
					groupLabel.setSclass("field-group-label");
					groupLabel.setIconSclass("z-group-icon-open");
				}
				Div groupHeader = createGridItem(groupLabel, null, numCols+1);
				groupHeader.setSclass("field-group-div grid-item");
				gridContainer.appendChild(groupHeader);
				fieldGroupHeaderMap.put(groupIndex, groupHeader);
				groupLabel.addEventListener(Events.ON_CLICK, this);
            }
        	
        	//normal field
			// Fill left part of the field
			if (field.getXPosition()-1 - actualxpos > 0) {
				gridItem = createGridItem(createSpacer(), (actualxpos+1), (field.getXPosition()-1 - actualxpos));
				gridContainer.appendChild(gridItem);
				gridRow.add(gridItem);
				gridGroup.add(gridItem);
			}
			boolean paintLabel = ! (field.getDisplayType() == DisplayType.Button || field.getDisplayType() == DisplayType.YesNo || field.isFieldOnly()); 
			if (field.isHeading())
				actualxpos = field.getXPosition();
			else
				actualxpos = field.getXPosition() + field.getColumnSpan()-1 + (paintLabel ? 1 : 0);

			if (! field.isHeading()) {
	            WEditor editor = WebEditorFactory.getEditor(gridTab, field, false);
	
	            if (editor != null) // Not heading
	            {
	                editor.setGridTab(this.getGridTab());
	            	field.addPropertyChangeListener(editor);
	                editors.add(editor);
	                editorComps.add(editor.getComponent());
	                if (paintLabel) {
						Cell cell = new Cell();
						cell.setSclass("field-label");
						cell.setAlign("right");
						
						WEditorPopupMenu popupMenu = editor.getPopupMenu();
						if (popupMenu != null && field.isHeading() && popupMenu.isZoomEnabled()
								&& (editor instanceof IZoomableEditor) && !(editor instanceof WButtonEditor))
						{
							Button button = new Button();
							button.addEventListener(Events.ON_CLICK, new ZoomListener((IZoomableEditor) editor));
							button.setLabel(editor.getLabel().getValue());
							button.setIconSclass("z-icon-Find");
							cell.appendChild(button);
							ZKUpdateUtil.setWidth(button, "99%");
							String zclass = ((HtmlBasedComponent) cell).getZclass();
							if (zclass == null)
							{
								((HtmlBasedComponent) cell).setZclass("form-button");
							}
							else if (!zclass.contains("form-button"))
							{
								((HtmlBasedComponent) cell).setZclass("form-button " + zclass);
							}
						}
						else
						{
							Span span = new Span();
							Label label;
							if (field.isHeading() && Util.isEmpty(editor.getLabel().getValue(), true)
									&& !(editor instanceof WButtonEditor))
								// yes-no reference
								label = new Label(field.getHeader());
							else
								label = editor.getLabel();
							span.appendChild(label);
							if (label.getDecorator() != null)
								span.appendChild(label.getDecorator());
							cell.appendChild(span);
						}
						gridItem = createGridItem(cell, null, null, "right");
	                    gridContainer.appendChild(gridItem);
	                    gridRow.add(gridItem);
	                    gridGroup.add(gridItem);
	                }
	                gridItem = createGridItem(editor.getComponent(), null, field.getColumnSpan());
	                gridContainer.appendChild(gridItem);
	                gridRow.add(gridItem);
	                gridGroup.add(gridItem);
	
	                if (editor instanceof WButtonEditor)
	                {
	                	if (windowPanel != null)
	                		((WButtonEditor)editor).addActionListener(windowPanel);
	                }
	                else
	                {
	                	editor.addValueChangeListener(dataBinder);
	                }
	
	                //streach component to fill grid cell
	                editor.fillHorizontal();
	
	                //setup editor context menu
	                WEditorPopupMenu popupMenu = editor.getPopupMenu();
	                if (popupMenu != null)
	                {
	                	popupMenu.addMenuListener((ContextMenuListener)editor);
	                    this.appendChild(popupMenu);
	                    if (!field.isFieldOnly())
	                    {
	                    	Label label = editor.getLabel();
	                        if (popupMenu.isZoomEnabled() && editor instanceof IZoomableEditor)
	                        {
	                        	label.setZoomable(true);
	                        	label.setStyle("cursor: pointer; text-decoration: underline;");
	                        	label.addEventListener(Events.ON_CLICK, new ZoomListener((IZoomableEditor) editor));
	                        }
	                        label.setContext(popupMenu);
	                    }
	                }
	            }
			}
			else 
			{
				//display just a label if we are "heading only"
				Label label = new Label(field.getHeader());
				Div div = new Div();
				div.setSclass("form-label-heading");
				gridItem = createGridItem(createSpacer());
				gridContainer.appendChild(gridItem);
				gridRow.add(gridItem);
				gridGroup.add(gridItem);
				
				div.appendChild(label);
				gridItem = createGridItem(div);
				gridContainer.appendChild(gridItem);
				gridRow.add(gridItem);
				gridGroup.add(gridItem);
			}
        }
        
        if (numCols - actualxpos + 1 > 0) {
        	gridItem = createGridItem(createSpacer(), null, (numCols - actualxpos + 1));
			gridContainer.appendChild(gridItem);
			gridRow.add(gridItem);
			gridGroup.add(gridItem);
        }
        gridRows.put(rowIndex++, gridRow);
        
        if (!gridGroup.isEmpty()) {
			fieldGroupMemberMap.put(groupIndex, gridGroup);
		}
        
        // close collapse by default
        for (Integer i : fieldGroupHeaderMap.keySet()) {
        	Div groupDiv = fieldGroupHeaderMap.get(i);
        	A groupLabel = (A) groupDiv.getFirstChild();
        	Boolean collapsed = (Boolean) groupLabel.getAttribute("collapsed");
        	if (collapsed) {
        		List<Div> gridList = fieldGroupMemberMap.get(i);
				if (gridList != null && !gridList.isEmpty()) {
					for (Div div : gridList) {
						div.setVisible(!collapsed);
						div.setAttribute("collapsed", collapsed);
					}
				}
        	}
        }
        
        loadToolbarButtons();

        refreshTree();

        if (!gridTab.isSingleRow() && !isGridView())
        	switchRowPresentation();
    }
    
    private void loadToolbarButtons() {
		//get extra toolbar process buttons
        MUIAction[] mUIActions = MUIAction.getOfTab(gridTab.getAD_Tab_ID(), null);
        for(MUIAction mUIAction : mUIActions) {
        	ToolbarProcessButton toolbarProcessButton = new ToolbarProcessButton(mUIAction, this, windowPanel, windowNo);
        	toolbarProcessButtons.add(toolbarProcessButton);
        }
	}

	public void refreshTree() {
		//create tree
        if (gridTab.isTreeTab() && treePanel != null) {
        	int AD_Tree_ID= 0;
			if (gridTab.getKeyColumnName().equals("C_ElementValue_ID")
					&& gridTab.getParentTab() != null
					&& !gridTab.getParentTab().get_ValueAsString("ElementType").equals("A")
					&& gridTab.getParentTab().getValue("AD_Tree_ID") != null)				
			{
				AD_Tree_ID= Integer.valueOf(gridTab.getParentTab().get_ValueAsString("AD_Tree_ID"));
			}
			else
				AD_Tree_ID = MTree.getDefaultAD_Tree_ID (
	        			Env.getAD_Client_ID(Env.getCtx()), gridTab.getKeyColumnName());
			if (treePanel.getTree() != null)
				
			{
				while (treePanel.getTree().getItemCount() > 0) 
				{
					treePanel.getTree().removeChild(treePanel.getTree().getFirstChild());
				}
				// Fix error 'Only one treecols is allowed' while open
				// Organization window in System Admin role
				if (treePanel.getTree().getTreecols() != null)
				{
					treePanel.getTree().removeChild(treePanel.getTree().getTreecols());
				}
			}
			treePanel.initTree(AD_Tree_ID, windowNo);
        }
	}

	private Component createSpacer() {
		return new Space();
	}
	
	private Div createGridItem(Component comp) {
		return createGridItem(comp, null, null);
	}
	
	private Div createGridItem(Component comp, Integer xpos, Integer span) {
		return createGridItem(comp, xpos, span, null);
	}
	
	private Div createGridItem(Component comp, Integer xpos, Integer span, String textAlign) {
		Div div = new Div();
		div.setSclass("grid-item");
		StringBuilder style = new StringBuilder();
		if (xpos != null)
			style.append("grid-column-start: "+ xpos +";");
		if (span != null)
			style.append("grid-column-end: span "+ span +";");
		if (!Util.isEmpty(textAlign))
			style.append("text-align: "+ textAlign +";");
		if (!Util.isEmpty(style.toString()))
			div.setStyle(style.toString());
		div.appendChild(comp);
		return div;
	}

	/**
	 * Validate display properties of fields of current row
	 * @param col
	 */
    public void dynamicDisplay (int col)
    {
        if (!gridTab.isOpen())
        {
            return;
        }

        for (WEditor comp : editors)
        {
        	comp.setMandatoryLabels();
        }
        
        //  Selective
        if (col > 0)
        {
            GridField changedField = gridTab.getField(col);
            String columnName = changedField.getColumnName();
            ArrayList<?> dependants = gridTab.getDependantFields(columnName);
            logger.config("(" + gridTab.toString() + ") "
                + columnName + " - Dependents=" + dependants.size());

			if (changedField.getNext_Column_ID() > 0)
			{
				executeNextAction(changedField);
			}

            if (dependants.size() == 0 && changedField.getCallout().length() > 0)
            {
                return;
            }
        }

        boolean noData = gridTab.getRowCount() == 0;
        logger.config(gridTab.toString() + " - Rows=" + gridTab.getRowCount());
        for (WEditor comp : editors)
        {
            GridField mField = comp.getGridField();
            if (mField != null && mField.getIncluded_Tab_ID() <= 0)
            {
            	
                if (mField.isDisplayed(true))       //  check context
                {
                    if (!comp.isVisible())
                    {
                        comp.setVisible(true);      //  visibility
                    }
                    if (noData)
                    {
                        comp.setReadWrite(false);
                    }
                    else
                    {
                    	comp.dynamicDisplay();
                        boolean rw = mField.isEditable(true);   //  r/w - check Context
                        comp.setReadWrite(rw);
                        comp.setMandatory(mField.isMandatory(true));    //  check context
                    }
                }
                else if (comp.isVisible())
                {
                    comp.setVisible(false);
                }
            }
        }   //  all components

        //hide row if all editor within the row is invisible
        for (Integer index : gridRows.keySet()) {
			List<Div> rowList = gridRows.get(index);
			boolean visible = false;
			boolean editorRow = false;
			for (Div div : rowList) {
				if (div.getAttribute("collapsed") != null && (Boolean)div.getAttribute("collapsed")) {
					break;
				}
				
				Component component = div.getFirstChild();

				if (component == null)
					continue;

				if (editorComps.contains(component))
				{
					editorRow = true;
					if (component.isVisible())
					{	
						visible = true;
						break;
					}
				}
			}
			if (editorRow) {
				for (Div d : rowList) {
					d.setVisible(visible);
				}
			}
		}

        //hide fieldgroup if all editor row within the fieldgroup is invisible
        for (Integer groupIndex : fieldGroupHeaderMap.keySet()) {
        	Div groupHeader = fieldGroupHeaderMap.get(groupIndex);
        	boolean visible = false;
        	List<Div> gridGroup = fieldGroupMemberMap.get(groupIndex);
        	for (Div div : gridGroup) {
				Component component = div.getFirstChild();

				if (component == null)
					continue;

				if (editorComps.contains(component))
				{
					if (component.isVisible())
					{	
						visible = true;
						break;
					}
				}
			}
        	groupHeader.setVisible(visible);
        }

		if (listPanel.isVisible())
		{
			listPanel.dynamicDisplay(col);
		}
		
		for (ToolbarProcessButton btn : toolbarProcessButtons) {
        	btn.dynamicDisplay();
        }

		Events.sendEvent(this, new Event(ON_DYNAMIC_DISPLAY_EVENT, this));
        logger.config(gridTab.toString() + " - fini - " + (col<=0 ? "complete" : "seletive"));
    }   //  dynamicDisplay

	/**
	 * Execute next action
	 * 
	 * @param currField
	 */
	private void executeNextAction(GridField currField)
	{
		switch (currField.getDisplayType())
		{
			case DisplayType.DigiSignature:
				for (WEditor editor : editors)
				{
					if (editor.getGridField() != null
							&& editor.getGridField().getAD_Column_ID() == currField.getNext_Column_ID())
					{
						fireNextActionEvent(editor);
						break;
					}
				}
				break;
			default:
				break;
		}
	} // executeNextAction

	/**
	 * Execute event for perform action on next column
	 * 
	 * @param editor
	 */
	private void fireNextActionEvent(WEditor editor)
	{
		if (editor.getField() != null)
		{
			switch (editor.getField().getDisplayType())
			{
				case DisplayType.Gallery:
					Events.postEvent(Events.ON_CLICK, editor.getComponent(), null);
					break;
				default:
					break;
			}
		}
	} // fireNextActionEvent

	/**
     * @return String
     */
    public String getDisplayLogic()
    {
        return gridTab.getDisplayLogic();
    }

    /**
     * @return String
     */
    public String getTitle()
    {
        return gridTab.getName();
    } // getTitle

    /**
     * @param variableName
     */
    public String get_ValueAsString(String variableName)
    {
        return Env.getContext(Env.getCtx(), windowNo, variableName);
    } // get_ValueAsString

    /**
     * @return The tab level of this Tabpanel
     */
    public int getTabLevel()
    {
        return gridTab.getTabLevel();
    }

    /**
     * Is panel need refresh
     * @return boolean
     */
    public boolean isCurrent()
    {
        return gridTab != null ? gridTab.isCurrent() : false;
    }

    /**
     *
     * @return windowNo
     */
    public int getWindowNo()
    {
        return windowNo;
    }

    /**
     * Retrieve from db
     */
    public void query()
    {
    	boolean open = gridTab.isOpen();
        gridTab.query(false);
        if (listPanel.isVisible() && !open)
        	gridTab.getTableModel().fireTableDataChanged();
    }

    /**
     * Retrieve from db
     * @param onlyCurrentRows
     * @param onlyCurrentDays
     * @param maxRows
     */
    public void query (boolean onlyCurrentRows, int onlyCurrentDays, int maxRows)
    {
    	boolean open = gridTab.isOpen();
        gridTab.query(onlyCurrentRows, onlyCurrentDays, maxRows);
        if (listPanel.isVisible() && !open)
        	gridTab.getTableModel().fireTableDataChanged();
    }

    /**
     * @return GridTab
     */
    public GridTab getGridTab()
    {
        return gridTab;
    }

    public void setGridTab(GridTab gridTab)
	{
		this.gridTab = gridTab;
	}

	/**
     * Refresh current row
     */
    public void refresh()
    {
        gridTab.dataRefresh();
    }

	public GridPanel getListPanel() {
		return listPanel;
	}

	/**
	 * Activate/deactivate panel
	 * 
	 * @param activate
	 */
    public void activate(boolean activate)
    {
		if (getGrid() != null && activate) {
			Div gridCurrent = getGrid();
			((HtmlBasedComponent) gridCurrent).setStyle("overflow-y: auto; ");
			ZKUpdateUtil.setWidth(gridCurrent, "100%");
			ZKUpdateUtil.setHeight(gridCurrent, "100%");
		} else if (getGrid() != null && !activate) {
			Div gridtPrevious = getGrid();
			((HtmlBasedComponent) gridtPrevious).setStyle("border:none; overflow-y: auto;");
			ZKUpdateUtil.setWidth(gridtPrevious, "100%");
			ZKUpdateUtil.setHeight(gridtPrevious, "100%");
		}
		if (getListPanel() != null && activate) {
			GridPanel gridPanel = getListPanel();
			((HtmlBasedComponent) gridPanel).setStyle("overflow-y: auto; ");
			ZKUpdateUtil.setWidth(gridPanel, "100%");
			ZKUpdateUtil.setHeight(gridPanel, "100%");
		} else if (getListPanel() != null && !activate) {
			GridPanel gridPanel = getListPanel();
			((HtmlBasedComponent) gridPanel).setStyle("border:none; overflow-y: auto;");
			ZKUpdateUtil.setWidth(gridPanel, "100%");
			ZKUpdateUtil.setHeight(gridPanel, "100%");
		}

		if (listPanel != null)
			listPanel.addOrRemoveKeyEventListener(activate);

    	active = activate;
        if (listPanel.isVisible()) {
        	if (activate)
        		listPanel.activate(gridTab);
        	else
        		listPanel.deactivate();
        } else {
        	if (activate) {
        		formComponent.setVisible(activate);
        		setFocusToField();
        	}
        }

        //activate embedded panel
        for(EmbeddedPanel ep : includedPanel)
        {
        	activateChild(activate, ep);
        }
    }

	private void activateChild(boolean activate, EmbeddedPanel panel) {
		if (activate)
		{
			panel.windowPanel.getADTab().evaluate(null);
			panel.windowPanel.getADTab().setSelectedIndex(0);
			panel.tabPanel.query(false, 0, 0);
		}
		panel.tabPanel.activate(activate);
	}

	/**
	 * set focus to first active editor
	 */
	private void setFocusToField() {
		WEditor toFocus = null;
		for (WEditor editor : editors) {
			if (editor.isHasFocus() && editor.isVisible() && editor.getComponent().getParent() != null) {
				toFocus = editor;
				break;
			}

			if (toFocus == null) {
				if (editor.isVisible() && editor.isReadWrite() && editor.getComponent().getParent() != null) {
					toFocus = editor;
				}
			}
		}
		if (toFocus != null) {
			Component c = toFocus.getComponent();
			if (c instanceof EditorBox) {
				c = ((EditorBox)c).getTextbox();
			}
			Clients.response(new AuFocus(c));
		}
	}

    /**
     * @param event
     * @see EventListener#onEvent(Event)
     */
    public void onEvent(Event event)
    {
    	Component comp = event.getTarget();
        String eventName = event.getName();
        
    	if (event.getTarget() == listPanel.getListbox())
    	{
    		this.switchRowPresentation();
    	}
    	else if (Events.ON_CLICK.equals(eventName)) {
    		if (comp instanceof A && comp.getAttribute("collapsed") != null) {
    			A groupLabel = (A) comp;
    			Div groupHeader = (Div) groupLabel.getParent();
    			Integer groupIndex = fieldGroupHeaderMap.entrySet().stream()
    					.filter(e -> e.getValue().equals(groupHeader))
    					.map(Map.Entry :: getKey)
    					.findFirst().get();
    			if (groupIndex != null) {
    				List<Div> gridGroup = fieldGroupMemberMap.get(groupIndex);
    				if (gridGroup != null && !gridGroup.isEmpty()) {
    					Boolean collapsed = (Boolean) comp.getAttribute("collapsed");
    					for (Div div : gridGroup) {
    						div.setVisible(collapsed);
    						div.setAttribute("collapsed", !collapsed);
    					}
    					groupLabel.setAttribute("collapsed", !collapsed);
    					collapsed = !collapsed;
    					groupLabel.setSclass(collapsed ? "field-group-label-collapsed" : "field-group-label");
    					groupLabel.setIconSclass(collapsed ? "z-group-icon-close" : "z-group-icon-open");
    					dynamicDisplay(0);
    				}
    			}
    		}
    	}
    	else if (event.getTarget() == treePanel.getTree()) {
    		Treeitem item =  treePanel.getTree().getSelectedItem();
    		navigateTo((DefaultTreeNode<?>)item.getValue());
    	}
    }

	private List<HorizontalEmbeddedPanel> horizontalIncludedPanel = new ArrayList<HorizontalEmbeddedPanel>();

	class HorizontalEmbeddedPanel {
		org.zkoss.zul.Div divComponent;
		org.zkoss.zul.Row toolbarRow;
		Grid embeddedGrid;
		GridWindow gridWindow;
		int tabIndex;
		ADWindowPanel windowPanel;
		IADTabpanel tabPanel;
		int adTabId;
		Panelchildren panelChildren;
	}

	public int doAutoSize() {
		int size = 0;
		for (EmbeddedPanel panel : includedPanel) {
			size += includedAutoResize(panel);
		}

		for (HorizontalEmbeddedPanel panel : horizontalIncludedPanel) {
			size += includedAutoResize(panel);
		}
		return size;
	}

	private int getComponentSize(Row row) {

		int addSize = 0;

		for (Object o : ((Row) row).getChildren()) {

			if (o instanceof org.zkoss.zul.Borderlayout) {
				return 0;
			}

			if (o instanceof org.zkoss.zk.ui.HtmlBasedComponent) {

				String height = ((org.zkoss.zk.ui.HtmlBasedComponent) o).getHeight();

				if (height == null) {
					height = "30";
				}

				if (!height.contains("%")) {

					int size = Integer.parseInt(height.replace("px", "")) + 6;

					if (size > addSize)
						addSize = size;
				}

			}
		}

		return addSize;
	}

	@Override
	public List<EmbeddedPanel> getIncludedPanel() {
		return includedPanel;
	}

	public int includedAutoResize(EmbeddedPanel embeddedpanel) {
		if (embeddedpanel.windowPanel != null) {
			if (embeddedpanel.windowPanel.isEmbedded()) {
				Borderlayout window = embeddedpanel.windowPanel.getComponent();
				try {

					int size = 0;
					int addSize = 0;
					if (!embeddedpanel.tabPanel.getGridTab().isSingleRow()) {
						size = MSysConfig.getIntValue("TAB_INCLUDING_HEIGHT", 400);
					} else {

//						for (Object o : embeddedpanel.tabPanel.getGrid().getRows().getChildren()) {
//							if (o instanceof Row) {
//								if (((Row) o).isVisible()) {
//									size += getComponentSize((Row) o);
//								}
//							} else if (o instanceof org.zkoss.zul.Group) {
//								size += 20; // Group
//							}
//
//						}
//						size += 25; // 25 = statusbar
//						size += addSize;
//						List<EmbeddedPanel> included = embeddedpanel.tabPanel.getIncludedPanel();
//						if (included.size() > 0) {
//							for (EmbeddedPanel panel : included) {
//								size += includedAutoResize(panel);
//							}
//						}
					}

					ZKUpdateUtil.setWindowHeightX(window, size);
					window.resize();
					return size;
				} catch (Exception e) {
					e.printStackTrace();
				}

				return 0;
			}

		}
		return 0;
	}

	public int includedAutoResize(HorizontalEmbeddedPanel embeddedPanel) {
		if (embeddedPanel.windowPanel != null) {
			if (embeddedPanel.windowPanel.isEmbedded()) {
				Borderlayout window = embeddedPanel.windowPanel.getComponent();
				try {
					int size = 0;
					int addSize = 0;
					if (!embeddedPanel.tabPanel.getGridTab().isSingleRow()) {
						size = MSysConfig.getIntValue("TAB_INCLUDING_HEIGHT", 400);
					} else {
//						for (Object o : embeddedPanel.tabPanel.getGrid().getRows().getChildren()) {
//							if (o instanceof Row) {
//								if (((Row) o).isVisible()) {
//									size += getComponentSize((Row) o);
//								}
//							} else if (o instanceof org.zkoss.zul.Div) {
//								size += 20; // Group
//							}
//
//						}
//						size += 25; // 25 = statusbar
//
//						size += addSize;
//						List<HorizontalEmbeddedPanel> included = embeddedPanel.tabPanel.getHorizontalIncludedPanel();
//						if (included.size() > 0) {
//							for (HorizontalEmbeddedPanel panel : included) {
//								size += includedAutoResize(panel);
//							}
//						}
					}

					ZKUpdateUtil.setWindowHeightX(window, size);
					window.resize();
					return size;
				} catch (Exception e) {
					e.printStackTrace();
				}

				return 0;
			}

		}
		return 0;
	}

	public void autoResize() {
		if (windowPanel != null) {
			if (windowPanel.isEmbedded()) {
				Borderlayout window = ((ADWindowPanel) windowPanel).getComponent();

				if (isGridView()) {
					int size = MSysConfig.getIntValue("TAB_INCLUDING_HEIGHT", 400);
					ZKUpdateUtil.setWindowHeightX(window, size);
					listPanel.resize();
					window.resize();
				} else {
					try {
//						int addSize = 0;
//						int size = 0;
//						if (grid.getRows() != null) {
//							for (Object o : grid.getRows().getChildren()) {
//								if (o instanceof Row) {
//									if (((Row) o).isVisible()) {
//										size += getComponentSize((Row) o);
//									}
//								} else if (o instanceof org.zkoss.zul.Group) {
//									size += 20;
//								}
//							}
//						}
//
//						size += 25; // 25 = statusbar
//						size += addSize;
//						size += doAutoSize();
//						ZKUpdateUtil.setWindowHeightX(window, size);
//						window.resize();
					} catch (Exception e) {
						e.printStackTrace();
						// nothing to do, just ignore
						ZKUpdateUtil.setWindowHeightX(window, 61);
						window.resize();
					}

				}

			}
		}
	}

    private void navigateTo(DefaultTreeNode<?> value) {
    	MTreeNode treeNode = (MTreeNode) value.getData();
    	//  We Have a TreeNode
		int nodeID = treeNode.getNode_ID();
		//  root of tree selected - ignore
		//if (nodeID == 0)
			//return;

		//  Search all rows for mode id
		int size = gridTab.getRowCount();
		int row = -1;
		for (int i = 0; i < size; i++)
		{
			if (gridTab.getKeyID(i) == nodeID)
			{
				row = i;
				break;
			}
		}
		if (row == -1)
		{
			if (nodeID > 0)
				logger.log(Level.WARNING, "Tab does not have ID with Node_ID=" + nodeID);
			return;
		}

		//  Navigate to node row
		gridTab.navigate(row);
	}

    /**
     * @param e
     * @see DataStatusListener#dataStatusChanged(DataStatusEvent)
     */
	public void dataStatusChanged(DataStatusEvent e)
    {
    	//ignore background event
    	if (Executions.getCurrent() == null) return;

        int col = e.getChangedColumn();
        logger.config("(" + gridTab + ") Col=" + col + ": " + e.toString());

        //  Process Callout
        GridField mField = gridTab.getField(col);
        if (mField != null
            && (mField.getCallout().length() > 0 || gridTab.hasDependants(mField.getColumnName())))
        {
            String msg = gridTab.processFieldChange(mField);     //  Dependencies & Callout
            if (msg.length() > 0)
            {
                FDialog.error(windowNo, this, msg);
            }

            // Refresh the list on dependant fields
    		ArrayList<GridField> list = gridTab.getDependantFields(mField.getColumnName());
    		for (int i = 0; i < list.size(); i++)
    		{
    			GridField dependentField = (GridField)list.get(i);
    		//	log.trace(log.l5_DData, "Dependent Field", dependentField==null ? "null" : dependentField.getColumnName());
    			//  if the field has a lookup
    			if (dependentField != null && dependentField.getLookup() instanceof MLookup)
    			{
    				MLookup mLookup = (MLookup)dependentField.getLookup();
    				//  if the lookup is dynamic (i.e. contains this columnName as variable)
    				if (mLookup.getValidation().indexOf("@"+mField.getColumnName()+"@") != -1)
    				{
    					mLookup.refresh();
    				}
    			}
    		}   //  for all dependent fields

        }
        //if (col >= 0)
        if (!uiCreated)
        	createUI();
        
        if ( mField != null && mField.isLookup() )
		{
			Lookup lookup = (Lookup) mField.getLookup();
			if (lookup != null  && lookup instanceof MLookup )
			{
				MLookup mlookup = (MLookup) lookup;
				Object value = mField.getValue();
				if ( mlookup.isAlert() && value != null && value instanceof Integer )
				{
					String alert = MMemo.getAlerts(Env.getCtx(), mlookup.getTableName(), (Integer) value);
					if ( !Util.isEmpty(alert) )
					{
						WAlertDialog dialog = new WAlertDialog(alert);
						dialog.setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
						SessionManager.getAppDesktop().showWindow(dialog);
						
					}
				}
			}
		}
        
        dynamicDisplay(col);

        //sync tree
        if (treePanel != null) {
        	if ("Deleted".equalsIgnoreCase(e.getAD_Message()))
        		if (e.Record_ID != null
        				&& e.Record_ID instanceof Integer
        				&& ((Integer)e.Record_ID != gridTab.getRecord_ID()))
        			deleteNode((Integer)e.Record_ID);
        		else
        			setSelectedNode(gridTab.getRecord_ID());
        	else
        		setSelectedNode(gridTab.getRecord_ID());
        }

        if (listPanel.isVisible()) {
        	listPanel.updateListIndex();
        	listPanel.dynamicDisplay(col);
        }

		if (!includedPanel.isEmpty() && e.getChangedColumn() == -1)
		{
			for (EmbeddedPanel panel : includedPanel)
			{
				if (!panel.windowPanel.toolbar.isSaveEnable())
					panel.tabPanel.query(false, 0, 0);
			}
		}

    }

    private void deleteNode(int recordId) {
		if (recordId <= 0) return;

		SimpleTreeModel model = (SimpleTreeModel) ((TreeModel<?>)treePanel.getTree().getModel());

		if (treePanel.getTree().getSelectedItem() != null) {
			DefaultTreeNode<Object> treeNode = treePanel.getTree().getSelectedItem().getValue();
			MTreeNode data = (MTreeNode) treeNode.getData();
			if (data.getNode_ID() == recordId) {
				model.removeNode(treeNode);
				return;
			}
		}

		DefaultTreeNode<Object> treeNode = model.find(null, recordId);
		if (treeNode != null) {
			model.removeNode(treeNode);
		}
	}

	private void addNewNode() {
    	if (gridTab.getRecord_ID() > 0) {
	    	String name = (String)gridTab.getValue("Name");
			String description = (String)gridTab.getValue("Description");
			boolean summary = gridTab.getValueAsBoolean("IsSummary");
			String imageIndicator = (String)gridTab.getValue("Action");  //  Menu - Action
			//
			SimpleTreeModel model = (SimpleTreeModel) ((TreeModel<?>) treePanel.getTree().getModel());
			DefaultTreeNode<Object> treeNode = model.getRoot();
			MTreeNode root = (MTreeNode) treeNode.getData();
			MTreeNode node = new MTreeNode (gridTab.getRecord_ID(), 0, name, description,
					root.getNode_ID(), summary, imageIndicator, false, null);
			DefaultTreeNode<Object> newNode = new DefaultTreeNode<Object>(node, new ArrayList<>());
			model.addNode(newNode);
			int[] path = model.getPath(newNode);
			Treeitem ti = treePanel.getTree().renderItemByPath(path);
			treePanel.getTree().setSelectedItem(ti);
    	}
	}

	private void setSelectedNode(int recordId) {
		if (recordId <= 0) return;

		if (TreeUtils.isOnInitRenderPosted(treePanel.getTree()) || treePanel.getTree().getTreechildren() == null
				|| treePanel.getTree().getTreechildren().getItemCount() == 0) {
			treePanel.getTree().onInitRender();
		}

		if (treePanel.getTree().getSelectedItem() != null) {
			DefaultTreeNode<Object> treeNode = treePanel.getTree().getSelectedItem().getValue();
			MTreeNode data = (MTreeNode) treeNode.getData();
			if (data.getNode_ID() == recordId) return;
		}

		SimpleTreeModel model = (SimpleTreeModel) ((TreeModel<?>) treePanel.getTree().getModel());
		DefaultTreeNode<Object> treeNode = model.find(null, recordId);
		if (treeNode != null) {
			int[] path = model.getPath(treeNode);
			Treeitem ti = treePanel.getTree().renderItemByPath(path);
			treePanel.getTree().setSelectedItem(ti);
		} else {
			addNewNode();
		}
	}

	/**
	 * Toggle between form and grid view
	 */
	public void switchRowPresentation() {
		if (formComponent.isVisible()) {
			formComponent.setVisible(false);
			//de-activate embedded panel
	        for(EmbeddedPanel ep : includedPanel)
	        {
	        	activateChild(false, ep);
	        }
		} else {
			formComponent.setVisible(true);
			//activate embedded panel
	        for(EmbeddedPanel ep : includedPanel)
	        {
	        	activateChild(true, ep);
	        }
		}
		listPanel.setVisible(!formComponent.isVisible());
		if (listPanel.isVisible()) {
			listPanel.refresh(gridTab);
			listPanel.scrollToCurrentRow();
			listPanel.invalidate();
		} else {
			listPanel.deactivate();
		}
		gridTab.setIsGridView(listPanel.isVisible());
	}

	public class ZoomListener implements EventListener<Event> {

		private IZoomableEditor searchEditor;

		public ZoomListener(IZoomableEditor editor) {
			searchEditor = editor;
		}

		public void onEvent(Event event) throws Exception {
			if (Events.ON_CLICK.equals(event.getName())) {
				searchEditor.actionZoom();
			}

		}

	}

	/**
	 * Embed detail tab
	 * @param ctx
	 * @param windowNo
	 * @param gridWindow
	 * @param adTabId
	 * @param tabIndex
	 * @param tabPanel
	 */
	public void embed(Properties ctx, int windowNo, GridWindow gridWindow,
			int adTabId, int tabIndex, IADTabpanel tabPanel) {
		EmbeddedPanel ep = new EmbeddedPanel();
		ep.tabPanel = tabPanel;
		ep.adTabId = adTabId;
		ep.tabIndex = tabIndex;
		ep.gridWindow = gridWindow;
		includedPanel.add(ep);
		Div group = includedTab.get(adTabId);
		ep.group = group;
		if (tabPanel instanceof ADTabpanel) {
			ADTabpanel atp = (ADTabpanel) tabPanel;
			atp.listPanel.setPageSize(MSysConfig.getIntValue("ZK_PAGING_SIZE", 100));
		}
		ADWindowPanel panel = new ADWindowPanel(ctx, windowNo, gridWindow, tabIndex, tabPanel);
		ep.windowPanel = panel;

		if (group != null) {
			createEmbeddedPanelUI(ep);
			if (active)
				activateChild(true, ep);
		}
	}

	class EmbeddedPanel {
		Div group;
		GridWindow gridWindow;
		int tabIndex;
		ADWindowPanel windowPanel;
		IADTabpanel tabPanel;
		int adTabId;
	}

	/**
	 * @see IADTabpanel#afterSave(boolean)
	 */
	public void afterSave(boolean onSaveEvent)
	{
		if (!includedPanel.isEmpty())
		{
			for (EmbeddedPanel panel : includedPanel)
			{
				if (!panel.windowPanel.toolbar.isSaveEnable())
					panel.tabPanel.query(false, 0, 0);
			}
		}
	}

	private void createEmbeddedPanelUI(EmbeddedPanel ep) {
		ep.windowPanel.createPart(gridContainer);
		ep.windowPanel.getComponent().setStyle("position: relative; grid-column-end: span "+ (numCols+1) +";");
		ep.windowPanel.getComponent().setSclass("grid-item");
		ZKUpdateUtil.setWidth(ep.windowPanel.getComponent(), "100%");
		ZKUpdateUtil.setHeight(ep.windowPanel.getComponent(), "350px");

		Cell label = new Cell();
		label.appendChild(new Label(ep.gridWindow.getTab(ep.tabIndex).getName()));
		label.setColspan(1);
		label.setSclass("z-group-inner");
		label.setAlign("left");
		ep.group.appendChild(label);

		CWindowToolbar cwtoolbar = ep.windowPanel.getToolbar();
		cwtoolbar.setAlign("start");

		String sql = "SELECT componentname FROM AD_UIAction WHERE IsActive = 'N' AND AD_Tab_ID = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, ep.adTabId);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				ToolBarButton tbBtn = cwtoolbar.getButton(rs.getString(1));
				if (tbBtn != null)
					tbBtn.setVisible(false);
			}
		}
		catch (SQLException e)
		{
			logger.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		Cell toobar = new Cell();
		toobar.appendChild(cwtoolbar);
		toobar.setSclass("z-group-inner");
		toobar.setColspan(4);
		ep.group.appendChild(toobar);
		ep.windowPanel.getStatusBar().setZclass("z-group-foot");
		ep.windowPanel.initPanel(-1, null);
	}

	@Override
	public void focus() {
		if (formComponent.isVisible())
			this.setFocusToField();
		else
			listPanel.focus();
	}

	public void setFocusToField(String columnName) {
		if (formComponent.isVisible()) {
			boolean found = false;
			for (WEditor editor : editors) {
				if (found)
					editor.setHasFocus(false);
				else if (columnName.equals(editor.getColumnName())) {
					editor.setHasFocus(true);
					Clients.response(new AuFocus(editor.getComponent()));
					found = true;
				}
			}
		} else {
			listPanel.setFocusToField(columnName);
		}
	}

	/**
	 * @see IADTabpanel#onEnterKey()
	 */
	public boolean onEnterKey() {
		if (listPanel.isVisible()) {
			return listPanel.onEnterKey();
		}
		return false;
	}

	/**
	 * @return boolean
	 */
	public boolean isGridView() {
		return listPanel.isVisible();
	}

	/**
	 * @param gTab
	 * @return embedded panel or null if not found
	 */
	public IADTabpanel findEmbeddedPanel(GridTab gTab) {
		IADTabpanel panel = null;
		for(EmbeddedPanel ep : includedPanel) {
			if (ep.tabPanel.getGridTab().equals(gTab)) {
				return ep.tabPanel;
			}
		}
		return panel;
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.panel.IADTabpanel#getGridView()
	 */
	@Override
	public GridPanel getGridView()
	{
		return listPanel;
	}

	/* (non-Javadoc)
	 * @see org.adempiere.webui.panel.IADTabpanel#isEnableCustomizeButton()
	 */
	@Override
	public boolean isEnableCustomizeButton()
	{
		return isGridView();
	}

	@Override
	public Div getGrid() {
		return grid;
	}

	@Override
	public List<HorizontalEmbeddedPanel> getHorizontalIncludedPanel() {
		return null;
	}
	
	/**
	 * Get all visible button editors
	 * @return List<WButtonEditor>
	 */
	public List<Button> getToolbarButtons() {
		List<Button> buttonList = new ArrayList<Button>();
		for(WButtonEditor editor : toolbarButtonEditors) {
			if (editor.getComponent() != null 
					&& editor.getComponent().isVisible()) {
				buttonList.add(editor.getComponent());
			}
		}

		for(ToolbarProcessButton processButton : toolbarProcessButtons) {
			if (processButton.getButton().isVisible()) {
				buttonList.add(processButton.getButton());
			}
		}
		return buttonList;
	}
	
	@Override
	public void setTabNo(int tabNo) {
		this.tabNo = tabNo;
	}

	@Override
	public int getTabNo() {
		return tabNo;
	}
}

