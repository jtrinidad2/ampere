/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.util.Properties;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Menupopup;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.WPreference;
import org.compiere.model.MClient;
import org.compiere.model.MOrg;
import org.compiere.model.MRole;
import org.compiere.model.MUser;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;

/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @date    Feb 25, 2007
 * @version $Revision: 0.10 $
 */
public class UserPanel extends Menubar  implements EventListener
{

	private static final long serialVersionUID = -45350536628290540L;

	public static final String ATTR_IS_MIGRATION_ON = "ATTR_IS_MIGRATION_ON";

	private Properties ctx;

    private Menuitem logout = new Menuitem();
    private Menuitem role = new Menuitem();
    private Menuitem preference = new Menuitem();
    private Menuitem changeRole = new Menuitem();

    private Menu menuUser = new Menu();
	private WPreference preferencePopup;

    public UserPanel()
    {
        this.ctx = Env.getCtx();
        init();
    }

    private void init()
    {
    	setWidth("100%");
    	setSclass("desktop-header-usermenubar");

    	menuUser.setLabel(getUserName() + "@" + getClientName() + "." + getOrgName());
    	LayoutUtils.addSclass("desktop-header-usermenu", menuUser);
    	this.appendChild(menuUser);

    	Menupopup menuPopup = new Menupopup();
    	menuPopup.setParent(menuUser);

    	preference.setLabel(Msg.getMsg(Env.getCtx(), "Preference"));
    	preference.addEventListener(Events.ON_CLICK, this);
    	preference.setStyle("text-align:right");
    	LayoutUtils.addSclass("desktop-header-font", preference);
    	preference.setParent(menuPopup);

    	role.setLabel(this.getRoleName());
    	role.addEventListener(Events.ON_CLICK, this);
    	role.setStyle("text-align:right");
    	LayoutUtils.addSclass("desktop-header-font", role);
    	role.setParent(menuPopup);

    	changeRole.setLabel(Msg.getMsg(Env.getCtx(),"changeRole"));
    	changeRole.addEventListener(Events.ON_CLICK, this);
    	changeRole.setStyle("text-align:right");
    	LayoutUtils.addSclass("desktop-header-font", changeRole);
    	changeRole.setParent(menuPopup);

    	logout.setLabel(Msg.getMsg(Env.getCtx(),"Logout"));
    	logout.addEventListener(Events.ON_CLICK, this);
    	logout.setStyle("text-align:right");
    	LayoutUtils.addSclass("desktop-header-font", logout);
    	logout.setParent(menuPopup);

    }

    private String getUserName()
    {
        MUser user = MUser.get(ctx);
        return user.getName();
    }

    private String getRoleName()
    {
        MRole role = MRole.getDefault(ctx, false);
        return role.getName();
    }

    private String getClientName()
    {
        MClient client = MClient.get(ctx);
        return client.getName();
    }

    private String getOrgName()
    {
    	int orgId = Env.getAD_Org_ID(ctx);
    	if (orgId > 0)
    	{
    		MOrg org = MOrg.get(ctx, orgId);
    		return org.getName();
    	}
    	else
    	{
    		return "*";
    	}
    }

	public void onEvent(Event event) throws Exception {
		if (event == null)
			return;

		if (logout == event.getTarget())
        {
            SessionManager.logoutSession();
        }
		else if (role == event.getTarget())
		{
			String roleInfo = MRole.getDefault().toStringX(Env.getCtx());
			roleInfo = roleInfo.replace(Env.NL, "<br>");
			Messagebox.showDialog(roleInfo, Msg.getMsg(ctx, "RoleInfo"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else if (preference == event.getTarget())
		{
			if (preferencePopup != null)
			{
				preferencePopup.detach();
			}
			preferencePopup = new WPreference();
			preferencePopup.setPage(this.getPage());
			preferencePopup.open(preference);
		}
		else if (changeRole == event.getTarget())
		{
			MUser user = MUser.get(ctx);
			Clients.confirmClose(null);
			SessionManager.changeRole(user);
		}
	}
}
