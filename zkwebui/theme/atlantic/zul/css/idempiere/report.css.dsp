<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.rp-href {
  color: #19171D;
  text-decoration: none;
  font-weight: 600;
}
.rp-table {
  border-width: 1px;
  border-color: #E3E3E3;
  border-style: solid;
  border-collapse: collapse;
  width: 90%;
  background-color: #E5E5E5;
  border-width: 0px;
}
.rp-table th {
  text-align: center;
  padding: 2px;
  color: #FFF;
  font-weight: bold;
  background-color: #19171D;
  border-width: 1px;
  border-color: #E5E5E5;
  border-width: 2px 1px 2px 0px;
  border-style: solid;
}
.rp-table th:last-child {
  border-width: 2px 0px 2px 0px;
}
.rp-table td {
  border-width: 1px;
  border-color: #E3E3E3;
  border-style: solid;
  border-width: 1px 1px 1px 0px;
  padding: 3px;
}
.rp-table td:last-child {
  border-width: 1px 0px 1px 0px;
}
.rp-number {
  background-color: #E5E5E5;
  text-align: right !important;
  padding: 3px;
}
.rp-text,
.rp-date {
  background-color: #E5E5E5;
  text-align: left;
  padding: 3px;
}
.rp-lastgrouprow td {
  border-width: 0px 1px 2px 0px;
  border-color: white #E5E5E5 black white;
}
.rp-lastgrouprow td:last-child {
  border-width: 0px 0px 2px 0px;
  border-color: white #E5E5E5 black white;
}
.rp-functionrow td {
  background-color: #E5E5E5;
  font-weight: bold;
  border-width: 0px 1px 0px 0px;
}
.rp-functionrow td:last-child {
  border-width: 0px 0px 0px 0px;
}
