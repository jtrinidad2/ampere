<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-slider {
  background-image: none;
  overflow: hidden;
  position: relative;
  width: 100%;
  height: 100%;
}
.z-slider-center {
  cursor: pointer;
}
.z-slider-button {
  width: 18px;
  height: 18px;
  background: #4DA18F;
  position: relative;
  cursor: pointer;
}
.z-slider-knob-area {
  stroke: #4DA18F;
  data-angleOffset: 0;
}
.z-slider-knob-inner {
  stroke: #E3E3E3;
  data-angleOffset: 0;
}
.z-slider-knob-svg {
  width: 100%;
  height: 100%;
  cursor: pointer;
}
.z-slider-input {
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  -webkit-appearance: none;
  text-align: center;
  position: absolute;
  line-height: 1.5;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: bold;
  font-style: normal;
  color: #555555;
}
.z-slider-input:focus {
  border-color: #4DA18F;
}
.z-slider-horizontal {
  height: 30px;
  padding: 12px 0;
  width: 200px;
}
.z-slider-horizontal .z-slider-center {
  width: 100%;
  height: 6px;
  background: #ACACAC;
}
.z-slider-horizontal .z-slider-button {
  top: -6px;
}
.z-slider-vertical {
  width: 30px;
  padding: 0 12px;
  height: 200px;
}
.z-slider-vertical .z-slider-center {
  width: 6px;
  height: 100%;
  background: #ACACAC;
}
.z-slider-vertical .z-slider-button {
  left: -6px;
}
.z-slider-sphere .z-slider-vertical .z-slider-button {
  bottom: 0;
}
.z-slider-sphere .z-slider-button,
.z-slider-scale .z-slider-button {
  -webkit-border-radius: 9px;
  -moz-border-radius: 9px;
  -o-border-radius: 9px;
  -ms-border-radius: 9px;
  border-radius: 9px;
}
.z-slider-scale {
  background: url(${c:encodeThemeURL("~./zul/img/slider/ticks.gif")}) no-repeat;
}
.z-slider-popup {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #555555;
  border: 1px solid #E3E3E3;
  padding: 4px;
  background: #FFFFFF;
  position: absolute;
  z-index: 60000;
}
