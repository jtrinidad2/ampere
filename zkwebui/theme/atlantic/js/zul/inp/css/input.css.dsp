<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-textbox,
.z-decimalbox,
.z-intbox,
.z-longbox,
.z-doublebox {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #555555;
  min-height: 24px;
  border: 1px solid #E3E3E3;
  margin: 0;
  padding: 3px 7px;
  line-height: 16px;
  background: #FFFFFF;
}
.z-textbox:focus,
.z-decimalbox:focus,
.z-intbox:focus,
.z-longbox:focus,
.z-doublebox:focus {
  border-color: #4DA18F;
}
.z-textbox[readonly],
.z-decimalbox[readonly],
.z-intbox[readonly],
.z-longbox[readonly],
.z-doublebox[readonly] {
  border-color: #E3E3E3;
  background: #E5E5E5;
}
.z-textbox-invalid,
.z-decimalbox-invalid,
.z-intbox-invalid,
.z-longbox-invalid,
.z-doublebox-invalid {
  border-color: #AE312E;
  background: #FFFFFF;
}
.z-textbox[disabled],
.z-decimalbox[disabled],
.z-intbox[disabled],
.z-longbox[disabled],
.z-doublebox[disabled] {
  color: #ACACAC !important;
  background: #F2F2F2 !important;
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default !important;
}
.z-textbox-inplace,
.z-decimalbox-inplace,
.z-intbox-inplace,
.z-longbox-inplace,
.z-doublebox-inplace {
  border-width: 0;
  padding: 4px;
  background: none;
  resize: none;
}
.z-errorbox {
  color: #FFFFFF;
  width: 260px;
  position: absolute;
  top: 0;
  left: 0;
}
.z-errorbox > .z-errorbox-icon {
  font-size: 100%;
  color: #FFFFFF;
  position: absolute;
  top: 8px;
  left: 8px;
  z-index: 2;
}
.z-errorbox-left + .z-errorbox-icon {
  left: 16px;
}
.z-errorbox-up + .z-errorbox-icon {
  top: 16px;
}
.z-errorbox-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  width: 100%;
  border: 1px solid #AE312E;
  padding: 6px 20px 7px 26px;
  background: #AE312E;
  vertical-align: middle;
  position: relative;
  overflow: hidden;
  cursor: move;
}
.z-errorbox-pointer {
  display: none;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  position: absolute;
  z-index: 100;
}
.z-errorbox-left,
.z-errorbox-right,
.z-errorbox-up,
.z-errorbox-down {
  border: 6px solid transparent;
}
.z-errorbox-left {
  border-right-color: #AE312E;
}
.z-errorbox-right {
  border-left-color: #AE312E;
}
.z-errorbox-up {
  border-bottom-color: #AE312E;
}
.z-errorbox-down {
  border-top-color: #AE312E;
}
.z-errorbox-close {
  font-size: 70%;
  color: #FFFFFF;
  width: 12px;
  height: 12px;
  border: 1px solid transparent;
  line-height: 10px;
  text-align: center;
  position: absolute;
  top: 4px;
  right: 4px;
  cursor: pointer;
}
.z-errorbox-close:hover {
  color: #AE312E;
  background: #FFFFFF;
}
.z-errorbox-right ~ .z-errorbox-close {
  right: 12px;
}
.z-errorbox-up ~ .z-errorbox-close {
  top: 12px;
}
.ie8 .z-textbox,
.ie8 .z-decimalbox,
.ie8 .z-intbox,
.ie8 .z-longbox,
.ie8 .z-doublebox {
  min-height: 18px;
}
