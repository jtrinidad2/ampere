<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-calendar {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #555555;
  min-width: 210px;
  border: 1px solid #E3E3E3;
}
.z-calendar a {
  text-decoration: none;
}
.z-calendar-header {
  width: 100%;
  height: 30px;
  padding: 4px 8px;
  background: #00867D;
  text-align: center;
  position: relative;
}
.z-calendar-header > a {
  display: inline-block;
  line-height: 22px;
}
.z-calendar-header:hover {
  background: #19171D;
}
.z-calendar-title {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 114%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
}
.z-calendar-icon {
  font-size: 114%;
  color: #FFFFFF;
  width: 30px;
  height: 30px;
  padding: 4px;
  position: absolute;
  top: 0;
  cursor: pointer;
}
.z-calendar-right {
  border-left: 1px solid #329386;
  right: 0px;
}
.z-calendar-left {
  border-right: 1px solid #329386;
  left: 0px;
}
.z-calendar-left[disabled],
.z-calendar-right[disabled],
.z-calendar-disabled {
  color: #ACACAC !important;
  background: #E3E3E3 !important;
  cursor: default !important;
}
.z-calendar-body {
  width: 100%;
  height: 100%;
}
.z-calendar th {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 80%;
  width: 30px;
  height: 24px;
  background: #dfe1e1;
}
.z-calendar-decade {
  min-height: 144px;
}
.z-calendar-month,
.z-calendar-year {
  min-height: 144px;
}
.z-calendar-month .z-calendar-cell,
.z-calendar-year .z-calendar-cell {
  min-height: 48px;
}
.z-calendar-cell {
  width: 30px;
  height: 30px;
  border-width: 1px 0 0 1px;
  border-style: solid;
  border-color: #E3E3E3;
  background: #FFFFFF;
  text-align: center;
  cursor: pointer;
}
.z-calendar-cell:first-child {
  border-left-width: 0;
}
.z-calendar-cell:hover {
  color: #FFFFFF;
  background: #4DA18F;
}
.z-calendar-weekend {
  color: #555555;
  background: #FFFFFF;
}
.z-calendar-outrange {
  color: #ACACAC;
  background: #FFFFFF;
}
.z-calendar-weekofyear {
  color: #ACACAC;
  border-width: 0;
  background: #dfe1e1;
  cursor: default;
}
.z-calendar-weekofyear:hover {
  color: #ACACAC;
  background: #dfe1e1;
}
.z-calendar-anima {
  position: relative;
  overflow: hidden;
}
.z-calendar-anima-inner {
  width: 200%;
  position: absolute;
}
.z-calendar-anima-inner table {
  width: 50%;
  float: left;
}
.z-calendar-selected {
  color: #7BBCA3;
  font-weight: 700;
  background: #FFFFFF;
}
.z-calendar-selected:hover {
  color: #FFFFFF;
  background: #7BBCA3;
}
.z-calendar-outside {
  color: #ACACAC;
  background: #FFFFFF;
}
.z-datebox-popup {
  position: absolute;
}
.z-datebox-popup .z-calendar + .z-timebox {
  margin: 4px;
}
.z-datebox-popup .z-calendar ~ .z-datebox-timezone {
  margin: 0 4px 4px;
}
.z-datebox-timezone {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #555555;
}
