<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-popup {
  background: #7BBCA3;
  position: absolute;
  top: 0;
  left: 0;
  overflow: hidden;
}
.z-popup-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  height: 100%;
  padding: 8px;
  line-height: normal;
}
.z-notification {
  color: #FFFFFF;
  position: absolute;
  top: 0;
  left: 0;
}
.z-notification-icon {
  position: absolute;
  top: 50%;
  left: 0;
  z-index: 1;
}
.z-notification-icon.z-icon-times {
  margin-top: -6px;
  left: 3px;
}
.z-notification-icon.z-icon-times-circle,
.z-notification-icon.z-icon-exclamation-circle,
.z-notification-icon.z-icon-info-circle {
  font-size: 24px;
  margin-top: -12px;
  left: 12px;
}
.z-notification-pointer + .z-notification-icon {
  left: 14px;
}
.z-notification-left + .z-notification-icon {
  left: 24px;
}
.z-notification-up + .z-notification-icon {
  margin-top: -6px;
}
.z-notification-down + .z-notification-icon {
  margin-top: -18px;
}
.z-notification-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  width: 250px;
  min-height: 130px;
  padding: 15px 18px 15px 45px;
  position: relative;
  overflow: hidden;
}
.z-notification-pointer ~ .z-notification-content {
  display: table-cell;
  width: 125px;
  height: 60px;
  min-height: 60px;
  padding: 5px 18px 5px 45px;
  vertical-align: middle;
}
.z-notification-pointer {
  display: none;
  width: 0;
  height: 0;
  border: 10px solid transparent;
  position: absolute;
  z-index: 100;
}
.z-notification-left,
.z-notification-right,
.z-notification-up,
.z-notification-down {
  border: 10px solid transparent;
}
.z-notification-left {
  border-right-color: #333333;
}
.z-notification-right {
  border-left-color: #333333;
}
.z-notification-up {
  border-down-color: #333333;
}
.z-notification-down {
  border-top-color: #333333;
}
.z-notification-info .z-notification-content {
  background: #59A755;
}
.z-notification-info .z-notification-left {
  border-right-color: #59A755;
}
.z-notification-info .z-notification-right {
  border-left-color: #59A755;
}
.z-notification-info .z-notification-up {
  border-down-color: #59A755;
}
.z-notification-info .z-notification-down {
  border-top-color: #59A755;
}
.z-notification-warning .z-notification-content {
  background: #CC6D33;
}
.z-notification-warning .z-notification-left {
  border-right-color: #CC6D33;
}
.z-notification-warning .z-notification-right {
  border-left-color: #CC6D33;
}
.z-notification-warning .z-notification-up {
  border-down-color: #CC6D33;
}
.z-notification-warning .z-notification-down {
  border-top-color: #CC6D33;
}
.z-notification-error .z-notification-content {
  background: #AE312E;
}
.z-notification-error .z-notification-left {
  border-right-color: #AE312E;
}
.z-notification-error .z-notification-right {
  border-left-color: #AE312E;
}
.z-notification-error .z-notification-up {
  border-down-color: #AE312E;
}
.z-notification-error .z-notification-down {
  border-top-color: #AE312E;
}
.z-notification-close {
  font-size: 12px;
  display: block;
  width: 12px;
  height: 12px;
  position: absolute;
  top: 8px;
  right: 8px;
  cursor: pointer;
}
.z-notification-close:hover {
  background: #FFFFFF;
}
.z-notification-pointer ~ .z-notification-close {
  top: 5px;
  right: 5px;
}
.z-notification-right ~ .z-notification-close {
  top: 5px;
  right: 17px;
}
.z-notification-up ~ .z-notification-close {
  top: 17px;
}
.z-notification-info .z-notification-close:hover {
  color: #59A755;
}
.z-notification-warning .z-notification-close:hover {
  color: #CC6D33;
}
.z-notification-error .z-notification-close:hover {
  color: #AE312E;
}
