<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-toolbar {
  display: block;
  border: 1px solid #E3E3E3;
  padding: 7px;
  background: #FFFFFF;
  position: relative;
}
.z-toolbar-overflowpopup {
  white-space: nowrap;
  font-size: 0;
}
.z-toolbar-overflowpopup-button {
  font-size: 114%;
}
.z-toolbar-overflowpopup-on > .z-toolbar-overflowpopup-button {
  cursor: pointer;
  float: right;
  position: absolute;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -moz-transform: translateY(-50%);
  -o-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  right: 7px;
}
.z-toolbar-overflowpopup-off > .z-toolbar-overflowpopup-button {
  display: none;
}
.z-toolbar-popup {
  border: 1px solid #d9d9d9;
  background-color: #FFFFFF;
  -webkit-box-shadow: 0 3px 6px 0 rgba(0,0,0,0.16), 0 2px 4px 0 rgba(0,0,0,0.24);
  -moz-box-shadow: 0 3px 6px 0 rgba(0,0,0,0.16), 0 2px 4px 0 rgba(0,0,0,0.24);
  -o-box-shadow: 0 3px 6px 0 rgba(0,0,0,0.16), 0 2px 4px 0 rgba(0,0,0,0.24);
  -ms-box-shadow: 0 3px 6px 0 rgba(0,0,0,0.16), 0 2px 4px 0 rgba(0,0,0,0.24);
  box-shadow: 0 3px 6px 0 rgba(0,0,0,0.16), 0 2px 4px 0 rgba(0,0,0,0.24);
  display: block;
  position: absolute;
  padding: 0 4px;
  overflow: auto;
  max-height: 350px;
}
.z-toolbar-popup > * {
  display: table !important;
  margin: 4px 0 !important;
}
.z-toolbar-popup-open {
  visibility: visible;
}
.z-toolbar-popup-close {
  visibility: hidden;
}
.z-caption .z-toolbar {
  border-width: 0;
  background: none;
}
.z-toolbar-tabs .z-toolbar-content,
.z-toolbar-tabs .z-toolbar-content span,
.z-toolbar-panel .z-toolbar-content,
.z-toolbar-panel .z-toolbar-content span {
  font-size: 100%;
}
.z-toolbar-start {
  float: left;
  clear: none;
}
.z-toolbar-center {
  text-align: center;
  margin: 0 auto;
}
.z-toolbar-end {
  float: right;
  clear: none;
}
.z-toolbar-panel {
  background: none;
  border-width: 0;
  padding: 4px;
}
.z-toolbar-panel .z-toolbar-horizontal,
.z-toolbar-panel .z-toolbar-vertical {
  border: 0;
}
.z-toolbar-panel .z-toolbar-horizontal {
  padding-left: 3px;
}
.z-toolbar-panel .z-toolbar-vertical {
  padding-bottom: 1px;
}
.z-toolbarbutton {
  display: inline-block;
  width: auto;
  height: auto;
  border: 1px solid transparent;
  margin: 0 2px;
  padding: 3px;
  line-height: 16px;
  position: relative;
  cursor: pointer;
}
.z-toolbarbutton:hover {
  border-color: #4DA18F;
  background: #4DA18F;
  -webkit-box-shadow: inset 0 -1px 0 #4DA18F;
  -moz-box-shadow: inset 0 -1px 0 #4DA18F;
  -o-box-shadow: inset 0 -1px 0 #4DA18F;
  -ms-box-shadow: inset 0 -1px 0 #4DA18F;
  box-shadow: inset 0 -1px 0 #4DA18F;
}
.z-toolbarbutton:hover .z-toolbarbutton-content {
  color: #FFFFFF;
}
.z-toolbarbutton:active {
  border-color: #65AE99;
  background: #65AE99;
  -webkit-box-shadow: inset 0px 1px 0 #329386;
  -moz-box-shadow: inset 0px 1px 0 #329386;
  -o-box-shadow: inset 0px 1px 0 #329386;
  -ms-box-shadow: inset 0px 1px 0 #329386;
  box-shadow: inset 0px 1px 0 #329386;
}
.z-toolbarbutton:active .z-toolbarbutton-content {
  color: #FFFFFF;
}
.z-toolbarbutton[disabled],
.z-toolbarbutton[disabled]:hover {
  border-color: transparent;
  background: #E3E3E3;
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
  cursor: default !important;
}
.z-toolbarbutton[disabled] .z-toolbarbutton-content,
.z-toolbarbutton[disabled]:hover .z-toolbarbutton-content {
  color: #ACACAC;
}
.z-toolbarbutton-checked {
  border-color: #4DA18F;
  background: #FFFFFF;
}
.z-toolbarbutton-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #7BBCA3;
  white-space: nowrap;
}
.z-toolbar-vertical .z-toolbar-content > * {
  display: block;
}
