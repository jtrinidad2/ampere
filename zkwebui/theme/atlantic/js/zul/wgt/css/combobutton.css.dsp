<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-combobutton {
  display: inline-block;
  min-height: 24px;
  cursor: pointer;
}
.z-combobutton-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  display: block;
  min-height: 24px;
  border: 1px solid #7BBCA3;
  padding: 3px 8px;
  padding-right: 32px;
  line-height: 16px;
  background: #7BBCA3;
  vertical-align: middle;
  position: relative;
  white-space: nowrap;
}
.z-combobutton-button {
  display: block;
  width: 24px;
  height: 100%;
  border-left: 1px solid #65AE99;
  line-height: normal;
  position: absolute;
  top: 0;
  right: 0;
}
.z-combobutton-icon {
  font-size: 114%;
  color: #FFFFFF;
  display: block;
  width: 22px;
  line-height: 22px;
  text-align: center;
}
.z-combobutton:hover .z-combobutton-content {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
  -webkit-box-shadow: inset 0 -1px 0 #4DA18F;
  -moz-box-shadow: inset 0 -1px 0 #4DA18F;
  -o-box-shadow: inset 0 -1px 0 #4DA18F;
  -ms-box-shadow: inset 0 -1px 0 #4DA18F;
  box-shadow: inset 0 -1px 0 #4DA18F;
}
.z-combobutton:hover .z-combobutton-button {
  border-left-color: #448e7e;
}
.z-combobutton:focus .z-combobutton-content,
.z-combobutton:focus .z-combobutton-button {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
}
.z-combobutton:focus .z-combobutton-button {
  border-left-color: #448e7e;
}
.z-combobutton:active .z-combobutton-content {
  color: #FFFFFF;
  border-color: #65AE99;
  background: #65AE99;
  -webkit-box-shadow: inset 0px 1px 0 #329386;
  -moz-box-shadow: inset 0px 1px 0 #329386;
  -o-box-shadow: inset 0px 1px 0 #329386;
  -ms-box-shadow: inset 0px 1px 0 #329386;
  box-shadow: inset 0px 1px 0 #329386;
}
.z-combobutton:active .z-combobutton-button {
  border-left-color: #55a18b;
}
.z-combobutton[disabled],
.z-combobutton[disabled]:hover {
  cursor: default;
}
.z-combobutton[disabled] .z-combobutton-content,
.z-combobutton[disabled]:hover .z-combobutton-content,
.z-combobutton[disabled] .z-combobutton-button,
.z-combobutton[disabled]:hover .z-combobutton-button,
.z-combobutton[disabled] .z-combobutton-icon,
.z-combobutton[disabled]:hover .z-combobutton-icon {
  color: #ACACAC;
  border-color: #E3E3E3;
  background: #E3E3E3;
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-combobutton-toolbar .z-combobutton-content,
.z-combobutton-toolbar .z-combobutton-button {
  border-color: transparent;
}
.z-combobutton-toolbar[disabled] .z-combobutton-content,
.z-combobutton-toolbar[disabled] .z-combobutton-button {
  border-color: transparent;
}
.ie8 .z-combobutton {
  min-height: 18px;
}
.ie8 .z-combobutton .z-combobutton-content {
  min-height: 18px;
}
