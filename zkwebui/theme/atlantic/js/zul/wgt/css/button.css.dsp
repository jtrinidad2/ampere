<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-button {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  min-height: 24px;
  border: 1px solid #7BBCA3;
  padding: 3px 15px;
  line-height: 16px;
  background: #7BBCA3;
  cursor: pointer;
  white-space: nowrap;
}
.z-button:hover {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
}
.z-button:focus {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-button:active {
  color: #FFFFFF;
  border-color: #65AE99;
  background: #65AE99;
  -webkit-box-shadow: inset 0px 1px 0 #329386;
  -moz-box-shadow: inset 0px 1px 0 #329386;
  -o-box-shadow: inset 0px 1px 0 #329386;
  -ms-box-shadow: inset 0px 1px 0 #329386;
  box-shadow: inset 0px 1px 0 #329386;
}
.z-button[disabled] {
  color: #ACACAC;
  border: 1px solid #E3E3E3;
  background: #E3E3E3;
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
  cursor: default;
}
.ie8 .z-button {
  min-height: 16px;
}
