<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-biglistbox {
  border: 1px solid #E3E3E3;
  overflow: hidden;
  zoom: 1;
}
.z-biglistbox-outer {
  border: 1px solid #E3E3E3;
  border-top-width: 0;
  border-left-width: 0;
  margin: 0 15px 15px 0;
  background: #FFFFFF;
  position: relative;
}
.z-biglistbox-faker th {
  font-size: 0;
  width: 45px;
  height: 0;
  border: 0;
  margin: 0;
  padding: 0;
  line-height: 0;
  overflow: hidden;
}
.z-biglistbox-head-outer {
  overflow: hidden;
}
.z-biglistbox-head {
  width: 100%;
  border: 0;
  overflow: hidden;
  float: left;
}
.z-biglistbox-head table {
  border-spacing: 0;
}
.z-biglistbox-head table th,
.z-biglistbox-head table td {
  background-clip: padding-box;
}
.z-biglistbox-header {
  border: 1px solid #FFFFFF;
  border-top-width: 0;
  padding: 0;
  background: #329386;
  text-align: left;
  position: relative;
  overflow: hidden;
  cursor: default;
  white-space: nowrap;
}
.z-biglistbox-header-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  padding: 4px 8px;
  line-height: 28px;
  overflow: hidden;
  white-space: nowrap;
}
.z-biglistbox-body-outer {
  overflow: hidden;
}
.z-biglistbox-body {
  width: 100%;
  border-bottom: 1px solid #E3E3E3;
  border-right: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: relative;
  overflow: hidden;
  float: left;
}
.z-biglistbox-body table {
  border-spacing: 0;
}
.z-biglistbox-body table th,
.z-biglistbox-body table td {
  background-clip: padding-box;
}
.z-biglistbox-body td {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #555555;
  border-top: 1px solid #E3E3E3;
  padding: 3px 4px 4px;
  line-height: 16px;
  overflow: hidden;
  cursor: pointer;
  white-space: nowrap;
}
.z-biglistbox-body tr:first-child td {
  border-top-width: 0;
  padding-top: 4px;
}
.z-biglistbox-row:hover td {
  color: #FFFFFF;
  background: #4DA18F;
}
.z-biglistbox-row.z-biglistbox-selected td {
  color: #555555;
  border-color: #E3E3E3;
  background: #E5E5E5;
}
.z-biglistbox-row.z-biglistbox-selected:hover td {
  color: #FFFFFF;
  border-color: #E3E3E3;
  background: #7BBCA3;
}
.z-biglistbox-odd {
  background: #FFFFFF;
}
.z-biglistbox-sort {
  cursor: pointer;
}
.z-biglistbox-sorticon {
  font-size: 100%;
  color: #FFFFFF;
  line-height: normal;
  position: absolute;
  top: 0;
  left: 50%;
}
.z-biglistbox-head-shim,
.z-biglistbox-body-shim {
  width: 3px;
  height: 1px;
  overflow: hidden;
  float: left;
}
.z-biglistbox-verticalbar-frozen {
  position: absolute;
  top: -3px;
  background: #E0E0E0;
  border: 1px solid #E3E3E3;
  width: 3px;
  height: 100%;
}
.z-biglistbox-verticalbar-tick {
  position: absolute;
  bottom: 1px;
  background: url(${c:encodeThemeURL("~./zkmax/img/big/vbar-tick.png")}) 0px 0px no-repeat;
  height: 16px;
  overflow: hidden;
  width: 8px;
  z-index: 20;
  cursor: w-resize;
}
.z-biglistbox-wscroll-vertical {
  position: absolute;
  top: 0;
  z-index: 10;
  width: 15px;
  height: 100%;
  right: -16px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag {
  height: 115px;
  width: 15px;
  overflow: hidden;
  background: url(${c:encodeThemeURL("~./zkmax/img/big/drag-v.png")}) no-repeat scroll 0 0 transparent;
  position: absolute;
  z-index: 15;
  cursor: pointer;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  position: absolute;
  width: 15px;
  height: 15px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home {
  top: 0;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up {
  top: 15px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down {
  bottom: 15px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  bottom: 0;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-body {
  width: 15px;
  height: 55px;
  position: absolute;
  top: 30px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-pos {
  position: absolute;
  z-index: 10;
  width: 15px;
  height: 115px;
  left: 0;
  background: #000000;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  -o-border-radius: 2px;
  -ms-border-radius: 2px;
  border-radius: 2px;
  top: 0;
  opacity: 0.25;
  filter: alpha(opacity=25);;
  visibility: visible;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-endbar {
  .horGradient(#FFFFFF, #EEEEEE);
  border: 1px solid #E3E3E3;
  height: 7px;
  overflow: hidden;
  position: absolute;
  width: 15px;
  z-index: 20;
  right: 0;
}
.z-biglistbox-wscroll-horizontal {
  position: absolute;
  left: 0;
  z-index: 10;
  width: 100%;
  height: 15px;
  bottom: -16px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag {
  height: 15px;
  width: 115px;
  overflow: hidden;
  background: url(${c:encodeThemeURL("~./zkmax/img/big/drag-h.png")}) no-repeat scroll 0 0 transparent;
  position: absolute;
  z-index: 15;
  cursor: pointer;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  position: absolute;
  width: 15px;
  height: 15px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home {
  left: 0;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up {
  left: 15px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down {
  right: 15px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  right: 0;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-body {
  width: 55px;
  height: 15px;
  position: absolute;
  left: 30px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-pos {
  position: absolute;
  z-index: 10;
  width: 115px;
  height: 15px;
  top: 0;
  background: #000000;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  -o-border-radius: 2px;
  -ms-border-radius: 2px;
  border-radius: 2px;
  left: 0;
  opacity: 0.25;
  filter: alpha(opacity=25);;
  visibility: visible;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-endbar {
  .verGradient(#FFFFFF, #EEEEEE);
  border: 1px solid #E3E3E3;
  height: 15px;
  overflow: hidden;
  position: absolute;
  width: 7px;
  z-index: 20;
  right: -12px;
}
