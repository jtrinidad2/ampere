<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-signature {
  position: relative;
  border: 1px solid #D9D9D9;
}
.z-signature-toolbar {
  position: absolute;
  bottom: 8px;
  right: 12px;
}
.z-signature-toolbar-hide {
  display: none;
}
.z-signature-tool-button {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  min-height: 24px;
  border: 1px solid #7BBCA3;
  padding: 3px 15px;
  line-height: 16px;
  background: #7BBCA3;
  cursor: pointer;
  white-space: nowrap;
  margin-right: 4px;
}
.z-signature-tool-button:hover {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
  -webkit-box-shadow: inset 0 -1px 0 #4DA18F;
  -moz-box-shadow: inset 0 -1px 0 #4DA18F;
  -o-box-shadow: inset 0 -1px 0 #4DA18F;
  -ms-box-shadow: inset 0 -1px 0 #4DA18F;
  box-shadow: inset 0 -1px 0 #4DA18F;
}
.z-signature-tool-button:focus {
  color: #FFFFFF;
  border-color: #4DA18F;
  background: #4DA18F;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-signature-tool-button:active {
  color: #FFFFFF;
  border-color: #65AE99;
  background: #65AE99;
  -webkit-box-shadow: inset 0px 1px 0 #329386;
  -moz-box-shadow: inset 0px 1px 0 #329386;
  -o-box-shadow: inset 0px 1px 0 #329386;
  -ms-box-shadow: inset 0px 1px 0 #329386;
  box-shadow: inset 0px 1px 0 #329386;
}
.z-signature-tool-button-icon {
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  font-size: inherit;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-rendering: auto;
  transform: translate(0, 0);
  font-size: 18px;
  vertical-align: bottom;
}
.z-signature-tool-button-undo:before {
  content: "\f112";
}
.z-signature-tool-button-save:before {
  content: "\f0c7";
}
.z-signature-tool-button-clear:before {
  content: "\f00d";
}
.z-signature-tool-button-label:not(:empty) {
  margin-left: 4px;
}
.z-signature-canvas {
  width: 100%;
  height: 100%;
}
