<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-fisheye {
  position: absolute;
  cursor: pointer;
  z-index: 2;
}
.z-fisheye-image {
  width: 100%;
  height: 100%;
  border-width: 0;
  position: absolute;
  cursor: pointer;
}
.z-fisheye-text {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #555555;
  display: none;
  border: 1px solid #E3E3E3;
  padding: 3px;
  background: #FFFFFF;
  text-align: center;
  position: absolute;
  cursor: pointer;
}
.z-fisheyebar-inner {
  position: relative;
}
