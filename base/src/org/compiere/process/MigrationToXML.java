/*******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution * Copyright (C)
 * 1999-2009 Adempiere, Inc. All Rights Reserved. * This program is free
 * software; you can redistribute it and/or modify it * under the terms version
 * 2 of the GNU General Public License as published * by the Free Software
 * Foundation. This program is distributed in the hope * that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied * warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. * See the GNU General
 * Public License for more details. * You should have received a copy of the GNU
 * General Public License along * with this program; if not, write to the Free
 * Software Foundation, Inc., * 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA. *
 * 
 ******************************************************************************/

package org.compiere.process;

import java.io.File;
import java.io.FileWriter;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.compiere.model.MAttachment;
import org.compiere.model.MMigration;
import org.compiere.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 * Process to export an AD migration script as xml
 * 
 * @author Paul Bowden, Adaxa Pty Ltd
 *
 */
public class MigrationToXML extends SvrProcess {

	private int migrationId = 0;
	private String fileName;
	private boolean	p_IsExportAsAttachment;
	private File	attachTempFile;

	@Override
	protected String doIt() throws Exception {
		MMigration migration = new MMigration(getCtx(), migrationId, get_TrxName());
		if ( migration == null || migration.is_new() )
			return "No migration to export";
		
		log.log(Level.FINE, "Creating xml document for migration: " + migration);
		Document document = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		document = builder.newDocument();
		Element root = document.createElement("Migrations");
		document.appendChild(root);
		root.appendChild(migration.toXmlNode(document));
		
		  //set up a transformer
        TransformerFactory transfac = TransformerFactory.newInstance();
        transfac.setAttribute("indent-number", 2);
        Transformer trans;
        
		trans = transfac.newTransformer();
		
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty(OutputKeys.STANDALONE, "yes");

		if (p_IsExportAsAttachment)
		{
			// Extract the file name
			String tempFileName = fileName;
			if (Util.isEmpty(tempFileName, true))
				tempFileName = migration.getName();
			if (tempFileName.lastIndexOf(File.separator) != -1)
				tempFileName = tempFileName.substring(tempFileName.lastIndexOf(File.separator) + 1, tempFileName.length());
			if (tempFileName.indexOf(".") != -1)
				tempFileName = tempFileName.substring(0, tempFileName.indexOf("."));
			// create file to add it to attachment
			attachTempFile = new File(tempFileName + ".xml");
			fileName = attachTempFile.getAbsolutePath();
		}

        log.log(Level.FINE, "Writing xml to file.");
        //create string from xml tree
        FileWriter fw = new FileWriter(fileName);
        StreamResult result = new StreamResult(fw);
        DOMSource source = new DOMSource(document);
        trans.transform(source, result);
        fw.close();
        
		if (p_IsExportAsAttachment)
		{
			MAttachment attachment = migration.createAttachment();
			// Deleting old attached entries
			while (attachment.getEntryCount() > 0)
			{
				attachment.deleteEntry(0);
			}
			
			// adding new script file
			attachment.addEntry(attachTempFile);
			attachment.saveEx();
			
			// delete file after added to attachment
			if (attachTempFile.exists())
				attachTempFile.delete();
			return "Migration script attached : " + fileName;
		}

		return "Exported migration to: " + fileName;
	}

	@Override
	protected void prepare() {

		ProcessInfoParameter[] paras = getParameter();
		for ( ProcessInfoParameter para : paras )
		{
			if ( para.getParameterName().equals("AD_Migration_ID"))
				migrationId =  para.getParameterAsInt();
			else if ( para.getParameterName().equals("FileName"))
				fileName = (String) para.getParameter();
			else if (para.getParameterName().equals("IsExportAsAttachment"))
				p_IsExportAsAttachment = para.getParameterAsBoolean();
		}
		
		// if run from Migration window
		if ( migrationId == 0 )
			migrationId = getRecord_ID();
		
		log.log(Level.FINE, "AD_Migration_ID = " + migrationId + ", filename = " + fileName);

	}

}
