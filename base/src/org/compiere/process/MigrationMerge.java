/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.compiere.model.MAttachment;
import org.compiere.model.MMigration;
import org.compiere.model.Query;
import org.compiere.model.X_AD_Migration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MigrationMerge extends SvrProcess
{

	private List<X_AD_Migration>	migrationFrom;
	private MMigration				migrationTo;

	/**
	 * Process to merge two migrations together
	 * 
	 * @author Paul Bowden, Adaxa Pty Ltd
	 */
	@Override
	protected String doIt() throws Exception
	{
		if (migrationFrom == null || migrationTo == null || migrationTo.is_new())
		{
			addLog("Two different existing migrations required for merge");
			return "@Error@";
		}

		for (X_AD_Migration migration : migrationFrom)
		{
			if (migration == null || migration.is_new() || migration.getAD_Migration_ID() == migrationTo.getAD_Migration_ID())
			{
				addLog("Two different existing migrations required for merge");
				return "@Error@";
			}
			MAttachment attachment = migration.getAttachment(true);
			if (attachment != null)
				attachment.deleteEx(true);

			migrationTo.mergeMigration((MMigration) migration);
		}

		createMigrationAttachment(migrationTo);

		return "@OK@";
	}

	@Override
	protected void prepare()
	{

		int[] fromIds = null;
		int toId = 0;

		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter p : params)
		{
			String para = p.getParameterName();
			if (para.equals("AD_Migration_ID"))
				fromIds = (int[]) p.getParameterAsIntArray();
			else if (para.equals("AD_MigrationTo_ID"))
				toId = p.getParameterAsInt();
		}

		// launched from migration window
		if (toId == 0)
			toId = getRecord_ID();

		migrationTo = new MMigration(getCtx(), toId, get_TrxName());

		if (fromIds != null)
		{
			String whereParem = "";
			for (int fID : fromIds)
			{

				if (whereParem != "")
					whereParem += ", ";
				whereParem += fID;
			}
			migrationFrom = new Query(getCtx(), X_AD_Migration.Table_Name, " AD_Migration_ID IN (" + whereParem + ")", get_TrxName()).setOrderBy(
					X_AD_Migration.COLUMNNAME_SeqNo).list();
		}
	}

	public void createMigrationAttachment(MMigration migration)
	{
		if (migration != null)
		{
			try
			{
				log.log(Level.FINE, "Creating xml document for migration: " + migration);
				Document document = null;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder;

				builder = factory.newDocumentBuilder();

				document = builder.newDocument();
				Element root = document.createElement("Migrations");
				document.appendChild(root);
				root.appendChild(migration.toXmlNode(document));

				// set up a transformer
				TransformerFactory transfac = TransformerFactory.newInstance();
				transfac.setAttribute("indent-number", 2);
				Transformer trans;

				trans = transfac.newTransformer();

				trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty(OutputKeys.STANDALONE, "yes");

				log.log(Level.FINE, "Writing xml to file.");
				// create string from xml tree
				File attachTempFile = File.createTempFile(migration.getSeqNo() + "_" + migration.getName(), ".xml");
				FileWriter fw = new FileWriter(attachTempFile);
				StreamResult result = new StreamResult(fw);
				DOMSource source = new DOMSource(document);
				trans.transform(source, result);
				fw.close();

				MAttachment attachment = migration.createAttachment();
				while (attachment.getEntryCount() > 0)
				{
					attachment.deleteEntry(0);
				}
				attachment.addEntry(attachTempFile);
				attachment.saveEx();
			}
			catch (Exception e)
			{
				// TODO ask do i throw the error or just log
				log.log(Level.SEVERE, e.getLocalizedMessage());
			}
		}
	} // createMigrationAttachment

}
