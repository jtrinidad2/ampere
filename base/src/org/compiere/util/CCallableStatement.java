/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.util;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;

/**
 * 
 * Callable statement wrapper
 */
public class CCallableStatement extends CPreparedStatement implements CallableStatement
{
	public CCallableStatement(CStatementVO vo) {
		super(vo);
	}

	public CCallableStatement(int resultSetType, int resultSetConcurrency,
			String sql0, String trxName) {
		super(resultSetType, resultSetConcurrency, sql0, trxName);
	}
	
	/**
     * Initialise the prepared statement wrapper object 
     */
    protected void init()
    {
        try
        {
            Connection conn = null;
            Trx trx = p_vo.getTrxName() == null ? null : Trx.get(p_vo.getTrxName(), true);
            if (trx != null)
            {
                conn = trx.getConnection();
            }
            else
            {
                if (p_vo.getResultSetConcurrency() == ResultSet.CONCUR_UPDATABLE)
                    m_conn = DB.getConnectionRW ();
                else
                    m_conn = DB.getConnectionRO();
                conn = m_conn;
            }
            if (conn == null)
                throw new DBException("No Connection");
            p_stmt = conn.prepareCall(p_vo.getSql(), p_vo.getResultSetType(), p_vo.getResultSetConcurrency());
            return;
        }
        catch (Exception e)
        {
            log.log(Level.SEVERE, p_vo.getSql(), e);
            throw new DBException(e);
        }
    }

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getArray(int)
	 */
	public Array getArray(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getArray(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getArray(java.lang.String)
	 */
	public Array getArray(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getArray(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBlob(int)
	 */
	public Blob getBlob(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBlob(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBlob(java.lang.String)
	 */
	public Blob getBlob(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBlob(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBytes(int)
	 */
	public byte[] getBytes(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBytes(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBytes(java.lang.String)
	 */
	public byte[] getBytes(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBytes(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getCharacterStream(int)
	 */
	public Reader getCharacterStream(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getCharacterStream(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getCharacterStream(java.lang.String)
	 */
	public Reader getCharacterStream(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getCharacterStream(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getClob(int)
	 */
	public Clob getClob(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getClob(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getClob(java.lang.String)
	 */
	public Clob getClob(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getClob(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDate(int, java.util.Calendar)
	 */
	public Date getDate(int arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getDate(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDate(int)
	 */
	public Date getDate(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getDate(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDate(java.lang.String, java.util.Calendar)
	 */
	public Date getDate(String arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getDate(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDate(java.lang.String)
	 */
	public Date getDate(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getDate(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDouble(int)
	 */
	public double getDouble(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getDouble(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getDouble(java.lang.String)
	 */
	public double getDouble(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getDouble(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getFloat(int)
	 */
	public float getFloat(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getFloat(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getFloat(java.lang.String)
	 */
	public float getFloat(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getFloat(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getInt(int)
	 */
	public int getInt(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getInt(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getInt(java.lang.String)
	 */
	public int getInt(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getInt(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getLong(int)
	 */
	public long getLong(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getLong(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getLong(java.lang.String)
	 */
	public long getLong(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getLong(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNCharacterStream(int)
	 */
	public Reader getNCharacterStream(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNCharacterStream(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNCharacterStream(java.lang.String)
	 */
	public Reader getNCharacterStream(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNCharacterStream(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNClob(int)
	 */
	public NClob getNClob(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNClob(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNClob(java.lang.String)
	 */
	public NClob getNClob(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNClob(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNString(int)
	 */
	public String getNString(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNString(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getNString(java.lang.String)
	 */
	public String getNString(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getNString(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(java.lang.String)
	 */
	public Object getObject(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getRef(int)
	 */
	public Ref getRef(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getRef(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getRef(java.lang.String)
	 */
	public Ref getRef(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getRef(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getRowId(int)
	 */
	public RowId getRowId(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getRowId(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getRowId(java.lang.String)
	 */
	public RowId getRowId(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getRowId(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getSQLXML(int)
	 */
	public SQLXML getSQLXML(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getSQLXML(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getSQLXML(java.lang.String)
	 */
	public SQLXML getSQLXML(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getSQLXML(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getShort(int)
	 */
	public short getShort(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getShort(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getShort(java.lang.String)
	 */
	public short getShort(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getShort(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getString(int)
	 */
	public String getString(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getString(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getString(java.lang.String)
	 */
	public String getString(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getString(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTime(int, java.util.Calendar)
	 */
	public Time getTime(int arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getTime(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTime(int)
	 */
	public Time getTime(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getTime(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTime(java.lang.String, java.util.Calendar)
	 */
	public Time getTime(String arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getTime(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTime(java.lang.String)
	 */
	public Time getTime(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getTime(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTimestamp(int, java.util.Calendar)
	 */
	public Timestamp getTimestamp(int arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getTimestamp(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTimestamp(int)
	 */
	public Timestamp getTimestamp(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getTimestamp(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String, java.util.Calendar)
	 */
	public Timestamp getTimestamp(String arg0, Calendar arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getTimestamp(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String)
	 */
	public Timestamp getTimestamp(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getTimestamp(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getURL(int)
	 */
	public URL getURL(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getURL(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getURL(java.lang.String)
	 */
	public URL getURL(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getURL(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @deprecated
	 * @see java.sql.CallableStatement#getBigDecimal(int, int)
	 */
	public BigDecimal getBigDecimal(int arg0, int arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getBigDecimal(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBigDecimal(int)
	 */
	public BigDecimal getBigDecimal(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBigDecimal(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBigDecimal(java.lang.String)
	 */
	public BigDecimal getBigDecimal(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBigDecimal(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBoolean(int)
	 */
	public boolean getBoolean(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBoolean(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getBoolean(java.lang.String)
	 */
	public boolean getBoolean(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getBoolean(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getByte(int)
	 */
	public byte getByte(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getByte(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getByte(java.lang.String)
	 */
	public byte getByte(String arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getByte(arg0);
	}

	/**
	 * @param <T>
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(int, java.lang.Class)
	 */
	public <T> T getObject(int arg0, Class<T> arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(int, java.util.Map)
	 */
	public Object getObject(int arg0, Map<String, Class<?>> arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(int)
	 */
	public Object getObject(int arg0) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0);
	}

	/**
	 * @param <T>
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(java.lang.String, java.lang.Class)
	 */
	public <T> T getObject(String arg0, Class<T> arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#getObject(java.lang.String, java.util.Map)
	 */
	public Object getObject(String arg0, Map<String, Class<?>> arg1) throws SQLException {
		return ((CallableStatement) p_stmt).getObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(int, int, int)
	 */
	public void registerOutParameter(int arg0, int arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(int, int, java.lang.String)
	 */
	public void registerOutParameter(int arg0, int arg1, String arg2) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(int, int)
	 */
	public void registerOutParameter(int arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, int)
	 */
	public void registerOutParameter(String arg0, int arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, java.lang.String)
	 */
	public void registerOutParameter(String arg0, int arg1, String arg2) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int)
	 */
	public void registerOutParameter(String arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).registerOutParameter(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, int)
	 */
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, long)
	 */
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream)
	 */
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, int)
	 */
	public void setAsciiStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, long)
	 */
	public void setAsciiStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream)
	 */
	public void setAsciiStream(String arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setAsciiStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal)
	 */
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		((CallableStatement) p_stmt).setBigDecimal(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBigDecimal(java.lang.String, java.math.BigDecimal)
	 */
	public void setBigDecimal(String arg0, BigDecimal arg1) throws SQLException {
		((CallableStatement) p_stmt).setBigDecimal(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, int)
	 */
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, long)
	 */
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream)
	 */
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, int)
	 */
	public void setBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, long)
	 */
	public void setBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream)
	 */
	public void setBinaryStream(String arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setBinaryStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob)
	 */
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.io.InputStream, long)
	 */
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.io.InputStream)
	 */
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBlob(java.lang.String, java.sql.Blob)
	 */
	public void setBlob(String arg0, Blob arg1) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBlob(java.lang.String, java.io.InputStream, long)
	 */
	public void setBlob(String arg0, InputStream arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBlob(java.lang.String, java.io.InputStream)
	 */
	public void setBlob(String arg0, InputStream arg1) throws SQLException {
		((CallableStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBoolean(int, boolean)
	 */
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		((CallableStatement) p_stmt).setBoolean(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBoolean(java.lang.String, boolean)
	 */
	public void setBoolean(String arg0, boolean arg1) throws SQLException {
		((CallableStatement) p_stmt).setBoolean(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setByte(int, byte)
	 */
	public void setByte(int arg0, byte arg1) throws SQLException {
		((CallableStatement) p_stmt).setByte(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setByte(java.lang.String, byte)
	 */
	public void setByte(String arg0, byte arg1) throws SQLException {
		((CallableStatement) p_stmt).setByte(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBytes(int, byte[])
	 */
	public void setBytes(int arg0, byte[] arg1) throws SQLException {
		((CallableStatement) p_stmt).setBytes(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setBytes(java.lang.String, byte[])
	 */
	public void setBytes(String arg0, byte[] arg1) throws SQLException {
		((CallableStatement) p_stmt).setBytes(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, int)
	 */
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, long)
	 */
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader)
	 */
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, int)
	 */
	public void setCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, long)
	 */
	public void setCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader)
	 */
	public void setCharacterStream(String arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.sql.Clob)
	 */
	public void setClob(int arg0, Clob arg1) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.io.Reader, long)
	 */
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.io.Reader)
	 */
	public void setClob(int arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setClob(java.lang.String, java.sql.Clob)
	 */
	public void setClob(String arg0, Clob arg1) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setClob(java.lang.String, java.io.Reader, long)
	 */
	public void setClob(String arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setClob(java.lang.String, java.io.Reader)
	 */
	public void setClob(String arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date, java.util.Calendar)
	 */
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setDate(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date)
	 */
	public void setDate(int arg0, Date arg1) throws SQLException {
		((CallableStatement) p_stmt).setDate(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date, java.util.Calendar)
	 */
	public void setDate(String arg0, Date arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setDate(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date)
	 */
	public void setDate(String arg0, Date arg1) throws SQLException {
		((CallableStatement) p_stmt).setDate(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDouble(int, double)
	 */
	public void setDouble(int arg0, double arg1) throws SQLException {
		((CallableStatement) p_stmt).setDouble(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setDouble(java.lang.String, double)
	 */
	public void setDouble(String arg0, double arg1) throws SQLException {
		((CallableStatement) p_stmt).setDouble(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setFloat(int, float)
	 */
	public void setFloat(int arg0, float arg1) throws SQLException {
		((CallableStatement) p_stmt).setFloat(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setFloat(java.lang.String, float)
	 */
	public void setFloat(String arg0, float arg1) throws SQLException {
		((CallableStatement) p_stmt).setFloat(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setInt(int, int)
	 */
	public void setInt(int arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).setInt(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setInt(java.lang.String, int)
	 */
	public void setInt(String arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).setInt(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setLong(int, long)
	 */
	public void setLong(int arg0, long arg1) throws SQLException {
		((CallableStatement) p_stmt).setLong(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setLong(java.lang.String, long)
	 */
	public void setLong(String arg0, long arg1) throws SQLException {
		((CallableStatement) p_stmt).setLong(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNCharacterStream(int, java.io.Reader, long)
	 */
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setNCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNCharacterStream(int, java.io.Reader)
	 */
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setNCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNCharacterStream(java.lang.String, java.io.Reader, long)
	 */
	public void setNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setNCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNCharacterStream(java.lang.String, java.io.Reader)
	 */
	public void setNCharacterStream(String arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setNCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.sql.NClob)
	 */
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.io.Reader, long)
	 */
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.io.Reader)
	 */
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNClob(java.lang.String, java.sql.NClob)
	 */
	public void setNClob(String arg0, NClob arg1) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNClob(java.lang.String, java.io.Reader, long)
	 */
	public void setNClob(String arg0, Reader arg1, long arg2) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNClob(java.lang.String, java.io.Reader)
	 */
	public void setNClob(String arg0, Reader arg1) throws SQLException {
		((CallableStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNString(int, java.lang.String)
	 */
	public void setNString(int arg0, String arg1) throws SQLException {
		((CallableStatement) p_stmt).setNString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNString(java.lang.String, java.lang.String)
	 */
	public void setNString(String arg0, String arg1) throws SQLException {
		((CallableStatement) p_stmt).setNString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int, java.lang.String)
	 */
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		((CallableStatement) p_stmt).setNull(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int)
	 */
	public void setNull(int arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).setNull(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int, java.lang.String)
	 */
	public void setNull(String arg0, int arg1, String arg2) throws SQLException {
		((CallableStatement) p_stmt).setNull(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int)
	 */
	public void setNull(String arg0, int arg1) throws SQLException {
		((CallableStatement) p_stmt).setNull(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int, int)
	 */
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int)
	 */
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1, arg2);
	}

	/**
	 * @param parameterIndex
	 * @param x
	 * @param targetSqlType
	 * @param scaleOrLength
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, java.sql.SQLType, int)
	 */
	public void setObject(int parameterIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
		((CallableStatement) p_stmt).setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	/**
	 * @param parameterIndex
	 * @param x
	 * @param targetSqlType
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, java.sql.SQLType)
	 */
	public void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
		((CallableStatement) p_stmt).setObject(parameterIndex, x, targetSqlType);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object)
	 */
	public void setObject(int arg0, Object arg1) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int, int)
	 */
	public void setObject(String arg0, Object arg1, int arg2, int arg3) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int)
	 */
	public void setObject(String arg0, Object arg1, int arg2) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1, arg2);
	}

	/**
	 * @param parameterName
	 * @param x
	 * @param targetSqlType
	 * @param scaleOrLength
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, java.sql.SQLType, int)
	 */
	public void setObject(String parameterName, Object x, SQLType targetSqlType, int scaleOrLength)
			throws SQLException {
		((CallableStatement) p_stmt).setObject(parameterName, x, targetSqlType, scaleOrLength);
	}

	/**
	 * @param parameterName
	 * @param x
	 * @param targetSqlType
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, java.sql.SQLType)
	 */
	public void setObject(String parameterName, Object x, SQLType targetSqlType) throws SQLException {
		((CallableStatement) p_stmt).setObject(parameterName, x, targetSqlType);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object)
	 */
	public void setObject(String arg0, Object arg1) throws SQLException {
		((CallableStatement) p_stmt).setObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setRowId(int, java.sql.RowId)
	 */
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		((CallableStatement) p_stmt).setRowId(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setRowId(java.lang.String, java.sql.RowId)
	 */
	public void setRowId(String arg0, RowId arg1) throws SQLException {
		((CallableStatement) p_stmt).setRowId(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setSQLXML(int, java.sql.SQLXML)
	 */
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		((CallableStatement) p_stmt).setSQLXML(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setSQLXML(java.lang.String, java.sql.SQLXML)
	 */
	public void setSQLXML(String arg0, SQLXML arg1) throws SQLException {
		((CallableStatement) p_stmt).setSQLXML(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setShort(int, short)
	 */
	public void setShort(int arg0, short arg1) throws SQLException {
		((CallableStatement) p_stmt).setShort(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setShort(java.lang.String, short)
	 */
	public void setShort(String arg0, short arg1) throws SQLException {
		((CallableStatement) p_stmt).setShort(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setString(int, java.lang.String)
	 */
	public void setString(int arg0, String arg1) throws SQLException {
		((CallableStatement) p_stmt).setString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setString(java.lang.String, java.lang.String)
	 */
	public void setString(String arg0, String arg1) throws SQLException {
		((CallableStatement) p_stmt).setString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time, java.util.Calendar)
	 */
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setTime(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time)
	 */
	public void setTime(int arg0, Time arg1) throws SQLException {
		((CallableStatement) p_stmt).setTime(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time, java.util.Calendar)
	 */
	public void setTime(String arg0, Time arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setTime(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time)
	 */
	public void setTime(String arg0, Time arg1) throws SQLException {
		((CallableStatement) p_stmt).setTime(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp, java.util.Calendar)
	 */
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setTimestamp(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp)
	 */
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		((CallableStatement) p_stmt).setTimestamp(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp, java.util.Calendar)
	 */
	public void setTimestamp(String arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		((CallableStatement) p_stmt).setTimestamp(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp)
	 */
	public void setTimestamp(String arg0, Timestamp arg1) throws SQLException {
		((CallableStatement) p_stmt).setTimestamp(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public void setURL(int arg0, URL arg1) throws SQLException {
		((CallableStatement) p_stmt).setURL(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.CallableStatement#setURL(java.lang.String, java.net.URL)
	 */
	public void setURL(String arg0, URL arg1) throws SQLException {
		((CallableStatement) p_stmt).setURL(arg0, arg1);
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.CallableStatement#wasNull()
	 */
	public boolean wasNull() throws SQLException {
		return ((CallableStatement) p_stmt).wasNull();
	}
}
