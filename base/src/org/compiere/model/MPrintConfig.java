package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.Env;


public class MPrintConfig extends X_AD_PrintConfig {


	/**
	 * 
	 */
	private static final long serialVersionUID = 596827192757413637L;

	public MPrintConfig(Properties ctx, int AD_PrintConfig_ID, String trxName) {
		super(ctx, AD_PrintConfig_ID, trxName);
	}
	
	public MPrintConfig(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}

	public static MPrintConfig get(String configButton) {
		MPrintConfig config = new Query(Env.getCtx(), X_AD_PrintConfig.Table_Name, "Value=? AND AD_Client_ID=?", null)
			.setParameters(new Object[]{configButton, Env.getAD_Client_ID(Env.getCtx())})
			.firstOnly();
		
		if  (config == null)  {
			//create default so the user could simply fill up the Print Format
			config = new MPrintConfig(Env.getCtx(), 0, null);
			config.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
			//config.setAD_PrintConfig_ID(MSequence.getNextID(Env.getAD_Client_ID(Env.getCtx()), X_AD_PrintConfig.Table_Name));
			config.setAD_Org_ID(0);
			config.setValue(configButton);
			config.setName(configButton + " is not set.  Please update.");
			config.setIsVisible(true);
			config.save();
		}
		return config;
	}
	
}
