/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/*
 * Represents PO column data for a migration step
 */
public class MMigrationData extends X_AD_MigrationData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3597076848116587021L;

	public MMigrationData(Properties ctx, int AD_MigrationData_ID,
			String trxName) {
		super(ctx, AD_MigrationData_ID, trxName);
		if ( AD_MigrationData_ID == 0)
		{
			setClientOrg(0,0);
		}
	}

	public MMigrationData(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public MMigrationData(MMigrationStep parent) {
		this(parent.getCtx(), 0, parent.get_TrxName());
		setAD_MigrationStep_ID(parent.getAD_MigrationStep_ID());
	}

	public Node toXmlNode(MMigrationStep parent, Document document) {
		MColumn column = (MColumn) getAD_Column();
		Element data = document.createElement("Data");
		data.setAttribute("Column", column.getColumnName());
		data.setAttribute("AD_Column_ID", Integer.toString(getAD_Column_ID()));
		if ( !parent.getAction().equals(MMigrationStep.ACTION_Insert) )
		{
			if ( isOldNull() )
				data.setAttribute("isOldNull", "true");
			else
				data.setAttribute("oldValue", getOldValue());
		}
		if ( isNewNull() || getNewValue() == null )
			data.setAttribute("isNewNull", "true");
		else {
			if (column.getColumnName().equals("BinaryData")) {
				return null;
			} else {
				data.appendChild(document.createTextNode(getNewValue()));
			}
			
		}
		
		return data;
	}

	/*
	 * Create MMigration data from an xml <Data/> node
	 */
	public static void fromXmlNode(MMigrationStep parent, Node item, PreparedStatement ps) throws SQLException {
		/* ad_client_id, ad_column_id, ad_migrationstep_id, ad_org_id, " + 
		"            created, createdby,  isoldnull, isnewnull, " + 
		"            newvalue, oldvalue, updated, updatedb */

		Element element = (Element) item;
		ps.clearParameters();
		ps.setInt(1, 0);
		ps.setInt(2,Integer.parseInt(element.getAttribute("AD_Column_ID")));
		ps.setInt(3, parent.get_ID());
		ps.setInt(4, 0);
		ps.setTimestamp(5, parent.getCreated());
		ps.setInt(6, parent.getCreatedBy());
		ps.setString(7, "true".equals(element.getAttribute("isOldNull")) ? "Y" : "N");
		ps.setString(8, "true".equals(element.getAttribute("isNewNull")) ? "Y" : "N");
		ps.setString(9, element.getTextContent());
		ps.setString(10, element.getAttribute("oldValue"));
		ps.setTimestamp(11, parent.getUpdated());
		ps.setInt(12, parent.getUpdatedBy());
		ps.addBatch();
	}

}
