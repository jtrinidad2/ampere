/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2011 Adaxa Pty Ltd. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @author Paul Bowden, Adaxa Pty Ltd
 */
public class MSetup_AU extends MSetup {
	
	@Override
	protected MBPGroup createBPGroups(String defaultName) {
		
		//  Create BP Group
		MBPGroup bpg = new MBPGroup (m_ctx, 0, m_trx.getTrxName());
		bpg.setValue("Staff");
		bpg.setName("Staff");
		bpg.setAD_Org_ID(0);
		if (bpg.save())
			m_info.append(Msg.translate(m_lang, "C_BP_Group_ID")).append("=").append(defaultName).append("\n");
		else
			log.log(Level.SEVERE, "BP Group NOT inserted");

		bpg = new MBPGroup (m_ctx, 0, m_trx.getTrxName());
		bpg.setValue("Suppliers");
		bpg.setName("Suppliers");
		bpg.setAD_Org_ID(0);
		if (bpg.save())
			m_info.append(Msg.translate(m_lang, "C_BP_Group_ID")).append("=").append(defaultName).append("\n");
		else
			log.log(Level.SEVERE, "BP Group NOT inserted");
		
		bpg = new MBPGroup (m_ctx, 0, m_trx.getTrxName());
		bpg.setValue("Customers");
		bpg.setName("Customers");
		bpg.setIsDefault(true);
		bpg.setAD_Org_ID(0);
		if (bpg.save())
			m_info.append(Msg.translate(m_lang, "C_BP_Group_ID")).append("=").append(defaultName).append("\n");
		else
			log.log(Level.SEVERE, "BP Group NOT inserted");
		return bpg;
	}
	
	
	@Override
	protected int createTax(int C_Country_ID, String defaultEntry) {
		
		StringBuffer sqlCmd;
		int no;
		//  TaxCategory
		
		MTaxCategory category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Error! Tax Not Resolved");
		category.setIsDefault(true);
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		MTax tax = new MTax (m_ctx, "Error! Tax Not Resolved", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_Both);
		tax.setIsDefault(true);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Entertain 50% Recoverable");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Entertain Purch 10% (50% Recoverable)", new BigDecimal("5.0"), category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setTo_Country_ID(C_Country_ID);  //
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Export Sales (G2)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("GST Capital Purchases (G10)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		

		tax = new MTax (m_ctx, "GST Capital Purchase at 10%", new BigDecimal("10"), category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setC_Country_ID(C_Country_ID);
		tax.setTo_Country_ID(C_Country_ID);  //
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Input Taxed Sales (G4)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Input Taxed Sales 0%", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_SalesTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setC_Country_ID(C_Country_ID);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Other GST Free Sales (G3)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Other GST Free Sales", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_SalesTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setC_Country_ID(C_Country_ID);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Purchases for Input Taxed Sales (G13)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Purchases No GST in Price (G14)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Purchases No GST in Price", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Purchases Private Use (G15)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Purchases Private Use", new BigDecimal("10"), category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setC_Country_ID(C_Country_ID);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("Not Supply");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		tax = new MTax (m_ctx, "Not Supply", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		category = new MTaxCategory(m_ctx, 0, m_trx.getTrxName());
		category.setName("GST (G11)");
		category.setClientOrg(m_client.getAD_Client_ID(), 0);
		category.saveEx();
		
		
		tax = new MTax (m_ctx, "GST Sale at 10%", new BigDecimal("10"), category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_SalesTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setC_Country_ID(C_Country_ID);
		tax.setIsDefault(true);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		tax = new MTax (m_ctx, "GST Purchase at 10%", new BigDecimal("10"), category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setC_Country_ID(C_Country_ID);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		tax = new MTax (m_ctx, "GST Purchase at 0%", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_PurchaseTax);
		tax.setTo_Country_ID(C_Country_ID); 
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		tax = new MTax (m_ctx, "Export Sales", Env.ZERO, category.getC_TaxCategory_ID(), m_trx.getTrxName());
		tax.setSOPOType(MTax.SOPOTYPE_SalesTax);
		tax.setC_Country_ID(C_Country_ID);
		tax.setAD_Org_ID(0);
		tax.setIsDocumentLevel(false);
		if (tax.save())
			m_info.append(Msg.translate(m_lang, "C_Tax_ID"))
				.append("=").append(tax.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Tax NOT inserted");
		
		sqlCmd = new StringBuffer ("INSERT INTO C_TaxCategory_Trl (AD_Language,C_TaxCategory_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy)");
		sqlCmd.append(" SELECT l.AD_Language,t.C_TaxCategory_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, C_TaxCategory t");
		sqlCmd.append(" WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Client_ID=").append(m_client.getAD_Client_ID());
		sqlCmd.append(" AND NOT EXISTS (SELECT * FROM C_TaxCategory_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.C_TaxCategory_ID=t.C_TaxCategory_ID)");
		no = DB.executeUpdate(sqlCmd.toString(), m_trx.getTrxName());
		if (no < 0)
			log.log(Level.SEVERE, "TaxCategory Translation NOT inserted");
		
		return category.getC_TaxCategory_ID();
	}
	
	@Override
	protected void createPaymentTerms() {
		
		MPaymentTerm term = new MPaymentTerm(m_ctx, 0, m_trx.getTrxName());
		term.setNetDays(30);
		term.setValue("30 days");
		term.setName("30 Days from Invoice");
		term.validate();
		if ( term.save() )
			m_info.append(Msg.translate(m_lang, "C_PaymentTerm_ID"))
			.append("=").append(term.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Payment Term NOT inserted");
		
		term = new MPaymentTerm(m_ctx, 0, m_trx.getTrxName());
		term.setIsDefault(true);
		term.setIsDueFixed(true);
		term.setFixMonthCutoff(31);
		term.setFixMonthDay(30);
		term.setFixMonthOffset(1);
		term.setGraceDays(2);
		term.setValue("30 days EOM");
		term.setName("30 Days from End of Month");
		term.validate();
		if ( term.save() )
			m_info.append(Msg.translate(m_lang, "C_PaymentTerm_ID"))
			.append("=").append(term.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Payment Term NOT inserted");

		term = new MPaymentTerm(m_ctx, 0, m_trx.getTrxName());
		term.setNetDays(0);
		term.setValue("Immediate");
		term.setIsNextBusinessDay(true);
		term.setName("Immediate");
		term.validate();
		if ( term.save() )
			m_info.append(Msg.translate(m_lang, "C_PaymentTerm_ID"))
			.append("=").append(term.getName()).append("\n");
		else
			log.log(Level.SEVERE, "Payment Term NOT inserted");
	}

}
