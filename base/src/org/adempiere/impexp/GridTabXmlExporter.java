package org.adempiere.impexp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridTable;
import org.compiere.model.MClient;
import org.compiere.model.MTab;
import org.compiere.model.MUser;
import org.compiere.print.PrintData;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GridTabXmlExporter implements IGridTabExporter
{
	private static CLogger log = CLogger.getCLogger(GridTabXmlExporter.class);

	@Override
	public void export(GridTab gridTab, List<GridTab> childs, boolean isCurrentRowOnly, File file,
			int indxDetailSelected)
	{
		try
		{
			Writer fw = new OutputStreamWriter(new FileOutputStream(file, false), Ini.getCharset());
			BufferedWriter bw = new BufferedWriter(fw);
			StreamResult sr = new StreamResult(bw);
			DOMSource source = new DOMSource(getDocument(gridTab, isCurrentRowOnly, childs));
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = tFactory.newTransformer();
			transformer.transform(source, sr);

		}
		catch (FileNotFoundException e)
		{
			log.log(Level.SEVERE, "(FileNotFoundException)", e);
		}
		catch (TransformerConfigurationException e)
		{
			log.log(Level.SEVERE, "(TransformerConfigurationException)", e);
		}
		catch (TransformerException e)
		{
			log.log(Level.SEVERE, "(TransformerException)", e);
		}
	}

	/**
	 * Get XML Document representation
	 * 
	 * @param gridTab
	 * @param isCurrentRowOnly
	 * @param childs
	 * @return XML document
	 */
	public Document getDocument(GridTab gridTab, boolean isCurrentRowOnly, List<GridTab> childs)
	{
		Document document = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.newDocument();
			document.appendChild(document.createComment(Adempiere.getSummaryAscii()));
		}
		catch (Exception e)
		{
			System.err.println(e);
			e.printStackTrace();
		}
		// Root
		if (document != null)
		{
			MUser user = MUser.get(Env.getCtx());
			MClient client = MClient.get(Env.getCtx());
			StringBuffer sb = new StringBuffer().append(client.get_ID()).append("-").append(client.getValue())
					.append("-").append(client.getName());
			Element root = document.createElement(PrintData.XML_TAG);
			root.setAttribute("Name", gridTab.getTableName());
			root.setAttribute("Client", emptyIfNull(sb.toString()));
			root.setAttribute("AdempiereVersion", emptyIfNull(Adempiere.getVersion()));
			root.setAttribute("DataBaseVersion", emptyIfNull(Adempiere.DB_VERSION));
			root.setAttribute("Author", emptyIfNull(user.getName()));
			root.setAttribute("AuthorEmail", emptyIfNull((user.getEMail())));
			root.setAttribute("CreatedDate", new Date().toString());
			root.setAttribute("UpdatedDate", new Date().toString());
			document.appendChild(root);
			processXML(gridTab, document, root, isCurrentRowOnly, childs);
		}

		return document;
	} // getDocument

	/**
	 * Process PrintData Tree
	 * 
	 * @param gridTab Print Data
	 * @param document document
	 * @param root element to add to
	 * @param childs
	 * @param isCurrentRowOnly
	 */
	private static void processXML(GridTab gridTab, Document document, Element root, boolean currentRowOnly,
			List<GridTab> childs)
	{
		GridField[] gridFields = gridTab.getExportFields();
		if (gridFields.length == 0)
			throw new AdempiereException(gridTab.getName() + ": Did not find any available field to be exported.");
		GridTable girdTableModel = gridTab.getTableModel();
		int start = 0;
		int end = 0;
		if (currentRowOnly)
		{
			start = gridTab.getCurrentRow();
			end = start + 1;
		}
		else
		{
			end = girdTableModel.getRowCount();
		}
		for (int idxrow = start; idxrow < end; idxrow++)
		{
			gridTab.setCurrentRow(idxrow);

			Element row = document.createElement(gridTab.getTableName());
			row.setAttribute("AD_Table_ID", String.valueOf(gridTab.getAD_Table_ID()));
			if (gridTab.getRecord_ID() != -1)
				row.setAttribute("Record_ID", String.valueOf(gridTab.getRecord_ID()));

			root.appendChild(row);

			for (int i = 0; i < gridFields.length; i++)
			{
				GridField field = gridFields[i];

				if (!(field.isDisplayed() || field.isDisplayedGrid()))
				{
					continue;
				}
				if (field.getAD_Column_ID() < 1)
				{
					continue;
				}
				Element element = document.createElement(field.getColumnName());

				if (field.getValue() == null)
					element.setAttribute("isNull", "true");
				Object value = field.getValue();
				if (value instanceof String[] || value instanceof Integer[])
				{
					String svalue = Util.convertArrayToStringForDB(value);
					if (svalue != null && svalue.length() > 0)
						svalue = svalue.replaceAll("([{}])", "\"");
					element.appendChild(document.createTextNode(emptyIfNull(svalue)));
				}
				else
					element.appendChild(document.createTextNode(emptyIfNull(String.valueOf(value))));
				row.appendChild(element);
			} // columns
			if (childs != null && childs.size() > 0)
			{
				for (GridTab detail : childs)
				{
					if (!detail.isLoadComplete())
					{
						detail.initTab(false);
					}

					detail.query(false, 0, 0);
					processXML(detail, document, row, false, null);
				} // childs
			}
		} // rows
	}// processTree

	@Override
	public String getFileExtension()
	{
		return "xml";
	}

	@Override
	public String getFileExtensionLabel()
	{
		return Msg.getMsg(Env.getCtx(), "FileXML");
	}

	@Override
	public String getContentType()
	{
		return "application/xml";
	}

	@Override
	public String getSuggestedFileName(GridTab gridTab)
	{
		return gridTab.getName() + "." + getFileExtension();
	}

	@Override
	public boolean isExportableTab(GridTab gridTab)
	{
		if (!gridTab.isDisplayed())
			return false;

		if (gridTab.getTabLevel() > 1)
			return false;

		if (isValidTabToExport(gridTab) != null)
		{
			return false;
		}

		return true;
	}

	private String isValidTabToExport(GridTab gridTab)
	{
		String result = null;

		MTab tab = new MTab(Env.getCtx(), gridTab.getAD_Tab_ID(), null);

		if (tab.isReadOnly())
			result = Msg.getMsg(Env.getCtx(), "FieldIsReadOnly", new Object[] { gridTab.getName() });

		if (gridTab.getTableName().endsWith("_Acct"))
			result = "Accounting Tab are not exported by default: " + gridTab.getName();

		return result;
	}

	private static String emptyIfNull(String input)
	{
		return (input != null && !input.equalsIgnoreCase("null")) ? input : "";
	}

}
