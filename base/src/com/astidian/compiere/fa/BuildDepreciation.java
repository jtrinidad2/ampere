/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/
package com.astidian.compiere.fa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MGAASDepreciationWorkfile;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Ashley G Ramdass
 * 
 */
public class BuildDepreciation extends SvrProcess
{
    private static final CLogger logger = CLogger
            .getCLogger(BuildDepreciation.class);
    private int p_assetId;

    @Override
    protected void prepare()
    {
        if (getTable_ID() == MAsset.Table_ID)
        {
            p_assetId = getRecord_ID();
        }
        if (getTable_ID() == MGAASDepreciationWorkfile.Table_ID)
        {
            String sql = "SELECT A_Asset_ID FROM GAAS_Depreciation_Workfile "
                    + "WHERE GAAS_Depreciation_Workfile_ID=?";
            p_assetId = DB.getSQLValue(get_TrxName(), sql, getRecord_ID());
        }
    }

    @Override
    protected String doIt() throws Exception
    {
        if (p_assetId <= 0)
        {
            throw new IllegalStateException("No Asset");
        }
        
        Depreciation.updateDepreciationPeriods(getCtx(), p_assetId, null);

        String sql = "SELECT aa.GAAS_Asset_Acct_ID, aa.GAAS_Depreciation_Type"
                + " FROM GAAS_Asset_Acct aa  "
                + " WHERE aa.A_Asset_ID=? AND aa.IsActive='Y'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, p_assetId);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                int assetAcctId = rs.getInt(1);
                String depreciationType = rs.getString(2);
                Depreciation depreciation;

                if ("SL".equals(depreciationType))
                {
                    depreciation = new SLDepreciation(getCtx(), p_assetId,
                            get_TrxName());
                }
                else if ("RB".equals(depreciationType))
                {
                    depreciation = new RBDepreciation(getCtx(), p_assetId,
                            get_TrxName());
                }
                else
                {
                    throw new IllegalStateException(
                            "Depreciation type not yet implemented");
                }
                depreciation.updateDepreciationExpenses(assetAcctId);
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not asset account information", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return "OK";
    }

}
