package org.compiere.model;

import static org.junit.jupiter.api.Assertions.*;

import org.compiere.util.Env;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MColumnTest
{

	private static MTable test_table = null;

	@BeforeAll
	public static void setUp() throws Exception, Exception
	{
		org.compiere.Adempiere.startupEnvironment(false);
		test_table = MTable.get(Env.getCtx(), "Test");
	}

	/**
	 * Test MColumn.getSQLAdd() method
	 */
	@Test
	public void testSQLAddPK()
	{
		MColumn col = test_table.getColumn("Test_ID");
		String sql = col.getSQLAdd(test_table);
		assertEquals("ALTER TABLE Test ADD Test_ID NUMERIC(10) NOT NULL; ALTER TABLE Test ADD CONSTRAINT Test_Key PRIMARY KEY (Test_ID)", sql);
	}
	
	/**
	 * Test MColumn.getSQLAdd() method
	 */
	@Test
	public void testSQLAddMandatoryText()
	{
		MColumn col = test_table.getColumn("Name");
		String sql = col.getSQLAdd(test_table);
		assertEquals("ALTER TABLE Test ADD Name TEXT NOT NULL", sql);
	}
	
	/**
	 * Test MColumn.getSQLAdd() method
	 */
	@Test
	public void testSQLAddMandatoryTimestamp()
	{
		MColumn col = test_table.getColumn("Updated");
		String sql = col.getSQLAdd(test_table);
		assertEquals("ALTER TABLE Test ADD Updated TIMESTAMP NOT NULL", sql);
	}
	
	/**
	 * Test MColumn.getSQLAdd() method
	 */
	@Test
	public void testSQLAddText()
	{
		MColumn col = test_table.getColumn("Help");
		String sql = col.getSQLAdd(test_table);
		assertEquals("ALTER TABLE Test ADD Help TEXT DEFAULT NULL ", sql);
	}


	/**
	 * Test MColumn.getSQLModify() method
	 */
	@Test
	public void testSQLModifyMandatory()
	{
		MColumn col = test_table.getColumn("Help");
		col.setIsMandatory(true);
		String sql = col.getSQLModify(test_table);
		col.setIsMandatory(false);
		assertEquals("INSERT INTO t_alter_column values('test','help','TEXT','NOT NULL','null')", sql);
	}
}	//	MColumnTest
