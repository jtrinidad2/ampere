/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import org.compiere.util.KeyNamePair;

/** Generated Interface for T_Aged_Inventory
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_T_Aged_Inventory 
{

    /** TableName=T_Aged_Inventory */
    public static final String Table_Name = "T_Aged_Inventory";

    /** AD_Table_ID=53603 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_PInstance_ID */
    public static final String COLUMNNAME_AD_PInstance_ID = "AD_PInstance_ID";

	/** Set Process Instance.
	  * Instance of the process
	  */
	public void setAD_PInstance_ID (int AD_PInstance_ID);

	/** Get Process Instance.
	  * Instance of the process
	  */
	public int getAD_PInstance_ID();

	public I_AD_PInstance getAD_PInstance() throws RuntimeException;

    /** Column name Cost */
    public static final String COLUMNNAME_Cost = "Cost";

	/** Set Cost.
	  * Cost information
	  */
	public void setCost (BigDecimal Cost);

	/** Get Cost.
	  * Cost information
	  */
	public BigDecimal getCost();

    /** Column name Month_0 */
    public static final String COLUMNNAME_Month_0 = "Month_0";

	/** Set Month_0	  */
	public void setMonth_0 (BigDecimal Month_0);

	/** Get Month_0	  */
	public BigDecimal getMonth_0();

    /** Column name Month_1 */
    public static final String COLUMNNAME_Month_1 = "Month_1";

	/** Set Month_1	  */
	public void setMonth_1 (BigDecimal Month_1);

	/** Get Month_1	  */
	public BigDecimal getMonth_1();

    /** Column name Month_10 */
    public static final String COLUMNNAME_Month_10 = "Month_10";

	/** Set Month_10	  */
	public void setMonth_10 (BigDecimal Month_10);

	/** Get Month_10	  */
	public BigDecimal getMonth_10();

    /** Column name Month_11 */
    public static final String COLUMNNAME_Month_11 = "Month_11";

	/** Set Month_11	  */
	public void setMonth_11 (BigDecimal Month_11);

	/** Get Month_11	  */
	public BigDecimal getMonth_11();

    /** Column name Month_2 */
    public static final String COLUMNNAME_Month_2 = "Month_2";

	/** Set Month_2	  */
	public void setMonth_2 (BigDecimal Month_2);

	/** Get Month_2	  */
	public BigDecimal getMonth_2();

    /** Column name Month_3 */
    public static final String COLUMNNAME_Month_3 = "Month_3";

	/** Set Month_3	  */
	public void setMonth_3 (BigDecimal Month_3);

	/** Get Month_3	  */
	public BigDecimal getMonth_3();

    /** Column name Month_4 */
    public static final String COLUMNNAME_Month_4 = "Month_4";

	/** Set Month_4	  */
	public void setMonth_4 (BigDecimal Month_4);

	/** Get Month_4	  */
	public BigDecimal getMonth_4();

    /** Column name Month_5 */
    public static final String COLUMNNAME_Month_5 = "Month_5";

	/** Set Month_5	  */
	public void setMonth_5 (BigDecimal Month_5);

	/** Get Month_5	  */
	public BigDecimal getMonth_5();

    /** Column name Month_6 */
    public static final String COLUMNNAME_Month_6 = "Month_6";

	/** Set Month_6	  */
	public void setMonth_6 (BigDecimal Month_6);

	/** Get Month_6	  */
	public BigDecimal getMonth_6();

    /** Column name Month_7 */
    public static final String COLUMNNAME_Month_7 = "Month_7";

	/** Set Month_7	  */
	public void setMonth_7 (BigDecimal Month_7);

	/** Get Month_7	  */
	public BigDecimal getMonth_7();

    /** Column name Month_8 */
    public static final String COLUMNNAME_Month_8 = "Month_8";

	/** Set Month_8	  */
	public void setMonth_8 (BigDecimal Month_8);

	/** Get Month_8	  */
	public BigDecimal getMonth_8();

    /** Column name Month_9 */
    public static final String COLUMNNAME_Month_9 = "Month_9";

	/** Set Month_9	  */
	public void setMonth_9 (BigDecimal Month_9);

	/** Get Month_9	  */
	public BigDecimal getMonth_9();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name Total */
    public static final String COLUMNNAME_Total = "Total";

	/** Set Total	  */
	public void setTotal (BigDecimal Total);

	/** Get Total	  */
	public BigDecimal getTotal();

    /** Column name TotalValue */
    public static final String COLUMNNAME_TotalValue = "TotalValue";

	/** Set TotalValue	  */
	public void setTotalValue (BigDecimal TotalValue);

	/** Get TotalValue	  */
	public BigDecimal getTotalValue();

    /** Column name Usage_12_Months */
    public static final String COLUMNNAME_Usage_12_Months = "Usage_12_Months";

	/** Set Usage_12_Months	  */
	public void setUsage_12_Months (BigDecimal Usage_12_Months);

	/** Get Usage_12_Months	  */
	public BigDecimal getUsage_12_Months();

    /** Column name Usage_18_Months */
    public static final String COLUMNNAME_Usage_18_Months = "Usage_18_Months";

	/** Set Usage_18_Months	  */
	public void setUsage_18_Months (BigDecimal Usage_18_Months);

	/** Get Usage_18_Months	  */
	public BigDecimal getUsage_18_Months();

    /** Column name Usage_24_Months */
    public static final String COLUMNNAME_Usage_24_Months = "Usage_24_Months";

	/** Set Usage_24_Months	  */
	public void setUsage_24_Months (BigDecimal Usage_24_Months);

	/** Get Usage_24_Months	  */
	public BigDecimal getUsage_24_Months();

    /** Column name Usage_3_Months */
    public static final String COLUMNNAME_Usage_3_Months = "Usage_3_Months";

	/** Set Usage_3_Months	  */
	public void setUsage_3_Months (BigDecimal Usage_3_Months);

	/** Get Usage_3_Months	  */
	public BigDecimal getUsage_3_Months();

    /** Column name Usage_6_Months */
    public static final String COLUMNNAME_Usage_6_Months = "Usage_6_Months";

	/** Set Usage_6_Months	  */
	public void setUsage_6_Months (BigDecimal Usage_6_Months);

	/** Get Usage_6_Months	  */
	public BigDecimal getUsage_6_Months();
}
