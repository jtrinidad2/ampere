#!/bin/sh

DB_NAME=${1:-ampere}
DB_USER=${2:-ampere}
DB_HOST=${3:-localhost}
DB_PORT=${4:-5432}

dropdb -U $DB_USER -h $DB_HOST -p $DB_PORT $DB_NAME

createdb -U $DB_USER -h $DB_HOST -p $DB_PORT $DB_NAME

psql -U $DB_USER -h $DB_HOST -p $DB_PORT -d $DB_NAME -f init-db.sql
