copy "a_asset_group" ("a_asset_group_id", "name", "value", "ad_client_id", "ad_org_id", "created", "createdby", "description", "gaas_asset_type", "gaas_depreciatefrom", "gaas_depreciatefromaddition", "gaas_subassetsallowed", "help", "isactive", "iscreateasactive", "isdefault", "isdepreciated", "isoneassetperuom", "isowned", "istrackissues", "processing", "updated", "updatedby") from STDIN;
100	Documentation	\N	11	11	2003-01-24 00:11:50	100	Download Documentation	\N	\N	Y	N	\N	Y	Y	N	Y	N	N	N	\N	2003-01-24 00:11:50	0
50000	Buildings	\N	11	11	2008-05-30 17:05:58	100	Buildings	\N	\N	Y	N	All Buildings	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:05:58	100
50001	Software	\N	11	11	2008-05-30 17:06:01	100	Software	\N	\N	Y	N	Software	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:01	100
50002	Documentation	\N	11	11	2008-05-30 17:06:02	100	Download Documentation	\N	\N	Y	N	\N	Y	Y	N	Y	N	N	N	\N	2008-05-30 17:06:02	100
50003	Data Processing Equipment	\N	11	11	2008-05-30 17:06:03	100	Data Processing Equipment	\N	\N	Y	N	Group defines Data Processing Equipment	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:03	100
50004	Furniture	\N	11	11	2008-05-30 17:06:04	100	Furniture	\N	\N	Y	N	Office furniture including office and shop	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:04	100
50005	Fixtures	\N	11	11	2008-05-30 17:06:05	100	Fixtures	\N	\N	Y	N	Fixtures including tools & dies	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:05	100
50006	Vehicles	\N	11	11	2008-05-30 17:06:06	100	Trucks Cars Vans Forklifts	\N	\N	Y	N	All vehicles	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:06	100
50007	Equipment	\N	11	11	2008-05-30 17:06:10	100	Equipment	\N	\N	Y	N	Light equipment including  test equipment and computers	Y	Y	N	Y	N	Y	N	\N	2008-05-30 17:06:10	100
\.
