copy "c_country" ("c_country_id", "name", "ad_client_id", "ad_language", "ad_org_id", "allowcitiesoutoflist", "capturesequence", "c_currency_id", "countrycode", "created", "createdby", "description", "displaysequence", "displaysequencelocal", "expressionbankaccountno", "expressionbankroutingno", "expressionphone", "expressionpostal", "expressionpostal_add", "haspostal_add", "hasregion", "isactive", "isaddresslineslocalreverse", "isaddresslinesreverse", "ispostcodelookup", "lookupclassname", "lookupclientid", "lookuppassword", "lookupurl", "mediasize", "regionname", "updated", "updatedby") from STDIN;
100	United States	0	en_US	0	Y	@A1@ @A2@ @A3@ @A4@ @C@, @R@ @P@ @CO@	100	US	1999-12-20 09:55:35	0	United States of America	@C@, @R@ @P@	\N	\N	\N	(000) 000-0000	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	State	2000-01-02 00:00:00	0
101	Germany - Deutschland	0	de_DE	0	Y	@A1@ @A2@ @A3@ @A4@ D-@P@ @R@ @C@ @CO@	102	DE	1999-12-20 09:56:40	0	\N	D-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Bundesland	2010-04-19 17:30:18	100
102	France	0	fr_FR	0	Y	@A1@ @A2@ @A3@ @A4@ F-@P@ @C@ @CO@	102	FR	2000-11-28 17:23:06	0	France	F-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
103	Belgium	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ B-@P@ @C@ @CO@	102	BE	2000-11-28 17:23:54	0	\N	B-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
105	Nederland	0	nl_NL	0	Y	@A1@ @A2@ @A3@ @A4@ @P@ @C@ @CO@	102	NL	2000-11-28 17:25:11	0	\N	@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2005-06-27 16:14:58	100
106	Spain	0	es_ES	0	Y	@A1@ @A2@ @A3@ @A4@ E-@P@ @C@ @R@ @CO@	102	ES	2000-11-28 17:25:42	0	\N	E-@P@ @C@ @R@	@P@ @C@ @R@	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	\N	2006-04-13 13:29:48	100
107	Switzerland	0	de_CH	0	Y	@A1@ @A2@ @A3@ @A4@ CH-@P@ @C@ @CO@	318	CH	2000-11-28 17:26:22	0	\N	CH-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
108	Austria	0	de_AT	0	Y	@A1@ @A2@ @A3@ @A4@ A-@P@ @C@ @CO@	102	AT	2000-11-28 17:27:26	0	Österreich	A-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
109	Canada	0	en_US	0	Y	@A1@ @A2@ @A3@ @A4@ @C@, @R@ @P@ @CO@	116	CA	2003-01-08 13:06:10	0	\N	@C@, @R@ @P@	\N	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Province	2000-01-02 00:00:00	0
110	Afghanistan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	195	AF	2003-03-09 00:00:00	0	Afghanistan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
111	Albania	0	sq_AL	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	271	AL	2003-03-09 00:00:00	0	Albania	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
112	Algeria	0	ar_DZ	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	235	DZ	2003-03-09 00:00:00	0	Algeria	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
113	American Samoa	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	AS	2003-03-09 00:00:00	0	American Samoa	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
114	Andorra	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	AD	2003-03-09 00:00:00	0	Andorra	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
115	Angola	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	144	AO	2003-03-09 00:00:00	0	Angola	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
116	Anguilla	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	AI	2003-03-09 00:00:00	0	Anguilla	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
117	Antarctica	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	AQ	2003-03-09 00:00:00	0	Antarctica	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
118	Antigua And Barbuda	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	AG	2003-03-09 00:00:00	0	Antigua And Barbuda	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
119	Argentina	0	es_AR	0	Y	@A1@ @A2@ @A3@ @A4@  @C@ @P@, @R@  @CO@	118	AR	2003-03-09 00:00:00	0	Argentina	 @C@ @P@, @R@ 	\N	\N	\N	\N	L000LLL	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Provincia 	2005-05-20 17:16:37	100
120	Armenia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	143	AM	2003-03-09 00:00:00	0	Armenia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
121	Aruba	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	196	AW	2003-03-09 00:00:00	0	Aruba	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
122	Australia	0	en_AU	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @R@ @P@ @CO@	120	AU	2003-03-09 00:00:00	0	Australia	@C@ @R@ @P@	\N	\N	\N	(00) 0000 0000	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	State	2000-01-02 00:00:00	0
124	Azerbaijan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	145	AZ	2003-03-09 00:00:00	0	Azerbaijan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
125	Bahamas	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	199	BS	2003-03-09 00:00:00	0	Bahamas	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
126	Bahrain	0	ar_BH	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	200	BH	2003-03-09 00:00:00	0	Bahrain	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
127	Bangladesh	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	322	BD	2003-03-09 00:00:00	0	Bangladesh	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
128	Barbados	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	202	BB	2003-03-09 00:00:00	0	Barbados	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
129	Belarus	0	be_BY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	203	BY	2003-03-09 00:00:00	0	Belarus	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
131	Belize	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	207	BZ	2003-03-09 00:00:00	0	Belize	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
132	Benin	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	BJ	2003-03-09 00:00:00	0	Benin	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
133	Bermuda	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	201	BM	2003-03-09 00:00:00	0	Bermuda	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
134	Bhutan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	304	BT	2003-03-09 00:00:00	0	Bhutan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
135	Bolivia	0	es_BO	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	146	BO	2003-03-09 00:00:00	0	Bolivia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
136	Bosnia And Herzegovina	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	268	BA	2003-03-09 00:00:00	0	Bosnia And Herzegovina	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
137	Botswana	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	290	BW	2003-03-09 00:00:00	0	Botswana	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
138	Bouvet Island	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	BV	2003-03-09 00:00:00	0	Bouvet Island	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
139	Brazil	0	pt_BR	0	Y	@A1@ @A2@ @A3@ @A4@ @R@ @C@, @P@ @CO@	297	BR	2003-03-09 00:00:00	0	Brazil	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Estado	2000-01-02 00:00:00	0
140	British Indian Ocean Territory	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	IO	2003-03-09 00:00:00	0	British Indian Ocean Territory	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
141	Brunei Darussalam	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	198	BN	2003-03-09 00:00:00	0	Brunei Darussalam	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
142	Bulgaria	0	bg_BG	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	279	BG	2003-03-09 00:00:00	0	Bulgaria	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
143	Burkina Faso	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	BF	2003-03-09 00:00:00	0	Burkina Faso	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
144	Burundi	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	253	BI	2003-03-09 00:00:00	0	Burundi	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
145	Cambodia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	231	KH	2003-03-09 00:00:00	0	Cambodia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
146	Cameroon	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	CM	2003-03-09 00:00:00	0	Cameroon	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
148	Cape Verde	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	210	CV	2003-03-09 00:00:00	0	Cape Verde	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
149	Cayman Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	229	KY	2003-03-09 00:00:00	0	Cayman Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
150	Central African Republic	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	CF	2003-03-09 00:00:00	0	Central African Republic	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
151	Chad	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	TD	2003-03-09 00:00:00	0	Chad	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
152	Chile	0	es_CL	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	228	CL	2003-03-09 00:00:00	0	Chile	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
153	China	0	zh_CN	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	332	CN	2003-03-09 00:00:00	0	China	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
154	Christmas Island	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	CX	2003-03-09 00:00:00	0	Christmas Island	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
155	Cocos (keeling) Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	CC	2003-03-09 00:00:00	0	Cocos (keeling) Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
156	Colombia	0	es_CO	0	Y	@CO@ @R!@ @C!@ @A1!@ @A2@ @A3@ @A4@ @P@	230	CO	2003-03-09 00:00:00	0	Colombia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Department	2009-09-17 21:57:21	100
157	Comoros	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	211	KM	2003-03-09 00:00:00	0	Comoros	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
158	Congo	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	CG	2003-03-09 00:00:00	0	Congo	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
159	Congo The Democratic Republic Of The	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	147	CD	2003-03-09 00:00:00	0	Congo The Democratic Republic Of The	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
160	Cook Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	121	CK	2003-03-09 00:00:00	0	Cook Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
161	Costa Rica	0	es_CR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	336	CR	2003-03-09 00:00:00	0	Costa Rica	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
162	Cote D'ivoire	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	CI	2003-03-09 00:00:00	0	Cote D'ivoire	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
163	Croatia	0	hr_HR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	259	HR	2003-03-09 00:00:00	0	Croatia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
164	Cuba	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	232	CU	2003-03-09 00:00:00	0	Cuba	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
165	Cyprus	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	CY	2003-03-09 00:00:00	0	Cyprus	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2008-02-25 13:28:26	100
166	Czech Republic	0	cs_CZ	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	148	CZ	2003-03-09 00:00:00	0	Czech Republic	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
167	Denmark	0	da_DK	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	241	DK	2003-03-09 00:00:00	0	Denmark	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
168	Djibouti	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	237	DJ	2003-03-09 00:00:00	0	Djibouti	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
169	Dominica	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	DM	2003-03-09 00:00:00	0	Dominica	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
170	Dominican Republic	0	es_DO	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	298	DO	2003-03-09 00:00:00	0	Dominican Republic	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
171	Ecuador	0	es_EC	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	EC	2003-03-09 00:00:00	0	Ecuador	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
172	Egypt	0	ar_EG	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	164	EG	2003-03-09 00:00:00	0	Egypt	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
173	El Salvador	0	es_SV	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	171	SV	2003-03-09 00:00:00	0	El Salvador	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
174	Equatorial Guinea	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	GQ	2003-03-09 00:00:00	0	Equatorial Guinea	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
175	Eritrea	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	270	ER	2003-03-09 00:00:00	0	Eritrea	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
176	Estonia	0	et_EE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	285	EE	2003-03-09 00:00:00	0	Estonia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
177	Ethiopia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	204	ET	2003-03-09 00:00:00	0	Ethiopia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
178	Falkland Islands (malvinas)	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	165	FK	2003-03-09 00:00:00	0	Falkland Islands (malvinas)	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
179	Faroe Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	241	FO	2003-03-09 00:00:00	0	Faroe Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
180	Fiji	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	252	FJ	2003-03-09 00:00:00	0	Fiji	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
181	Finland	0	fi_FI	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	FI	2003-03-09 00:00:00	0	Finland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
183	French Guiana	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	GF	2003-03-09 00:00:00	0	French Guiana	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
184	French Polynesia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	225	PF	2003-03-09 00:00:00	0	French Polynesia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
185	French Southern Territories	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	TF	2003-03-09 00:00:00	0	French Southern Territories	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
186	Gabon	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	214	GA	2003-03-09 00:00:00	0	Gabon	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
187	Gambia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	233	GM	2003-03-09 00:00:00	0	Gambia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
188	Georgia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	149	GE	2003-03-09 00:00:00	0	Georgia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
190	Ghana	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	170	GH	2003-03-09 00:00:00	0	Ghana	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
191	Gibraltar	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	166	GI	2003-03-09 00:00:00	0	Gibraltar	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
192	Greece	0	el_GR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	GR	2003-03-09 00:00:00	0	Greece	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
193	Greenland	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	241	GL	2003-03-09 00:00:00	0	Greenland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
194	Grenada	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	GD	2003-03-09 00:00:00	0	Grenada	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
195	Guadeloupe	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	GP	2003-03-09 00:00:00	0	Guadeloupe	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
196	Guam	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	GU	2003-03-09 00:00:00	0	Guam	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
197	Guatemala	0	es_GT	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	292	GT	2003-03-09 00:00:00	0	Guatemala	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
198	Guinea	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	337	GN	2003-03-09 00:00:00	0	Guinea	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
199	Guinea-bissau	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	338	GW	2003-03-09 00:00:00	0	Guinea-bissau	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
200	Guyana	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	257	GY	2003-03-09 00:00:00	0	Guyana	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
201	Haiti	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	256	HT	2003-03-09 00:00:00	0	Haiti	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
202	Heard Island And Mcdonald Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	HM	2003-03-09 00:00:00	0	Heard Island And Mcdonald Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
203	Holy See (vatican City State)	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	VA	2003-03-09 00:00:00	0	Holy See (vatican City State)	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
204	Honduras	0	es_HN	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	272	HN	2003-03-09 00:00:00	0	Honduras	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
205	Hong Kong	0	zh_HK	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	258	HK	2003-03-09 00:00:00	0	Hong Kong	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
206	Hungary	0	hu_HU	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	255	HU	2003-03-09 00:00:00	0	Hungary	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
207	Iceland	0	is_IS	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	261	IS	2003-03-09 00:00:00	0	Iceland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
208	India	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	304	IN	2003-03-09 00:00:00	0	India	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
209	Indonesia	0	in_ID	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	303	ID	2003-03-09 00:00:00	0	Indonesia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2010-03-18 19:08:42	100
210	Iran Islamic Republic Of	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	150	IR	2003-03-09 00:00:00	0	Iran Islamic Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
211	Iraq	0	ar_IQ	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	260	IQ	2003-03-09 00:00:00	0	Iraq	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
212	Ireland	0	en_IE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	IE	2003-03-09 00:00:00	0	Ireland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
213	Israel	0	iw_IL	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	286	IL	2003-03-09 00:00:00	0	Israel	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
214	Italy	0	it_IT	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	IT	2003-03-09 00:00:00	0	Italy	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
215	Jamaica	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	262	JM	2003-03-09 00:00:00	0	Jamaica	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
216	Japan	0	ja_JP	0	Y	@A1@ @A2@ @A3@ @A4@ @C@\\n@R@\\n@P@-@A@ @CO@	113	JP	2003-03-09 00:00:00	0	Japan	@C@\\n@R@\\n@P@-@A@	@P@-@A@\\n@R@\\n@C@	\N	\N	\N	\N	\N	Y	Y	Y	Y	N	N	\N	\N	\N	\N	\N	Prefecture	2006-02-23 16:16:56	100
217	Jordan	0	ar_JO	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	263	JO	2003-03-09 00:00:00	0	Jordan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
218	Kazakhstan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	341	KZ	2003-03-09 00:00:00	0	Kazakhstan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
219	Kenya	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	266	KE	2003-03-09 00:00:00	0	Kenya	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
220	Kiribati	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	KI	2003-03-09 00:00:00	0	Kiribati	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
221	Korea Democratic People's Republic Of	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	340	KP	2003-03-09 00:00:00	0	Korea Democratic People's Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
222	Korea Republic Of	0	ko_KR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	330	KR	2003-03-09 00:00:00	0	Korea Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
223	Kuwait	0	ar_KW	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	267	KW	2003-03-09 00:00:00	0	Kuwait	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
224	Kyrgyzstan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	339	KG	2003-03-09 00:00:00	0	Kyrgyzstan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
225	Lao People's Democratic Republic	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	269	LA	2003-03-09 00:00:00	0	Lao People's Democratic Republic	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
226	Latvia	0	lv_LV	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	278	LV	2003-03-09 00:00:00	0	Latvia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
227	Lebanon	0	ar_LB	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	342	LB	2003-03-09 00:00:00	0	Lebanon	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
228	Lesotho	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	294	LS	2003-03-09 00:00:00	0	Lesotho	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
229	Liberia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	127	LR	2003-03-09 00:00:00	0	Liberia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
230	Libyan Arab Jamahiriya	0	ar_LY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	275	LY	2003-03-09 00:00:00	0	Libyan Arab Jamahiriya	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
231	Liechtenstein	0	de_CH	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	318	LI	2003-03-09 00:00:00	0	Liechtenstein	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
232	Lithuania	0	lt_LT	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	151	LT	2003-03-09 00:00:00	0	Lithuania	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
233	Luxembourg	0	fr_LU	0	Y	@A1@ @A2@ @A3@ @A4@ L-@P@ @C@ @CO@	102	LU	2003-03-09 00:00:00	0	Luxembourg	L-@P@ @C@	@P@ @C@	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
234	Macao	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	291	MO	2003-03-09 00:00:00	0	Macao	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
235	Macedonia Former Yugoslav Republic Of	0	mk_MK	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	282	MK	2003-03-09 00:00:00	0	Macedonia Former Yugoslav Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
236	Madagascar	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	254	MG	2003-03-09 00:00:00	0	Madagascar	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
237	Malawi	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	281	MW	2003-03-09 00:00:00	0	Malawi	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
238	Malaysia	0	ms_MY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	301	MY	2003-03-09 00:00:00	0	Malaysia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2010-03-18 19:17:36	100
239	Maldives	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	299	MV	2003-03-09 00:00:00	0	Maldives	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
240	Mali	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	ML	2003-03-09 00:00:00	0	Mali	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
241	Malta	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	MT	2003-03-09 00:00:00	0	Malta	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2008-02-25 13:28:46	100
242	Marshall Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	MH	2003-03-09 00:00:00	0	Marshall Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
243	Martinique	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	MQ	2003-03-09 00:00:00	0	Martinique	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
244	Mauritania	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	327	MR	2003-03-09 00:00:00	0	Mauritania	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
245	Mauritius	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	280	MU	2003-03-09 00:00:00	0	Mauritius	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
246	Mayotte	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	YT	2003-03-09 00:00:00	0	Mayotte	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
247	Mexico	0	es_MX	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @R@ @P@ @CO@	130	MX	2003-03-09 00:00:00	0	Mexico	@C@,  @R@ @P@	\N	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	State	2005-05-07 20:51:55	100
248	Micronesia Federated States Of	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	FM	2003-03-09 00:00:00	0	Micronesia Federated States Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
249	Moldova Republic Of	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	152	MD	2003-03-09 00:00:00	0	Moldova Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
250	Monaco	0	fr_FR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	MC	2003-03-09 00:00:00	0	Monaco	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
251	Mongolia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	326	MN	2003-03-09 00:00:00	0	Mongolia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
252	Montserrat	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	MS	2003-03-09 00:00:00	0	Montserrat	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
253	Morocco	0	ar_MA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	239	MA	2003-03-09 00:00:00	0	Morocco	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
254	Mozambique	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	283	MZ	2003-03-09 00:00:00	0	Mozambique	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
255	Myanmar	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	264	MM	2003-03-09 00:00:00	0	Myanmar	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
256	Namibia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	294	NA	2003-03-09 00:00:00	0	Namibia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
257	Nauru	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	NR	2003-03-09 00:00:00	0	Nauru	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
258	Nepal	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	288	NP	2003-03-09 00:00:00	0	Nepal	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
260	Netherlands Antilles	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	284	AN	2003-03-09 00:00:00	0	Netherlands Antilles	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
261	New Caledonia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	225	NC	2003-03-09 00:00:00	0	New Caledonia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
262	New Zealand	0	en_NZ	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	121	NZ	2003-03-09 00:00:00	0	New Zealand	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
263	Nicaragua	0	es_NI	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	209	NI	2003-03-09 00:00:00	0	Nicaragua	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
264	Niger	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	NE	2003-03-09 00:00:00	0	Niger	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
265	Nigeria	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	343	NG	2003-03-09 00:00:00	0	Nigeria	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
266	Niue	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	121	NU	2003-03-09 00:00:00	0	Niue	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
267	Norfolk Island	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	NF	2003-03-09 00:00:00	0	Norfolk Island	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
268	Northern Mariana Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	MP	2003-03-09 00:00:00	0	Northern Mariana Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
269	Norway	0	no_NO	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	287	NO	2003-03-09 00:00:00	0	Norway	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
270	Oman	0	ar_OM	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	302	OM	2003-03-09 00:00:00	0	Oman	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
271	Pakistan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	306	PK	2003-03-09 00:00:00	0	Pakistan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
272	Palau	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	PW	2003-03-09 00:00:00	0	Palau	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
273	Palestinian Territory Occupied	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	PS	2003-03-09 00:00:00	0	Palestinian Territory Occupied	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
274	Panama	0	es_PA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	197	PA	2003-03-09 00:00:00	0	Panama	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
275	Papua New Guinea	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	265	PG	2003-03-09 00:00:00	0	Papua New Guinea	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
276	Paraguay	0	es_PY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	344	PY	2003-03-09 00:00:00	0	Paraguay	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
277	Peru	0	es_PE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	308	PE	2003-03-09 00:00:00	0	Peru	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
278	Philippines	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	153	PH	2003-03-09 00:00:00	0	Philippines	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
279	Pitcairn	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	PN	2003-03-09 00:00:00	0	Pitcairn	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
280	Poland	0	pl_PL	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	154	PL	2003-03-09 00:00:00	0	Poland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
281	Portugal	0	pt_PT	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	PT	2003-03-09 00:00:00	0	Portugal	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
282	Puerto Rico	0	es_PR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	PR	2003-03-09 00:00:00	0	Puerto Rico	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
283	Qatar	0	ar_QA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	293	QA	2003-03-09 00:00:00	0	Qatar	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
284	Reunion	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	RE	2003-03-09 00:00:00	0	Reunion	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
285	Romania	0	ro_RO	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	273	RO	2003-03-09 00:00:00	0	Romania	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
286	Russian Federation	0	ru_RU	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @R@ @P@ @CO@	155	RU	2003-03-09 00:00:00	0	Russian Federation	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	Y	Y	N	N	N	\N	\N	\N	\N	\N	Federal Subject	2010-12-18 12:46:11	100
287	Rwanda	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	300	RW	2003-03-09 00:00:00	0	Rwanda	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
288	Saint Helena	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	167	SH	2003-03-09 00:00:00	0	Saint Helena	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
289	Saint Kitts And Nevis	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	KN	2003-03-09 00:00:00	0	Saint Kitts And Nevis	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
290	Saint Lucia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	LC	2003-03-09 00:00:00	0	Saint Lucia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
291	Saint Pierre And Miquelon	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	PM	2003-03-09 00:00:00	0	Saint Pierre And Miquelon	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
292	Saint Vincent And The Grenadines	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	244	VC	2003-03-09 00:00:00	0	Saint Vincent And The Grenadines	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
293	Samoa	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	331	WS	2003-03-09 00:00:00	0	Samoa	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
294	San Marino	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	102	SM	2003-03-09 00:00:00	0	San Marino	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
295	Sao Tome And Principe	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	236	ST	2003-03-09 00:00:00	0	Sao Tome And Principe	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
296	Saudi Arabia	0	ar_SA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	317	SA	2003-03-09 00:00:00	0	Saudi Arabia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
297	Senegal	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	SN	2003-03-09 00:00:00	0	Senegal	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
298	Seychelles	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	316	SC	2003-03-09 00:00:00	0	Seychelles	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
299	Sierra Leone	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	276	SL	2003-03-09 00:00:00	0	Sierra Leone	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
300	Singapore	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	307	SG	2003-03-09 00:00:00	0	Singapore	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
301	Slovakia	0	sk_SK	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	312	SK	2003-03-09 00:00:00	0	Slovakia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
302	Slovenia	0	sl_SI	0	Y	@A1@ @A2@ @A3@ @A4@ @P@ @C@ @CO@	102	SI	2003-03-09 00:00:00	0	Slovenia	@P@ @C@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2008-02-25 13:28:06	100
303	Solomon Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	310	SB	2003-03-09 00:00:00	0	Solomon Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
304	Somalia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	315	SO	2003-03-09 00:00:00	0	Somalia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
305	South Africa	0	en_ZA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	294	ZA	2003-03-09 00:00:00	0	South Africa	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
306	South Georgia And The South Sandwich Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	GS	2003-03-09 00:00:00	0	South Georgia And The South Sandwich Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
308	Sri Lanka	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	313	LK	2003-03-09 00:00:00	0	Sri Lanka	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
309	Sudan	0	ar_SD	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	156	SD	2003-03-09 00:00:00	0	Sudan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
310	Suriname	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	309	SR	2003-03-09 00:00:00	0	Suriname	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
311	Svalbard And Jan Mayen	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	SJ	2003-03-09 00:00:00	0	Svalbard And Jan Mayen	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
312	Swaziland	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	274	SZ	2003-03-09 00:00:00	0	Swaziland	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
313	Sweden	0	sv_SE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	311	SE	2003-03-09 00:00:00	0	Sweden	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
315	Syrian Arab Republic	0	ar_SY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	168	SY	2003-03-09 00:00:00	0	Syrian Arab Republic	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
316	Taiwan	0	zh_TW	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	289	TW	2003-03-09 00:00:00	0	Taiwan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
317	Tajikistan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	157	TJ	2003-03-09 00:00:00	0	Tajikistan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
318	Tanzania United Republic Of	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	324	TZ	2003-03-09 00:00:00	0	Tanzania United Republic Of	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
319	Thailand	0	th_TH	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	206	TH	2003-03-09 00:00:00	0	Thailand	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
320	Timor-leste	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	TL	2003-03-09 00:00:00	0	Timor-leste	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
321	Togo	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	212	TG	2003-03-09 00:00:00	0	Togo	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
322	Tokelau	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	121	TK	2003-03-09 00:00:00	0	Tokelau	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
323	Tonga	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	320	TO	2003-03-09 00:00:00	0	Tonga	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
324	Trinidad And Tobago	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	325	TT	2003-03-09 00:00:00	0	Trinidad And Tobago	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
325	Tunisia	0	ar_TN	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	321	TN	2003-03-09 00:00:00	0	Tunisia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
326	Turkey	0	tr_TR	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	323	TR	2003-03-09 00:00:00	0	Turkey	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
327	Turkmenistan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	158	TM	2003-03-09 00:00:00	0	Turkmenistan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
328	Turks And Caicos Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	TC	2003-03-09 00:00:00	0	Turks And Caicos Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
329	Tuvalu	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	120	TV	2003-03-09 00:00:00	0	Tuvalu	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
330	Uganda	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	328	UG	2003-03-09 00:00:00	0	Uganda	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
331	Ukraine	0	uk_UA	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	160	UA	2003-03-09 00:00:00	0	Ukraine	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
332	United Arab Emirates	0	ar_AE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	238	AE	2003-03-09 00:00:00	0	United Arab Emirates	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
333	United Kingdom	0	en_GB	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	114	GB	2003-03-09 00:00:00	0	United Kingdom	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
335	United States Minor Outlying Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	UM	2003-03-09 00:00:00	0	United States Minor Outlying Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
336	Uruguay	0	es_UY	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	142	UY	2003-03-09 00:00:00	0	Uruguay	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
337	Uzbekistan	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	161	UZ	2003-03-09 00:00:00	0	Uzbekistan	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
338	Vanuatu	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	329	VU	2003-03-09 00:00:00	0	Vanuatu	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
339	Venezuela	0	es_VE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	205	VE	2003-03-09 00:00:00	0	Venezuela	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
340	Viet Nam	0	vi_VN	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	234	VN	2003-03-09 00:00:00	0	Viet Nam	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2005-07-25 10:22:34	100
341	Virgin Islands British	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	VG	2003-03-09 00:00:00	0	Virgin Islands British	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
342	Virgin Islands U.s.	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	100	VI	2003-03-09 00:00:00	0	Virgin Islands U.s.	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
343	Wallis And Futuna	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	225	WF	2003-03-09 00:00:00	0	Wallis And Futuna	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
344	Western Sahara	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	\N	EH	2003-03-09 00:00:00	0	Western Sahara	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
345	Yemen	0	ar_YE	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	333	YE	2003-03-09 00:00:00	0	Yemen	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
346	Yugoslavia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	240	YU	2003-03-09 00:00:00	0	Yugoslavia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	N	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
347	Zambia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	335	ZM	2003-03-09 00:00:00	0	Zambia	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
348	Zimbabwe	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@,  @P@ @CO@	334	ZW	2003-03-09 00:00:00	0	Zimbabwe	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
349	Serbia	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@, @P@ @CO@	347	RS	2003-03-09 00:00:00	0	Serbia	@C@, @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
350	Montenegro	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@, @P@ @CO@	102	ME	2003-03-09 00:00:00	0	Montenegro	@C@, @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	\N	2000-01-02 00:00:00	0
50000	Åland Islands	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	AX	2010-03-18 19:14:35	100	Åland Islands	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:15:45	100
50001	Saint Barthélemy	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	BL	2010-03-18 19:16:02	100	Saint Barthélemy	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:16:02	100
50002	Guernsey	0	en_US	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	GG	2010-03-18 19:16:25	100	\N	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:16:25	100
50003	Isle of Man	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	IM	2010-03-18 19:16:46	100	Isle of Man	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:16:46	100
50004	Jersey	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	JE	2010-03-18 19:17:04	100	Jersey	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:17:04	100
50005	Saint Martin	0	\N	0	Y	@A1@ @A2@ @A3@ @A4@ @C@ @P@ @CO@	\N	MF	2010-03-18 19:17:19	100	Saint Martin	@C@,  @P@	\N	\N	\N	\N	\N	\N	N	N	Y	N	N	N	\N	\N	\N	\N	\N	State	2010-03-18 19:17:19	100
\.
