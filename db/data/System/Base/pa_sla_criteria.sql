copy "pa_sla_criteria" ("pa_sla_criteria_id", "name", "ad_client_id", "ad_org_id", "classname", "created", "createdby", "description", "help", "isactive", "ismanual", "updated", "updatedby") from STDIN;
101	Delivery Accuracy	0	0	org.compiere.sla.DeliveryAccuracy	2004-07-07 20:16:05	0	How accurate is the promise date?	The measure are the average days between promise date (PO/SO) and delivery date (Material receipt/shipment)\nIt is positive if before, negative if later. The lower the number, the better	Y	N	2000-01-02 00:00:00	0
\.
