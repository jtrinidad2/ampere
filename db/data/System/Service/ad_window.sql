copy "ad_window" ("ad_window_id", "name", "ad_client_id", "ad_color_id", "ad_image_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "isdefault", "issotrx", "processing", "updated", "updatedby", "windowtype", "winheight", "winwidth") from STDIN;
220	Service Level	0	\N	\N	0	2001-05-13 12:18:34	0	Maintain Service Levels	Service	Service Levels are generated when an invoice with products based on revenue recognition rules are generated.  You need to update the actual service level by adding an additional line.	Y	Y	N	Y	N	2005-02-11 19:45:46	100	M	\N	\N
234	Expense Type	0	\N	\N	0	2002-06-15 21:53:30	0	Maintain Expense Report Types	Service	\N	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
235	Expense Report	0	\N	\N	0	2002-06-15 22:08:17	0	Time and Expense Report	Service	The time and expense report allows you to capture time spent on a project, billable time and to claim expenses.	Y	N	N	Y	N	2000-01-02 00:00:00	0	T	\N	\N
236	Resource	0	\N	\N	0	2002-06-15 22:20:43	0	Maintain Resources	Service	Maintain your Resources. The product for the resource is automatically created and synchronized. Update Name, Unit of Measure, etc. in the Resource and don't change it in the product.	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
242	Expenses (to be invoiced)	0	\N	\N	0	2002-07-14 18:54:12	0	View expenses and charges not invoiced to customers	Service	Before invoicing to customers, check the expense lines to be invoiced	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
253	Training	0	\N	\N	0	2003-01-23 01:10:53	0	Repeated Training	Service	The training may have multiple actual classes	Y	Y	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
254	Expenses (not reimbursed)	0	\N	\N	0	2003-02-06 12:27:51	0	View expenses and charges not reimbursed	Service	Before reimbursing expenses, check the open expense items	Y	N	N	N	N	2000-01-02 00:00:00	0	M	\N	\N
272	Time Type	0	\N	\N	0	2003-06-02 00:02:59	0	Maintain Time Recording Type	Service	Maintain different types of time for reporting	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
53350	Timesheet Entry	0	\N	\N	0	2014-03-18 00:50:28	100	\N	Service	\N	Y	N	N	Y	N	2014-03-18 00:50:28	100	M	\N	\N
\.
