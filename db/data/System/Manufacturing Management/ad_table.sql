copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
117	AD_Workflow	Workflow	6	0	0	\N	113	\N	1999-05-21 00:00:00	0	Workflow or combination of tasks	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	125	\N	L	\N	2007-12-17 03:05:38	0
480	S_ResourceType	Resource Type	3	0	0	\N	237	\N	2002-06-15 21:02:57	0	\N	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	80	\N	L	\N	2007-12-17 01:34:54	0
665	M_DistributionListLine	Distribution List Line	3	0	0	\N	\N	\N	2004-02-19 10:29:53	0	Distribution List Line with Business Partner and Quantity/Percentage	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
666	M_DistributionList	Distribution List	3	0	0	\N	\N	\N	2004-02-19 10:29:53	0	Distribution Lists allow to distribute products to a selected list of partners	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
712	M_DistributionRun	Distribution Run	3	0	0	\N	\N	\N	2004-04-01 17:19:08	0	Distribution Run create Orders to distribute products to a selected list of partners	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
713	M_DistributionRunLine	Distribution Run Line	3	0	0	\N	\N	\N	2004-04-01 17:19:08	0	Distribution Run Lines define Distribution List, the Product and Quantiries	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
720	M_Forecast	Forecast	3	0	0	\N	328	\N	2004-04-17 11:09:19	0	Material Forecast	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2008-06-25 22:50:47	0
722	M_ForecastLine	Forecast Line	3	0	0	\N	328	\N	2004-04-17 11:09:19	0	Forecast Line	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2008-06-25 22:51:19	0
737	M_MovementLineConfirm	Move Line Confirm	1	0	0	\N	333	\N	2004-06-17 11:24:41	0	Inventory Move Line Confirmation	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-04-26 20:41:33	100
738	M_MovementConfirm	Move Confirm	1	0	0	\N	333	\N	2004-06-17 11:24:42	0	Inventory Move Confirmation	Manufacturing Management	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2005-04-26 20:41:26	100
799	M_ChangeNotice	Change Notice	3	0	0	\N	\N	\N	2005-05-15 14:52:45	100	\N	Manufacturing Management	\N	N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2007-12-16 22:25:02	0
800	M_ChangeRequest	Change Request	3	0	0	\N	\N	\N	2005-05-15 14:57:47	100	\N	Manufacturing Management	\N	N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2007-12-16 22:35:13	0
53142	RV_M_Forecast	Forecast Report	3	0	0	\N	\N	\N	2008-06-25 22:51:33	0	\N	Manufacturing Management	\N	N	Y	Y	N	N	N	N	Y	\N	\N	L	\N	2008-06-25 22:51:35	0
53143	RV_M_Forecast_Period	Forecast Report for Period	3	0	0	\N	\N	\N	2008-06-25 22:52:05	0	\N	Manufacturing Management	\N	N	Y	Y	N	N	N	N	Y	\N	\N	L	\N	2008-06-25 22:52:07	0
\.
