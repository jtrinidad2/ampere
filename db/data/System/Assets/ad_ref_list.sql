copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
53356	Amount	0	0	53255	2008-05-30 16:35:33	100	Depreciation entered in field is an amount	Assets	Y	2008-05-30 16:35:33	100	\N	\N	AM
53357	Rate	0	0	53255	2008-05-30 16:35:34	100	Depreciation entered in field is a rate	Assets	Y	2008-05-30 16:35:34	100	\N	\N	RT
53358	Period	0	0	53256	2008-05-30 16:35:44	100	Amount entered in the Manual Depreciation Field is by period	Assets	Y	2008-05-30 16:35:44	100	\N	\N	PR
53359	Yearly	0	0	53256	2008-05-30 16:35:45	100	The amount entered into the manual depreciation field is for the year	Assets	Y	2008-05-30 16:35:45	100	\N	\N	YR
53360	Depreciation	0	0	53257	2008-05-30 16:36:02	100	Depreciation Entries	Assets	Y	2008-05-30 16:36:02	100	\N	\N	DEP
53361	Disposals	0	0	53257	2008-05-30 16:36:03	100	Disposal Entries for Fixed Assets	Assets	Y	2008-05-30 16:36:03	100	\N	\N	DIS
53362	Forecasts	0	0	53257	2008-05-30 16:36:04	100	Forecasts	Assets	Y	2008-05-30 16:36:04	100	\N	\N	FOR
53363	New	0	0	53257	2008-05-30 16:36:04	100	New Assets	Assets	Y	2008-05-30 16:36:04	100	\N	\N	NEW
53364	Splits	0	0	53257	2008-05-30 16:36:05	100	Split Entries for Fixed Assets	Assets	Y	2008-05-30 16:36:05	100	\N	\N	SPL
53365	Transfers	0	0	53257	2008-05-30 16:36:06	100	Transfer Entries for Fixed Assets	Assets	Y	2008-05-30 16:36:06	100	\N	\N	TRN
53366	Default	0	0	53259	2008-05-30 16:38:48	100	Default	Assets	Y	2008-05-30 16:38:48	100	\N	\N	DFT
53367	Inception to date	0	0	53259	2008-05-30 16:38:49	100	Revaluate each year seperately	Assets	N	2008-05-30 16:38:49	100	\N	\N	IDF
53368	Year Balances	0	0	53259	2008-05-30 16:38:50	100	Revaluate current year to date balances and then revaluates the beginning balance	Assets	Y	2008-05-30 16:38:50	100	\N	\N	YBF
53369	Factor	0	0	53260	2008-05-30 16:38:53	100	Revaluation Code Type is a Factor 	Assets	Y	2008-05-30 16:38:53	100	\N	\N	FAC
53370	Index	0	0	53260	2008-05-30 16:38:54	100	Revaluation Code Type is an Index 	Assets	Y	2008-05-30 16:38:54	100	\N	\N	IND
53371	Date Aquired	0	0	53261	2008-05-30 16:39:12	100	\N	Assets	Y	2008-05-30 16:39:12	100	\N	\N	DA
53372	Revaluation Date	0	0	53261	2008-05-30 16:39:13	100	\N	Assets	Y	2008-05-30 16:39:13	100	\N	\N	RD
53373	Date Depreciation Started	0	0	53261	2008-05-30 16:39:13	100	\N	Assets	Y	2008-05-30 16:39:13	100	\N	\N	SD
53374	Revaluation Code #1	0	0	53262	2008-05-30 16:39:16	100	\N	Assets	Y	2008-05-30 16:39:16	100	\N	\N	R01
53375	Revaluation Code #2	0	0	53262	2008-05-30 16:39:17	100	\N	Assets	Y	2008-05-30 16:39:17	100	\N	\N	R02
53376	Revaluation Code #3	0	0	53262	2008-05-30 16:39:18	100	\N	Assets	Y	2008-05-30 16:39:18	100	\N	\N	R03
53377	Amount	0	0	53263	2008-05-30 16:42:05	100	Split Asset by Currency Amount	Assets	Y	2008-05-30 16:42:05	100	\N	\N	AMT
53378	Percentage	0	0	53263	2008-05-30 16:42:06	100	Split Asset by Percentage	Assets	Y	2008-05-30 16:42:06	100	\N	\N	PER
53379	Quantity	0	0	53263	2008-05-30 16:42:10	100	Split Percentage by Quantity	Assets	Y	2008-05-30 16:42:10	100	\N	\N	QTY
53380	Charity	0	0	53269	2008-05-30 16:45:41	100	Donated to Charity	Assets	Y	2008-05-30 16:45:41	100	\N	\N	C
53381	Destroyed	0	0	53269	2008-05-30 16:45:42	100	Asset Destroyed	Assets	Y	2008-05-30 16:45:42	100	\N	\N	D
53382	Scraped	0	0	53269	2008-05-30 16:45:42	100	Scraped	Assets	Y	2008-05-30 16:45:42	100	\N	\N	S
53383	Sold	0	0	53269	2008-05-30 16:45:43	100	Asset Sold	Assets	Y	2008-05-30 16:45:43	100	\N	\N	S1
53384	Sold w/Trade	0	0	53269	2008-05-30 16:45:44	100	Asset Sold with trade	Assets	Y	2008-05-30 16:45:44	100	\N	\N	S2
53385	Theft	0	0	53269	2008-05-30 16:45:44	100	Asset Stolen	Assets	Y	2008-05-30 16:45:44	100	\N	\N	T
53386	Cash	0	0	53270	2008-05-30 16:45:58	100	Sold for cash	Assets	Y	2008-05-30 16:45:58	100	\N	\N	C
53387	Simple	0	0	53270	2008-05-30 16:45:59	100	No cash or trade	Assets	Y	2008-05-30 16:45:59	100	\N	\N	S
53388	Trade	0	0	53270	2008-05-30 16:46:00	100	Traded in	Assets	Y	2008-05-30 16:46:00	100	\N	\N	T1
53389	Trade w/cash	0	0	53270	2008-05-30 16:46:00	100	Traded In with cash	Assets	Y	2008-05-30 16:46:00	100	\N	\N	T2
53398	Addition	0	0	53273	2008-05-30 16:56:35	100	Cost/quantity addition to asset	Assets	Y	2008-05-30 16:56:35	100	\N	\N	ADD
53399	Balance	0	0	53273	2008-05-30 16:56:36	100	Transaction updating the cost or accum depr balance	Assets	Y	2008-05-30 16:56:36	100	\N	\N	BAL
53400	Create	0	0	53273	2008-05-30 16:56:37	100	Asset Master Record Created	Assets	Y	2008-05-30 16:56:37	100	\N	\N	CRT
53401	Depreciation	0	0	53273	2008-05-30 16:56:38	100	Depreciation Expense Recorded	Assets	Y	2008-05-30 16:56:38	100	\N	\N	DEP
53402	Disposal	0	0	53273	2008-05-30 16:56:38	100	Disposal of Asset	Assets	Y	2008-05-30 16:56:38	100	\N	\N	DIS
53403	Expense	0	0	53273	2008-05-30 16:56:39	100	Expense invoice was recorded against this asset	Assets	Y	2008-05-30 16:56:39	100	\N	\N	EXP
53404	Forecast	0	0	53273	2008-05-30 16:56:39	100	Forecast Type	Assets	Y	2008-05-30 16:56:39	100	\N	\N	FOR
53405	Import	0	0	53273	2008-05-30 16:56:40	100	Asset created by import	Assets	Y	2008-05-30 16:56:40	100	\N	\N	IMP
53406	Revaluation	0	0	53273	2008-05-30 16:56:40	100	Revaluation of Asset	Assets	Y	2008-05-30 16:56:40	100	\N	\N	RVL
53407	Setup	0	0	53273	2008-05-30 16:56:41	100	Asset Accounting Setup Modified	Assets	Y	2008-05-30 16:56:41	100	\N	\N	SET
53408	Split	0	0	53273	2008-05-30 16:56:42	100	Asset Split	Assets	Y	2008-05-30 16:56:42	100	\N	\N	SPL
53409	Transfer	0	0	53273	2008-05-30 16:56:42	100	Asset Transfered	Assets	Y	2008-05-30 16:56:42	100	\N	\N	TRN
53410	Update	0	0	53273	2008-05-30 16:56:43	100	Asset Master Record Updated	Assets	Y	2008-05-30 16:56:43	100	\N	\N	UPD
53411	Usage	0	0	53273	2008-05-30 16:56:44	100	Record asset usage	Assets	Y	2008-05-30 16:56:44	100	\N	\N	USE
53412	Imported	0	0	53276	2008-05-30 17:00:01	100	This entry was created from an imported document	Assets	Y	2008-05-30 17:00:01	100	\N	\N	IMP
53413	Invoice	0	0	53276	2008-05-30 17:00:02	100	This entry is related to an invoice	Assets	Y	2008-05-30 17:00:02	100	\N	\N	INV
53414	Journal Entry	0	0	53276	2008-05-30 17:00:02	100	The entry was created from a journal entry	Assets	Y	2008-05-30 17:00:02	100	\N	\N	JRN
53415	Manual	0	0	53276	2008-05-30 17:00:03	100	This entry was manually created	Assets	Y	2008-05-30 17:00:03	100	\N	\N	MAN
\.
