copy "ad_window" ("ad_window_id", "name", "ad_client_id", "ad_color_id", "ad_image_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "isdefault", "issotrx", "processing", "updated", "updatedby", "windowtype", "winheight", "winwidth") from STDIN;
201	Request	0	\N	105	0	2001-01-11 17:31:31	0	Work on your requests	Sales Management	The Request Window is used to define and track any request assigned to you.	Y	N	N	Y	N	2005-02-09 22:17:48	100	T	\N	\N
232	Request (all)	0	\N	105	0	2001-12-08 21:21:53	0	View and work on all requests	Sales Management	This Request window is used to view all available requests	Y	N	N	Y	N	2005-02-09 22:17:53	100	T	\N	\N
53153	Lead	0	\N	\N	0	2011-09-01 21:18:43	100	Maintain Leads	Sales Management	The Lead Window allows you to maintain Sales Leads.  These are unqualified contacts to who you wish to market to. 	Y	Y	N	Y	N	2012-06-08 15:16:56	0	M	700	1000
53154	Sales Stage	0	\N	\N	0	2011-09-01 22:26:09	100	\N	Sales Management	\N	Y	Y	N	Y	N	2012-06-08 15:16:56	0	M	0	0
53156	Sales Rep Dashboard	0	\N	\N	0	2011-09-02 12:21:26	100	The Sales Rep Dashboard provides a single location for managing sales opportunities	Sales Management	\N	Y	Y	N	Y	N	2012-06-08 15:16:56	0	M	1000	2000
53165	Customer	0	\N	\N	0	2011-10-28 15:03:49	100	\N	Sales Management	\N	Y	Y	N	Y	N	2012-06-08 15:16:56	0	M	700	1200
\.
