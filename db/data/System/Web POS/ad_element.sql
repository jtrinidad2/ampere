copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
52001	U_BlackListCheque_ID	0	0	13	\N	2008-03-26 13:20:00.863	0	\N	Web POS	22	\N	Y	Black List Cheque	\N	\N	\N	\N	Black List Cheque	2008-01-15 00:36:17	100
52002	BankName	0	0	10	\N	2008-03-26 13:20:00.865	0	\N	Web POS	120	\N	Y	Bank Name	\N	\N	\N	\N	Bank Name	2008-01-15 00:38:51	100
52003	ChequeNo	0	0	10	\N	2008-03-26 13:20:00.943	0	\N	Web POS	120	\N	Y	Cheque No	\N	\N	\N	\N	Cheque No	2008-01-15 00:39:00	100
52004	U_Web_Properties_ID	0	0	13	\N	2008-03-26 13:20:00.969	0	\N	Web POS	22	\N	Y	Web Properties	\N	\N	\N	\N	Web Properties	2008-01-15 00:40:02	100
52005	U_Key	0	0	10	\N	2008-03-26 13:20:00.975	0	\N	Web POS	240	\N	Y	Key	\N	\N	\N	\N	Key	2008-01-15 00:39:35	100
52006	U_Value	0	0	10	\N	2008-03-26 13:20:01.005	0	\N	Web POS	240	\N	Y	Value	\N	\N	\N	\N	Value	2008-01-15 00:39:47	100
52007	U_RoleMenu_ID	0	0	13	\N	2008-03-26 13:20:01.007	0	\N	Web POS	22	\N	Y	Role Menu	\N	\N	\N	\N	Role Menu	2008-01-15 00:36:49	100
52008	U_WebMenu_ID	0	0	19	\N	2008-03-26 13:20:01.027	0	\N	Web POS	10	\N	Y	Web Menu	\N	\N	\N	\N	Web Menu	2008-01-15 00:37:16	100
52009	MenuLink	0	0	10	\N	2008-03-26 13:20:01.03	0	\N	Web POS	510	\N	Y	Menu Link	\N	\N	\N	\N	Menu Link	2008-01-15 00:38:34	100
52010	Module	0	0	10	\N	2008-03-26 13:20:01.032	0	\N	Web POS	120	\N	Y	Module	\N	\N	\N	\N	Module	2008-03-26 13:20:01.032	0
52011	ParentMenu_ID	0	0	18	52000	2008-03-26 13:20:01.034	0	\N	Web POS	10	\N	Y	Parent Menu	\N	\N	\N	\N	Parent Menu	2008-01-15 00:37:45	100
52012	HasSubMenu	0	0	20	\N	2008-03-26 13:20:01.037	0	\N	Web POS	1	\N	Y	Has SubMenu	\N	\N	\N	\N	Has SubMenu	2008-01-15 00:38:07	100
52013	ImageLink	0	0	10	\N	2008-03-26 13:20:01.04	0	\N	Web POS	510	\N	Y	Image Link	\N	\N	\N	\N	Image Link	2008-01-15 00:38:24	100
52014	Position	0	0	10	\N	2008-03-26 13:20:01.042	0	\N	Web POS	10	\N	Y	Position	\N	\N	\N	\N	Position	2008-03-26 13:20:01.042	0
52016	Sequence	0	0	22	\N	2008-03-26 13:20:01.05	0	\N	Web POS	22	\N	Y	Sequence	\N	\N	\N	\N	Sequence	2008-03-26 13:20:01.05	0
52017	Category	0	0	10	\N	2008-03-26 13:20:01.052	0	\N	Web POS	120	\N	Y	Category	\N	\N	\N	\N	Category	2008-03-26 13:20:01.052	0
52030	AutoLock	0	0	20	\N	2008-06-02 00:00:00	100	Whether to automatically lock the terminal when till is closed	Web POS	1	\N	Y	Auto Lock	\N	\N	\N	\N	Auto Lock	2008-06-02 00:00:00	100
52031	CashBookTransferType	0	0	17	52002	2008-06-02 00:00:00	100	Where the money in the cash book should be transfered to. Either a Bank Account or another Cash Book	Web POS	1	\N	Y	Cash Book Transfer Type	\N	\N	\N	\N	Cash Book Transfer Type	2008-06-02 00:00:00	100
52033	CashTransferCashBook_ID	0	0	18	52004	2008-06-02 00:00:00	100	Cash Book on which to transfer all Cash transactions	Web POS	22	\N	Y	Transfer Cash trx to	\N	\N	\N	\N	Transfer Cash trx to	2008-06-02 00:00:00	100
52034	C_CashBPartner_ID	0	0	18	173	2008-06-02 00:00:00	100	BPartner to be used for Cash transactions	Web POS	22	\N	Y	Cash BPartner	\N	\N	\N	\N	Cash BPartner	2008-06-02 00:00:00	100
52035	Check_BankAccount_ID	0	0	18	53283	2008-06-02 00:00:00	100	Bank Account to be used for processing Cheque transactions	Web POS	22	\N	Y	Cheque Bank Account	\N	\N	\N	\N	Cheque Bank Account	2016-04-11 16:07:03	0
52036	CheckTransferBankAccount_ID	0	0	18	53283	2008-06-02 00:00:00	100	Bank account on which to transfer Cheque transactions	Web POS	22	\N	Y	Tranfer Cheque trx to	\N	\N	\N	\N	Transfer Cheque trx to	2016-04-11 16:07:03	0
52037	CheckTransferType	0	0	17	52002	2008-06-02 00:00:00	100	\N	Web POS	1	\N	Y	Cheque Transfer Type	\N	\N	\N	\N	Cheque Transfer Type	2016-04-11 16:07:04	0
52038	Card_BankAccount_ID	0	0	18	53283	2008-06-02 00:00:00	100	Bank Account on which card transactions will be processed	Web POS	22	\N	Y	Card Bank Account	\N	\N	\N	\N	Card Bank Account	2008-06-02 00:00:00	100
52039	CardTransferBankAccount_ID	0	0	18	53283	2008-06-02 00:00:00	100	Bank account on which to transfer Card transactions	Web POS	22	\N	Y	Transfer Card trx to	\N	\N	\N	\N	Transfer Card trx to	2008-06-02 00:00:00	100
52040	CardTransferCashBook_ID	0	0	18	52004	2008-06-02 00:00:00	100	Cash Book on which to transfer all Card transactions	Web POS	22	\N	Y	Transfer Card trx to	\N	\N	\N	\N	Transfer Card trx to	2008-06-02 00:00:00	100
52041	CardTransferType	0	0	17	52002	2008-06-02 00:00:00	100	\N	Web POS	1	\N	Y	Card Transfer Type	\N	\N	\N	\N	Card Transfer Type	2008-06-02 00:00:00	100
52042	C_TemplateBPartner_ID	0	0	18	173	2008-06-02 00:00:00	100	BPartner that is to be used as template when new customers are created	Web POS	22	\N	Y	Template BPartner	\N	\N	\N	\N	Template BPartner	2008-06-02 00:00:00	100
52043	LastLockTime	0	0	16	\N	2008-06-02 00:00:00	100	Last time at which the terminal was locked	Web POS	7	\N	Y	Last Lock Time	\N	\N	\N	\N	Last Lock Time	2008-06-02 00:00:00	100
52044	Locked	0	0	20	\N	2008-06-02 00:00:00	100	Whether the terminal is locked	Web POS	1	\N	Y	Locked	\N	\N	\N	\N	Locked	2008-06-02 00:00:00	100
52045	LockTime	0	0	11	\N	2008-06-02 00:00:00	100	Time in minutes the terminal should be kept in a locked state.	Web POS	10	\N	Y	Lock Time	\N	\N	\N	\N	Lock Time	2008-06-02 00:00:00	100
52046	SO_PriceList_ID	0	0	18	166	2008-06-02 00:00:00	100	\N	Web POS	22	\N	Y	Sales Pricelist	\N	\N	\N	\N	Sales Pricelist	2008-06-02 00:00:00	100
52047	UnlockingTime	0	0	16	\N	2008-06-02 00:00:00	100	Time at which the terminal should be unlocked	Web POS	7	\N	Y	UnlockingTime	\N	\N	\N	\N	UnlockingTime	2008-06-02 00:00:00	100
52049	CheckTransferCashBook_ID	0	0	18	52004	2008-06-02 00:00:00	100	Cash Book on which to transfer all Cheque transactions	Web POS	22	\N	Y	Transfer Cheque trx to	\N	\N	\N	\N	Transfer Cheque trx to	2016-04-11 16:07:03	0
\.
