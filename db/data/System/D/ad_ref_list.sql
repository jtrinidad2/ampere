copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
114	Spot	0	0	111	1999-06-22 00:00:00	0	Spot Conversation Rate Type	D	Y	2000-01-02 00:00:00	0	\N	\N	S
115	Period End	0	0	111	1999-06-22 00:00:00	0	Period Conversion Type	D	Y	2000-01-02 00:00:00	0	\N	\N	P
133	Regional Office	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	12
134	Accounts Payable Office	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	15
135	Plant	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	16
136	Small Business	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	21
137	Minority-Owned Small Business	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	22
138	Minority-Owned Business	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	23
139	Woman-Owned Small Business	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	24
140	Woman-Owned Business	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	25
141	Subcontractor	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	28
142	Individual	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	2J
143	Partnership	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	2K
144	Corporation	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	2L
145	Drop-off Location	0	0	121	1999-06-23 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	45
198	None	0	0	111	1999-07-17 00:00:00	0	No Conversion Rate	D	Y	2000-01-02 00:00:00	0	\N	\N	N
199	Fixed	0	0	111	1999-07-17 00:00:00	0	Euro Fixed Currency	D	Y	2000-01-02 00:00:00	0	\N	\N	F
200	Average	0	0	111	1999-07-17 00:00:00	0	Average Rates	D	Y	2000-01-02 00:00:00	0	\N	\N	A
201	Company	0	0	111	1999-07-17 00:00:00	0	Company Rate	D	Y	2000-01-02 00:00:00	0	\N	\N	C
202	User Type	0	0	111	1999-07-17 00:00:00	0	User Rate Type	D	Y	2000-01-02 00:00:00	0	\N	\N	U
203	Manual Rate	0	0	111	1999-07-17 00:00:00	0	Manual Rate	D	Y	2000-01-02 00:00:00	0	\N	\N	M
268	Error	0	0	180	1999-09-29 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	E
269	Warning	0	0	180	1999-09-29 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	W
270	Information	0	0	180	1999-09-29 00:00:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	I
342	Blind list (w/o book quantity)	0	0	198	2000-02-06 21:36:25	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	B
343	Count list (with book quantity)	0	0	198	2000-02-06 21:42:18	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	L
344	Control list	0	0	198	2000-02-06 21:43:42	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	C
377	Product	0	0	208	2000-09-15 15:03:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	P
378	Business Partner	0	0	208	2000-09-15 15:03:24	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	B
379	Accounts	0	0	208	2000-09-15 15:05:21	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	A
380	GL Balances	0	0	208	2000-09-15 15:06:08	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	S
381	Exchange Rates	0	0	208	2000-09-15 15:08:25	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	E
382	Inventory Count	0	0	208	2000-09-15 15:08:46	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	I
420	None	0	0	219	2001-01-11 18:13:32	0	\N	D	Y	2007-12-16 23:03:03	0	\N	\N	N
421	Follow up	0	0	219	2001-01-11 18:14:00	0	\N	D	Y	2007-12-16 23:04:27	0	\N	\N	F
422	Information	0	0	218	2001-01-11 18:19:54	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	I
423	Service	0	0	218	2001-01-11 18:20:15	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	S
424	Charge	0	0	218	2001-01-11 18:20:35	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	C
425	Account	0	0	218	2001-01-11 18:34:45	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	A
426	Help	0	0	218	2001-01-11 18:35:07	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	H
427	Alert	0	0	218	2001-01-11 18:35:30	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	X
428	Warranty	0	0	218	2001-01-11 18:36:37	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	W
429	Other	0	0	218	2001-01-11 18:37:31	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	O
442	Overdue	0	0	222	2001-01-11 19:13:34	0	\N	D	Y	2007-12-16 22:55:52	0	\N	\N	3
443	Due	0	0	222	2001-01-11 19:13:58	0	\N	D	Y	2007-12-16 22:56:00	0	\N	\N	5
444	Scheduled	0	0	222	2001-01-11 19:14:21	0	\N	D	Y	2007-12-16 22:56:09	0	\N	\N	7
451	System	0	0	226	2001-04-05 17:52:29	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	S  
452	Client	0	0	226	2001-04-05 17:54:36	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	 C 
453	Organization	0	0	226	2001-04-05 17:55:10	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	  O
454	Client+Organization	0	0	226	2001-04-05 17:55:52	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	 CO
487	Dictionary	0	0	245	2001-11-18 18:23:10	0	Application Dictionary (synchronized)	D	Y	2000-01-02 00:00:00	0	\N	\N	D
488	Adempiere	0	0	245	2001-11-18 18:24:07	0	Adempiere Application (synchronized)	D	Y	2000-01-02 00:00:00	0	\N	\N	C
489	User maintained	0	0	245	2001-11-18 18:25:12	0	User maintained Entity (not synchronized)	D	Y	2000-01-02 00:00:00	0	\N	\N	U
490	Applications	0	0	245	2001-11-18 18:25:57	0	Other Applications (not synchronized)	D	Y	2000-01-02 00:00:00	0	\N	\N	A
595	LiFo	0	0	294	2003-12-14 22:58:37	0	Last In First Out	D	Y	2000-01-02 00:00:00	0	\N	\N	L
596	FiFo	0	0	294	2003-12-14 22:59:00	0	First In First Out	D	Y	2000-01-02 00:00:00	0	\N	\N	F
604	Regular	0	0	300	2003-12-31 00:18:38	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	R
605	Loop Begin	0	0	300	2003-12-31 00:18:59	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	B
606	Loop End	0	0	300	2003-12-31 00:19:09	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	E
630	While Loop	0	0	308	2003-12-31 18:25:34	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	W
631	Repeat Until Loop	0	0	308	2003-12-31 18:26:06	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	R
661	Funding - All Bidders help funding a Topic	0	0	315	2004-02-19 11:48:16	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	F
662	Auction - The highest Bidder wins the Topic	0	0	315	2004-02-19 11:49:06	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	A
666	Customization	0	0	245	2004-03-13 11:26:49	0	Distributed Extensions should have 4 characters	D	Y	2000-01-02 00:00:00	0	\N	\N	CUST
695	Client	0	0	330	2004-11-27 11:37:37	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	C
696	Organization	0	0	330	2004-11-27 11:37:52	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	O
697	User	0	0	330	2004-11-27 11:38:00	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	U
698	None	0	0	330	2004-11-27 11:38:26	0	\N	D	Y	2000-01-02 00:00:00	0	\N	\N	N
815	 0% Not Started	0	0	366	2005-12-21 16:20:08	100	\N	D	Y	2007-12-16 23:18:17	0	\N	\N	0
816	100% Complete	0	0	366	2005-12-21 16:20:45	100	\N	D	Y	2007-12-16 23:18:26	0	\N	\N	D
817	 20% Started	0	0	366	2005-12-21 16:21:26	100	\N	D	Y	2007-12-16 23:18:33	0	\N	\N	2
818	 80% Nearly Done	0	0	366	2005-12-21 16:21:49	100	\N	D	Y	2007-12-16 23:18:41	0	\N	\N	8
819	 40% Busy	0	0	366	2005-12-21 16:23:16	100	\N	D	Y	2007-12-16 23:18:49	0	\N	\N	4
820	 60% Good Progress	0	0	366	2005-12-21 16:23:28	100	\N	D	Y	2007-12-16 23:18:56	0	\N	\N	6
821	 90% Finishing	0	0	366	2005-12-21 16:23:40	100	\N	D	Y	2007-12-16 23:19:04	0	\N	\N	9
822	 95% Almost Done	0	0	366	2005-12-21 16:26:03	100	\N	D	Y	2007-12-16 23:19:11	0	\N	\N	A
823	 99% Cleaning up	0	0	366	2005-12-21 16:28:10	100	\N	D	Y	2007-12-16 23:19:18	0	\N	\N	C
911	Collaboration Management	0	0	391	2006-06-24 12:54:34	100	\N	D	Y	2006-06-24 12:54:34	100	\N	\N	C
912	Java Client	0	0	391	2006-06-24 12:54:46	100	\N	D	Y	2006-06-24 12:54:46	100	\N	\N	J
913	HTML Client	0	0	391	2006-06-24 12:55:06	100	\N	D	Y	2006-06-24 12:55:06	100	\N	\N	H
914	Self Service	0	0	391	2006-06-24 12:55:34	100	\N	D	Y	2006-06-24 12:55:34	100	\N	\N	W
54442	Default	0	0	53714	2014-08-06 13:48:42	100	Notify user of Tax Rate mismatch, and allow the user to fix it manually to agree with the system calculated tax rate	D	Y	2014-08-06 13:48:42	100	\N	\N	D
54443	Force Correct	0	0	53714	2014-08-06 13:49:11	100	System forces to calculate correct tax rates.	D	Y	2014-08-06 13:49:11	100	\N	\N	F
54444	Override	0	0	53714	2014-08-06 13:49:35	100	Allow to change Tax Rate and complete - Override Tax flag in Line	D	Y	2014-08-06 13:49:35	100	\N	\N	O
54604	Current	0	0	53797	2015-09-23 12:20:04	100	\N	D	Y	2016-04-11 16:52:25	0	\N	\N	0
54605	0-30 days	0	0	53797	2015-09-23 12:20:36	100	\N	D	Y	2015-09-23 12:20:36	100	\N	\N	1
54606	31-60 days	0	0	53797	2015-09-23 12:20:50	100	\N	D	Y	2015-09-23 12:20:50	100	\N	\N	2
54608	61-90 days	0	0	53797	2015-09-23 12:21:07	100	\N	D	Y	2015-09-23 12:21:07	100	\N	\N	3
54609	90+ days	0	0	53797	2015-09-23 12:21:30	100	\N	D	Y	2015-09-23 12:21:30	100	\N	\N	4
55304	1 - Report Header	0	0	256	2019-01-30 17:52:14	100	Report Header area only prints once at the beginning of the report.	D	Y	2020-03-16 17:10:01	0	\N	\N	T
55305	5 - Report Footer	0	0	256	2019-01-30 17:52:34	100	Report Footer area only prints once at the end of the report.	D	Y	2020-03-16 17:10:01	0	\N	\N	S
\.
