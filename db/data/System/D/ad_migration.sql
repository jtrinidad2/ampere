copy "ad_migration" ("ad_migration_id", "seqno", "name", "ad_client_id", "ad_org_id", "apply", "comments", "created", "createdby", "entitytype", "exportxml", "isactive", "releaseno", "statuscode", "updated", "updatedby") from STDIN;
1000000	350	Filter order report document type parameter	0	0	R	\N	2011-09-05 15:25:14	100	D	\N	Y	\N	A	2011-09-05 15:31:39	100
1000001	140	Report cube viewer	0	0	R	\N	2011-09-05 15:25:19	100	D	\N	Y	\N	A	2011-09-05 15:30:09	100
1000002	100	Manufacturing Light phantom	0	0	R	Add phantom bom support and replenishment batching	2011-09-05 15:25:27	100	D	\N	Y	\N	A	2011-09-05 15:29:52	100
1000003	110	Test data generator	0	0	R	\N	2011-09-05 15:25:32	100	D	\N	Y	\N	A	2011-09-05 15:29:59	100
1000004	320	Add charge to import payment	0	0	R	\N	2011-09-05 15:25:40	100	D	\N	Y	\N	A	2011-09-05 15:31:35	100
1000005	290	Delete Entities	0	0	R	\N	2011-09-05 15:25:42	100	D	\N	Y	\N	A	2011-09-05 15:31:23	100
1000006	180	Add parent virtual column to element	0	0	R	\N	2011-09-05 15:25:43	100	D	\N	Y	\N	A	2011-09-05 15:30:31	100
1000007	270	Financial Reporting import	0	0	R	\N	2011-09-05 15:25:46	100	D	\N	Y	\N	A	2011-09-05 15:30:49	100
1000008	120	Merge migration	0	0	R	\N	2011-09-05 15:26:32	100	D	\N	Y	\N	A	2011-09-05 15:30:03	100
1000009	260	One account per charge	0	0	R	\N	2011-09-05 15:26:34	100	D	\N	Y	\N	A	2011-09-05 15:30:40	100
1000010	190	Copy report	0	0	R	\N	2011-09-05 15:26:35	100	D	\N	Y	\N	A	2011-09-05 15:30:35	100
1000011	150	Create charge when importing account	0	0	R	\N	2011-09-05 15:26:35	100	D	\N	Y	\N	A	2011-09-05 15:30:15	100
1000012	370	Adaxa 1.01	0	0	R	\N	2011-09-05 15:26:43	100	D	\N	Y	\N	A	2011-09-05 15:32:06	100
1000013	170	Use logo field in template formats	0	0	R	\N	2011-09-05 15:26:43	100	D	\N	Y	\N	A	2011-09-05 15:30:29	100
1000014	160	Improve initial setup	0	0	R	\N	2011-09-05 15:26:44	100	D	\N	Y	\N	A	2011-09-05 15:30:21	100
1000015	300	Email parameters	0	0	R	\N	2011-09-05 15:26:55	100	D	\N	Y	\N	A	2011-09-05 15:31:29	100
1000016	280	Drop Shipment print views	0	0	R	\N	2011-09-05 15:27:00	100	D	\N	Y	\N	A	2011-09-05 15:31:17	100
1000017	360	Memo, alert and lookup display	0	0	R	\N	2011-09-05 15:28:10	100	D	\N	Y	\N	A	2011-09-05 15:32:00	100
1000018	365	Fix chart field type	0	0	R	\N	2011-09-06 12:00:25	0	D	\N	Y	\N	A	2011-09-06 12:08:41	0
1000019	380	Replenishment Kanban	0	0	R	\N	2011-11-02 16:34:31	0	D	\N	Y	\N	A	2011-11-02 16:34:57	0
1000020	460	Add series to chart data	0	0	R	\N	2011-11-02 16:34:31	0	D	\N	Y	\N	A	2011-11-02 16:34:59	0
1000021	590	Adaxa 1.02	0	0	R	\N	2011-11-02 16:34:34	0	D	\N	Y	\N	A	2011-11-02 16:38:51	100
1000022	510	Auto GL reconciliation rule	0	0	R	\N	2011-11-02 16:34:35	0	D	\N	Y	\N	A	2011-11-02 16:35:01	0
1000023	490	Add address 3 and 4 to import bp	0	0	R	\N	2011-11-02 16:34:35	0	D	\N	Y	\N	A	2011-11-02 16:35:00	0
1000024	550	Grid column display	0	0	R	\N	2011-11-02 16:34:40	0	D	\N	Y	\N	A	2011-11-02 16:35:04	0
1000025	580	Flag beta functionality	0	0	R	\N	2011-11-02 16:34:46	0	D	\N	Y	\N	A	2011-11-02 16:35:06	0
1000026	450	Save process parameters	0	0	R	\N	2011-11-02 16:34:49	0	D	\N	Y	\N	A	2011-11-02 16:34:58	0
1000027	500	Improve generate test data	0	0	R	\N	2011-11-02 16:34:51	0	D	\N	Y	\N	A	2011-11-02 16:35:01	0
1000028	810	Freight Calculation	0	0	R	\N	2012-06-08 14:48:30	0	D	\N	Y	\N	A	2012-06-08 15:19:17	0
1000029	665	DMS integration 2	0	0	R	\N	2012-06-08 14:50:29	0	D	\N	Y	\N	A	2012-06-08 15:17:23	0
1000030	800	Make quantity break pricing not BP specific	0	0	R	\N	2012-06-08 14:50:31	0	D	\N	Y	\N	A	2012-06-08 15:18:42	0
1000031	690	Manufacturing light fixes	0	0	R	\N	2012-06-08 14:50:31	0	D	\N	Y	\N	A	2012-06-08 15:17:53	0
1000032	620	Add geocode info to address	0	0	R	\N	2012-06-08 14:52:25	0	D	\N	Y	\N	A	2012-06-08 15:17:03	0
1000033	610	Add name to bank account	0	0	R	\N	2012-06-08 14:52:33	0	D	\N	Y	\N	A	2012-06-08 15:17:00	0
1000034	650	Control shipment process	0	0	R	\N	2012-06-08 14:52:36	0	D	\N	Y	\N	A	2012-06-08 15:17:15	0
1000035	811	Freight region reference	0	0	R	\N	2012-06-08 14:53:03	0	D	\N	Y	\N	A	2012-06-08 15:19:17	0
1000036	700	Import field default value	0	0	R	\N	2012-06-08 14:53:06	0	D	\N	Y	\N	A	2012-06-08 15:17:54	0
1000037	880	SmartPOS	0	0	R	\N	2012-06-08 14:53:15	0	D	\N	Y	\N	A	2012-06-08 15:19:39	0
1000038	860	Add charge to I_Inventory	0	0	R	\N	2012-06-08 14:54:12	0	D	\N	Y	\N	A	2012-06-08 15:19:20	0
1000039	710	Import Loader Process	0	0	R	\N	2012-06-08 14:54:15	0	D	\N	Y	\N	A	2012-06-08 15:17:55	0
1000040	560	CRM	0	0	R	\N	2012-06-08 14:54:20	0	D	\N	Y	\N	A	2012-06-08 15:16:57	0
1000041	740	Import user	0	0	R	\N	2012-06-08 15:10:02	0	D	\N	Y	\N	A	2012-06-08 15:18:42	0
1000042	870	Adaxa 1.03	0	0	R	\N	2012-06-08 15:11:47	0	D	\N	Y	\N	A	2012-06-08 15:19:20	0
1000043	730	Data import definition	0	0	R	\N	2012-06-08 15:11:47	0	D	\N	Y	\N	A	2012-06-08 15:18:14	0
1000044	890	Report Column User element selection	0	0	R	\N	2012-06-08 15:12:57	0	D	\N	Y	\N	A	2012-06-08 15:19:39	0
1000045	600	Fix spelling error	0	0	R	\N	2012-06-08 15:12:57	0	D	\N	Y	\N	A	2012-06-08 15:16:58	0
1000046	720	Merge migration	0	0	R	\N	2012-06-08 15:12:57	0	D	\N	Y	\N	A	2012-06-08 15:17:56	0
1000047	630	Add report data table	0	0	R	\N	2012-06-08 15:12:58	0	D	\N	Y	\N	A	2012-06-08 15:17:09	0
1000048	660	DMS integration	0	0	R	\N	2012-06-08 15:13:18	0	D	\N	Y	\N	A	2012-06-08 15:17:22	0
1000049	830	Import inventory guarantee date	0	0	R	\N	2012-06-08 15:13:38	0	D	\N	Y	\N	A	2012-06-08 15:19:19	0
1000050	680	Add description to import payment	0	0	R	\N	2012-06-08 15:13:41	0	D	\N	Y	\N	A	2012-06-08 15:17:27	0
1000051	670	Override doc controlled acct	0	0	R	\N	2012-06-08 15:13:45	0	D	\N	Y	\N	A	2012-06-08 15:17:25	0
1000052	5	Add Adaxa Quickstart entity	0	0	R	\N	2016-04-11 13:47:30	0	D	\N	Y	\N	A	2016-04-11 15:58:08	0
1000053	1000	Table selection	0	0	R	\N	2016-04-11 13:47:32	0	D	\N	Y	\N	A	2016-04-11 16:20:43	0
1000054	1010	Merge migration setup	0	0	R	\N	2016-04-11 13:48:58	0	D	\N	Y	\N	A	2016-04-11 16:20:44	0
1000055	3890	#1186_add_BankStatLine_to_Payment_Window	0	0	R	\N	2016-04-11 13:48:58	0	D	\N	Y	\N	A	2016-04-11 16:41:36	0
1000056	1200	EDI	0	0	R	\N	2016-04-11 13:49:23	0	D	\N	Y	\N	A	2016-04-11 16:24:39	0
1000057	1270	Default location	0	0	R	\N	2016-04-11 13:59:28	0	D	\N	Y	\N	A	2016-04-11 16:24:47	0
1000058	1340	Barcode text print	0	0	R	\N	2016-04-11 13:59:42	0	D	\N	Y	\N	A	2016-04-11 16:24:54	0
1000059	1360	Print format enh	0	0	R	\N	2016-04-11 13:59:46	0	D	\N	Y	\N	A	2016-04-11 16:25:00	0
1000060	1380	Add cash tender type	0	0	R	\N	2016-04-11 13:59:50	0	D	\N	Y	\N	A	2016-04-11 16:25:01	0
1000061	4750	#1383 Data Import Definition for BOM & ASI	0	0	R	\N	2016-04-11 13:59:51	0	D	\N	Y	\N	A	2016-04-11 16:51:59	0
1000062	1395	#1028 Fixing Deadlock	0	0	R	\N	2016-04-11 14:00:39	0	D	\N	Y	\N	A	2016-04-11 16:25:02	0
1000063	1390	#1028 Fixing mail template vulenarability	0	0	R	\N	2016-04-11 14:00:39	0	D	\N	Y	\N	A	2016-04-11 16:25:02	0
1000064	1400	IDempire Calendar functionality	0	0	R	\N	2016-04-11 14:00:40	0	D	\N	Y	\N	A	2016-04-11 16:25:04	0
1000065	1410	Make 'IsSecure' field true for password columns	0	0	R	\N	2016-04-11 14:00:44	0	D	\N	Y	\N	A	2016-04-11 16:25:05	0
1000066	1415	ZK client branding	0	0	R	\N	2016-04-11 14:00:44	0	D	\N	Y	\N	A	2016-04-11 16:25:08	0
1000067	1420	1420_T_MiniMRP	0	0	R	\N	2016-04-11 14:00:49	0	D	\N	Y	\N	A	2016-04-11 16:25:34	0
1000068	1421	1421_MiniMRP_Menu_Report_View_AD_Changes	0	0	R	\N	2016-04-11 14:01:29	0	D	\N	Y	\N	A	2016-04-11 16:25:36	0
1000069	1422	MiniMRP_PrintFormat	0	0	R	\N	2016-04-11 14:01:32	0	D	\N	Y	\N	A	2016-04-11 16:25:51	0
1000070	1430	Print format and report type on scheduler.	0	0	R	\N	2016-04-11 14:02:04	0	D	\N	Y	\N	A	2016-04-11 16:25:53	0
1000071	1440	Bug #866 Unable to sort window after applying date filter	0	0	R	\N	2016-04-11 14:02:08	0	D	\N	Y	\N	A	2016-04-11 16:25:54	0
1000072	1450	#778 Adding C_OrderLine_ID into Fact_acct.	0	0	R	\N	2016-04-11 14:02:10	0	D	\N	Y	\N	A	2016-04-11 16:25:55	0
1000073	1460	Budget Importer	0	0	R	\N	2016-04-11 14:02:11	0	D	\N	Y	\N	A	2016-04-11 16:26:27	0
1000074	1470	Budget Importer	0	0	R	\N	2016-04-11 14:03:34	0	D	\N	Y	\N	A	2016-04-11 16:26:28	0
1000075	1480	Budget Importer file loader	0	0	R	\N	2016-04-11 14:03:39	0	D	\N	Y	\N	A	2016-04-11 16:26:34	0
1000076	1490	798_Budget import changes11_20	0	0	R	\N	2016-04-11 14:03:51	0	D	\N	Y	\N	A	2016-04-11 16:26:35	0
1000077	1500	#855 Extend Import Budget to handle Quantities	0	0	R	\N	2016-04-11 14:03:52	0	D	\N	Y	\N	A	2016-04-11 16:26:45	0
1000078	1510	Add doc action paramater to import budget process	0	0	R	\N	2016-04-11 14:04:15	0	D	\N	Y	\N	A	2016-04-11 16:26:46	0
1000079	1520	#588 Import Loader Format for Budget	0	0	R	\N	2016-04-11 14:04:17	0	D	\N	Y	\N	A	2016-04-11 16:26:48	0
1000080	1530	Feature #1050 Validation on Date Invoiced Ad Changes	0	0	R	\N	2016-04-11 14:04:21	0	D	\N	Y	\N	A	2016-04-11 16:26:49	0
1000081	1530	Simplified Gl journal	0	0	R	\N	2016-04-11 14:04:22	0	D	\N	Y	\N	A	2016-04-11 16:27:20	0
1000082	1540	Feature #1050 Validation on Date Invoiced requirement change	0	0	R	\N	2016-04-11 14:05:28	0	D	\N	Y	\N	A	2016-04-11 16:27:20	0
1000083	1540	Simplified GL Journal	0	0	R	\N	2016-04-11 14:05:28	0	D	\N	Y	\N	A	2016-04-11 16:27:25	0
1000084	1570	Copy lines on GL Journal	0	0	R	\N	2016-04-11 14:05:38	0	D	\N	Y	\N	A	2016-04-11 16:27:26	0
1000085	1570	Dynamic view	0	0	R	\N	2016-04-11 14:05:41	0	D	\N	Y	\N	A	2016-04-11 16:27:30	0
1000086	1580	Feature #1055 Recurring Documents does not support the 2 tab	0	0	R	\N	2016-04-11 14:05:44	0	D	\N	Y	\N	A	2016-04-11 16:27:32	0
1000087	1590	Feature #1055 feedback AD Changes	0	0	R	\N	2016-04-11 14:05:47	0	D	\N	Y	\N	A	2016-04-11 16:27:32	0
1000088	1660	Print_display_logic	0	0	R	\N	2016-04-11 14:05:48	0	D	\N	Y	\N	A	2016-04-11 16:27:55	0
1000089	1600	Feature #777 ​Linking ASI and asset	0	0	R	\N	2016-04-11 14:05:53	0	D	\N	Y	\N	A	2016-04-11 16:27:34	0
1000090	1610	Financial Report Header Format	0	0	R	\N	2016-04-11 14:05:57	0	D	\N	Y	\N	A	2016-04-11 16:27:38	0
1000091	1630	Financial Report Enhancement Change	0	0	R	\N	2016-04-11 14:06:09	0	D	\N	Y	\N	A	2016-04-11 16:27:46	0
1000092	1640	Financial Report Enhancement	0	0	R	\N	2016-04-11 14:06:11	0	D	\N	Y	\N	A	2016-04-12 10:04:34	100
1000093	1660	Aging with payments (revalued)	0	0	R	\N	2016-04-11 14:06:14	0	D	\N	Y	\N	A	2016-04-11 16:28:03	0
1000094	1670	Menu and process for AP/AR Restatement	0	0	R	\N	2016-04-11 14:06:35	0	D	\N	Y	\N	A	2016-04-11 16:28:03	0
1000095	1680	Paramaters for AP/AR Restatement	0	0	R	\N	2016-04-11 14:06:36	0	D	\N	Y	\N	A	2016-04-11 16:28:06	0
1000096	1690	Payment Restatement Revalure Print format	0	0	R	\N	2016-04-11 14:06:41	0	D	\N	Y	\N	A	2016-04-11 16:28:09	0
1000097	1700	Max over due and grace amt on bpaetner	0	0	R	\N	2016-04-11 14:06:51	0	D	\N	Y	\N	A	2016-04-11 16:28:11	0
1000098	1710	FA Module	0	0	R	\N	2016-04-11 14:06:55	0	D	\N	Y	\N	A	2016-04-11 16:34:10	0
1000099	1720	FA  Import Module	0	0	R	\N	2016-04-11 14:29:22	0	D	\N	Y	\N	A	2016-04-11 16:37:05	0
1000100	920	Missing AD changes DA module.	0	0	R	\N	2016-04-11 14:37:36	0	D	\N	Y	\N	A	2016-04-12 09:47:49	0
1000101	1740	PO_Commitment_Posting	0	0	R	\N	2016-04-11 14:37:39	0	D	\N	Y	\N	A	2016-04-11 16:37:09	0
1000102	1750	Feature #1133 add PO accrual flag to Charge to get finer con	0	0	R	\N	2016-04-11 14:37:47	0	D	\N	Y	\N	A	2016-04-11 16:37:10	0
1000103	1760	Function_Changes	0	0	R	Function_Changes	2016-04-11 14:37:49	0	D	\N	Y	\N	A	2016-04-11 16:37:11	0
1000104	1770	1045 M_Inout on invoice IsLandedCost On Charge	0	0	R	\N	2016-04-11 14:37:49	0	D	\N	Y	\N	A	2016-04-11 16:37:13	0
1000105	1790	Timesheet Entry	0	0	R	\N	2016-04-11 14:37:52	0	D	\N	Y	\N	A	2016-04-11 16:37:29	0
1000106	1800	Bank transfer change	0	0	R	\N	2016-04-11 14:38:33	0	D	\N	Y	\N	A	2016-04-11 16:37:31	0
1000107	1800	Time Sheet Form	0	0	R	\N	2016-04-11 14:38:37	0	D	\N	Y	\N	A	2016-04-11 16:37:29	0
1000108	1810	Bank transfer change for amount and org	0	0	R	\N	2016-04-11 14:38:38	0	D	\N	Y	\N	A	2016-04-11 16:37:33	0
1000109	1810	Added Column for Copy Project On Project Window	0	0	R	\N	2016-04-11 14:38:38	0	D	\N	Y	\N	A	2016-04-11 16:37:33	0
1000110	1820	Feature #1134 add Reverse-Accrue to Purchase Order	0	0	R	\N	2016-04-11 14:38:40	0	D	\N	Y	\N	A	2016-04-11 16:37:34	0
1000111	1830	Feature #1061 Import Credit Card statement to Payment and In	0	0	R	\N	2016-04-11 14:38:41	0	D	\N	Y	\N	A	2016-04-11 16:37:46	0
1000112	1840	#1061_I_imp_invoice_payment_v View	0	0	R	I_imp_invoice_payment_v View	2016-04-11 14:39:07	0	D	\N	Y	\N	A	2016-04-11 16:37:47	0
1000113	1850	Feature #1061 Simulate Import Process	0	0	R	\N	2016-04-11 14:39:07	0	D	\N	Y	\N	A	2016-04-11 16:37:58	0
1000114	1860	Feature #1061 Import Invoice/Payment Report	0	0	R	\N	2016-04-11 14:39:16	0	D	\N	Y	\N	A	2016-04-11 16:38:06	0
1000115	1870	Improve Remittance advice print process	0	0	R	\N	2016-04-11 14:39:42	0	D	\N	Y	\N	A	2016-04-11 16:38:13	0
1000116	1880	Create_ReportType_List and AD_changes in AD_PInstance Table	0	0	R	\N	2016-04-11 14:39:55	0	D	\N	Y	\N	A	2016-04-11 16:38:14	0
1000117	1890	Feature #1127 add automatic consolidation eliminations to AD	0	0	R	\N	2016-04-11 14:39:57	0	D	\N	Y	\N	A	2016-04-11 16:38:22	0
1000118	1890	Add 'TB Drillable Report' Form and Menu and IsShowRetainedEa	0	0	R	\N	2016-04-11 14:40:04	0	D	\N	Y	\N	A	2016-04-11 16:38:15	0
1000119	1900	Feature #1127 suspense acct for Elim entries	0	0	R	\N	2016-04-11 14:40:06	0	D	\N	Y	\N	A	2016-04-11 16:38:22	0
1000120	1910	Table of authorities	0	0	R	\N	2016-04-11 14:40:08	0	D	\N	Y	\N	A	2016-04-11 16:38:37	0
1000121	1920	aged inventory report	0	0	R	\N	2016-04-11 14:40:41	0	D	\N	Y	\N	A	2016-04-11 16:38:49	0
1000122	1930	aged inventory	0	0	R	\N	2016-04-11 14:41:11	0	D	\N	Y	\N	A	2016-04-11 16:38:49	0
1000123	1940	Aged inventory report	0	0	R	\N	2016-04-11 14:41:11	0	D	\N	Y	\N	A	2016-04-11 16:38:50	0
1000124	1950	aged inventory report	0	0	R	\N	2016-04-11 14:41:12	0	D	\N	Y	\N	A	2016-04-11 16:38:56	0
1000125	1960	Aged inventory report change(Product id to table direct)	0	0	R	\N	2016-04-11 14:41:36	0	D	\N	Y	\N	A	2016-04-11 16:38:57	0
1000126	1970	Add Table AD_Tab_Customization,AD_TootBarButton and Messages	0	0	R	\N	2016-04-11 14:41:36	0	D	\N	Y	\N	A	2016-04-11 16:39:07	0
1000127	1970	IsShowInList flag on find winodow to list saved search under	0	0	R	\N	2016-04-11 14:42:02	0	D	\N	Y	\N	A	2016-04-11 16:39:07	0
1000128	1980	Create_List_ToolbarButton-ComponentName	0	0	R	\N	2016-04-11 14:42:03	0	D	\N	Y	\N	A	2016-04-11 16:39:13	0
1000129	1990	#1186_Add_BankStatementLine_Tab_In_Payment_Window	0	0	R	\N	2016-04-11 14:42:12	0	D	\N	Y	\N	A	2016-04-11 16:39:44	0
1000130	1990	#1177 Account Element Validation tab	0	0	R	\N	2016-04-11 14:42:37	0	D	\N	Y	\N	A	2016-04-11 16:39:34	0
1000131	2000	Change to avoid DB error when simulating process in import i	0	0	R	\N	2016-04-11 14:43:19	0	D	\N	Y	\N	A	2016-04-11 16:39:44	0
1000132	2000	add Import Budget to Menu and tidy up	0	0	R	add Import budget to menu - get rid of cyan colour from print format - reorg menu in GL and Material management	2016-04-11 14:43:19	0	D	\N	Y	\N	A	2016-04-11 16:39:45	0
1000133	200	Template report formats 1	0	0	R	Business Partner Detail, Business Partner Open, Order Detail, Order Transactions, Shipment details, Open Orders	2016-04-11 14:43:21	0	D	\N	Y	\N	A	2016-04-11 16:00:42	0
1000134	2010	set_A4_paper_size_everywhere	0	0	R	\N	2016-04-11 14:52:54	0	D	\N	Y	\N	A	2016-04-11 16:40:06	0
1000135	2010	Timesheet Entry 2	0	0	R	\N	2016-04-11 14:52:55	0	D	\N	Y	\N	A	2016-04-11 16:40:05	0
1000136	2020	add_Curr_Rate_Importer	0	0	R	\N	2016-04-11 14:53:26	0	D	\N	Y	\N	A	2016-04-11 16:40:12	0
1000137	2020	Add project to internal use line	0	0	R	\N	2016-04-11 14:53:29	0	D	\N	Y	\N	A	2016-04-11 16:40:10	0
1000138	2030	Process report	0	0	R	\N	2016-04-11 14:53:38	0	D	\N	Y	\N	A	2016-04-11 16:40:30	0
1000139	2040	Process report2	0	0	R	\N	2016-04-11 14:54:16	0	D	\N	Y	\N	A	2016-04-11 16:40:30	0
1000140	2050	Separate column for sales invoice in project report	0	0	R	\N	2016-04-11 14:54:17	0	D	\N	Y	\N	A	2016-04-11 16:40:31	0
1000141	2060	822_Add 'MultiplyRate' column to solve issue of amount when 	0	0	R	\N	2016-04-11 14:54:19	0	D	\N	Y	\N	A	2016-04-11 16:40:32	0
1000142	2070	Copy acct on asset group	0	0	R	\N	2016-04-11 14:54:19	0	D	\N	Y	\N	A	2016-04-11 16:40:35	0
1000143	2070	Move Reservation  to Default Locator	0	0	R	\N	2016-04-11 14:54:23	0	D	\N	Y	\N	A	2016-04-11 16:40:33	0
1000144	2080	Mandatory description  on Asset journal line	0	0	R	\N	2016-04-11 14:54:25	0	D	\N	Y	\N	A	2016-04-11 16:40:35	0
1000145	2090	1120_Distribute_Cost_On_InvoiceLine	0	0	R	\N	2016-04-11 14:54:26	0	D	\N	Y	\N	A	2016-04-11 16:40:36	0
1000146	210	Print format defaults	0	0	R	\N	2016-04-11 14:54:28	0	D	\N	Y	\N	A	2016-04-11 16:00:43	0
1000147	220	Add org data to print formats	0	0	R	\N	2016-04-11 14:54:29	0	D	\N	Y	\N	A	2016-04-11 16:00:53	0
1000148	230	Template Reports 2	0	0	R	invoice Tax, Open Items, Aging, Aging with Payments, Unrealised Gain/Loss, Payment Details	2016-04-11 14:54:53	0	D	\N	Y	\N	A	2016-04-11 16:02:50	0
1000149	240	Template reports 3	0	0	R	Allocation, Unallocated Invoices, Unallocated Payments, Unreconciled Payments, Transaction Detail	2016-04-11 15:01:34	0	D	\N	Y	\N	A	2016-04-11 16:04:29	0
1000150	250	Template Reports 4	0	0	R	Product Transaction Summary, Replenish Report, Storage Detail, Storage per Product, Inventory Valuation, Trial Balance, Product Cost, Accounting Fact Details	2016-04-11 15:07:36	0	D	\N	Y	\N	A	2016-04-11 16:06:11	0
1000151	281	Reapply custom order header view	0	0	R	\N	2016-04-11 15:13:01	0	D	\N	Y	\N	A	2016-04-11 16:06:12	0
1000152	3000	1120A_Display the "referenced invoice" field in Invoice	0	0	R	\N	2016-04-11 15:13:02	0	D	\N	Y	\N	A	2016-04-11 16:40:39	0
1000153	3010	1219_Callout to default charge revenue acct same as expense 	0	0	R	\N	2016-04-11 15:13:04	0	D	\N	Y	\N	A	2016-04-11 16:40:39	0
1000154	3020	Add C_Order_ID and C_Order.DocNo to I_Payment	0	0	R	\N	2016-04-11 15:13:04	0	D	\N	Y	\N	A	2016-04-11 16:40:46	0
1000155	3040	Warehouse Info form	0	0	R	\N	2016-04-11 15:13:12	0	D	\N	Y	\N	A	2016-04-11 16:40:47	0
1000156	3060	User Favorite Panel Migration	0	0	R	\N	2016-04-11 15:13:13	0	D	\N	Y	\N	A	2016-04-11 16:40:56	0
1000157	3060	User List1 And User List2 In Combination	0	0	R	\N	2016-04-11 15:13:39	0	D	\N	Y	\N	A	2016-04-11 16:41:00	0
1000158	3060	AccountNo on c_payselectioncheck_v	0	0	R	\N	2016-04-11 15:13:46	0	D	\N	Y	\N	A	2016-04-11 16:41:00	0
1000159	3070	3070_#1326_filter_invoice_dropdown_on_payment	0	0	R	\N	2016-04-11 15:13:47	0	D	\N	Y	\N	A	2016-04-11 16:41:00	0
1000160	3080	MiniMRP_Print_Format_Fix	0	0	R	\N	2016-04-11 15:13:48	0	D	\N	Y	\N	A	2016-04-11 16:41:01	0
1000161	310	Import formats	0	0	R	\N	2016-04-11 15:13:49	0	D	\N	Y	\N	A	2016-04-11 16:06:32	0
1000162	3110	Override Tax	0	0	R	\N	2016-04-11 15:14:34	0	D	\N	Y	\N	A	2016-04-12 10:00:41	100
1000163	330	Disable foreign languages	0	0	R	\N	2016-04-11 15:14:41	0	D	\N	Y	\N	A	2016-04-11 16:06:49	0
1000164	340	Replace American terminology	0	0	R	\N	2016-04-11 15:14:55	0	D	\N	Y	\N	A	2016-04-11 16:07:05	0
1000165	3800	Create journal from AR/AP Revaluation	0	0	R	\N	2016-04-11 15:15:08	0	D	\N	Y	\N	A	2016-04-11 16:41:05	0
1000166	3820	Revenue recognition	0	0	R	\N	2016-04-11 15:15:09	0	D	\N	Y	\N	A	2016-04-11 16:41:19	0
1000167	3830	Revenue recognition process	0	0	R	\N	2016-04-11 15:15:37	0	D	\N	Y	\N	A	2016-04-11 16:41:20	0
1000168	3880	3880_#1185_update_help_on_credit_limit	0	0	R	3880_#1185_update_help_on_credit_limit .. and rename Direct debit to EFT (AR) etc	2016-04-11 15:15:40	0	D	\N	Y	\N	A	2016-04-11 16:41:23	0
1000169	3910	View_RV_Fact_Acct	0	0	R	\N	2016-04-11 15:15:42	0	D	\N	Y	\N	A	2016-04-11 16:41:37	0
1000170	3920	1187_FinReport_Periods_newest_first	0	0	R	change FinbReports parameters .. Period .. most recent first, rows always before totals	2016-04-11 15:15:42	0	D	\N	Y	\N	A	2016-04-11 16:41:37	0
1000171	3930	#1188 Fix Account Sign in C_ElementValue for GW	0	0	R	\N	2016-04-11 15:15:43	0	D	\N	Y	\N	A	2016-04-11 16:41:38	0
1000172	3950	Storage Detail by Locator	0	0	R	Storage Detail Report by Locator	2016-04-11 15:15:43	0	D	\N	Y	\N	A	2016-04-11 16:41:51	0
1000173	3960	Business Partner window added reporting button	0	0	R	\N	2016-04-11 15:16:18	0	D	\N	Y	\N	A	2016-04-11 16:42:19	0
1000174	3980	1200_add_Fixed_Asset_Menu_Items	0	0	R	\N	2016-04-11 15:17:07	0	D	\N	Y	\N	A	2016-04-11 16:42:21	0
1000175	3990	Default accounts for FA in GardenWorld	0	0	R	\N	2016-04-11 15:17:11	0	D	\N	Y	\N	A	2016-04-11 16:42:23	0
1000176	4010	4010_#1340_ImportOrder_Added_ShipAddressColumns	0	0	R	\N	2016-04-11 15:17:16	0	D	\N	Y	\N	A	2016-04-11 16:42:37	0
1000177	4020	Add Ship Contact Name	0	0	R	\N	2016-04-11 15:17:40	0	D	\N	Y	\N	A	2016-04-11 16:42:39	0
1000178	4030	MiniMRP Run	0	0	R	\N	2016-04-11 15:17:44	0	D	\N	Y	\N	A	2016-04-11 16:43:25	0
1000179	4040	MiniMRP_Run_DisplayedGrid	0	0	R	\N	2016-04-11 15:19:18	0	D	\N	Y	\N	A	2016-04-11 16:43:32	0
1000180	4050	MiniERP_AddProductionColumn	0	0	R	\N	2016-04-11 15:19:23	0	D	\N	Y	\N	A	2016-04-11 16:43:36	0
1000181	4060	Convert Currency Rate Functionality	0	0	R	\N	2016-04-11 15:19:31	0	D	\N	Y	\N	A	2016-04-11 16:43:58	0
1000182	4070	Added C_DocType_ID In ProductionOrder	0	0	R	\N	2016-04-11 15:20:15	0	D	\N	Y	\N	A	2016-04-11 16:43:59	0
1000183	4090	Implement Document Action for Production	0	0	R	\N	2016-04-11 15:20:17	0	D	\N	Y	\N	A	2016-04-11 16:44:08	0
1000184	4130	Doc Status Indicators	0	0	R	\N	2016-04-11 15:20:33	0	D	\N	Y	\N	A	2016-04-11 16:44:32	0
1000185	4170	Open Orders report customization	0	0	R	added virtual columns for Documentno, Ref_Order_ID, ProductNo, and C_Charge_ID	2016-04-11 15:21:18	0	D	\N	Y	\N	A	2016-04-11 16:44:34	0
1000186	4180	1204-clarify_help_on_Product_Purchasing	0	0	R	\N	2016-04-11 15:21:22	0	D	\N	Y	\N	A	2016-04-11 16:44:34	0
1000187	4190	1206_Add_Consolidation_Org_To_GW	0	0	R	\N	2016-04-11 15:21:22	0	D	\N	Y	\N	A	2016-04-11 16:44:37	0
1000188	4200	1209_add_AuthorityType_window_to_menu	0	0	R	add AuthorityType window to menu	2016-04-11 15:21:26	0	D	\N	Y	\N	A	2016-04-11 16:44:38	0
1000189	4210	560_Automatic_production.xml	0	0	R	\N	2016-04-11 15:21:27	0	D	\N	Y	\N	A	2016-04-11 16:44:46	0
1000190	4230	Inherit tab filter	0	0	R	\N	2016-04-11 15:21:40	0	D	\N	Y	\N	A	2016-04-11 16:44:50	0
1000191	4240	Update menu	0	0	R	\N	2016-04-11 15:21:45	0	D	\N	Y	\N	A	2016-04-11 16:46:00	0
1000192	4250	#1232_PurchasingFromReplenishment	0	0	R	\N	2016-04-11 15:22:30	0	D	\N	Y	\N	A	2016-04-11 16:47:22	0
1000193	4270	#1232_Add_SeasonalityWindow	0	0	R	\N	2016-04-11 15:24:42	0	D	\N	Y	\N	A	2016-04-11 16:47:28	0
1000194	4280	#1245_ProductAvailabilityReports	0	0	R	\N	2016-04-11 15:24:55	0	D	\N	Y	\N	A	2016-04-11 16:47:41	0
1000195	4290	#1249_DatePromised on I_Order	0	0	R	\N	2016-04-11 15:25:25	0	D	\N	Y	\N	A	2016-04-11 16:47:42	0
1000196	4300	#1244 GenerateShipment Manual	0	0	R	\N	2016-04-11 15:25:27	0	D	\N	Y	\N	A	2016-04-11 16:47:42	0
1000197	4300	1250_TemplateProduct	0	0	R	\N	2016-04-11 15:25:27	0	D	\N	Y	\N	A	2016-04-11 16:47:50	0
1000198	4300	Open_items_include_reversed	0	0	R	\N	2016-04-11 15:25:38	0	D	\N	Y	\N	A	2016-04-11 16:47:51	0
1000199	4305	#1246 Barcode Scanner support	0	0	R	\N	2016-04-11 15:25:38	0	D	\N	Y	\N	A	2016-04-11 16:48:16	0
1000200	445	Production IsWIP flag	0	0	R	\N	2016-04-11 15:26:32	0	D	\N	Y	\N	A	2016-04-11 16:07:06	0
1000201	4580	#1327 Indented BOM Report	0	0	R	add columns\nc_uom_id - UOM\nIsPurchased - Purchased(Y)/Manufactured(N)	2016-04-11 15:26:33	0	D	\N	Y	\N	A	2016-04-11 16:48:18	0
1000202	4590	#1329 Shipment Manifest	0	0	R	\N	2016-04-11 15:26:36	0	D	\N	Y	\N	A	2016-04-12 09:49:41	0
1000203	4600	M_InOutCandidate_V	0	0	R	\N	2016-04-11 15:29:40	0	D	\N	Y	\N	A	2016-04-11 16:49:46	0
1000204	4600	Report source on column	0	0	R	\N	2016-04-11 15:30:00	0	D	\N	Y	\N	A	2016-04-11 16:50:10	0
1000205	4610	#1354_OrderAndPaymentinOneTransaction	0	0	R	1354: Load Order and Payment in one single transaction\nhttp://redmine.adaxa.com/issues/1354	2016-04-11 15:30:41	0	D	\N	Y	\N	A	2016-04-12 09:49:47	0
1000206	4610	Invoke Allocation from Payment window	0	0	R	\N	2016-04-11 15:30:53	0	D	\N	Y	\N	A	2016-04-11 16:50:20	0
1000207	4620	Feature #1356 Create Receipt from PO 	0	0	R	\N	2016-04-11 15:30:59	0	D	\N	Y	\N	A	2016-04-11 16:50:22	0
1000208	4620	Tidy up menu	0	0	R	\N	2016-04-11 15:31:02	0	D	\N	Y	\N	A	2016-04-11 16:50:32	0
1000209	4630	#1329_ShipmentManifestUpdate	0	0	R	\N	2016-04-11 15:31:14	0	D	\N	Y	\N	A	2016-04-11 16:50:39	0
1000210	4630	Clean up GW	0	0	R	\N	2016-04-11 15:31:26	0	D	\N	Y	1	A	2016-04-11 16:50:32	0
1000211	4640	Revenue recog	0	0	R	\N	2016-04-11 15:31:26	0	D	\N	Y	\N	A	2016-04-11 16:50:39	0
1000212	4650	Add BP to Allocation Hdr	0	0	R	\N	2016-04-11 15:31:27	0	D	\N	Y	\N	A	2016-04-11 16:50:42	0
1000213	4660	Timesheet function	0	0	R	\N	2016-04-11 15:31:30	0	D	\N	Y	\N	A	2016-04-11 16:50:43	0
1000214	4670	Add doc status GW	0	0	R	\N	2016-04-11 15:31:33	0	D	\N	Y	\N	A	2016-04-11 16:50:44	0
1000215	4680	Copy Print Format to System	0	0	R	\N	2016-04-11 15:31:33	0	D	\N	Y	\N	A	2016-04-11 16:50:44	0
1000216	4690	Default to immediate accounting	0	0	R	\N	2016-04-11 15:31:34	0	D	\N	Y	\N	A	2016-04-11 16:50:44	0
1000217	4700	Non_Product_Markup on project	0	0	R	\N	2016-04-11 15:31:34	0	D	\N	Y	\N	A	2016-04-11 16:51:12	0
1000218	4710	Void Payment Selection	0	0	R	\N	2016-04-11 15:32:38	0	D	\N	Y	\N	A	2016-04-11 16:51:13	0
1000219	4720	GL_Rec_by_BP	0	0	R	\N	2016-04-11 15:32:41	0	D	\N	Y	\N	A	2016-04-11 16:51:14	0
1000220	4730	GW Asset Group Accts	0	0	R	\N	2016-04-11 15:32:41	0	D	\N	Y	\N	A	2016-04-11 16:51:16	0
1000221	4740	Locator on Import Product	0	0	R	\N	2016-04-11 15:32:46	0	D	\N	Y	\N	A	2016-04-11 16:51:17	0
1000222	4740	Set Grid Display	0	0	R	\N	2016-04-11 15:32:50	0	D	\N	Y	\N	A	2016-04-11 16:51:35	0
1000223	4750	Filter prospects from BP list	0	0	R	\N	2016-04-11 15:33:07	0	D	\N	Y	\N	A	2016-04-11 16:52:01	0
1000224	4750	Reference to Magento	0	0	R	IDs as reference to other database	2016-04-11 15:33:09	0	D	\N	Y	\N	A	2016-04-11 16:52:09	0
1000225	4760	Bigger Fonts Definition	0	0	R	\N	2016-04-11 15:33:25	0	D	\N	Y	\N	A	2016-04-11 16:52:16	0
1000226	4760	Revenue recognition changes	0	0	R	\N	2016-04-11 15:33:27	0	D	\N	Y	\N	A	2016-04-11 16:52:15	0
1000227	4770	Add dunning age groups	0	0	R	\N	2016-04-11 15:33:34	0	D	\N	Y	\N	A	2016-04-11 16:52:25	0
1000228	4770	Table Definition - C_Invoice_Candidate_V	0	0	R	\N	2016-04-11 15:33:41	0	D	\N	Y	\N	A	2016-04-11 16:52:21	0
1000229	4780	Hide Jasper Report Directory	0	0	R	\N	2016-04-11 15:33:54	0	D	\N	Y	\N	A	2016-04-11 16:52:26	0
1000230	4790	Disable Favourites from Dashboard	0	0	R	\N	2016-04-11 15:33:56	0	D	\N	Y	\N	A	2016-04-11 16:52:27	0
1000231	4790	Document Status Indicator display in Grid	0	0	R	\N	2016-04-11 15:33:56	0	D	\N	Y	\N	A	2016-04-11 16:52:28	0
1000232	4790	Ext Confirmed Qty	0	0	R	for Magento and other ecommerce integration	2016-04-11 15:33:57	0	D	\N	Y	\N	A	2016-04-11 16:52:29	0
1000233	4800	Fix m_inout_candidate_v	0	0	R	\N	2016-04-11 15:33:59	0	D	\N	Y	\N	A	2016-04-11 16:52:29	0
1000234	4810	BP Group in Import Order	0	0	R	\N	2016-04-11 15:34:00	0	D	\N	Y	\N	A	2016-04-11 16:52:40	0
1000235	4810	Menu tidy-up	0	0	R	\N	2016-04-11 15:34:12	0	D	\N	Y	\N	A	2016-04-11 16:52:42	0
1000236	4820	Payment allocation	0	0	R	\N	2016-04-11 15:34:14	0	D	\N	Y	\N	A	2016-04-11 16:52:43	0
1000237	4820	Product Validator	0	0	R	\N	2016-04-11 15:34:14	0	D	\N	Y	\N	A	2016-04-11 16:52:43	0
1000238	4830	#1388 add extra computed columns to Account Cube based VIEW	0	0	R	\N	2016-04-11 15:34:14	0	D	\N	Y	\N	A	2016-04-11 16:52:57	0
1000239	4840	Template Product on Org	0	0	R	\N	2016-04-11 15:34:47	0	D	\N	Y	\N	A	2016-04-11 16:52:58	0
1000240	4850	Client setup- Warehouse Staff	0	0	R	Add flag in Employee as Warehouse staff 	2016-04-11 15:34:49	0	D	\N	Y	\N	A	2016-04-11 16:53:00	0
1000241	4860	Fin Report lines	0	0	R	\N	2016-04-11 15:34:53	0	D	\N	Y	\N	A	2016-04-12 10:03:27	100
1000242	4860	Web Store setup	0	0	R	\N	2016-04-11 15:35:04	0	D	\N	Y	\N	A	2016-04-11 16:53:11	0
1000243	4880	Add_dyn_validation_to_payment	0	0	R	\N	2016-04-11 15:35:09	0	D	\N	Y	\N	A	2016-04-11 16:53:12	0
1000244	4880	External Product ID	0	0	R	\N	2016-04-11 15:35:10	0	D	\N	Y	\N	A	2016-04-11 16:53:17	0
1000245	4890	BOM Ext ID	0	0	R	\N	2016-04-11 15:35:20	0	D	\N	Y	\N	A	2016-04-11 16:53:43	0
1000246	4890	DynValidation on tax	0	0	R	\N	2016-04-11 15:35:57	0	D	\N	Y	\N	A	2016-04-11 16:53:44	0
1000247	4900	ETL Definition	0	0	R	\N	2016-04-11 15:35:58	0	D	\N	Y	\N	A	2016-04-11 16:54:03	0
1000248	4900	Virtual_columns_T_Aging	0	0	R	\N	2016-04-11 15:36:38	0	D	\N	Y	\N	A	2016-04-11 16:54:05	0
1000249	4910	Make IsBOM selection column	0	0	R	\N	2016-04-11 15:36:42	0	D	\N	Y	\N	A	2016-04-11 16:54:16	0
1000250	4910	Setup Magento Integration	0	0	R	\N	2016-04-11 15:36:42	0	D	\N	Y	\N	A	2016-04-11 16:54:15	0
1000251	4910	4910_set_view_flag_on_RV_Fact_Acct_Proj	0	0	R	\N	2016-04-11 15:37:02	0	D	\N	Y	\N	A	2016-04-11 16:54:16	0
1000252	4920	Auto fill charge acct	0	0	R	\N	2016-04-11 15:37:02	0	D	\N	Y	\N	A	2016-04-11 16:54:16	0
1000253	4930	Process Default Logic	0	0	R	Increase field size to accomodate longer SQL for default value	2016-04-11 15:37:02	0	D	\N	Y	\N	A	2016-04-11 16:54:17	0
1000254	4940	ClientSetup no COA file	0	0	R	\N	2016-04-11 15:37:03	0	D	\N	Y	\N	A	2016-04-11 16:54:17	0
1000255	4950	DocumentStatus Template	0	0	R	\N	2016-04-11 15:37:03	0	D	\N	Y	\N	A	2016-04-11 16:54:18	0
1000256	4960	COA - AU and NZ	0	0	R	\N	2016-04-11 15:37:05	0	D	\N	Y	\N	A	2016-04-11 16:54:18	0
1000257	4970	fix for AD_Attachment migration	0	0	R	\N	2016-04-11 15:37:06	0	D	\N	Y	\N	A	2016-04-11 16:54:19	0
1000258	5000	Delete Unconfirmed Production	0	0	R	\N	2016-04-11 15:37:06	0	D	\N	Y	\N	A	2016-04-11 16:54:20	0
1000259	5010	Delete Planned Purchase Orders	0	0	R	\N	2016-04-11 15:37:08	0	D	\N	Y	\N	A	2016-04-11 16:54:21	0
1000260	5020	5020_#830_MiniMRP_Change_Column_Reference	0	0	R	\N	2016-04-11 15:37:10	0	D	\N	Y	\N	A	2016-04-11 16:54:30	0
1000261	5020	Production Batch Table	0	0	R	\N	2016-04-11 15:37:18	0	D	\N	Y	\N	A	2016-04-11 16:54:42	0
1000262	5030	Added Price List column on MiniMRP	0	0	R	\N	2016-04-11 15:37:44	0	D	\N	Y	\N	A	2016-04-12 10:01:52	100
1000263	5040	Migration Imrovements	0	0	R	\N	2016-04-11 15:37:46	0	D	\N	Y	\N	A	2016-04-12 09:50:02	0
1000264	5040	Suggested Requisition and Planned Production Report On MiniM	0	0	R	\N	2016-04-11 15:37:47	0	D	\N	Y	\N	A	2016-04-11 16:55:16	0
1000265	5030	Production Batch Window	0	0	R	\N	2016-04-11 15:38:30	0	D	\N	Y	\N	A	2016-04-11 16:54:58	0
1000266	5050	DocType on Production	0	0	R	\N	2016-04-11 15:38:56	0	D	\N	Y	\N	A	2016-04-12 09:50:10	0
1000267	5070	Inventory Move in Production	0	0	R	\N	2016-04-11 15:39:05	0	D	\N	Y	\N	A	2016-04-12 09:50:37	0
1000268	5080	Production_IsCreatedAsBoolean	0	0	R	Change reference from List to Y/N	2016-04-11 15:40:00	0	D	\N	Y	\N	A	2016-04-12 09:50:46	0
1000269	5090	#1406_Allow New Attribute Instance Flag	0	0	R	\N	2016-04-11 15:40:17	0	D	\N	Y	\N	A	2016-04-12 09:50:48	0
1000270	5100	Indented BOM Report Update	0	0	R	\N	2016-04-11 15:40:20	0	D	\N	Y	\N	A	2016-04-12 09:50:50	0
1000271	5100	Production Batch window 2	0	0	R	\N	2016-04-11 15:40:24	0	D	\N	Y	\N	A	2016-04-12 09:50:53	0
1000272	5110	User HasRole	0	0	R	\N	2016-04-11 15:40:27	0	D	\N	Y	\N	A	2016-04-12 09:50:54	0
1000273	5120	Display Error Dialog	0	0	R	\N	2016-04-11 15:40:29	0	D	\N	Y	\N	A	2016-04-12 09:50:56	0
1000274	5130	MRP Convert Document	0	0	R	\N	2016-04-11 15:40:34	0	D	\N	Y	\N	A	2016-04-12 09:50:57	0
1000275	5140	Excessive Percent Diff Max	0	0	R	\N	2016-04-11 15:40:35	0	D	\N	Y	\N	A	2016-04-12 09:50:58	0
1000276	5150	Allow Same Locator Inventory Move	0	0	R	\N	2016-04-11 15:40:38	0	D	\N	Y	\N	A	2016-04-12 09:50:59	0
1000277	5160	MRP report filtering	0	0	R	\N	2016-04-11 15:40:40	0	D	\N	Y	\N	A	2016-04-12 09:51:01	0
1000278	5160	QOH as Default Move Qty	0	0	R	\N	2016-04-11 15:40:43	0	D	\N	Y	\N	A	2016-04-12 09:51:02	0
1000279	5170	RV_MiniMRP_Requisition update	0	0	R	\N	2016-04-11 15:40:45	0	D	\N	Y	\N	A	2016-04-12 09:51:02	0
1000280	5180	Replenish by production	0	0	R	\N	2016-04-11 15:40:46	0	D	\N	Y	\N	A	2016-04-12 09:51:03	0
1000281	5180	Production Batch Window tidy up	0	0	R	\N	2016-04-11 15:40:46	0	D	\N	Y	\N	A	2016-04-12 09:51:06	0
1000282	5190	MPO - Manufacturing Planned Order	0	0	R	add to reference list	2016-04-11 15:40:51	0	D	\N	Y	\N	A	2016-04-12 09:51:06	0
1000283	5200	ProductionBatchMove tidyup	0	0	R	\N	2016-04-11 15:40:51	0	D	\N	Y	\N	A	2016-04-12 09:51:07	0
1000284	520	Clear default language on BP	0	0	R	\N	2016-04-11 15:40:52	0	D	\N	Y	\N	A	2016-04-11 16:07:07	0
1000285	5210	RV_ProductionLine	0	0	R	\N	2016-04-11 15:40:52	0	D	\N	Y	\N	A	2016-04-12 09:51:15	0
1000286	5220	Create ReplenishType As MRP Calculated Replenish	0	0	R	\N	2016-04-11 15:41:16	0	D	\N	Y	\N	A	2016-04-12 09:51:16	0
1000287	5220	Separate Window for Planned Manufacturing Order	0	0	R	\N	2016-04-11 15:41:17	0	D	\N	Y	\N	A	2016-04-12 09:51:54	0
1000288	5230	Added Change Role Message	0	0	R	\N	2016-04-11 15:43:06	0	D	\N	Y	\N	A	2016-04-12 09:51:55	0
1000289	5230	Make PrintFormat TableBased Updateable	0	0	R	\N	2016-04-11 15:43:07	0	D	\N	Y	\N	A	2016-04-12 09:51:55	0
1000290	5240	Add Unique constraint to C_Conversation_Rate Table	0	0	R	\N	2016-04-11 15:43:07	0	D	\N	Y	\N	A	2016-04-12 09:51:55	0
1000291	5240	RV_ProductionLine 2	0	0	R	\N	2016-04-11 15:43:07	0	D	\N	Y	\N	A	2016-04-12 09:51:56	0
1000292	5250	Unique Constraint	0	0	R	\N	2016-04-11 15:43:09	0	D	\N	Y	\N	A	2016-04-12 09:51:58	0
1000293	5250	ABA export	0	0	R	\N	2016-04-11 15:43:12	0	D	\N	Y	\N	A	2016-04-12 09:52:06	0
1000294	5260	Manufacturing PO Requisition	0	0	R	\N	2016-04-11 15:43:29	0	D	\N	Y	\N	A	2016-04-12 09:52:06	0
1000295	5270	Initial Request Setup	0	0	R	\N	2016-04-11 15:43:30	0	D	\N	Y	\N	A	2016-04-12 09:52:11	0
1000296	5290	AttributeSetInstance update	0	0	R	\N	2016-04-11 15:43:34	0	D	\N	Y	\N	A	2016-04-12 09:52:12	0
1000297	5290	Production Batch default Doc Type	0	0	R	\N	2016-04-11 15:43:34	0	D	\N	Y	\N	A	2016-04-12 09:52:12	0
1000298	530	Template logo print	0	0	R	\N	2016-04-11 15:43:35	0	D	\N	Y	\N	A	2016-04-11 16:07:07	0
1000299	5310	SerialNo Inquiry Form	0	0	R	\N	2016-04-11 15:43:35	0	D	\N	Y	\N	A	2016-04-12 09:52:19	0
1000300	5340	Shipment candidate view fix	0	0	R	\N	2016-04-11 15:43:40	0	D	\N	Y	\N	A	2016-04-12 09:52:19	0
1000301	5400	Release Adaxa 1.4	0	0	R	\N	2016-04-11 15:43:41	0	D	\N	Y	\N	A	2016-04-12 09:52:19	0
1000302	557	BOM Rollup Future Cost	0	0	R	\N	2016-04-11 15:43:41	0	D	\N	Y	\N	A	2016-04-11 16:07:08	0
1000303	570	Invoice Tax Accounted Report	0	0	R	\N	2016-04-11 15:43:44	0	D	\N	Y	\N	A	2016-04-11 16:07:21	0
1000304	666	BOM Copy	0	0	R	\N	2016-04-11 15:44:20	0	D	\N	Y	\N	A	2016-04-11 16:07:22	0
1000305	701	Stocktake - Part1	0	0	R	Picked Qty	2016-04-11 15:44:23	0	D	\N	Y	\N	A	2016-04-11 16:07:24	0
1000306	702	Stocktake - Part2	0	0	R	Physical Inventory window additions	2016-04-11 15:44:26	0	D	\N	Y	\N	A	2016-04-11 16:07:47	0
1000307	703	Stocktake - Part3	0	0	R	Stocktake form	2016-04-11 15:45:20	0	D	\N	Y	\N	A	2016-04-11 16:07:55	0
1000308	704	Stocktake - Part4	0	0	R	Print Formats	2016-04-11 15:45:43	0	D	\N	Y	\N	A	2016-04-11 16:08:18	0
1000309	716	Delete open production orders	0	0	R	\N	2016-04-11 15:47:21	0	D	\N	Y	\N	A	2016-04-11 16:08:19	0
1000310	910	Storage cleanup parameters	0	0	R	\N	2016-04-11 15:47:23	0	D	\N	Y	\N	A	2016-04-11 16:08:19	0
1000311	920	Add paid amount to invoice view	0	0	R	1062_Add invoice paid amount and invoice unpaid amount to view used to print invoices	2016-04-11 15:47:24	0	D	\N	Y	\N	A	2016-04-11 16:08:20	0
1000312	920	Dunning create parameter	0	0	R	\N	2016-04-11 15:47:26	0	D	\N	Y	\N	A	2016-04-11 16:08:20	0
1000313	930	IDEMPIERE-45	0	0	R	\N	2016-04-11 15:47:27	0	D	\N	Y	\N	A	2016-04-11 16:08:25	0
1000314	931	AddColumnsPaySelectionCheck	0	0	R	\N	2016-04-11 15:47:29	0	D	\N	Y	\N	A	2016-04-11 16:08:27	0
1000315	932	FR2979756_AdaxaPOS	0	0	R	\N	2016-04-11 15:47:31	0	D	\N	Y	\N	A	2016-04-11 16:10:00	0
1000316	933	FR2979756_AdaxaPOS	0	0	R	\N	2016-04-11 15:49:03	0	D	\N	Y	\N	A	2016-04-11 16:11:12	0
1000317	934	FR2979756_AdaxaPOS	0	0	R	\N	2016-04-11 15:50:18	0	D	\N	Y	\N	A	2016-04-11 16:11:12	0
1000318	935	FK_POS_BF2975308_FR2979756	0	0	R	\N	2016-04-11 15:50:19	0	D	\N	Y	\N	A	2016-04-11 16:11:15	0
1000319	936	IDEMPIERE67_IsAllowCopy	0	0	R	\N	2016-04-11 15:50:22	0	D	\N	Y	\N	A	2016-04-11 16:11:25	0
1000320	937	IDEMPIERE23	0	0	R	\N	2016-04-11 15:50:29	0	D	\N	Y	\N	A	2016-04-11 16:11:26	0
1000321	938	IDEMPIERE65	0	0	R	\N	2016-04-11 15:50:31	0	D	\N	Y	\N	A	2016-04-11 16:11:28	0
1000322	940	EnableWorkflowProcessClient	0	0	R	\N	2016-04-11 15:50:34	0	D	\N	Y	\N	A	2016-04-11 16:11:29	0
1000323	942	IDEMPIERE-98	0	0	R	\N	2016-04-11 15:50:34	0	D	\N	Y	\N	A	2016-04-11 16:11:30	0
1000324	943	IDEMPIERE23	0	0	R	\N	2016-04-11 15:50:36	0	D	\N	Y	\N	A	2016-04-11 16:11:31	0
1000325	945	IDEMPIERE-126	0	0	R	\N	2016-04-11 15:50:36	0	D	\N	Y	\N	A	2016-04-11 16:11:32	0
1000326	946	IDEMPIERE-127_RecentItems	0	0	R	\N	2016-04-11 15:50:37	0	D	\N	Y	\N	A	2016-04-11 16:11:44	0
1000327	947	IDEMPIERE-134_NewGWPeriods	0	0	R	\N	2016-04-11 15:50:50	0	D	\N	Y	\N	A	2016-04-11 16:13:29	0
1000328	948	IDEMPIERE-133	0	0	R	\N	2016-04-11 15:52:37	0	D	\N	Y	\N	A	2016-04-11 16:13:30	0
1000329	949	IDEMPIERE-139_LastMigrationScriptApplied	0	0	R	\N	2016-04-11 15:52:38	0	D	\N	Y	\N	A	2016-04-11 16:13:32	0
1000330	950	IDEMPIERE-147	0	0	R	\N	2016-04-11 15:52:40	0	D	\N	Y	\N	A	2016-04-11 16:13:36	0
1000331	951	IDEMPIERE-148	0	0	R	\N	2016-04-11 15:52:43	0	D	\N	Y	\N	A	2016-04-11 16:13:36	0
1000332	952	IDEMPIERE-140	0	0	R	\N	2016-04-11 15:52:44	0	D	\N	Y	\N	A	2016-04-11 16:13:52	0
1000333	953	IDEMPIERE-153	0	0	R	\N	2016-04-11 15:53:00	0	D	\N	Y	\N	A	2016-04-11 16:14:08	0
1000334	955	IDEMPIERE-137_Ambidexter_GL_Rec	0	0	R	\N	2016-04-11 15:53:15	0	D	\N	Y	\N	A	2016-04-11 16:14:11	0
1000335	956	IDEMPIERE-178	0	0	R	\N	2016-04-11 15:53:18	0	D	\N	Y	\N	A	2016-04-11 16:14:17	0
1000336	960	ViewSmartBrowse	0	0	R	\N	2016-04-11 15:53:24	0	D	\N	Y	\N	A	2016-04-11 16:18:37	0
1000337	961	ApplicationDictionary	0	0	R	\N	2016-04-11 15:57:48	0	D	\N	Y	\N	A	2016-04-11 16:18:38	0
1000338	962	FixAccesLevel	0	0	R	\N	2016-04-11 15:57:50	0	D	\N	Y	\N	A	2016-04-11 16:18:40	0
1000339	963	Changes	0	0	R	\N	2016-04-11 15:57:51	0	D	\N	Y	\N	A	2016-04-11 16:18:44	0
1000340	964	FR2899403_AD_Element	0	0	R	\N	2016-04-11 15:57:55	0	D	\N	Y	\N	A	2016-04-11 16:19:58	0
1000341	990	AD Migration apply param	0	0	R	\N	2016-04-11 15:58:06	0	D	\N	Y	\N	A	2016-04-11 16:19:59	0
1000343	5350	menu tidy up	0	0	R	\N	2016-04-13 12:58:53	0	D	\N	Y	\N	A	2016-04-13 13:00:40	0
1000344	5360	Add processedon to s_timesheetentry	0	0	R	\N	2016-04-13 14:32:04	0	D	\N	Y	\N	A	2016-04-13 14:32:06	0
1000345	3090	Default_Period_for_Financial_Report	0	0	R	\N	2016-07-01 00:50:01	0	D	\N	Y	\N	A	2016-07-01 00:50:56	0
1000346	5040	Suggested Requisition and Planned Production Report On MiniM	0	0	R	\N	2016-07-01 00:50:02	0	D	\N	Y	\N	A	2016-07-01 00:51:04	0
1000347	5270	DocAction On Production Batch	0	0	R	\N	2016-07-01 00:50:05	0	D	\N	Y	\N	A	2016-07-01 00:51:06	0
1000348	5280	 AD fix for Accounting Fact Summary report	0	0	R	\N	2016-07-01 00:50:06	0	D	\N	Y	\N	A	2016-07-01 00:51:06	0
1000349	5280	QuickEntry	0	0	R	\N	2016-07-01 00:50:06	0	D	\N	Y	\N	A	2016-07-01 00:51:06	0
1000350	5320	Added IsQuickSheet and IsCanSaveColumnWidthEveryone Column	0	0	R	\N	2016-07-01 00:50:06	0	D	\N	Y	\N	A	2016-07-01 00:51:07	0
1000351	5330	Allow XLS and HTML view	0	0	R	\N	2016-07-01 00:50:06	0	D	\N	Y	\N	A	2016-07-01 00:51:08	0
1000352	5330	Added Column on M_PRODUCT_STOCK_V View	0	0	R	Added QtyOrdered Column On M_PRODUCT_STOCK_V View	2016-07-01 00:50:07	0	D	\N	Y	\N	A	2016-07-01 00:51:08	0
1000353	5340	XLSX support	0	0	R	\N	2016-07-01 00:50:07	0	D	\N	Y	\N	A	2016-07-01 00:51:08	0
1000354	5350	fix C_BPartner Location (Is ShipTo)	0	0	R	\N	2016-07-01 00:50:07	0	D	\N	Y	\N	A	2016-07-01 00:51:08	0
1000355	5360	#1441 Dynamic Zoom	0	0	R	\N	2016-07-01 00:50:07	0	D	\N	Y	\N	A	2016-07-01 00:51:11	0
1000356	5370	Use Dynamic Zoom for InternalUse	0	0	R	\N	2016-07-01 00:50:09	0	D	\N	Y	\N	A	2016-07-01 00:51:11	0
1000357	5380	Dynamic Zoom for Production Batch	0	0	R	\N	2016-07-01 00:50:09	0	D	\N	Y	\N	A	2016-07-01 00:51:11	0
1000358	5400	Create M_PBatch_Line table	0	0	R	\N	2016-07-01 00:50:09	0	D	\N	Y	\N	A	2016-07-01 00:51:13	0
1000359	5400	Production Recording	0	0	R	\N	2016-07-01 00:50:09	0	D	\N	Y	\N	A	2016-07-01 00:51:19	0
1000360	5410	Info Window Enhacement	0	0	R	\N	2016-07-01 00:50:13	0	D	\N	Y	\N	A	2016-07-01 00:51:31	0
1000361	5410	Add blank line to Fin report line types	0	0	R	\N	2016-07-01 00:50:21	0	D	\N	Y	\N	A	2016-07-01 00:51:31	0
1000362	5410	Production Recording#2	0	0	R	\N	2016-07-01 00:50:22	0	D	\N	Y	\N	A	2016-07-01 00:51:31	0
1000363	5420	CreateMoveonProduction	0	0	R	\N	2016-07-01 00:50:22	0	D	\N	Y	\N	A	2016-07-01 00:51:31	0
1000364	5440	Column Link Process	0	0	R	\N	2016-07-01 00:50:22	0	D	\N	Y	\N	A	2016-07-01 00:51:36	0
1000365	5450	Tidy MRP Run	0	0	R	\N	2016-07-01 00:50:24	0	D	\N	Y	\N	A	2016-07-01 00:51:37	0
1000366	5460	Link Process to MRP Line M_Production_ID	0	0	R	\N	2016-07-01 00:50:25	0	D	\N	Y	\N	A	2016-07-01 00:51:37	0
1000367	5460	Fix Fin report columns	0	0	R	\N	2016-07-01 00:50:25	0	D	\N	Y	\N	A	2016-07-01 00:51:38	0
1000368	5470	ClientSetup Report Line set	0	0	R	\N	2016-07-01 00:50:25	0	D	\N	Y	\N	A	2016-07-01 00:51:38	0
1000369	5470	Remove Display Logic from Report Cube elements	0	0	R	\N	2016-07-01 00:50:25	0	D	\N	Y	\N	A	2016-07-01 00:51:38	0
1000370	5480	5480_#1454_RunningBalanceOnStatementofAccount	0	0	R	View, Stored Procedure and extra table to calculate running Balance	2016-07-01 00:50:25	0	D	\N	Y	\N	A	2016-07-01 00:51:40	0
1000371	5490	BugfixGenerateDefaultReplenishment	0	0	R	\N	2016-07-01 00:50:26	0	D	\N	Y	\N	A	2016-07-01 00:51:40	0
1000372	5490	Delete invalid field on Vendor	0	0	R	\N	2016-07-01 00:50:26	0	D	\N	Y	\N	A	2016-07-01 00:51:40	0
1000373	5490	CapacityPlanning	0	0	R	\N	2016-07-01 00:50:26	0	D	\N	Y	\N	P	2016-07-01 00:52:00	0
1000374	5500	Add_import_fields_to_vendor	0	0	R	\N	2016-07-01 00:50:43	0	D	\N	Y	\N	A	2016-07-01 00:52:02	0
1000375	5500	Zoom Condition- Production	0	0	R	\N	2016-07-01 00:50:44	0	D	\N	Y	\N	A	2016-07-01 00:52:02	0
1000376	5510	StoragePerProduct-Add costing	0	0	R	\N	2016-07-01 00:50:44	0	D	\N	Y	\N	A	2016-07-01 00:52:03	0
1000377	5520	MRP_RunlineOptimization	0	0	R	\N	2016-07-01 00:50:45	0	D	\N	Y	\N	A	2016-07-01 00:52:03	0
1000378	5530	Define Planned Manufacturing Order Link	0	0	R	\N	2016-07-01 00:50:45	0	D	\N	Y	\N	A	2016-07-01 00:52:03	0
1000379	5540	Tidy Migration Lookup	0	0	R	\N	2016-07-01 00:50:45	0	D	\N	Y	\N	A	2016-07-01 00:52:03	0
1000380	5550	MRPRun_Enhancements	0	0	R	\N	2016-07-01 00:50:45	0	D	\N	Y	\N	A	2016-07-01 00:52:04	0
1000381	5560	MiniMRP Run QuickSetup	0	0	R	\N	2016-07-01 00:50:45	0	D	\N	Y	\N	A	2016-07-01 00:52:06	0
1000382	5570	PriceList column on Requisition line	0	0	R	\N	2016-07-01 00:50:47	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000383	5580	Update MRP default Print Format	0	0	R	\N	2016-07-01 00:50:47	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000384	5580	SysConfig_SUPPORT_MULTITAB_BROWSER	0	0	R	\N	2016-07-01 00:50:47	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000385	5590	GridCustomize	0	0	R	\N	2016-07-01 00:50:47	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000386	5590	Add shipper in Manifest	0	0	R	\N	2016-07-01 00:50:47	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000387	5600	Set Length of AD_Session Description Field	0	0	R	\N	2016-07-01 00:50:48	0	D	\N	Y	\N	A	2016-07-01 00:52:07	0
1000388	5600	Print Shipment Manifest	0	0	R	\N	2016-07-01 00:50:48	0	D	\N	Y	\N	A	2016-07-01 00:52:15	0
1000389	5610	Feature#1487_StorageEnhancements	0	0	R	\N	2016-07-01 00:50:54	0	D	\N	Y	\N	A	2016-07-01 00:52:16	0
1000390	5620	Feature#1488	0	0	R	Shipment Manifest is mandatory, set in Shipper	2016-07-01 00:50:55	0	D	\N	Y	\N	A	2016-07-01 00:52:16	0
1000391	5630	Feature#1489	0	0	R	Allow Generate Shipment Flag	2016-07-01 00:50:55	0	D	\N	Y	\N	A	2016-07-01 00:52:16	0
1000392	5650	Feature#1494	0	0	R	Close Sales Order when Shipment Closed	2016-07-01 00:50:55	0	D	\N	Y	\N	A	2016-07-01 00:52:17	0
1000393	5040	Suggested Requisition and Planned Production Report On MiniM	0	0	R	\N	2016-07-01 00:55:18	0	D	\N	Y	\N	A	2016-07-01 00:55:32	0
1000394	56600	Release Adaxa 1.5	0	0	R	\N	2016-07-01 00:55:22	0	D	\N	Y	\N	A	2016-07-01 00:55:37	0
1000395	73	Add Changes and translation ADempiere POS	0	0	R	\N	2019-02-04 13:37:12	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:23	0
1000396	74	Add View for Order Line ADempiere POS	0	0	R	With it migration you can have a view for POS Order Line	2019-02-04 13:37:12	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:23	0
1000397	75	Add Changes and translation ADempiere POS	0	0	R	\N	2019-02-04 13:37:12	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:23	0
1000398	115	Create new menu for POS and new business logic process	0	0	R	Create new POS menu and business logic process for  Create an Order based on other, and Immediate invoice and reverse sales trandaction 	2019-02-04 13:37:12	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:24	0
1000399	116	POS Contribution	0	0	R	Add reference list for credit memo.	2019-02-04 13:37:13	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:24	0
1000400	117	Add parameter for cancel POS Order	0	0	R	IsCancelled indicate that the cancel sales transaction is force.	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:24	0
1000401	118	Allows use any SO Document Type in POS	0	0	R	\N	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:24	0
1000402	119	Is Enable Product Lookup	0	0	R	\N	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:25	0
1000403	120	Is POS Required PIN	0	0	R	\N	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:25	0
1000404	121	Invoice Rule And Delivery Rule	0	0	R	Add Invoice Rule and Delivery Rule in POS Terminal	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:25	0
1000405	122	Electronic scales and Measure Request Code	0	0	R	\N	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:25	0
1000406	135	Add new field for POS Terminal	0	0	R	\N	2019-02-04 13:37:14	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:27	0
1000407	136	Add field C_POS_ID on related tables	0	0	R	\N	2019-02-04 13:37:15	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:27	0
1000408	139	#116 Implement Smart Browser for POS Withdrawal	0	0	R	\N	2019-02-04 13:37:15	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:28	0
1000409	140	#117 Implement Smart Browser for POS Statement Close	0	0	R	\N	2019-02-04 13:37:17	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:38	0
1000410	141	POS_OrderLine_v View don't have XML #123	0	0	R	See: https://github.com/adempiere/adempierePOS/issues/123	2019-02-04 13:37:33	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:38	0
1000411	146	#128 Setup Garden World POS data for new POS	0	0	R	\N	2019-02-04 13:37:33	0	D	\N	Y	3.8.0#002	A	2019-02-04 13:38:39	0
1000412	1100	Scheduler Run Now	0	0	R	\N	2019-02-04 13:37:34	0	D	\N	Y	\N	A	2019-02-04 13:38:39	0
1000413	1730	Missing AD changes DA module.	0	0	R	\N	2019-02-04 13:37:34	0	D	\N	Y	\N	A	2019-02-04 13:38:40	0
1000414	1820	Style Change for default Print Table Format	0	0	R	\N	2019-02-04 13:37:35	0	D	\N	Y	3.9.0	A	2019-02-04 13:38:40	0
1000415	3050	Add_RelativePeriodTo_Column_In_PA_ReportColumn	0	0	R	\N	2019-02-04 13:37:35	0	D	\N	Y	\N	A	2019-02-04 13:38:40	0
1000416	5040	Suggested Requisition and Planned Production Report On MiniM	0	0	R	\N	2019-02-04 13:37:36	0	D	\N	Y	\N	A	2019-02-04 13:38:44	0
1000417	5620	Fix payment dyn validation	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000418	5660	Release Adaxa 1.5	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000419	5670	Add doc action access to doc type window	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000420	5690	Inventory count update param	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000421	5700	5700_#1501_Update_RV_UnPosted_View	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000422	5710	ProductionBatchCreation	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:47	0
1000423	5710	Product Documents	0	0	R	\N	2019-02-04 13:37:37	0	D	\N	Y	\N	A	2019-02-04 13:38:48	0
1000424	5720	5720_#830_Create_DocType_Columns_MRP_Run	0	0	R	\N	2019-02-04 13:37:38	0	D	\N	Y	\N	A	2019-02-04 13:38:49	0
1000425	5720	Bugfix for #1454	0	0	R	\N	2019-02-04 13:37:39	0	D	\N	Y	\N	A	2019-02-04 13:38:49	0
1000426	5740	5740_#830_Process_ProductionBatchMigrateOlderDB	0	0	R	\N	2019-02-04 13:37:39	0	D	\N	Y	\N	A	2019-02-04 13:38:49	0
1000427	5750	MRP enhancements#1522	0	0	R	\N	2019-02-04 13:37:39	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000428	5760	MRPRun Reset	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000429	5760	Speed up Production Lines	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000430	5770	Use SP on Production createlines	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000431	5780	5780_#1431_RefreshTimer_on_AD_Tab_Customization_for_QuickShe	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000432	5780	Currency Convert default to Org=*	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000433	5780	Fix dunning view #1454	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000434	5790	5790_#1526_InterCompanyDueValidator	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000435	5790	Remittance Advice Mail Template as Mandatory	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:50	0
1000436	5790	Target Delivery Time in Requisition	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:51	0
1000437	5800	5800_#1530_IsWebstoreUser_On User	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:51	0
1000438	5800	ProductionPrepare_DoNotComplete	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:51	0
1000439	5800	GL Journal Batch Zoom To Window	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	A	2019-02-04 13:38:51	0
1000440	5810	Feature#1548_InventoryValueByLocator	0	0	R	\N	2019-02-04 13:37:40	0	D	\N	Y	\N	P	2019-02-04 13:38:56	0
1000441	5810	Product Documents Filter	0	0	R	\N	2019-02-04 13:37:45	0	D	\N	Y	\N	A	2019-02-04 13:38:56	0
1000442	5810	SchedulerType-Manual	0	0	R	\N	2019-02-04 13:37:45	0	D	\N	Y	\N	A	2019-02-04 13:38:56	0
1000443	5820	Fix DocStatus Allow Copy flags	0	0	R	\N	2019-02-04 13:37:45	0	D	\N	Y	\N	A	2019-02-04 13:38:56	0
1000444	5820	Feature#1549 Preserve Finished Goods	0	0	R	\N	2019-02-04 13:37:45	0	D	\N	Y	\N	A	2019-02-04 13:38:56	0
1000445	5830	#1513_Dashboard_added_DocStatus_Indicator	0	0	R	1. Added Document Status Indicator to Dashboard\n2. Added Client, Org, SeqNo to grid for  Document Status Indicator window\t	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000446	5830	Fix Production Batch copy record	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000447	5840	#1513_Dashboard_converted_Performance_to_DocStatus	0	0	R	Added GW Document Status Indicators for Notice, Request, WF, Unprocessed Document, etc\t	2019-02-04 13:37:46	0	D	\N	Y	\N	P	2019-02-04 13:38:57	0
1000448	5840	Rename GW price lists	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000449	5850	Release Adaxa 1.6	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000450	5860	Fix DocStatus Allow Copy flags	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000451	5860	#1559 Production Scanner	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:38:57	0
1000452	5860	Import Column	0	0	R	\N	2019-02-04 13:37:46	0	D	\N	Y	\N	A	2019-02-04 13:39:11	0
1000453	5870	ImportFormatFromPrintFormat	0	0	R	\N	2019-02-04 13:38:06	0	D	\N	Y	\N	A	2019-02-04 13:39:11	0
1000454	5880	Import Column 2	0	0	R	\N	2019-02-04 13:38:06	0	D	\N	Y	\N	A	2019-02-04 13:39:16	0
1000455	5890	Bug#1562 Production Order Workflow	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:16	0
1000456	5900	Feature#1564 ProductLocator	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:16	0
1000457	5910	Product Attribute fix	0	0	R	Should return empty string instead of null consistently	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:16	0
1000458	5920	5920_#1027_IsColl_UserFavPanel	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:16	0
1000459	5920	Copy binary objects	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:17	0
1000460	5930	5930_#1584_MiniMRP_IsExcludeKanbanProduct	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:17	0
1000461	5940	5940_#1585_MiniMRP_Aggregate	0	0	R	\N	2019-02-04 13:38:11	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000462	5950	5950_#1585_MRP_RunLine_OrderInfo_IncrSize	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000463	5950	#1591_CreatePOfromRequisition	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000464	5960	5960_#872_FinReport_PF_ShowLogo	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000465	5960	CRP enhancements	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000466	5960	Fix missing filling_line	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:18	0
1000467	5970	#1480_Fix_DefaultValue	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:19	0
1000468	5970	Import Report Line format	0	0	R	\N	2019-02-04 13:38:12	0	D	\N	Y	\N	A	2019-02-04 13:39:19	0
1000469	5980	#1595_GL_Distribution_Enhancements.xml	0	0	R	\N	2019-02-04 13:38:13	0	D	\N	Y	\N	A	2019-02-04 13:39:19	0
1000470	5990	#1598_KeyCaseInsentiive	0	0	R	\N	2019-02-04 13:38:13	0	D	\N	Y	\N	A	2019-02-04 13:39:19	0
1000471	6000	CRP Enhancements #2	0	0	R	\N	2019-02-04 13:38:13	0	D	\N	Y	\N	A	2019-02-04 13:39:20	0
1000472	6010	#1605_ChartSeriesSequence_and_Color	0	0	R	\N	2019-02-04 13:38:13	0	D	\N	Y	\N	A	2019-02-04 13:39:20	0
1000473	6010	6010_#830_TempTable_WorkCapacityReport	0	0	R	\N	2019-02-04 13:38:14	0	D	\N	Y	\N	A	2019-02-04 13:39:23	0
1000474	6020	#1606_ColorTable	0	0	R	\N	2019-02-04 13:38:16	0	D	\N	Y	\N	A	2019-02-04 13:39:23	0
1000475	6020	6020_#830_MRP_RUN_Work_CapacityPrintFormat	0	0	R	\N	2019-02-04 13:38:16	0	D	\N	Y	\N	A	2019-02-04 13:39:25	0
1000476	6030	#1631_AdditionalChartParameters	0	0	R	\N	2019-02-04 13:38:18	0	D	\N	Y	\N	A	2019-02-04 13:39:26	0
1000477	6030	6030_#830_T_Product_BOMLine_RV_PF_Menu	0	0	R	\N	2019-02-04 13:38:19	0	D	\N	Y	\N	A	2019-02-04 13:39:28	0
1000478	6040	CRP Charts	0	0	R	\N	2019-02-04 13:38:20	0	D	\N	Y	\N	A	2019-02-04 13:39:29	0
1000479	6040	Feature#1654	0	0	R	\N	2019-02-04 13:38:20	0	D	\N	Y	\N	A	2019-02-04 13:39:29	0
1000480	6050	Embedded Fonts	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000481	6060	Embed Liberation Fonts	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000482	6060	Tidy Up Generate Receipt from Invoice	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000483	6070	Embed OpenSans Fonts	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000484	6080	Embed Liberation Serif fonts	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000485	6080	Permit Complete order on Credit Hold	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000486	6090	Fix BOM Asi Functions	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000487	6090	TotalAmountReserved	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:30	0
1000488	6100	Validate Partner on BP Window	0	0	R	\N	2019-02-04 13:38:21	0	D	\N	Y	\N	A	2019-02-04 13:39:31	0
1000489	6110	Cleanup Copy C_Order Record	0	0	R	\N	2019-02-04 13:38:22	0	D	\N	Y	\N	P	2019-02-04 13:39:32	0
1000490	6120	Tidy up shipment window	0	0	R	\N	2019-02-04 13:38:22	0	D	\N	Y	\N	A	2019-02-04 13:39:32	0
1000491	6130	Edit Validation C_Order in M_InOut	0	0	R	\N	2019-02-04 13:38:22	0	D	\N	Y	\N	A	2019-02-04 13:39:32	0
1000492	6140	Force Complete Order On Hold	0	0	R	\N	2019-02-04 13:38:22	0	D	\N	Y	\N	A	2019-02-04 13:39:32	0
1000493	6150	Release Adaxa 1.7	0	0	R	\N	2019-02-04 13:38:23	0	D	\N	Y	\N	A	2019-02-04 13:39:32	0
\.
