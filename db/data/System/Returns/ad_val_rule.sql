copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
52000	M_InOutShipment/Receipt	0	0	M_InOut.MovementType IN ('C-', 'V+') AND M_InOut.DocStatus IN ('CO', 'CL')	2007-07-05 00:00:00	100	\N	Returns	Y	S	2007-07-05 00:00:00	100
52001	M_InOutShipment/Receipt (RMA)	0	0	M_InOutLine.M_InOut_ID=@InOut_ID@	2007-07-05 00:00:00	100	\N	Returns	Y	S	2010-03-01 12:10:16	100
52063	C_DocType Customer Return	0	0	C_DocType.DocBaseType IN ('MMR') AND IsSOTrx='Y'	2009-09-11 00:37:40	100	Document Type Material Receipts	Returns	Y	S	2009-09-11 00:37:40	100
52064	C_DocType Vendor Return	0	0	C_DocType.DocBaseType IN ('MMS') AND IsSOTrx='N'	2009-09-11 00:44:52	100	Document Type Vendor Return	Returns	Y	S	2009-09-11 00:44:52	100
52065	C_DocType Customer RMA	0	0	C_DocType.DocBaseType IN ('SOO', 'POO') AND C_DocType.DocSubTypeSO='RM' AND C_DocType.AD_Client_ID=@#AD_Client_ID@ AND IsSOTrx='Y'	2009-09-11 00:52:03	100	Document Type Customer RMA	Returns	Y	S	2009-09-11 00:52:03	100
52066	C_DocType Vendor RMA	0	0	C_DocType.DocBaseType IN ('SOO', 'POO') AND C_DocType.DocSubTypeSO='RM' AND C_DocType.AD_Client_ID=@#AD_Client_ID@ AND IsSOTrx='N'	2009-09-11 00:52:24	100	Document Type Vendor RMA	Returns	Y	S	2009-09-11 00:52:24	100
\.
