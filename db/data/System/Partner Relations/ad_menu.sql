copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
110	Business Partner	W	0	\N	\N	0	\N	123	1999-06-11 00:00:00	0	Maintain Business Partners	Partner Relations	Y	Y	N	Y	N	2000-01-02 00:00:00	0
127	Payment Term	W	0	\N	\N	0	\N	141	1999-08-09 00:00:00	0	Maintain Payment Terms	Partner Relations	Y	Y	N	Y	N	2000-01-02 00:00:00	0
133	Invoice Schedule	W	0	\N	\N	0	\N	147	1999-08-09 00:00:00	0	Maintain Invoicing Schedule	Partner Relations	Y	Y	N	Y	N	2000-01-02 00:00:00	0
165	Business Partner Setup	\N	0	\N	\N	0	\N	\N	1999-12-02 13:14:52	0	\N	Partner Relations	Y	N	N	Y	Y	2022-02-17 21:50:15	100
172	Debt Collection Setup	W	0	\N	\N	0	\N	159	1999-12-04 21:34:10	0	Configure Debt Collection Levels	Partner Relations	Y	Y	N	Y	N	2022-02-27 23:40:02	100
173	Withholding (1099)	W	0	\N	\N	0	\N	160	1999-12-04 21:37:24	0	Maintain Withholding Certificates	Partner Relations	N	Y	N	Y	N	2019-12-05 14:05:27	100
186	Revenue Recognition	W	0	\N	\N	0	\N	174	2000-01-25 10:09:01	0	Revenue Recognition Rules	Partner Relations	N	Y	N	Y	N	2019-12-05 14:05:36	100
190	Greeting	W	0	\N	\N	0	\N	178	2000-03-19 10:35:21	0	Maintain Greetings	Partner Relations	N	Y	N	Y	N	2019-12-05 14:06:07	100
232	Business Partner Group	W	0	\N	\N	0	\N	192	2000-12-18 22:10:58	0	Maintain Business Partner Groups	Partner Relations	Y	Y	N	Y	N	2000-01-02 00:00:00	0
238	Mail Template	W	0	\N	\N	0	\N	204	2000-05-22 21:37:39	0	Maintain Mail Template	Partner Relations	Y	Y	N	Y	N	2005-11-13 13:37:48	100
263	Contacts	\N	0	\N	\N	0	\N	\N	2001-04-01 23:04:28	0	Customer Relations and Partner Management	Partner Relations	Y	N	N	N	Y	2022-02-27 23:37:42	100
396	Send Mail Text	P	0	\N	\N	0	209	\N	2003-08-04 21:18:15	0	Send EMails to active subscribers of an Interest Area OR a Business Partner Group from a selected User	Partner Relations	Y	Y	N	N	N	2000-01-02 00:00:00	0
420	Business Partner Organisation	P	0	\N	\N	0	246	\N	2003-12-23 23:51:39	0	Set and verify Organisation ownership of Business Partners	Partner Relations	N	Y	N	Y	N	2022-02-17 21:49:15	100
451	Partner Relation	W	0	\N	\N	0	\N	313	2004-02-19 13:23:20	0	Maintain Business Partner Relations	Partner Relations	N	Y	N	Y	N	2022-02-27 23:40:33	100
473	Unlink BP Org	P	0	\N	\N	0	272	\N	2004-04-07 17:15:43	0	Unlink Business Partner from an Organization	Partner Relations	Y	Y	N	N	N	2022-02-17 22:04:38	100
506	Validate Business Partner	P	0	\N	\N	0	314	\N	2005-01-06 23:13:20	100	Check data consistency of Business Partner	Partner Relations	Y	Y	N	Y	N	2005-01-06 23:13:20	100
512	BP Open Amt	R	0	\N	\N	0	319	\N	2005-02-11 23:55:00	100	Business Partner Open Amount	Partner Relations	N	Y	N	Y	N	2022-02-27 23:42:18	100
544	BP Details	R	0	\N	\N	0	334	\N	2005-08-27 09:57:41	100	Business Partner Detail Report	Partner Relations	Y	Y	N	N	N	2022-02-27 23:41:23	100
53256	Withholding	W	0	\N	\N	0	\N	53104	2009-12-15 21:08:59	100	Define Withholding	Partner Relations	N	Y	N	Y	N	2019-12-05 13:55:59	100
53885	Revenue Recognition Run	P	0	\N	\N	0	53710	\N	2014-10-28 13:17:37	100	Generate revenue recognition journal	Partner Relations	N	Y	N	N	N	2019-12-05 14:05:39	100
\.
