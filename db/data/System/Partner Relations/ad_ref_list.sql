copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
210	Amex	0	0	149	1999-08-05 00:00:00	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	A
211	MasterCard	0	0	149	1999-08-05 00:00:00	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	M
212	Visa	0	0	149	1999-08-05 00:00:00	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	V
213	ATM	0	0	149	1999-08-05 00:00:00	0	\N	Partner Relations	N	2019-02-04 13:38:36	0	\N	\N	C
214	After Order delivered	0	0	150	1999-08-06 00:00:00	0	Invoice for every order	Partner Relations	N	2008-03-23 20:48:43	100	\N	\N	O
215	After Delivery	0	0	150	1999-08-06 00:00:00	0	Invoice per Delivery	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	D
216	Customer Schedule after Delivery	0	0	150	1999-08-06 00:00:00	0	Invoice per Customer Invoice Schedule	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	S
217	After Receipt	0	0	151	1999-08-06 00:00:00	0	After receipt of cash	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	R
218	Availability	0	0	151	1999-08-06 00:00:00	0	As soon as an item becomes available	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	A
219	Complete Line	0	0	151	1999-08-06 00:00:00	0	As soon as all items of a line become available	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	L
220	Complete Order	0	0	151	1999-08-06 00:00:00	0	As soon as all items of an order are available	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	O
221	Immediate	0	0	150	1999-08-06 00:00:00	0	Immediate Invoice	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	I
222	Pickup	0	0	152	1999-08-06 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:47:57	100	\N	\N	P
223	Delivery	0	0	152	1999-08-06 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:47:57	100	\N	\N	D
224	Shipper	0	0	152	1999-08-06 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:47:58	100	\N	\N	S
225	Freight included	0	0	153	1999-08-06 00:00:00	0	Freight cost included	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	I
226	Fix price	0	0	153	1999-08-06 00:00:00	0	Fixed freight price	Partner Relations	N	2000-01-02 00:00:00	0	\N	\N	F
227	Calculated	0	0	153	1999-08-06 00:00:00	0	Calculated based on Product Freight Rule	Partner Relations	Y	2012-06-08 15:19:04	0	\N	\N	C
228	Line	0	0	153	1999-08-06 00:00:00	0	Entered at Line level	Partner Relations	N	2000-01-02 00:00:00	0	\N	\N	L
252	Daily	0	0	168	1999-08-10 00:00:00	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	D
253	Weekly	0	0	168	1999-08-10 00:00:00	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	W
254	Monthly	0	0	168	1999-08-10 00:00:00	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	M
255	Twice Monthly	0	0	168	1999-08-10 00:00:00	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	T
338	Month	0	0	196	2000-02-01 22:05:09	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	M
339	Quarter	0	0	196	2000-02-01 22:05:29	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	Q
340	Year	0	0	196	2000-02-01 22:05:51	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	Y
359	Inquiry	0	0	203	2000-06-01 17:59:28	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	I
360	Purchase Order	0	0	203	2000-06-01 18:00:06	0	\N	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	O
397	Match	0	0	213	2000-12-17 20:35:47	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	Y
398	No Match	0	0	213	2000-12-17 20:36:12	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	N
399	Unavailable	0	0	213	2000-12-17 20:36:45	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	X
400	Credit Card	0	0	214	2000-12-17 20:37:49	0	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	C
401	Check	0	0	214	2000-12-17 20:38:15	0	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	K
402	Direct Deposit	0	0	214	2000-12-17 20:38:56	0	ACH Automatic Clearing House	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	A
403	Sales	0	0	215	2000-12-17 20:39:54	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	S
404	Delayed Capture	0	0	215	2000-12-17 20:40:51	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	D
405	Credit (Payment)	0	0	215	2000-12-17 20:41:09	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	C
406	Voice Authorization	0	0	215	2000-12-17 20:41:31	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	F
407	Authorization	0	0	215	2000-12-17 20:41:48	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	A
408	Void	0	0	215	2000-12-17 20:41:56	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	V
417	Diners	0	0	149	2000-12-31 14:27:11	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	D
418	Discover	0	0	149	2000-12-31 14:27:29	0	\N	Partner Relations	Y	2019-02-04 13:38:36	0	\N	\N	N
419	Purchase Card	0	0	149	2000-12-31 14:27:52	0	Corporate Purchase Card	Partner Relations	Y	2000-01-02 00:00:00	0	\N	\N	P
577	Credit Stop (Manual)	0	0	289	2003-09-03 10:59:08	0	\N	Partner Relations	Y	2016-04-11 16:41:22	0	\N	\N	S
578	Credit Hold (Auto)	0	0	289	2003-09-03 10:59:19	0	\N	Partner Relations	Y	2016-04-11 16:41:22	0	\N	\N	H
579	Credit Watch (Auto)	0	0	289	2003-09-03 10:59:31	0	\N	Partner Relations	Y	2016-04-11 16:41:22	0	\N	\N	W
580	No Credit Check (Manual)	0	0	289	2003-09-03 11:00:50	0	\N	Partner Relations	Y	2016-04-11 16:41:22	0	\N	\N	X
581	Credit OK (Auto)	0	0	289	2003-09-03 11:01:36	0	\N	Partner Relations	Y	2016-04-11 16:41:22	0	\N	\N	O
651	Direct Debit	0	0	214	2004-01-21 19:05:16	0	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	D
678	Force	0	0	151	2004-05-13 14:57:53	0	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	F
761	Same	0	0	350	2005-05-17 23:32:15	100	\N	Partner Relations	Y	2005-05-17 23:32:26	100	\N	\N	S
762	Lower	0	0	350	2005-05-17 23:32:40	100	\N	Partner Relations	Y	2005-05-17 23:32:40	100	\N	\N	L
763	Higher	0	0	350	2005-05-17 23:32:51	100	\N	Partner Relations	Y	2005-05-17 23:32:51	100	\N	\N	H
779	Business Documents	0	0	358	2005-09-09 14:51:10	100	\N	Partner Relations	Y	2005-09-09 14:51:10	100	\N	\N	B
780	Requests	0	0	358	2005-09-09 14:51:29	100	\N	Partner Relations	Y	2005-09-09 14:51:29	100	\N	\N	R
781	Assets, Download	0	0	358	2005-09-09 14:51:47	100	\N	Partner Relations	Y	2005-09-09 14:51:47	100	\N	\N	A
791	Manual	0	0	151	2005-10-22 05:01:55	100	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	M
920	None	0	0	393	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:50:42	100	\N	\N	N
921	Both	0	0	393	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:50:42	100	\N	\N	B
922	Direct Debit	0	0	393	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:50:43	100	\N	\N	D
923	Direct Deposit	0	0	393	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2008-03-23 20:50:43	100	\N	\N	T
924	Dunning	0	0	394	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2006-10-28 00:00:00	0	\N	\N	D
925	Collection Agency	0	0	394	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2006-10-28 00:00:00	0	\N	\N	C
926	Legal Procedure	0	0	394	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2006-10-28 00:00:00	0	\N	\N	L
927	Uncollectable	0	0	394	2006-10-28 00:00:00	0	\N	Partner Relations	Y	2006-10-28 00:00:00	0	\N	\N	U
53351	Account	0	0	214	2008-03-23 20:58:37	100	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	T
53438	Cash Journal Transfer	0	0	214	2008-10-26 11:10:56	100	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	X
53660	Both	0	0	53382	2010-12-08 19:16:18	100	\N	Partner Relations	Y	2010-12-08 19:16:18	100	\N	\N	B
53661	Sales	0	0	53382	2010-12-08 19:16:26	100	\N	Partner Relations	Y	2010-12-08 19:16:26	100	\N	\N	S
53662	Purchases	0	0	53382	2010-12-08 19:16:33	100	\N	Partner Relations	Y	2010-12-08 19:16:33	100	\N	\N	P
53703	Fixed Price	0	0	53410	2011-07-08 14:44:27	100	\N	Partner Relations	Y	2011-07-08 14:44:27	100	\N	\N	P
53704	Discount	0	0	53410	2011-07-08 14:44:43	100	\N	Partner Relations	Y	2011-07-08 14:44:43	100	\N	\N	D
53857	Cash	0	0	214	2012-10-25 11:28:12	100	\N	Partner Relations	Y	2012-10-25 11:28:12	100	\N	\N	M
54626	Credit Memo	0	0	214	2016-01-20 21:41:08	100	\N	Partner Relations	Y	2019-02-04 13:38:37	0	\N	\N	MM
\.
