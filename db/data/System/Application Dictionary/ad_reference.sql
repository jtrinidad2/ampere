copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
1	AD_Reference Data Types	T	0	0	1999-05-21 00:00:00	0	Data Type selection	Application Dictionary	\N	Y	N	2009-09-01 21:40:51	0	\N
2	AD_Reference Validation Types	L	0	0	1999-05-21 00:00:00	0	Reference Validation Type list	Application Dictionary	e.g. I - Independent 	Y	N	2000-01-02 00:00:00	0	L
3	AD_Column ColumName	T	0	0	1999-05-21 00:00:00	0	Column selection	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
4	AD_Reference Values	T	0	0	1999-05-21 00:00:00	0	Reference (List and Table) selection	Application Dictionary	(not DataType and Independent)	Y	N	2000-01-02 00:00:00	0	\N
5	AD_Table Access Levels	L	0	0	1999-05-21 00:00:00	0	Table Access and Sharing Level list	Application Dictionary	\N	Y	N	2007-12-17 03:06:09	0	0
101	AD_Validation Rule Types	L	0	0	1999-05-21 00:00:00	0	Validation Rule Type list	Application Dictionary	e.g. S - SQL	Y	N	2000-01-02 00:00:00	0	L
103	AD_Message Type	L	0	0	1999-05-21 00:00:00	0	Message Type list	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	L
104	AD_Menu Action	L	0	0	1999-05-21 00:00:00	0	Menu Action list	Application Dictionary	\N	Y	N	2009-09-01 21:40:08	0	L
106	AD_Language	T	0	0	1999-05-21 00:00:00	0	Language selection	Application Dictionary	\N	Y	N	2009-09-01 21:40:29	0	\N
108	AD_Window Types	L	0	0	1999-06-07 00:00:00	0	Window Type list	Application Dictionary	e.g. M = Multi/Single (one uppercase character)	Y	N	2000-01-02 00:00:00	0	L
110	AD_User	T	0	0	1999-06-21 00:00:00	0	User selection	Application Dictionary	\N	Y	N	2019-02-04 13:38:35	0	\N
126	AD_Table Replication Type	L	0	0	1999-06-29 00:00:00	0	Replication Type	Application Dictionary	\N	Y	N	2008-03-05 00:51:36	0	L
210	AD_ImpFormat_Row Type	L	0	0	2000-09-15 15:24:35	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	L
244	AD_Column Key ColumnNames	T	0	0	2001-09-08 14:13:39	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
251	AD_Column Name	T	0	0	2002-05-26 10:50:01	0	Column selection	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
257	AD_Column Integer	T	0	0	2002-07-11 21:22:18	0	Integer Type only (Name)	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
258	AD_Column YesNo	T	0	0	2002-07-11 21:30:41	0	Coluns YesNo (Name)	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
278	AD_Tab	T	0	0	2003-05-08 06:31:21	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
284	AD_Window	T	0	0	2003-06-19 16:55:43	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
291	AD_Field ObscureType	L	0	0	2003-09-28 18:20:04	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
293	AD_Table_Access RuleType	L	0	0	2003-10-21 17:41:57	0	AccessRuleType	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
306	WF_EventType	L	0	0	2003-12-31 15:05:10	0	\N	Application Dictionary	\N	Y	N	2000-01-02 00:00:00	0	\N
319	_YesNo	L	0	0	2004-03-18 21:36:46	0	\N	Application Dictionary	\N	Y	N	2019-02-04 13:38:36	0	\N
354	AD_Column Encrypted	L	0	0	2005-07-20 07:38:04	100	\N	Application Dictionary	\N	Y	N	2005-07-20 07:38:04	100	\N
389	_EntityTypeNew	T	0	0	2006-06-11 14:36:21	100	\N	Application Dictionary	\N	Y	N	2019-02-04 13:38:38	0	\N
50003	AD_Package_Exp_DB	L	0	0	2006-12-11 23:46:40	0	List of database types	Application Dictionary	This is a list of database types used by the inbound packaging processing to determine if the sql statement is fired.	Y	N	2006-12-12 00:13:31	0	\N
50007	ShowHelp List	L	0	0	2006-12-19 04:00:42	0	\N	Application Dictionary	\N	Y	N	2006-12-19 04:00:42	0	\N
53000	AD_FieldGroup	L	0	0	2007-07-18 00:00:00	100	Field Group Type	Application Dictionary		Y	N	2007-07-18 00:00:00	100	\N
53235	AD_Rule_RuleType	L	0	0	2008-01-23 11:49:53	100	\N	Application Dictionary	\N	Y	N	2008-01-23 11:49:53	100	\N
53236	AD_Rule_EventType	L	0	0	2008-01-23 11:54:58	100	\N	Application Dictionary	\N	Y	N	2008-01-23 11:54:58	100	\N
53237	EventModelValidator	L	0	0	2008-02-01 01:37:35	100	\N	Application Dictionary	\N	Y	N	2008-02-01 01:37:35	100	\N
53238	EventChangeLog	L	0	0	2008-02-12 21:27:47	100	\N	Application Dictionary	\N	Y	N	2008-02-12 21:27:47	100	\N
53239	MigrationScriptStatus	L	0	0	2008-02-15 15:08:39	100	Migration Script Status	Application Dictionary	\N	Y	N	2008-02-15 15:08:39	100	\N
53290	AD_Table TableName	T	0	0	2009-02-18 12:57:48	100	\N	Application Dictionary	\N	Y	N	2009-02-18 12:58:46	100	\N
53291	SearchType	L	0	0	2009-02-18 13:39:31	100	\N	Application Dictionary	\N	Y	N	2009-02-18 13:39:31	100	\N
53292	AD_SearchDefinition_DataType	L	0	0	2009-02-18 13:43:16	100	\N	Application Dictionary	\N	Y	N	2009-02-18 13:43:16	100	\N
53311	AD_Migration Status	L	0	0	2009-06-14 00:35:26	100	\N	Application Dictionary	\N	Y	N	2009-06-14 00:35:26	100	\N
53312	AD_Migration Apply/Rollback	L	0	0	2009-06-14 00:47:42	100	Apply or Rollback migration	Application Dictionary	\N	Y	N	2009-06-14 00:47:42	100	\N
53313	Migration step type	L	0	0	2009-06-14 01:03:39	100	Type of migration step	Application Dictionary	Either Application Dictionary change or SQL statement.	Y	N	2009-06-14 01:03:39	100	\N
53441	AD_Migration	T	0	0	2012-07-04 12:16:15	100	\N	Application Dictionary	\N	Y	N	2012-07-04 12:16:15	100	\N
53763	ToolbarButton_ComponentName	L	0	0	2014-12-06 18:17:25	100	Component Name of Toolbar Button	Application Dictionary	\N	Y	N	2014-12-06 18:17:25	100	\N
53806	AD_InfoProcess LayoutType	L	0	0	2016-04-20 15:49:47	100	Define layout type of info process button	Application Dictionary	\N	Y	N	2016-04-20 15:49:47	100	\N
53807	InfoQueryOperators	L	0	0	2016-04-21 12:07:48	100	Operator list for info query criteria	Application Dictionary	\N	Y	N	2016-04-21 12:07:48	100	\N
\.
